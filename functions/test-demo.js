const firebase = require('firebase-admin');
const Promise = require('promise');
const fs = require('fs');
const async = require('async');

//var serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-zukgq-dc434bdf0d");
//var serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-k6fyc-0088fff759.json"); // PROD
var serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-moong-5e8e33fd54"); // DEMO

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://lis-pipeline-demo.firebaseio.com",
    storageBucket: "lis-pipeline-demo.appspot.com"
});
functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';

var db = firebase.database().ref();
var storage = firebase.storage();

db.child('auditTrail').remove().then(function() {
    console.log('Done');
    process.exit();
});

// db.child('publicForms').orderByChild('spk').equalTo('-M0ZL3H46sHNaKx73yOj').once('value', function(snap) {
//     if (snap.exists()) {
//         snap.ref.child(Object.keys(snap.val())[0]).update({test: 'hello'}).then(function() {
            
//         });
//     }
// });

// db.child('policies').once('value', function(snap) {
//     if (snap.exists()) {
//         let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
//         async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
//             db.child('policies/' + policyKey).once('value', function(snap) {
//                 if (snap.exists()) {
//                     let policy = snap.val();
//                     if (policy.private && policy.private.les) {
//                         var updates = {};
//                         updates['les'] = policy.private.les;
//                         updates['private/les'] = null;
//                         db.child('policies/' + policyKey).update(updates).then(function() {
//                             console.log('Updated policy ' + policyKey);
//                             callback();
//                         });
//                     } else {
//                         callback();
//                     }
//                 } else {
//                     callback();
//                 }
//             });
//         }, function(err) {
//             console.log('Finished');
//         });
//     }
// });

// //console.log('Checking for org-level Policies');
// db.child('orgs/' + requestBody.requestorOrgType + '/' + requestBody.requestorOrgKey + '/policies').once('value', function(snap) {
//     if (snap.exists()) {
//         let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
//         let digestData = {
//             startDate: "",
//             endDate: "",
//             policies: []
//         };
//         //console.log(policyKeys.length + ' Policies found');
//         async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
//             orgPolicies.push(policyKey);
//             db.child('policies/' + policyKey).once('value', function(snap) {
//                 if (snap.exists()) {
//                     let policy = snap.val();
//                     if (policy.policyIsActive) {
//                         let newDigest = {
//                             LSHID: policy.LSHID,
//                             agentName: policy.createdBy,
//                             premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
//                             faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
//                             insuredName: policy.insured.fullName,
//                             insuredAge: policy.insured.age ? policy.insured.age : '',
//                             latestUpdate: policy.latestStatus ? policy.latestStatus : '',
//                             latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
//                             displayStatus: policy.status.display.text,
//                             link: ''
//                         };
//                         newDigest.logSummary = [];
//                         let miscUpdates = 0;
//                         let fileUploads = 0;
//                         let messages = 0;
//                         let formActions = 0;
//                         let buyerActions = 0;
//                         if (policy.updateLog) {
//                             policy.updateLog.forEach(function(log) {
//                                 if (log.type) {
//                                     if (log.type === 'Policy Assigned') {
//                                         miscUpdates = miscUpdates + 1;
//                                     } else if (log.type === 'File Uploaded') {
//                                         fileUploads = fileUploads + 1;
//                                     } else if (log.type.indexOf('Application') > -1) {
//                                         formActions = formActions + 1;
//                                     }
//                                 } else {
//                                     miscUpdates = miscUpdates + 1;
//                                 }
//                             });
//                         }
//                         if (policy.publicNotes) {
//                             policy.publicNotes.forEach(function(note) {
//                                 messages = messages + 1;
//                             });
//                         }
//                         // TODO: Map Buyer Actions
//                         newDigest.logSummary = {
//                             miscUpdates: miscUpdates,
//                             fileUploads: fileUploads,
//                             messages: messages,
//                             formActions: formActions,
//                             buyerActions: buyerActions
//                         };
//                         digestData.policies.push(newDigest);
//                     }
//                     callback();
//                 } else {
//                     callback();
//                 }
//             });
//         }, function(err) {
//             if (err) {
//                 console.log(err);
//             } else {
//                 //console.log(digestData);
//                 //console.log('Checking for user-level Policies');
//                 db.child('policies').orderByChild('createdById').equalTo(requestBody.requestorUid).once('value', function(snap) {
//                     if (snap.exists()) {
//                         let policyData = snap.val();
//                         Object.keys(policyData).forEach(function(policyKey) {
//                             if (orgPolicies.indexOf(policyKey) < 0) {
//                                 let policy = policyData[policyKey];
//                                 if (policy.policyIsActive) {
//                                     let newDigest = {
//                                         LSHID: policy.LSHID,
//                                         agentName: policy.createdBy,
//                                         premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
//                                         faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
//                                         insuredName: policy.insured.fullName,
//                                         insuredAge: policy.insured.age ? policy.insured.age.toString() : '',
//                                         latestUpdate: policy.latestStatus ? policy.latestStatus : '',
//                                         latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
//                                         displayStatus: policy.status.display.text,
//                                         link: ''
//                                     };
//                                     newDigest.logSummary = [];
//                                     let miscUpdates = 0;
//                                     let fileUploads = 0;
//                                     let messages = 0;
//                                     let formActions = 0;
//                                     let buyerActions = 0;
//                                     if (policy.updateLog) {
//                                         policy.updateLog.forEach(function(log) {
//                                             if (log.type) {
//                                                 if (log.type === 'Policy Assigned') {
//                                                     miscUpdates = miscUpdates + 1;
//                                                 } else if (log.type === 'File Uploaded') {
//                                                     fileUploads = fileUploads + 1;
//                                                 } else if (log.type.indexOf('Application') > -1) {
//                                                     formActions = formActions + 1;
//                                                 }
//                                             } else {
//                                                 miscUpdates = miscUpdates + 1;
//                                             }
//                                         });
//                                     }
//                                     if (policy.publicNotes) {
//                                         policy.publicNotes.forEach(function(note) {
//                                             messages = messages + 1;
//                                         });
//                                     }
//                                     // TODO: Map Buyer Actions
//                                     newDigest.logSummary = {
//                                         miscUpdates: miscUpdates,
//                                         fileUploads: fileUploads,
//                                         messages: messages,
//                                         formActions: formActions,
//                                         buyerActions: buyerActions
//                                     };
//                                     digestData.policies.push(newDigest);
//                                 }
//                             } else {
//                                 console.log('(policy already processed at org-level)');
//                             }
//                         });
//                         // console.log('Finished');
//                         // console.log(digestData);
//                     }
//                 });
//             }
//         });
//     }
// });