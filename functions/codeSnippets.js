async.forEachOfSeries(array, function(item, key, callback) {
    // DO ASYNC STUFF HERE,
    // CALL callback() WHEN FINISHED
}, function(err) {
    if (err) {
        resolve({
            status: 'error',
            message: err.message
        });
        // OR
        res.status('200').send({
            status: 'error',
            message: err.message,
            data: err
        });
    } else {
        resolve({
            status: 'success',
            message: 'All products have been updated'
        });
        // OR
        res.status('200').send({
            status: 'success',
            message: 'Success Notification message',
            data: 'Anything to send back...'
        });
    }
});