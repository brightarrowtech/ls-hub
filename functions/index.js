const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const axios = require('axios');
const fs = require('fs');
const Promise = require('promise');
const querystring = require('querystring');
const cors = require('cors')({ origin: true });
const Stampery = require('stampery');
const forge = require('node-forge');
const UUID = require("uuid-v4");
const async = require('async');
const request = require('request');
const ZCRMRestClient = require('zcrmsdk');
const moment = require('moment');
const diff = require('deep-diff').diff;
const getRandomValues = require('get-random-values');
const os = require("os");
const twilio = require('twilio');
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const pipedrive = require('pipedrive');
const esb = require('elastic-builder');
const prettyjson = require('prettyjson');

/** ELASTICSEARCH */
const { Client } = require('@elastic/elasticsearch');
const esClient = new Client({
    cloud: {
        id: 'lshub-elasticsearch:dXMtd2VzdDEuZ2NwLmNsb3VkLmVzLmlvJDQwZmI1NjBjNmFlMzQ1ZWM4YTk3MzE3ODBkYzdhMDk1JDJiNDFiZTVmMjhmNDQ5M2FiYWEzYTVhYzMwMzU5YzRl'
    },
    auth: {
        apiKey: {
            "api_key": "zTAW5S2CR8y3ludkPGIujg",
            "id": "dSdx1ngBfFj-5b2yhnMM",
            "name": "lsh-elastic-key"
        }
    }
});

pipedrive.Configuration.apiToken = 'ef66f46085806bbb021a9f2f045cd354c61c80eb';

const env = 'prod'; // 'dev', 'prod', 'demo'
const indexName = (env === 'prod') ? 'policies' : 'dev_policies';

var stampery = {};
var stampery_token = '';

if (env === 'dev' || env === 'demo') {
    stampery = new Stampery('9c76b54e-6e5c-44f9-b7f2-f857063e95ad');
    stampery_token = 'ced2f70e20c5e04';
} else {
    stampery = new Stampery('71dfcc47-b661-4071-afa7-f4ad7db42a23');
    stampery_token = '53b2c7efdc77d69';
}

var md = forge.md.sha256.create();

var serviceAccount = {};
if (env === 'demo') {
    serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-moong-5e8e33fd54");
} else if (env === 'prod') {
    serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-ojpku-876bd98f9d.json");
} else if (env === 'dev') {
    serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');
}

var functions_root_url = '';
var storage_root_url = '';
if (env === 'demo') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-demo.firebaseio.com",
        storageBucket: "lis-pipeline-demo.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline.firebaseio.com",
        storageBucket: "lis-pipeline.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-dev.firebaseio.com",
        storageBucket: "lis-pipeline-dev.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

var db = firebase.database().ref();
var storage = firebase.storage();

var zohoOrgId = '664900183';
var zohoAuthToken = '7a9ad030c04d887241d8ce058634bde1';

if (env === 'prod') {
    zohoOrgId = '665881413';
    zohoAuthToken = '32a520ce9289f0543a8d28e5829f8ec6';
}

const zohoService = {};
var zohoRootUrl = 'https://subscriptions.zoho.com/api/v1';
var zohoRedirect = 'https://app.lshub.net';
if (env === 'demo') {
    zohoRedirect = 'https://demo.lshub.net';
} else if (env === 'dev') {
    zohoRedirect = 'http://localhost:3000';
}

const pandadocService = {
    downloadDocument: downloadDocument,
    setStatus: setStatus
}

const stampService = {
    stampPandaDoc: stampPandaDoc,
    validateStampID: validateStampID,
    validateFile: validateFile
};

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

exports.testPandaDoc = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        pandadocService.downloadDocument(data.documentId, data.policyKey, data.formName).then(function(response) {
            console.log(response);
        });
        res.status(200).send('received');
    });
});

/**
* Execute ElasticSearch query
*/
exports.esSearch = functions.https.onRequest((req, res) => {
    cors(req, res, async () => {
        var data = req.body;
        let searchObj = data.searchObj;

        if (!searchObj.paging) {
            searchObj.paging = {
                from: 0,
                size: 20
            }
        }

        let mustQuery = [];
        let mustNotQuery = []; 
        let filterQuery = [];     
        
        // ADD ACCESS FILTER BY USER AND ORG
        // TODO: client should pass either a token or their uid only. Server should use this to validate the user and get their orgKey and uid
        let shouldArr = [];
        let mustArr = [];
        if (data.searchTab === 3) {
            // Searching "My Cases"
            if (data.orgKey) {
                shouldArr.push(
                    esb.nestedQuery()
                        .path('access')
                        .query(
                            esb.termQuery('access.orgKey',data.orgKey)
                        )
                );
            }
            if (data.uid) {
                shouldArr.push(
                    esb.nestedQuery()
                        .path('createdBy')
                        .query(
                            esb.termQuery('createdBy.createdByID',data.uid)
                        )
                );
            }
            mustQuery.push(
                esb.boolQuery()
                    .should(shouldArr)
            );
        } else if (data.searchTab === 1) {
            // Searching "New Cases"
            if (data.orgGuid) {
                mustArr.push(
                    esb.nestedQuery()
                        .path('access')
                        .query(
                            esb.termQuery('access.orgGuid',data.orgGuid)
                        )
                );
                mustArr.push(
                    esb.termQuery('isPendingAcceptance',true)
                );
                filterQuery.push(
                    esb.boolQuery()
                        .must(mustArr)
                );
            } else if (data.uid === 'sb3HjYILLeNdh08k9Sn0HEc7ZEk1') {
                mustQuery.push(
                    esb.termQuery('isPendingAcceptance',true)
                );
            } else {
                mustQuery.push(
                    esb.nestedQuery()
                        .path('createdBy')
                        .query(
                            esb.termQuery('createdBy.createdByID',data.uid)
                        )
                );
            }
        } else if (data.searchTab === 2) {
            // Searching "Active Cases"
            if (data.orgGuid) {
                mustArr.push(
                    esb.nestedQuery()
                        .path('access')
                        .query(
                            esb.termQuery('access.orgGuid',data.orgGuid)
                        )
                );
                mustArr.push(
                    esb.termQuery('isAccepted',true)
                );
                filterQuery.push(
                    esb.boolQuery()
                        .must(mustArr)
                );
            } else if (data.uid === 'sb3HjYILLeNdh08k9Sn0HEc7ZEk1') {
                mustQuery.push(
                    esb.termQuery('isAccepted',true)
                );
            } else {
                mustQuery.push(
                    esb.nestedQuery()
                        .path('createdBy')
                        .query(
                            esb.termQuery('createdBy.createdByID',data.uid)
                        )
                );
            }
        }

        // ADD ANY DATE FILTERS
        if (searchObj.dateFilters) {
            Object.keys(searchObj.dateFilters).forEach(function(key) {
                let filterItem = searchObj.dateFilters[key];
                if (filterItem && filterItem.comparator && (filterItem.value || (filterItem.from && filterItem.to))) {
                    if (filterItem.comparator === 'before') {
                        mustQuery.push(
                            esb.rangeQuery(key)
                                .lte(filterItem.value)
                        )
                    } else if (filterItem.comparator === 'after') {
                        mustQuery.push(
                            esb.rangeQuery(key)
                                .gte(filterItem.value)
                        )
                    } else if (filterItem.comparator === 'equal') {
                        mustQuery.push(
                            esb.rangeQuery(key)
                                .gte(filterItem.from).lt(filterItem.to)
                        )
                    }
                }
            });
        }

        // ADD ANY BOOL FILTERS
        if (searchObj.boolFilters) {
            searchObj.boolFilters.forEach(function(filter) {
                mustQuery.push(
                    esb.termQuery(filter.key, filter.value)
                )
            });
        }

        // ADD ANY NUMERICAL FILTERS
        if (searchObj.numberFilters) {
            let filterArr = []; 
            searchObj.numberFilters.forEach(function(filterItem) {
                if (filterItem && filterItem.field && filterItem.comparator && (filterItem.value !== null || (filterItem.from !== null && filterItem.to !== null))) {
                    if (filterItem.comparator === 'before') {
                        filterArr.push(
                            esb.rangeQuery(filterItem.field)
                                .lte(filterItem.value)
                        )
                    } else if (filterItem.comparator === 'after') {
                        filterArr.push(
                            esb.rangeQuery(filterItem.field)
                                .gte(filterItem.value)
                        )
                    } else if (filterItem.comparator === 'equal') {
                        filterArr.push(
                            esb.rangeQuery(filterItem.field)
                                .gte(filterItem.from).lt(filterItem.to)
                        )
                    } else if (filterItem.comparator === 'between') {
                        filterArr.push(
                            esb.rangeQuery(filterItem.field)
                                .gte(filterItem.from).lte(filterItem.to)
                        )
                    }
                }
            });
            filterQuery.push(
                esb.boolQuery()
                    .must(filterArr)
            );
        }

        // ADD ANY SEARCH FILTER, GLOBAL OR IN SPECIFIC FIELDS
        if (searchObj.searchFilter) {
            shouldArr = [];
            if (!searchObj.searchFilter.fields) {
                searchObj.searchFilter.fields = ['fullName','LSHID','carrier'];
            }
            searchObj.searchFilter.fields.forEach(function(field) {
                if (field.indexOf('.') > -1) {
                    // Nested field...split and build query
                    let fieldArr = field.split('.');
                    if (searchObj.searchFilter.isExact) {
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.matchPhraseQuery(field, searchObj.searchFilter.text)
                                )
                        )
                    } else {
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.matchQuery(field, '*' + searchObj.searchFilter.text + '*').operator('or').fuzziness('AUTO')
                                )
                        )
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.wildcardQuery(field, '*' + searchObj.searchFilter.text + '*')
                                )
                        )
                    }
                } else {
                    // Top level field...add to query
                    if (searchObj.searchFilter.isExact) {
                        shouldArr.push(
                            esb.matchPhraseQuery(field, searchObj.searchFilter.text)
                        )
                    } else {
                        shouldArr.push(
                            esb.matchQuery(field, '*' + searchObj.searchFilter.text + '*').operator('or').fuzziness('AUTO')
                        )
                        shouldArr.push(
                            esb.wildcardQuery(field, '*' + searchObj.searchFilter.text + '*')
                        )
                    }
                }
            });
            mustQuery.push(
                esb.boolQuery()
                    .should(shouldArr)
            );
        }

        // DEAL WITH SORTING
        let sortArr = [];
        if (searchObj.searchFilter) {
            sortArr.push('_score');
        }
        if (searchObj.sort) {
            if (searchObj.sort.field.indexOf('.') > -1) {
                // Nested sort
                sortArr.push(
                    esb.sort(searchObj.sort.field + '.sort', searchObj.sort.direction).nested({ 
                        path: searchObj.sort.field.split('.')[0], 
                        filter: esb.wildcardQuery(searchObj.sort.field, '*')
                    })
                );
            } else {
                sortArr.push(esb.sort(searchObj.sort.field + '.sort', searchObj.sort.direction).toJSON());
            }
        }

        // BUILD REQUEST
        let requestBody = esb.requestBodySearch()
            .query(
                esb.boolQuery()
                    .must(mustQuery)
                    .mustNot(mustNotQuery)
                    .filter(filterQuery)
            )
            .from(searchObj.paging.from)
            .size(searchObj.paging.size)
            .trackTotalHits(true)
            .toJSON();
        requestBody.sort = sortArr;

        // DO SEARCH
        const result = await esClient.search({
            index: indexName,
            body: requestBody
        });

        // COLLATE
        let results = [];
        result.body.hits.hits.forEach(function(hit) {
            let clean = hit._source;
            clean.score = hit._score;
            results.push(clean);
        });

        // RETURN RESULTS
        let response = {};
        response.hits = results;
        response.status = 'success';
        response.took = result.body.took;
        response.message = '';
        response.hitCount = result.body.hits.total.value;
        res.status(200).send(response);
    });
});

/**
 * Set user's Access Domain (from the SAdmin User Management panel)
 */
exports.setCustomClaims = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var claims = req.body.claims;
        firebase.auth().setCustomUserClaims(uid, claims)
            .then(function() {
                db.child('users').child(uid).update(claims).then(function() {
                    res.status('200').send({
                        status: 'success',
                        message: 'Custom Claims successfuly updated'
                    });
                }).catch(function(err) {
                    res.status('200').send({
                        status: 'error',
                        message: 'Error setting Custom Claims on USERS node',
                        data: err
                    });
                });
            })
            .catch(function(error) {
                res.status('200').send({
                    status: 'error',
                    message: 'Error setting Custom Claims on AUTH object',
                    data: error
                });
            });
    });
});

/**
 * Mints a new custom JWT token for the provided UID
 */
exports.mintCustomToken = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        firebase.auth().verifyIdToken(data.token).then(function(decodedToken) {
            firebase.auth().createCustomToken(decodedToken.uid).then(function(newToken) {
                res.status('200').send({
                    status: 'success',
                    message: 'Custom token created',
                    data: newToken
                });
            }).catch(function(err) {
                res.status('200').send({
                    status: 'error',
                    message: 'Error minting custom token',
                    data: err
                });
            });
        });
    });
});

/**
 * Link Seller Registration with Policy from Lead Form
 */
exports.linkSellerWithPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var authUser = req.body.authUser;
        var sellerPolicyKey = req.body.sellerPolicyKey;
        //  1) Change policy createdBy and createdById to match the Seller
        //  2) Remove policy createdByOrgKey and createdByOrgType if present
        //  3) Add policy to Seller's "myPolicies" node under their user account
        //     Only policyKey is known at this point...
        var updates = {};
        updates['policies/' + sellerPolicyKey + '/createdBy'] = authUser.displayName;
        updates['policies/' + sellerPolicyKey + '/createdById'] = authUser.uid;
        updates['policies/' + sellerPolicyKey + '/createdByOrgKey'] = null;
        updates['policies/' + sellerPolicyKey + '/createdByOrgType'] = null;

        updates['users/' + authUser.uid + '/myPolicies/' + sellerPolicyKey] = {
            policyKey: sellerPolicyKey
        };
        
        db.update(updates).then(function() {
            res.status('200').send({
                status: 'success',
                message: 'Seller successfully associated with Policy',
                data: null
            });
        });
    });
});

/**
 * Close or Archive all of the Buyer copies of a Policy
 */
exports.closeOrArchiveCopies = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        db.child('privatePolicies').orderByChild('rootKey').equalTo(data.key).once('value', function(snap) {
            if (snap.exists()) {
                async.forEachOfSeries(Object.keys(snap.val()), function(policyCopyKey, key, callback) {
                    if (data.action === 'close') {
                        db.child('privatePolicies/' + policyCopyKey).update({
                            isClosed: true,
                            status: {
                                caseClosed: true,
                                caseClosedAt: firebase.database.ServerValue.TIMESTAMP,
                                display: {
                                    status: "Closed",
                                    text: "Closed",
                                    step: "Closed"
                                }
                            }
                        }).then(function(){
                            callback();
                        });
                    } else if (data.action === 'archive') {
                        db.child('privatePolicies/' + policyCopyKey).update({
                            policyIsActive: false,
                            isArchived: true,
                            status: {
                                caseArchived: true,
                                caseArchivedAt: firebase.database.ServerValue.TIMESTAMP
                            }
                        }).then(function(){
                            callback();
                        });
                    }
                }, function(err) {
                    if (err) {
                        res.status('200').send({
                            status: 'error',
                            message: 'Error closing Policy copies',
                            data: err
                        });
                    } else {
                        res.status('200').send({
                            status: 'success',
                            message: 'Policy copies have been updated',
                            data: null
                        });
                    }
                });
            } else {
                res.status('200').send({
                    status: 'success',
                    message: 'No Policy copies found',
                    data: data.key
                });
            }
        });                
    });
});

/**
 * Create new subscription for provided customer using hosted page
 */
exports.createSubscription = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var url = zohoRootUrl + '/hostedpages/newsubscription';
        var redirectUrl = zohoRedirect + '/profile';

        axios.post(url, {
                customer: req.body.customer,
                plan: req.body.plan,
                custom_fields: req.body.customFields,
                redirect_url: redirectUrl
            }, {
                headers: {
                    Authorization: 'Zoho-authtoken ' + zohoAuthToken,
                    'X-com-zoho-subscriptions-organizationid': zohoOrgId,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }).then(function(response) {
                if (response.data.code === 0) {
                    res.status('200').send({
                        status: 'redirect',
                        message: 'Hosted page created successfully',
                        data: response.data.hostedpage
                    });
                }
            })
            .catch(function(err) {
                console.log(err);
                res.status('200').send({
                    status: 'error',
                    message: err.message
                });
            });
    });
});

/** 
 * Get Hosted Page subscription details
 */
exports.getHostedPageDetails = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var url = zohoRootUrl + '/hostedpages/' + req.query.hostedPageId
        axios.get(url, {
                headers: {
                    Authorization: 'Zoho-authtoken ' + zohoAuthToken,
                    'X-com-zoho-subscriptions-organizationid': zohoOrgId,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }).then(function(response) {
                console.log(response.data.data.subscription.subscription_id);
                res.status('200').send({
                    status: 'success',
                    message: 'Hosted page details retrieved',
                    data: {
                        subscriptionId: response.data.data.subscription.subscription_id,
                        planName: response.data.data.subscription.plan.name,
                        orgKey: response.data.data.subscription.custom_fields[0].value,
                        status: response.data.data.subscription.status
                    }
                });
            })
            .catch(function(err) {
                console.log(err);

            });
    });
});

/**
 * Notify LS Hub Admin of new Marketing Feature Requests
 */
exports.requestMarketingAccess = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;

        var recipients = [];
        recipients.push({
            email: 'stephen@lshub.net',
            template: 'marketing-access-request',
            subData: {
                orgType: data.orgType,
                orgName: data.orgName,
                orgPrimaryContact: data.orgPrimaryContact,
                orgPhone: data.orgPhone,
                orgEmail: data.orgEmail,
                userName: data.userName,
                userPhone: data.userPhone,
                userEmail: data.userEmail
            }
        });
        recipients.push({
            email: 'admin@lshub.net',
            template: 'marketing-access-request',
            subData: {
                orgType: data.orgType,
                orgName: data.orgName,
                orgPrimaryContact: data.orgPrimaryContact,
                orgPhone: data.orgPhone,
                orgEmail: data.orgEmail,
                userName: data.userName,
                userPhone: data.userPhone,
                userEmail: data.userEmail
            }
        });

        var formData = {
            env: env,
            recipients: recipients
        };
        // Step 4: Send email with digestData as payload
        var emailURL = 'https://api.lshub.net/email/send';
        request.post({
            url: emailURL,
            body: formData,
            json: true
        }, function(error, response, body) {
            if (error) {
                res.status(200).send({
                    status: 'error',
                    message: error.message
                });
            } else {
                // Update requesting org status as requested with timestamp...
                var updates = {};
                updates['marketingAccessRequestedAt'] = firebase.database.ServerValue.TIMESTAMP;
                updates['marketingAccessRequested'] = true;
                db.child('orgs').child(data.orgType).child(data.orgKey).update(updates).then(function() {
                    res.status(200).send({
                        status: 'success',
                        message: 'Marketing Access Request email sent successfully',
                        data: {}
                    });
                }).catch(function(err) {
                    res.status(200).send({
                        status: 'success',
                        message: 'Marketing Access Request email sent successfully',
                        data: {}
                    });
                });
            }
        });
    });
});

/** 
 * Post notifications to user orgs and/or accounts
 */
exports.sendInAppNotifications = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;

        var orgTypes = ['supplier', 'buyer', 'ibroker', 'broker', 'master'];
        if (data === undefined || !data.targets || data.targets.length === 0) {
            res.status('200').send({
                status: 'error',
                message: 'No Notification Targets provided',
                data: {}
            });
        } else {
            async.forEachOfSeries(data.targets, function(target, key, callback) {
                firebase.database().ref('notifications').push({
                    isUnread: true,
                    isUrgent: data.isUrgent ? data.isUrgent : null,
                    policyKey: data.policyKey ? data.policyKey : null,
                    LSHID: data.LSHID ? data.LSHID : null,
                    pid: data.pid ? data.pid : null,
                    bid: data.bid ? data.bid : null,
                    lvl: data.lvl ? data.lvl : null,
                    createdAt: firebase.database.ServerValue.TIMESTAMP,
                    createdBy: data.createdBy ? data.createdBy : null,
                    createdByRole: data.createdByRole ? data.createdByRole : null,
                    note: data.note ? data.note : null,
                    action: data.action ? data.action : null,
                    targetType: target.type,
                    targetId: target.id ? target.id : null,
                    targetGuid: target.guid ? target.guid : null,
                    iBrokerBOFlag: target.iBrokerBOFlag ? target.iBrokerBOFlag : null,
                    subData: {
                        link: (data.notificationSubData && data.notificationSubData.link) ? data.notificationSubData.link : null,
                        linkRoot: (data.notificationSubData && data.notificationSubData.linkRoot) ? data.notificationSubData.linkRoot : null,
                        platform: (data.notificationSubData && data.notificationSubData.platform) ? data.notificationSubData.platform : null,
                        headerImg: (data.notificationSubData && data.notificationSubData.headerImg) ? data.notificationSubData.headerImg : null
                    }
                }).then(function(ref) {
                    ref.update({
                        notifyKey: ref.key
                    }).then(function() {
                        callback();
                    }).catch(function(e) {
                        callback(e);
                    });
                });
            }, function(err) {
                if (err) {
                    res.status('200').send({
                        status: 'error',
                        message: err.message,
                        data: err
                    });
                } else {
                    res.status('200').send({
                        status: 'success',
                        message: 'All notifications sent successfully',
                        data: {}
                    });
                }
            });
        }
    });
});

/**
 * Store a Lead separate from iSubmit for future marketing efforts
 */
exports.createNonPolicyLead = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        db.child('leads').push(data).then(function() {
            res.status('200').send({
                status: 'success'
            });
        });
    });
});

/**
 * Returns an estimate for a policy
 */
exports.getPolicyEstimate = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        try {
            var yearBorn = data.yearBorn;
            var faceAmount = data.faceAmount;
            var healthStatus = data.healthStatus;

            var currentTime = new Date();
            var yearCurrent = currentTime.getFullYear();
            var age = yearCurrent - yearBorn;

            var resultArr = [
                [0,  0,  10, 25, 50], // 70 or Less
                [0,  5,  15, 25, 50], // 71 - 75
                [2,  5,  15, 30, 50], // 76 - 79
                [3,  8,  20, 30, 50], // 80
                [3,  8,  20, 35, 50], // 81 - 84
                [5,  15, 25, 40, 50], // 85 - 89
                [10, 20, 25, 45, 55], // 90
                [10, 20, 30, 45, 55], // 91 - 94
                [15, 25, 35, 50, 60], // 95 or more
            ];
            var ageStage = 0;
            var offerStage = 0;

            if (age <= 70) { // 70 or Less
                ageStage = 0;
            } else if (age >= 71 && age <= 75) { // 71 - 75
                ageStage = 1;
            } else if (age >= 76 && age <= 79) { // 76 - 79
                ageStage = 2;
            } else if (age == 80) { // 80
                ageStage = 3;
            } else if (age >= 81 && age <= 84) { // 81 - 84
                ageStage = 4;
            } else if (age >= 85 && age <= 89) { // 85 - 89
                ageStage = 5;
            } else if (age == 90) { // 90
                ageStage = 6;
            } else if (age >= 91 && age <= 94) { // 91 - 94
                ageStage = 7;
            } else if (age >= 95) { // 95 or more
                ageStage = 8;
            } else {
                ageStage = 0;
            }
            switch (healthStatus) {
                case 'Good':
                    offerStage = 0;
                    break;
                case 'Slightly Bad':
                    offerStage = 1;
                    break;
                case 'Bad':
                    offerStage = 2;
                    break;
                case 'Very Bad':
                    offerStage = 3;
                    break;
                case 'Terminal':
                    offerStage = 4;
                    break;
                default:
                    offerStage = 0;
            }
            var estimateMultiplier = resultArr[ageStage][offerStage];
            var est = parseInt(faceAmount * estimateMultiplier) / 100;
            var lowEstimate = est * .5;
            var highEstimate = est * .8;
            if (isNaN(parseFloat(lowEstimate))) {
                res.status('200').send({
                    status: 'error',
                    message: 'Expects faceAmount (int), yearBorn (int) and healthStatus (string)'
                });
            } else if (highEstimate === 0) {
                res.status('200').send({
                    status: 'notEligible',
                    message: 'Policy estimate is zero',
                    data: {
                        estimate: {
                            low: parseFloat(lowEstimate).toFixed(0),
                            high: parseFloat(highEstimate).toFixed(0)
                        }
                    }
                });
            } else {
                res.status('200').send({
                    status: 'success',
                    message: 'Policy estimate generated successfully',
                    data: {
                        estimate: {
                            low: parseFloat(lowEstimate).toFixed(0),
                            high: parseFloat(highEstimate).toFixed(0)
                        }
                    }
                });
            }
        } catch(err) {
            res.status('200').send({
                status: 'error',
                message: err.message
            });
        }
    });
});

/**
 * Gets details about a public marketing lead form
 */
exports.getPublicEmbedForm = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        db.child('publicEmbedForms').child(data.pef).once('value', function(snap) {
            if (snap.exists()) {
                var form = snap.val();
                db.child('marketing/' + form.ok + '/forms/' + form.fid).once('value', function(formSnap) {
                    if (formSnap.exists()) {
                        var formDetails = formSnap.val();
                        res.status('200').send({
                            status: 'success',
                            message: 'Public Embed Form found',
                            data: {
                                trackingType: (formDetails.trackingType) ? formDetails.trackingType : 'system',
                                webhooksURL: (formDetails.webhooksURL) ? formDetails.webhooksURL : null,
                                contactNumber: (formDetails.contactNumber) ? formDetails.contactNumber : null,
                                followUpLink: (formDetails.followUpLink) ? formDetails.followUpLink : null,
                                accentColor: (formDetails.accentColor) ? formDetails.accentColor : '#009ada',
                                managedBy: (formDetails.managedBy) ? formDetails.managedBy : 'broker',
                                sendEmail: (formDetails.sendEmail === false) ? false : true,
                                formShadow: formDetails.formShadow,
                                formBorder: formDetails.formBorder,
                                width: (formDetails.frameWidth) ? formDetails.frameWidth : '100%',
                                formTitle: (formDetails.formTitle) ? formDetails.formTitle : 'Sell Your Life Insurance Policy!'
                            }
                        });
                    } else {
                        res.status('200').send({
                            status: 'error',
                            message: 'Marketing Form not found for Org',
                            data: {}
                        });
                    }
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'Public Embed Form not found',
                    data: {}
                });
            }
        });
    });
});

// const server = require("./api/server");
// exports.api = functions.runWith({
//     memory: "2GB",
//     timeoutSeconds: 120
// }).https.onRequest(server);

/** Send SMS via Twilio (testing) */
exports.sendSMS = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        /** Twilio Settings */
        var accountSid = 'AC04064d9238b0177cf99f003b3ceed504';
        var authToken = 'fb0f623831ec4ab9c5a6574f66f4761f';
        let client = new twilio(accountSid, authToken);

        client.messages.create({
            body: 'We just recieved your inquiry about selling your life insurance. When is a good time for you to discuss this further? If you would prefer to schedule a call, my calendar is https://calendly.com/stephenjass/15min',
            to: '+13525096283',
            from: '+17542033312'
        }).then(function(message){
            console.log(message.sid);
            res.status(200).send({
                status:'success',
                sid: message.sid
            });
        }).catch(function(err) {
            console.log(err);
            res.status(400).send({
                status:'error'
            });
        });
    });
});

/**
 * Creates a new Lead in Pipedrive
 */
async function createPipedriveLead(policy, policyKey) {
    // Organization = policy.createdBy
    // Look up Organization...if result.length === 0, create Organization
    let input = {
        term: policy.createdBy,
        start: 0,
        limit: 1
    };
    let orgID = 0;
    let orgData = await pipedrive.OrganizationsController.findOrganizationsByName(input);
    if (orgData.data && orgData.data.length > 0) {
        orgID = orgData.data[0].id;
    } else {
        console.log('Org not found. Creating new Organization for Agent (policy.createdBy)');
        input = {
            body: {
                name: policy.createdBy
            }
        };
        let orgResponse = await pipedrive.OrganizationsController.addAnOrganization(input);
        orgID = orgResponse.data.id;
    }

    // Person = policy.insured
    // Look up Person...if result.length === 0, create Person
    input = {
        term: policy.insured.fullName,
        start: 0,
        limit: 2
    };
    let insuredPersonID = 0;
    let personData = await pipedrive.PersonsController.findPersonsByName(input);
    if (personData.data && personData.data.length > 1) {
        console.log('More than one Insured Person was found for the provided search. Unable to associate');
    } else if (personData.data && personData.data.length > 0) {
        personID = personData.data[0].id;
    } else if (!personData.data || personData.data.length === 0) {
        console.log('Person not found. Creating new Person for insured (policy.insured)');
        input = {
            body: {
                name: policy.insured.fullName,
                org_id: orgID
            }
        };
        let personResponse = await pipedrive.PersonsController.addAPerson(input);
        insuredPersonID = personResponse.data.id;
    }

    // Create new Deal in Pipeline (id: 17 - Lead Tracking / New)
    let dealID = 0;
    input = {
        body: {
            title: policy.insured.fullName + " - " + new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 0}).format(policy.faceAmount),
            stage_id: 17,
            org_id: orgID,
            person_id: insuredPersonID
        }
    }
    let newDeal = await pipedrive.DealsController.addADeal(input);
    dealID = newDeal.data.id;

    // Get dealFields:(key,name)
    //   Loop to find "Policy Face Value" (id: 9a4cecdd9514f690812f5ff5ba45670b096c13a8)
    input = {
        id: dealID,
        '9a4cecdd9514f690812f5ff5ba45670b096c13a8': parseInt(policy.faceAmount),
        'ad83f63cc02a8eb0f93b744120a6ab9cd5a7dc53': 'https://isubmit.lshub.net/policy?pid=' + policyKey
    };
    let updateDeal = await pipedrive.DealsController.updateADeal(input);

    // Associate Policy Contact with Deal
    if (policy.contacts && policy.contacts.length > 0) {
        if (policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName !== policy.insured.firstName + ' ' + policy.insured.lastName) {
            // Person = policy.contacts[0]
            // Look up Person (Contact) by email address...if result.length === 0, create Person
            let lookupValue = '';
            let contactPersonID = 0;
            let contactData = {};
            if (policy.contacts[0].emailAddress) {
                lookupValue = policy.contacts[0].emailAddress;
                input = {
                    term: lookupValue,
                    start: 0,
                    limit: 2
                };
                contactData = await pipedrive.PersonsController.findPersonsByName(input);
                console.log(contactData);
                if (contactData.data && contactData.data.length > 1) {
                    console.log('More than one Contact Person was found for the provided search. Unable to associate');
                } else if (contactData.data && contactData.data.length > 0) {
                    contactPersonID = contactData.data[0].id;
                }
            }
            if (contactPersonID === 0) {
                contactData = {};
                lookupValue = policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName;
                input = {
                    term: lookupValue,
                    start: 0,
                    limit: 2
                };
                contactData = await pipedrive.PersonsController.findPersonsByName(input);
                console.log(contactData);
                if (contactData.data && contactData.data.length > 1) {
                    console.log('More than one Contact Person was found for the provided search. Unable to associate');
                } else if (contactData.data && contactData.data.length > 0) {
                    contactPersonID = contactData.data[0].id;
                }
            }
            
            console.log('contactPersonID = ' + contactPersonID);

            if (contactPersonID === 0) {
                console.log('Person not found. Creating new Person for contact (policy.contacts[0])');
                input = {
                    body: {
                        name: policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName,
                        phone: [{
                            value: policy.contacts[0].phoneNumber.replace(/\D/g,''),
                            primary: true
                        }],
                        email: [{
                            value: policy.contacts[0].emailAddress,
                            primary: true
                        }]
                    }
                };
                let contactResponse = await pipedrive.PersonsController.addAPerson(input);
                contactPersonID = contactResponse.data.id;
            } else {
                console.log('Contact found as existing Person. Updating to include email and phone');
                input = {
                    id: contactPersonID,
                    phone: [{
                        value: policy.contacts[0].phoneNumber.replace(/\D/g,''),
                        primary: true
                    }],
                    email: [{
                        value: policy.contacts[0].emailAddress,
                        primary: true
                    }]
                };
                try {
                    await pipedrive.PersonsController.updateAPerson(input);
                } catch(e) {
                    console.log('Error updating Contact:');
                    console.log(e);
                }
            }
            
            if (contactPersonID !== 0 && contactPersonID !== insuredPersonID) {
                console.log('Adding additional Participant to Deal (for Contact)');
                input = {
                    id: dealID,
                    personId: contactPersonID
                };
                try {
                    await pipedrive.DealsController.addAParticipantToADeal(input);
                } catch(e) {
                    console.log('Error adding Participant:');
                    console.log(e);
                }
            }
        } else {
            console.log('Contact and Insured are the same Person. Updating to include email and phone');
            input = {
                id: insuredPersonID,
                phone: [{
                    value: policy.contacts[0].phoneNumber.replace(/\D/g,''),
                    primary: true
                }],
                email: [{
                    value: policy.contacts[0].emailAddress,
                    primary: true
                }]
            };
            try {
                await pipedrive.PersonsController.updateAPerson(input);
            } catch (e) {
                console.log('Error updating Contact:');
                console.log(e);
            }
        }
    }

    // Add an initial Note to the Deal
    console.log('Adding Note to Deal');
    input = {
        content: '<b>Insured DOB:</b> ' + policy.insured.dateOfBirth + ' (' + policy.insured.age + ' years old)<br><b>Health Status:</b> ' + policy.insured.healthStatus + '<br><b>LSHID:</b> ' + policy.LSHID + '<br><b>Low Estimate:</b> ' + policy.leadEstimate.data.estimate.low + '<br><b>High Estimate:</b> ' + policy.leadEstimate.data.estimate.high + '<br><b>Signing Agent:</b> ' + policy.signingAgent.fullName + ' (' + policy.signingAgent.emailAddress + ')<br><b>Link to iSubmit:</b> https://isubmit.lshub.net/policy?pid=' + policyKey,
        dealId: dealID
    };
    pipedrive.NotesController.addANote(input, function(error, response, context) {
        if (error) {
            console.log(error);
        } else {
            // Finished
            console.log('Deal ' + dealID + ' created');
        }
        return true;
    });
}

/**
 * Monitor for new Leads to notify orgs
 */
exports.sendLeadNotifications = functions.database.ref('orgs/{orgType}/{orgKey}/policies/{policyKey}').onCreate(function(snap, context) {
    return new Promise(function(resolve) {
        if (snap.val().isLead && context.params.orgType === 'supplier') {
            console.log('Sending lead notification to the ' + context.params.orgType + ' org key ' + context.params.orgKey + ' for policy key ' + context.params.policyKey);
            db.child('orgs/' + context.params.orgType + '/' + context.params.orgKey).once('value',function(orgSnap) {
                if (orgSnap.exists() && orgSnap.val().notificationEmails && orgSnap.val().notificationEmails.allEnabled && orgSnap.val().notificationEmails.emailAddress) {
                    var recipients = [];
                    let emailAddresses = orgSnap.val().notificationEmails.emailAddress.split(',');
                    emailAddresses.forEach(function(emailAddress) {
                        recipients.push({
                            email: emailAddress.trim(),
                            template: 'new-lead-submission',
                            subData: {
                                LSHID: snap.val().LSHID,
                                faceAmount: snap.val().faceAmount,
                                insured: snap.val().insured
                            }
                        });
                    });

                    var formData = {
                        env: env,
                        recipients: recipients
                    };

                    var emailURL = 'https://api.lshub.net/email/send';
                    request.post({
                        url: emailURL,
                        body: formData,
                        json: true
                    }, function(error, response, body) {
                        if (error) {
                            console.error(error.message);
                            resolve();
                        } else {
                            console.log('Emails sent successfully');
                            db.child('orgs/' + context.params.orgType + '/' + context.params.orgKey + '/policies/' + context.params.policyKey).set({
                                hasAccess: true
                            }).then(function() {
                                resolve();
                            });
                        }
                    });
                } else {
                    resolve();
                }
            });
        } else {
            resolve();
        }
    });
})

/**
 * Creates a new Policy from the marketing lead form
 */
exports.createNewPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        // Get form data from the embedded form public settings
        console.log('Getting public embed form');
        db.child('publicEmbedForms').child(data.pef).once('value', function(snap) {
            if (snap.exists()) {
                var form = snap.val();
                console.log('Form exists: OK = ' + form.ok + ', FID = ' + form.fid);
                console.log('Getting private details');
                // Get form details from the private form settings - "OK" = Org Key, "FID" = "Form ID"
                db.child('marketing/' + form.ok + '/forms/' + form.fid).once('value', function(formSnap) {
                    if (formSnap.exists()) {
                        var formDetails = formSnap.val();
                        console.log('Details exist: ', formDetails);
                        // Get current upline hierarchy for the user this form is being assigned to
                        //   Send assigned user to the server, along with current user orgKey and orgType
                        //   Server checks /users/[uid]/myUpline for selected user, and returns an array of all orgKey/orgType/[upline path] where the current user's Org is part of that Org's upline
                        var dataToSend = {
                            selectedUid: formDetails.leadsAssignedTo.userRef,
                            requestorOrgKey: formDetails.formOrg,
                            requestorOrgType: formDetails.formOrgType
                        };
                        console.log('Getting upline org chain');
                        doGetValidUplineOrgChains(dataToSend).then(function(response) {
                            if (response.status === 'success') {
                                console.log('Upline org chain found', response);
                                // TODO: If the selected user has more than one upline returned, prompt to select which upline org the policy should be associated with
                                //   For now, just submit to the first returned Org...
                                var policy = req.body.policyData;
                                var policyUpline = {};
                                if (response.data[0]) {
                                    policyUpline = response.data[0];
                                    policyUpline.forEach(function(org) {
                                        policy[org.orgKey] = org.orgType;
                                    });
                                } else {
                                    policyUpline = [];
                                    policy[response.ownOrg.orgKey] = response.ownOrg.orgType;
                                }

                                var applicationPublicGuid = uuidv4();
                                policy.createdAt = firebase.database.ServerValue.TIMESTAMP;

                                policy.isLead = true;
                                policy.managedBy = data.managedBy;

                                policy.status = {
                                    caseReceived: true,
                                    caseReceivedAt: firebase.database.ServerValue.TIMESTAMP,
                                    applicationInProgress: true,
                                    display:  {
                                        step: 'Application',
                                        status: 'In Progress',
                                        text: data.estimate.status === 'notEligible' ? 'Unqualified Lead' : 'Lead'
                                    }
                                };
                                policy.createdOnPlatform = 'isubmit';

                                policy.latestStatus = "No updates have been posted here yet";
                                policy.statusSelection = [{
                                    class: 'success',
                                    status: 'On Track'
                                }];

                                policy.LSHID = 'L' + moment().format('X').toString().substr(2, 8);

                                policy.signingAgent = {
                                    firstName: formDetails.signingAgent.firstName,
                                    lastName: formDetails.signingAgent.lastName,
                                    emailAddress: formDetails.signingAgent.email,
                                    fullName: formDetails.signingAgent.firstName + ' ' + formDetails.signingAgent.lastName
                                };

                                policy.forms = {
                                    application: {
                                        formName: 'Application Form',
                                        className: 'application',
                                        status: data.estimate.status === 'notEligible' ? 'In Progress' : 'Pending 3rd Party',
                                        publicGuid: applicationPublicGuid,
                                        recipients: {
                                            insured: {},
                                            secondaryInsured: {},
                                            owner: {},
                                            agent: policy.signingAgent
                                        }
                                    }
                                };

                                if (data.estimate.status !== 'notEligible') {
                                    policy.forms.application.thirdParty = {
                                        fullName: policy.contacts[0] ? policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName : null,
                                        emailAddress: policy.contacts[0] ? policy.contacts[0].emailAddress : null,
                                        requestedAt: policy.contacts[0] ? firebase.database.ServerValue.TIMESTAMP : null
                                    }
                                }

                                policy.actualCreatedBy = 'Marketing Lead Form';
                                policy.actualCreatedById = 'marketing-lead-form';
                                policy.createdBy = formDetails.leadsAssignedTo.name;
                                policy.createdById = formDetails.leadsAssignedTo.userRef;
                                policy.createdByOrgKey = formDetails.formOrg ? formDetails.formOrg : null;
                                policy.createdByOrgType = formDetails.formOrgType ? formDetails.formOrgType : null;
                                policy.upline = policyUpline;

                                policy.leadEstimate = data.estimate;

                                // set alertId to the profile alertId for the assigned user
                                policy.alertId = response.alertId ? response.alertId : null;

                                if (!formDetails.trackingData || formDetails.trackingData.length === 0) {
                                    formDetails.trackingData = [];
                                }
                                formDetails.trackingData.push({
                                    key: 'Form Name',
                                    value: formDetails.formName
                                });
                                formDetails.trackingData.push({
                                    key: 'Page URL',
                                    value: data.pageURL ? data.pageURL : 'n/a'
                                });
                                formDetails.trackingData.push({
                                    key: 'Lead Submitted At',
                                    value: data.leadCreatedOn
                                })
                                formDetails.trackingData.push({
                                    key: 'Estimate',
                                    value: 'Low: $' + data.estimate.data.estimate.low + ", High: $" + data.estimate.data.estimate.high
                                });

                                policy.trackingData = formDetails.trackingData;

                                // Get Agent profile info to add to Seller page
                                // TODO: move this to a db trigger when a new public form is submitted
                                // db.child('users/' + formDetails.leadsAssignedTo.userRef).once('value', function(snap) {
                                //     if (snap.exists()) {

                                //     } else {

                                //     }
                                // });

                                // console.log('Policy Data:');
                                // console.log(policy);
                                db.child('policies').push(policy).then(function(ref) {
                                    var policyKey = ref.key;
                                    console.log('Policy created. Key = ' + policyKey);
                                    console.log('Setting org access');
                                    let updates = {};
                                    policyUpline.forEach(function(org) {
                                        updates['orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey + '/hasAccess'] = true;
                                        updates['orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey + '/isLead'] = true;
                                        updates['orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey + '/LSHID'] = policy.LSHID;
                                        updates['orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey + '/faceAmount'] = policy.faceAmount ? policy.faceAmount : 'Not Provided';
                                        updates['orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey + '/insured'] = policy.insured && policy.insured.firstName ? policy.insured.firstName : 'Not Provided';

                                    });
                                    updates['users/' + formDetails.leadsAssignedTo.userRef + '/myPolicies/' + policyKey] = {
                                        policyKey: policyKey,
                                        insured: {
                                            firstName: policy.insured && policy.insured.firstName ? policy.insured.firstName : null,
                                            lastName: policy.insured && policy.insured.lastName ? policy.insured.lastName : null
                                        }
                                    };
                                    db.update(updates).then(async function() {
                                        console.log('Access set for all orgs and Policy saved to user node');
                                        if (!data.managedBy || data.managedBy === 'broker') {
                                            console.log('User policy set. Saving Lead to Pipedrive');
                                            await createPipedriveLead(policy, policyKey);
                                        } else {
                                            console.log('User policy set. Skipping Pipedrive as this is a self-managed Lead');
                                        }

                                        console.log('Creating 3rd Party Request form');

                                        delete policy.adminFiles;
                                        delete policy.caseFiles;
                                        delete policy.createdBy;
                                        delete policy.createdById;
                                        delete policy.createdByOrgKey;
                                        delete policy.createdByOrgType;
                                        delete policy.actualCreatedBy;
                                        delete policy.actualCreatedById;
                                        delete policy.actualCreatedByOrgKey;
                                        delete policy.actualCreatedByOrgType;
                                        delete policy.policyIsActive;
                                        delete policy.private;
                                        delete policy.publicGuid;
                                        delete policy.submittedTo;
                                        delete policy.updateLog;
                                        delete policy.lastUpdateAt;
                                        delete policy.lastUpdatedBy;
                                        delete policy.lastUpdatedById;
                                        delete policy.latestStatus;
                                        delete policy.latestStatusAt;

                                        policy.spk = policyKey;
                                        policy.linkRoot = formDetails.linkRoot;

                                        db.child('publicForms').child(applicationPublicGuid).update(policy).then(function(ref) {
                                            console.log('Form created. Sending email');
                                            // Create an email message to the recipient with link to [url]/view/application?pubId=[publicGuid]
                                            if (policy.contacts[0] && policy.contacts[0].emailAddress) {
                                                var recipients = [];
                                                recipients.push({
                                                    email: policy.contacts[0].emailAddress,
                                                    template: 'embedded-form-submission',
                                                    subData: {
                                                        LSHID: policy.LSHID,
                                                        faceAmount: policy.faceAmount ? policy.faceAmount : 'Not Provided',
                                                        insured: policy.insured && policy.insured.firstName ? policy.insured.firstName : 'Not Provided',
                                                        name: policy.contacts[0].firstName ? policy.contacts[0].firstName : '',
                                                        formLink: 'https:' + formDetails.linkRoot + '/view/application?pubId=' + applicationPublicGuid,
                                                        registerLink: 'https:' + formDetails.linkRoot + '/register?spk=' + policyKey
                                                    }
                                                });

                                                var formData = {
                                                    env: env,
                                                    recipients: recipients
                                                };

                                                // Send email to policy contact
                                                if (data.sendEmail) {
                                                    var emailURL = 'https://api.lshub.net/email/send';
                                                    request.post({
                                                        url: emailURL,
                                                        body: formData,
                                                        json: true
                                                    }, function(error, response, body) {
                                                        if (error) {
                                                            res.status(200).send({
                                                                status: 'error',
                                                                message: error.message
                                                            });
                                                        } else {
                                                            console.log('Emails sent successfully');
                                                            res.status(200).send({
                                                                status: 'success',
                                                                message: 'New Policy created successfully. Key: ' + policyKey,
                                                                data: {
                                                                    key: policyKey,
                                                                    formLink: formDetails.linkRoot + '/view/application?pubId=' + applicationPublicGuid,
                                                                    linkRoot: formDetails.linkRoot,
                                                                    smsSent: false
                                                                }
                                                            });
                                                        }
                                                    });
                                                } else {
                                                    res.status(200).send({
                                                        status: 'success',
                                                        message: 'New Policy created successfully. Key: ' + policyKey,
                                                        data: {
                                                            key: policyKey,
                                                            formLink: formDetails.linkRoot + '/view/application?pubId=' + applicationPublicGuid,
                                                            linkRoot: formDetails.linkRoot,
                                                            smsSent: false
                                                        }
                                                    });
                                                }
                                            } else {
                                                res.status(200).send({
                                                    status: 'success',
                                                    message: 'New Policy created successfully. Key: ' + policyKey,
                                                    data: {
                                                        key: policyKey,
                                                        formLink: formDetails.linkRoot + '/view/application?pubId=' + applicationPublicGuid,
                                                        linkRoot: formDetails.linkRoot,
                                                        smsSent: false
                                                    }
                                                });
                                            }
                                        }).catch(function(err) {
                                            console.log(err);
                                            res.status(200).send({
                                                status: 'error',
                                                message: err.message
                                            });
                                        });
                                    });
                                });
                            } else {
                                console.log('Form details not found');
                                res.status(200).send({
                                    status: 'error',
                                    message: response.message
                                });
                            }
                        });
                    } else {
                        console.log('Form details not found');
                        res.status(200).send({
                            status: 'error',
                            message: 'Form details not found'
                        });
                    }
                });
            } else {
                console.log('Form ID not valid');
                res.status(200).send({
                    status: 'error',
                    message: 'Form ID not valid'
                });
            }
        });
    });
});

/** 
 * Monitor Policy objects for changes and create audit trail notifications
 */
exports.auditTrailPolicies = functions.database.ref('/policies/{pid}').onWrite(async function(change, context) {
    var update = {};
    if (context.authType === 'ADMIN') {
        update.updatedBy = 'Server';
    } else if (context.authType === 'USER') {
        update.updatedBy = context.auth.uid;
    }
    update.updatedAt = firebase.database.ServerValue.TIMESTAMP;
    update.path = 'policies/' + context.params.pid;    

    if (!change.after.exists()) {
        // Policy was deleted. Remove from ElasticSearch
        let body = [{ delete: { _id: context.params.pid }}];
        await esClient.bulk({
            index: indexName,
            refresh: true,
            body
        });

        update.changes = [{
            kind: 'D',
            path: [''],
            lhs: null,
            rhs: 'Policy Deleted'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    } else {
        let policy = change.after.val();

        // ElasticSearch Record
        let record = {};
        
        let nameToIndex = '';
        if (policy.insured && policy.insured.fullName) {
            nameToIndex = policy.insured.fullName;
        }

        record.policyKey = context.params.pid;
        record.LSHID = policy.LSHID;
        record.fullName = nameToIndex;
        record.gender = (policy.insured && policy.insured.gender) ? policy.insured.gender[0].id : '';
        record.carrier = policy.carrier ? policy.carrier : '';
        record.createdAt = moment(policy.createdAt).format();
        record.faceAmount = policy.faceAmount ? policy.faceAmount : 0;
        record.annualPremiumAmount = policy.annualPremiumAmount ? policy.annualPremiumAmount : 0;
        record.age = (policy.insured && policy.insured.age) ? policy.insured.age : 0;
        record.policyType = policy.policyType ? policy.policyType : '';
        record.status = [];
        if (policy.status) {
            Object.keys(policy.status).forEach(function(key) {
                if (policy.status[key] === true) {
                    if (key === 'receivedFromSupplier') {
                        if (!record.isAccepted) {
                            record.isPendingAcceptance = true;
                        }
                    } else if (key === 'acceptedCase') {
                        record.isAccepted = true;
                        record.isPendingAcceptance = false;
                    }
                    record.status.push({
                        progress: {
                            activity: key,
                            activityAt: moment(policy.status[key + 'At']).format()
                        }
                    });
                }
            });
        }
        record.displayStatus = (policy.status && policy.status.display && policy.status.display.text) ? policy.status.display.text : '';
        
        record.isActive = policy.policyIsActive ? policy.policyIsActive : false;
        record.isClosed = policy.isClosed ? policy.isClosed : false;
        record.isArchived = policy.isArchived ? policy.isArchived : false;
        record.isLead = policy.isLead;
        record.isPrivate = false;
        record.hasInterest = false;
        if (policy.buyers) {
            for (let index = 0; index < policy.buyers.length; index++) {
                let buyer = policy.buyers[index];
                if (buyer.buyerInterested) {
                    record.hasInterest = true;
                    break;
                }
            }
        }
        record.imInterested = policy.buyerInterested ? policy.buyerInterested : false;

        record.healthStatus = (policy.insured && policy.insured.healthStatus) ? policy.insured.healthStatus : null;
        record.supplier = policy.supplier;
        record.les = [];
        if (policy.les) {
            policy.les.forEach(function(le) {
                if (le.showLE) {
                    record.les.push({
                        source: le.source,
                        description: le.description,
                        lshYears: le.lshYears,
                        type: le.type[0]
                    });
                }
            });
        }

        record.access = [];
        if (policy.upline) {
            policy.upline.forEach(function(upline) {
                record.access.push(upline);
            });
        } else {
            console.log('No upline for policy ' + policy.policyKey);
            record.access.push({
                "orgKey": "-L8crXXb_c8NRSJUgRpn",
                "orgType": "backoffice"
            });
        }
        record.access.push({
            "orgKey": (env === "prod") ? "-LH-BIGkYe_M_-C4gBEC" : "-LH-0PWxLXMhRFtJvwW6",
            "orgType": "master"
        });
        record.createdBy = [{
            createdByName: policy.createdBy,
            createdByID: policy.createdById,
            createdByOrgKey: policy.createdByOrgKey,
            createdByOrgType: policy.createdByOrgType
        }];

        record.id = context.params.pid;
        record.indexUpdatedDtTm = moment().format();

        try {
            let body = [{
                index: {
                    _index: indexName,
                    _type: '_doc',
                    _id: context.params.pid
                }
            }];
            body.push(record);
            let { body: bulkResponse } = await esClient.bulk({
                index: indexName,
                refresh: true,
                body
            });
            bulkResponse.items.forEach(function(item) {
                if (item.index.error) {
                    console.log(item.index.error);
                }
            });
            if (!change.before.exists()) {
                // Policy is new
                update.changes = [{
                    kind: 'N',
                    path: [''],
                    lhs: null,
                    rhs: 'New Policy Created'
                }];
                if (change.after.val().actualCreatedById === 'marketing-lead-form') {
                    // Policy was submitted by the Marketing Lead Form. Notify the agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }
                return db.child('auditTrail').child(context.params.pid).push(update);
            } else {
                // Policy was updated.
                const beforeData = change.before.val();
                const afterData = change.after.val();

                if (afterData.createdById !== beforeData.createdById && afterData.createdById !== afterData.actualCreatedById) {
                    // Policy has been reassigned on behalf of someone else. Notify new agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }

                update.changes = diff(beforeData, afterData);
                return db.child('auditTrail').child(context.params.pid).push(update);
            }
        } catch (error) {
            console.log('Error updating ElasticSearch:', error);
            if (!change.before.exists()) {
                // Policy is new
                update.changes = [{
                    kind: 'N',
                    path: [''],
                    lhs: null,
                    rhs: 'New Policy Created'
                }];
                if (change.after.val().actualCreatedById === 'marketing-lead-form') {
                    // Policy was submitted by the Marketing Lead Form. Notify the agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }
                return db.child('auditTrail').child(context.params.pid).push(update);
            } else {
                // Policy was updated.
                const beforeData = change.before.val();
                const afterData = change.after.val();

                if (afterData.createdById !== beforeData.createdById && afterData.createdById !== afterData.actualCreatedById) {
                    // Policy has been reassigned on behalf of someone else. Notify new agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }

                update.changes = diff(beforeData, afterData);
                return db.child('auditTrail').child(context.params.pid).push(update);
            }
        }
    }
});

/** 
 * Monitor Private Policy objects for changes and create audit trail notifications
 */
exports.auditTrailPrivatePolicies = functions.database.ref('/privatePolicies/{pid}').onWrite(async function(change, context) {
    var update = {};
    if (context.authType === 'ADMIN') {
        update.updatedBy = 'Server';
    } else if (context.authType === 'USER') {
        update.updatedBy = context.auth.uid;
    }
    update.updatedAt = firebase.database.ServerValue.TIMESTAMP;
    update.path = 'privatePolicies/' + context.params.pid;
    if (!change.after.exists()) {
        // Policy was deleted. Remove from ElasticSearch
        let body = [{ delete: { _id: context.params.pid }}];
        await esClient.bulk({
            index: indexName,
            refresh: true,
            body
        });

        update.changes = [{
            kind: 'D',
            path: [''],
            lhs: null,
            rhs: 'Policy Deleted'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    } else {
        let policy = change.after.val();

        // ElasticSearch Record
        let record = {};
        
        let nameToIndex = '';
        if (policy.insured && policy.insured.firstName && policy.insured.lastName) {
            nameToIndex = policy.insured.firstName + ' ' + policy.insured.lastNameInitial + '.';
        }
    
        record.policyKey = context.params.pid;
        record.LSHID = policy.LSHID;
        record.fullName = nameToIndex;
        record.gender = (policy.insured && policy.insured.gender) ? policy.insured.gender[0].id : '';
        record.carrier = policy.carrier ? policy.carrier : '';
        record.createdAt = moment(policy.createdAt).format();
        record.faceAmount = policy.faceAmount ? policy.faceAmount : 0;
        record.annualPremiumAmount = policy.annualPremiumAmount ? policy.annualPremiumAmount : 0;
        record.age = (policy.insured && policy.insured.age) ? policy.insured.age : 0;
        record.policyType = policy.policyType ? policy.policyType : '';
        record.status = [];
        if (policy.status) {
            Object.keys(policy.status).forEach(function(key) {
                if (policy.status[key] === true) {
                    if (key === 'receivedFromSupplier') {
                        if (!record.isAccepted) {
                            record.isPendingAcceptance = true;
                        }
                    } else if (key === 'acceptedCase') {
                        record.isAccepted = true;
                        record.isPendingAcceptance = false;
                    }
                    record.status.push({
                        progress: {
                            activity: key,
                            activityAt: moment(policy.status[key + 'At']).format()
                        }
                    });
                }
            });
        }
        record.displayStatus = policy.status.display.text;
        
        record.isActive = policy.policyIsActive ? policy.policyIsActive : false;
        record.isClosed = policy.isClosed ? policy.isClosed : false;
        record.isArchived = policy.isArchived ? policy.isArchived : false;
        record.isLead = policy.isLead;
        record.isPrivate = true;
        record.hasInterest = false;
        if (policy.buyers) {
            for (let index = 0; index < policy.buyers.length; index++) {
                let buyer = policy.buyers[index];
                if (buyer.buyerInterested) {
                    record.hasInterest = true;
                    break;
                }
            }
        }
        record.imInterested = policy.buyerInterested ? policy.buyerInterested : false;

        record.healthStatus = (policy.insured && policy.insured.healthStatus) ? policy.insured.healthStatus : null;
        record.supplier = policy.supplier;
        record.les = [];
        if (policy.les) {
            policy.les.forEach(function(le) {
                if (le.showLE) {
                    record.les.push({
                        source: le.source,
                        description: le.description,
                        lshYears: le.lshYears,
                        type: le.type[0]
                    });
                }
            });
        }

        record.access = [];
        record.access.push({
            "orgGuid": policy.recipient
        });
        record.access.push({
            "orgKey": (env === "prod") ? "-LH-BIGkYe_M_-C4gBEC" : "-LH-0PWxLXMhRFtJvwW6",
            "orgType": "master"
        });

        record.createdBy = [{
            createdByName: policy.createdBy,
            createdByID: policy.createdById,
            createdByOrgKey: policy.createdByOrgKey,
            createdByOrgType: policy.createdByOrgType
        }];

        record.id = context.params.pid;
        record.indexUpdatedDtTm = moment().format();

        try {
            let body = [{
                index: {
                    _index: indexName,
                    _type: '_doc',
                    _id: context.params.pid
                }
            }];
            body.push(record);
            let { body: bulkResponse } = await esClient.bulk({
                index: indexName,
                refresh: true,
                body
            });
            bulkResponse.items.forEach(function(item) {
                if (item.index.error) {
                    console.log(item.index.error);
                }
            });
            if (!change.before.exists()) {
                // Policy is new
                update.changes = [{
                    kind: 'N',
                    path: [''],
                    lhs: null,
                    rhs: 'New Policy Created'
                }];
                if (change.after.val().actualCreatedById === 'marketing-lead-form') {
                    // Policy was submitted by the Marketing Lead Form. Notify the agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }
                return db.child('auditTrail').child(context.params.pid).push(update);
            } else {
                // Policy was updated.
                const beforeData = change.before.val();
                const afterData = change.after.val();

                if (afterData.createdById !== beforeData.createdById && afterData.createdById !== afterData.actualCreatedById) {
                    // Policy has been reassigned on behalf of someone else. Notify new agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }

                update.changes = diff(beforeData, afterData);
                return db.child('auditTrail').child(context.params.pid).push(update);
            }
        } catch (error) {
            console.log('Error updating ElasticSearch:', error);
            if (!change.before.exists()) {
                // Policy is new
                update.changes = [{
                    kind: 'N',
                    path: [''],
                    lhs: null,
                    rhs: 'New Policy Created'
                }];
                if (change.after.val().actualCreatedById === 'marketing-lead-form') {
                    // Policy was submitted by the Marketing Lead Form. Notify the agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }
                return db.child('auditTrail').child(context.params.pid).push(update);
            } else {
                // Policy was updated.
                const beforeData = change.before.val();
                const afterData = change.after.val();

                if (afterData.createdById !== beforeData.createdById && afterData.createdById !== afterData.actualCreatedById) {
                    // Policy has been reassigned on behalf of someone else. Notify new agent.
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        link: 'https://isubmit' + (env === 'demo' ? '-demo' : '') + '.lshub.net/policy?pid=' + change.after.val().spk
                    };
                }

                update.changes = diff(beforeData, afterData);
                return db.child('auditTrail').child(context.params.pid).push(update);
            }
        }
    }
});

/**
 * For each new Notification, send email if marked isUrgent or if target is a user and their profile is set to receive emails for all notifications
 */
exports.notificationEmails = functions.database.ref('/notifications/{nid}').onCreate(function(snapshot, context) {
    const notification = snapshot.val();
    var sendEmail = false;
    var emailAddress = '';
    if (notification.isUnread) {
        if (notification.targetType === 'user') {
            // Check if the user has allNotifications enabled
            return db.child('users/' + notification.targetId + '/notificationEmails').once('value', function(snap) {
                if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                    sendEmail = true;
                    emailAddress = snap.val().emailAddress;
                    return continueNotificationEmail(sendEmail, emailAddress, notification);
                } else {
                    return false;
                }
            });
        } else if (notification.targetType !== 'user' && notification.targetType !== 'org') {
            // Check if the org has allNotifications enabled
            if (notification.iBrokerBOFlag && notification.iBrokerBOFlag === 'bo') {
                return db.child('orgs/' + notification.targetType + '/' + notification.targetId + '/backOfficeNotificationEmails').once('value', function(snap) {
                    if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                        sendEmail = true;
                        emailAddress = snap.val().emailAddress;
                        return continueNotificationEmail(sendEmail, emailAddress, notification);
                    } else {
                        return false;
                    }
                });
            } else {
                return db.child('orgs/' + notification.targetType + '/' + notification.targetId + '/notificationEmails').once('value', function(snap) {
                    if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                        sendEmail = true;
                        emailAddress = snap.val().emailAddress;
                        return continueNotificationEmail(sendEmail, emailAddress, notification);
                    } else {
                        return false;
                    }
                });
            }
        } else if (notification.targetType === 'buyer' && notification.targetGuid) {
            // Get buyer orgId given orgGuid
            return db.child('orgs/buyer').orderByChild('orgGuid').equalTo(notification.targetGuid).once('value', function(snap) {
                if (snap.exists()) {
                    console.log('Buyer found. GUID = ' + notification.targetGuid + '. Email = ' + snap.val().email);
                    sendEmail = true;
                    emailAddress = snap.val().email;
                    return continueNotificationEmail(sendEmail, emailAddress, notification);
                } else {
                    console.log('Buyer not found. GUID = ' + notification.targetGuid);
                    return false;
                }
            });
        } else {
            return continueNotificationEmail(sendEmail, emailAddress, notification);
        }
    } else {
        return false;
    }
});

function continueNotificationEmail(sendEmail, emailAddress, notification) {
    return new Promise(function(resolve, reject) {
        if (sendEmail) {
            var subData = notification.subData;

            // Determine auto-urgent action types
            if (notification.action === 'New Public Note' || notification.action === 'New Internal Note') {
                notification.isUrgent = true;
            }
            // If notification contains "bid" (buyer id) parameter, the policy link may need to be adjusted to include the original policy key and the correct target platform
            if (notification.bid) {
                // notification.subData.link
                if (subData.platform === 'iSubmit') {
                    // This notification was sent as an iBroker on iSubmit was viewing a Buyer's View, so it needs to target the iManager platform for the Buyer
                    subData = {
                        platform: 'iManager',
                        headerImg: 'imanager',
                        linkRoot: subData.linkRoot.replace('isubmit', 'imanager'),
                        link: subData.link.replace('isubmit', 'imanager')
                    };
                }
                if (notification.targetType === 'ibroker') {
                    // This notification was sent from someone on the iManager platform, but needs to target an iBroker on iSubmit
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        linkRoot: subData.linkRoot.replace('imanager', 'isubmit'),
                        link: subData.link.replace('imanager', 'isubmit')
                    };
                }
            }

            subData.action = notification.action;
            subData.note = notification.note;

            var recipients = [];

            recipients.push({
                email: emailAddress.trim(),
                template: 'new-event-notification',
                subData: subData
            });
            var formData = {
                env: env,
                recipients: recipients
            };

            console.log('Form Data:');
            console.log(formData);

            // Step 4: Send email with digestData as payload
            var emailURL = 'https://api.lshub.net/email/send';
            request.post({
                url: emailURL,
                body: formData,
                json: true
            }, function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    console.log('Emails sent successfully');
                    resolve('done');
                }
            });
        } else {
            reject();
        }
    });
}

/** 
 * Get digest data for all users scheduled to receive digests today
 */
exports.checkDailyDigest = functions.pubsub.topic('check-daily-digest').onPublish(function(message) {
    return new Promise(function(resolve, reject) {
        var recipients = [];
        // Step 1: Get any users with digests set up in their Profile
        db.child('users').orderByChild('hasDigestEmail').equalTo(true).once('value', function(snap) {
            if (snap.exists()) {
                let digestCount = 0;
                let userObjArr = snap.val();
                let usersWithDigest = Object.keys(userObjArr).map(function(x) { return userObjArr[x]; });
                let todayDate = new Date();
                console.log(dateFormat(todayDate.toString(), 'yyyymmdd'));
                async.forEachOfSeries(usersWithDigest, function(user, key, callback) {
                    console.log('--- ' + dateFormat(user.digestEmail.nextRunDate, 'yyyymmdd') + ' --- ' + user.uid);
                    // Step 2: If the digest is active (subscribed = true) and the nextRunDate is today, process the digest
                    if (user.digestEmail && user.digestEmail.subscribed === true && dateFormat(user.digestEmail.nextRunDate, 'yyyymmdd') === dateFormat(todayDate.toString(), 'yyyymmdd')) {
                        var data = {
                            requestorUid: user.uid,
                            accessDomain: user.accessDomain,
                            requestorOrgType: user.myOrg ? user.myOrg.orgType : null,
                            requestorOrgKey: user.myOrg ? user.myOrg.orgKey : null,
                            lastRunDate: user.digestEmail.lastRunDate ? user.digestEmail.lastRunDate : 0
                        };
                        processDigest(data).then(function(digestData) {
                            console.log('Finished processing digest for ' + user.fullName);
                            digestCount = digestCount + 1;
                            user.digestEmail.emailAddress.split(',').forEach(function(recipientEmail) {
                                recipients.push({
                                    email: recipientEmail.trim(),
                                    template: 'digest-template',
                                    subData: {
                                        startDate: dateFormat(user.digestEmail.lastRunDate, 'mm/dd/yyyy'),
                                        endDate: dateFormat(todayDate.toString(), 'mm/dd/yyyy'),
                                        policies: digestData
                                    }
                                });
                            });

                            // Step 3: Update user digest nextRunDate to (today + 7 days)
                            var newRunDate = new Date(user.digestEmail.nextRunDate);
                            newRunDate.setDate(newRunDate.getDate() + 7);
                            db.child('users/' + user.uid + '/digestEmail').update({
                                nextRunDate: newRunDate,
                                lastRunDate: firebase.database.ServerValue.TIMESTAMP
                            }).then(function() {
                                callback();
                            });
                        });
                    } else {
                        callback();
                    }
                }, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(digestCount.toString() + ' digest emails ready to send.');
                        if (digestCount > 0) {
                            var formData = {
                                env: env,
                                recipients: recipients
                            };
                            // Step 4: Send email with digestData as payload
                            request.post({
                                url: 'https://api.lshub.net/email/send',
                                body: formData,
                                json: true
                            }, function(error, response, body) {
                                if (error) {
                                    reject(error);
                                } else {
                                    console.log('Emails sent successfully');
                                    resolve('done');
                                }
                            });
                        } else {
                            resolve('done');
                        }
                    }
                });
            } else {
                resolve();
            }
        });
    });
});

function dateFormat(dateToTest, format) {
    var testDate = new Date(dateToTest);
    var y = testDate.getFullYear();
    var m = testDate.getMonth() + 1;
    var d = testDate.getDate();
    if (format === 'yyyymmdd') {
        return '' + y + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d;
    } else if (format === 'mm/dd/yyyy') {
        return '' + (m < 10 ? '0' : '') + m + '/' + (d < 10 ? '0' : '') + d + '/' + y;
    }
}

function processDigest(data) {
    return new Promise(function(resolve, reject) {
        let orgPolicies = [];

        db.child('orgs/' + data.requestorOrgType + '/' + data.requestorOrgKey + '/policies').once('value', function(snap) {
            let digestData = [];
            if (snap.exists()) {
                let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
                async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
                    orgPolicies.push(policyKey);
                    db.child('policies/' + policyKey).once('value', function(snap) {
                        if (snap.exists()) {
                            let policy = snap.val();
                            if (policy.policyIsActive) {
                                let newDigest = {
                                    LSHID: policy.LSHID,
                                    agentName: policy.createdBy,
                                    premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
                                    faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
                                    insuredName: policy.insured.fullName,
                                    insuredAge: policy.insured.age ? policy.insured.age : '',
                                    latestUpdate: policy.latestStatus ? policy.latestStatus : '',
                                    latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
                                    displayStatus: policy.status.display.text,
                                    link: data.accessDomain.replace(/_/g, '.') + '/policy?pid=' + snap.ref.key
                                };
                                newDigest.logSummary = [];
                                let miscUpdates = 0;
                                let fileUploads = 0;
                                let messages = 0;
                                let formActions = 0;
                                let buyerActions = 0;
                                if (policy.updateLog) {
                                    policy.updateLog.forEach(function(log) {
                                        if (log.updatedAt > data.lastRunDate) {
                                            if (log.type) {
                                                if (log.type === 'Policy Assigned') {
                                                    miscUpdates = miscUpdates + 1;
                                                } else if (log.type === 'File Uploaded') {
                                                    fileUploads = fileUploads + 1;
                                                } else if (log.type.indexOf('Application') > -1) {
                                                    formActions = formActions + 1;
                                                }
                                            } else {
                                                miscUpdates = miscUpdates + 1;
                                            }
                                        }
                                    });
                                }
                                if (policy.publicNotes) {
                                    policy.publicNotes.forEach(function(note) {
                                        if (note.createdAt > data.lastRunDate) {
                                            messages = messages + 1;
                                        }
                                    });
                                }
                                // TODO: Map Buyer Actions
                                newDigest.logSummary = {
                                    miscUpdates: miscUpdates,
                                    fileUploads: fileUploads,
                                    messages: messages,
                                    formActions: formActions,
                                    buyerActions: buyerActions
                                };
                                digestData.push(newDigest);
                            }
                            callback();
                        } else {
                            callback();
                        }
                    });
                }, function(err) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        processUserDigest(data, orgPolicies, digestData).then(function(response) {
                            resolve(response);
                        });
                    }
                });
            } else {
                processUserDigest(data, orgPolicies, digestData).then(function(response) {
                    resolve(response);
                });
            }
        });
    });
}

function processUserDigest(data, orgPolicies, digestData) {
    return new Promise(function(resolve, reject) {
        db.child('policies').orderByChild('createdById').equalTo(data.requestorUid).once('value', function(snap) {
            if (snap.exists()) {
                let policyData = snap.val();
                Object.keys(policyData).forEach(function(policyKey) {
                    if (orgPolicies.indexOf(policyKey) < 0) {
                        let policy = policyData[policyKey];
                        if (policy.policyIsActive) {
                            let newDigest = {
                                LSHID: policy.LSHID,
                                agentName: policy.createdBy,
                                premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
                                faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
                                insuredName: policy.insured.fullName,
                                insuredAge: policy.insured.age ? policy.insured.age.toString() : '',
                                latestUpdate: policy.latestStatus ? policy.latestStatus : '',
                                latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
                                displayStatus: policy.status.display.text,
                                link: ''
                            };
                            newDigest.logSummary = [];
                            let miscUpdates = 0;
                            let fileUploads = 0;
                            let messages = 0;
                            let formActions = 0;
                            let buyerActions = 0;
                            if (policy.updateLog) {
                                policy.updateLog.forEach(function(log) {
                                    if (log.updatedAt > data.lastRunDate) {
                                        if (log.type) {
                                            if (log.type === 'Policy Assigned') {
                                                miscUpdates = miscUpdates + 1;
                                            } else if (log.type === 'File Uploaded') {
                                                fileUploads = fileUploads + 1;
                                            } else if (log.type.indexOf('Application') > -1) {
                                                formActions = formActions + 1;
                                            }
                                        } else {
                                            miscUpdates = miscUpdates + 1;
                                        }
                                    }
                                });
                            }
                            if (policy.publicNotes) {
                                policy.publicNotes.forEach(function(note) {
                                    if (note.createdAt > data.lastRunDate) {
                                        messages = messages + 1;
                                    }
                                });
                            }
                            // TODO: Map Buyer Actions
                            newDigest.logSummary = {
                                miscUpdates: miscUpdates,
                                fileUploads: fileUploads,
                                messages: messages,
                                formActions: formActions,
                                buyerActions: buyerActions
                            };
                            digestData.push(newDigest);
                        }
                    }
                });
                resolve(digestData);
            } else {
                resolve(digestData);
            }
        });
    });
}

/** 
 * Get an array of all downline seat users
 * Requires no input parameters
 * Uses the passed auth token to do lookup for currently logged in user
 */
exports.getFullDownline = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
            console.log('Unauthorized request');
            res.status(403).send('Unauthorized');
        }
        let idToken = req.headers.authorization.split('Bearer ')[1];
        firebase.auth().verifyIdToken(idToken).then((decodedIdToken) => {
            // Get the requestor's orgKey from their Profile
            db.child('users/' + decodedIdToken.uid + '/myOrg/orgKey').once('value', function(snap) {
                if (snap.exists()) {
                    getMemberOrg(snap.val()).then(function(fullList) {
                        if (fullList) {
                            res.status(200).send({
                                status: 'success',
                                message: 'Retrieved full downline',
                                data: fullList
                            });
                        } else {
                            res.status(200).send({
                                status: 'error',
                                message: 'No downline list retrieved'
                            });
                        }
                    });
                } else {
                    res.status(200).send({
                        status: 'error',
                        message: 'User has no Organization'
                    });
                }
            });
        }).catch((error) => {
            console.error('Error while verifying Firebase ID token:', error);
            res.status(403).send('Unauthorized');
        });
    });
});

function getMemberOrg(orgKey) {
    var newDownlineList = [];

    function getNextMemberOrg(orgKey) {
        function getNextOrg(nextOrgKey) {
            return new Promise(function(resolve, reject) {
                firebase.database().ref().child('orgs/seats/' + nextOrgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        var memberArr = [];
                        snap.forEach(function(member) {
                            memberArr.push(member.val());
                        });
                        resolve(memberArr);
                    } else {
                        console.log('No org seats found');
                        resolve('no-org');
                    }
                }).catch(function(err) {
                    console.log(err);
                    resolve(err);
                });
            });
        }

        function processMembers(membersArr) {
            return new Promise(function(resolve, reject) {
                if (membersArr.length > 0) {
                    async.forEachOfSeries(membersArr, function(member, key, callback) {
                        if (!member.isOwner) {
                            newDownlineList.push(member);
                            if (member.hasOrg) {
                                getNextMemberOrg(member.orgKey).then(function(list) {
                                    callback();
                                });
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            resolve(newDownlineList);
                        }
                    });
                } else {
                    resolve(newDownlineList);
                }
            });
        }

        return getNextOrg(orgKey).then(processMembers).then(function(list) {
            return list;
        });
    }
    return getNextMemberOrg(orgKey);
}

/**
 * Get an array of valid upline org chains for a selected user, from a requeting user's perspective
 */
exports.getValidUplineOrgChains = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var requestBody = req.body;

        doGetValidUplineOrgChains(requestBody).then(function(response) {
            if (response.status === 'success') {
                res.status('200').send({
                    status: 'success',
                    message: response.message,
                    data: response.data,
                    ownOrg: response.ownOrg
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: response.message,
                    data: response.data
                });
            }
        });
    });
});

function doGetValidUplineOrgChains(requestBody) {
    return new Promise(function(resolve,reject) {
        db.child('users').child(requestBody.selectedUid).once('value', function(snap) {
            if (snap.exists()) {
                var validUplineOrgChains = [];
                var alertId = snap.val().alertId;
                var myOrgEntry = {};
                if (snap.val().myOrg) {
                    myOrgEntry = {
                        orgKey: snap.val().myOrg.orgKey,
                        orgType: snap.val().myOrg.orgType
                    }
                }
                if (snap.val().myUpline) {
                    var uplines = snap.val().myUpline;

                    if (uplines.length === 1) {
                        getUplineOrgChain(uplines[0].orgKey, myOrgEntry).then(function(uplineOrgArr) {
                            var keepChain = false;
                            uplineOrgArr.forEach(function(org) {
                                if (org.orgKey === requestBody.requestorOrgKey && org.orgType === requestBody.requestorOrgType) {
                                    keepChain = true;
                                }
                            });
                            if (keepChain) {
                                validUplineOrgChains.push(uplineOrgArr);
                            }
                            resolve({
                                status: 'success',
                                message: 'Valid upline chain(s) found',
                                data: validUplineOrgChains,
                                ownOrg: myOrgEntry,
                                alertId: alertId
                            });
                        });
                    } else {
                        async.forEachOfSeries(uplines, function(upline, key, callback) {
                            getUplineOrgChain(upline.orgKey, myOrgEntry).then(function(uplineOrgArr) {
                                var keepChain = false;
                                uplineOrgArr.forEach(function(org) {
                                    if (org.orgKey === requestBody.requestorOrgKey && org.orgType === requestBody.requestorOrgType) {
                                        keepChain = true;
                                    }
                                });
                                if (keepChain) {
                                    validUplineOrgChains.push(uplineOrgArr);
                                }
                                callback();
                            });
                        }, function(err) {
                            if (err) {
                                resolve({
                                    status: 'error',
                                    message: err.message,
                                    data: err
                                });
                            } else {
                                resolve({
                                    status: 'success',
                                    message: 'Valid upline chain(s) found',
                                    data: validUplineOrgChains,
                                    ownOrg: myOrgEntry,
                                    alertId: alertId
                                });
                            }
                        });
                    }
                } else {
                    resolve({
                        status: 'success',
                        message: 'No upline found. Not a problem',
                        data: [],
                        ownOrg: myOrgEntry,
                        alertId: alertId
                    });
                }
            } else {
                resolve({
                    status: 'error',
                    message: 'User does not exist',
                    data: {}
                });
            }
        }).catch(function(err) {
            resolve({
                status: 'error',
                message: err.message,
                data: err
            });
        });
    });
}

function getUplineOrgChain(currentOrgKey, myOrgEntry) {
    uplineOrgArr = [];

    if (myOrgEntry.orgKey) {
        uplineOrgArr.push(myOrgEntry);
    }

    function getNextUplineOrg(orgKey) {
        function getNextOrg(nextOrgKey) {
            return new Promise(function(resolve, reject) {
                db.child('orgs/public/' + nextOrgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        var org = snap.val();
                        org.orgKey = nextOrgKey;
                        resolve(org);
                    } else {
                        console.log('No org found');
                        resolve('no-org');
                    }
                }).catch(function(err) {
                    console.log(err);
                    resolve(err);
                });
            });
        }

        function buildOrgChain(org) {
            if (org.uplineOrgKey) {
                // This is an upline chain org. Add it and continue looking up
                uplineOrgArr.push({
                    orgKey: org.orgKey,
                    orgType: org.orgType
                });
                return getNextUplineOrg(org.uplineOrgKey);
            } else if (org === 'no-org') {
                console.log('The user has no upline?');
            } else if (!org.orgKey) {
                console.log('An error occurred');
                console.log(org); // org = err at this point
            } else {
                uplineOrgArr.push({
                    orgKey: org.orgKey,
                    orgType: org.orgType
                });
            }
            return uplineOrgArr;
        }

        return getNextOrg(orgKey).then(buildOrgChain);
        
    }
    return getNextUplineOrg(currentOrgKey);
}

/**
 * Check if the provided user has been made an Admin (promoted) or removed as Admin (demoted) from any Orgs
 */
exports.checkForAdminRights = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var orgType = req.body.orgType;

        firebase.database().ref().child('orgs/' + orgType).orderByChild(uid).startAt('admin').once('value', function(data) {
            if (data.exists()) {
                var dataToSend = {};
                data.forEach(function(org) {
                    dataToSend[org.key] = org.val();
                    dataToSend[org.key].key = org.key;
                });
                res.status('200').send({
                    status: 'success',
                    data: dataToSend
                });
            } else {
                // Check for promotion to iBroker org
                firebase.database().ref().child('orgs/ibroker').orderByChild(uid).startAt('admin').once('value', function(data) {
                    if (data.exists()) {
                        var dataToSend = {};
                        data.forEach(function(org) {
                            dataToSend[org.key] = org.val();
                            dataToSend[org.key].key = org.key;
                        });
                        res.status('200').send({
                            status: 'success',
                            data: dataToSend
                        });
                    } else {                        
                        res.status('200').send({
                            status: 'empty',
                            data: 'No new Admin Orgs'
                        });
                    }
                });
            }
        });
    });
});

/**
 * Check if the email address being used to register (on iManager only) is already associated with a pending invitation
 */
exports.checkForPendingRegistration = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var email = req.body.email;
        firebase.database().ref().child('orgs/buyer').orderByChild('email').equalTo(email).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(org) {
                    res.status('200').send({
                        status: 'error',
                        data: org.val().orgKey
                    });
                });
            } else {
                firebase.database().ref().child('orgs/broker').orderByChild('email').equalTo(email).once('value', function(snap) {
                    if (snap.exists()) {
                        snap.forEach(function(org) {
                            res.status('200').send({
                                status: 'error',
                                data: org.val().orgKey
                            });
                        });
                    } else {
                        res.status('200').send({
                            status: 'success'
                        });
                    }
                });
            }
        });
    });
});

/**
 * Set policy permissions in iSumbmit to upline Org Hierarchy
 */
exports.setPolicyPermissions = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var org = req.body.org;
        var policyKey = req.body.policyKey;

        // Verify that UID has access to Policy
        firebase.database().ref().child('users/' + uid).once('value', function(snap) {
            if (!snap.exists()) {
                res.status('200').send({
                    status: 'error',
                    message: 'User does not exist'
                });
            } else {
                verifyPolicyPermission(uid, policyKey, snap.val().myOrg).then(function(response) {
                    if (response.status === 'granted') {
                        firebase.database().ref().child('orgs/' + org.type + '/' + org.key + '/policies/' + policyKey).set({
                            'hasAccess': true
                        }).then(function() {
                            res.status('200').send({
                                status: 'success',
                                message: 'Policy associated with upline Org'
                            });
                        }).catch(function(err) {
                            res.status('200').send({
                                status: 'error',
                                message: err
                            });
                        });
                    } else {
                        res.status('200').send({
                            status: 'error',
                            message: 'User does not have permission to this Policy'
                        });
                    }
                });
            }
        });
    });
});

function verifyPolicyPermission(uid, policyKey, myOrg) {
    var promise = new Promise(function(resolve, reject) {
        if (myOrg) {
            firebase.database().ref().child('policies/' + policyKey + '/' + myOrg.orgKey).once('value', function(snap) {
                if (snap.exists()) {
                    resolve({
                        status: 'granted'
                    });
                } else {
                    resolve({
                        status: 'denied'
                    });
                }
            });
        } else {
            firebase.database().ref().child('policies/' + policyKey + '/createdById').once('value', function(snap) {
                if (snap.exists() && snap.val() === uid) {
                    resolve({
                        status: 'granted'
                    });
                } else {
                    resolve({
                        status: 'denied'
                    });
                }
            });
        }
    });
    return promise;
}

/**
 * Listen for Webhooks from PandaDoc
 */
exports.pandaDocWebhooks = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var message = req.body[0];

        if (message.event === 'recipient_completed') {
            console.log({
                status: 'signed',
                document_id: message.data.id,
                recipients: message.data.recipients,
                action_date: message.data.action_date,
                action_by: message.data.action_by,
                metadata: message.data.metadata
            });

            // determine role of the recipient that triggered this webhook
            var actionById = message.data.action_by.id;
            var actionByRole = '';
            message.data.recipients.forEach(function(recipient) {
                if (recipient.id === actionById) {
                    actionByRole = recipient.role;
                }
            });

            if (actionByRole === '') {
                // User may be logged into PandaDoc with an account using the same email as the recipient...ID will be different. Lookup by email
                var actionByEmail = message.data.action_by.email;
                message.data.recipients.forEach(function(recipient) {
                    if (recipient.email === actionByEmail) {
                        actionByRole = recipient.role;
                    }
                });
            }

            // Convert actionByRole to Firebase role node value
            //   Primary Insured = insured
            //   Secondary Insured = secondaryInsured
            //   Agent = agent
            //   Policy Owner = owner
            if (actionByRole === 'Primary Insured') {
                actionByRole = 'insured';
            } else if (actionByRole === 'Secondary Insured') {
                actionByRole = 'secondaryInsured';
            } else if (actionByRole === 'Agent') {
                actionByRole = 'agent';
            } else if (actionByRole === 'Policy Owner') {
                actionByRole = 'owner';
            } else if (actionByRole === 'Recipient') {
                actionByRole = 'recipient';
            }

            db.child('policies/' + message.data.metadata.policyKey + '/forms/' + message.data.metadata.className + '/recipients/' + actionByRole + '/signed').set(firebase.database.ServerValue.TIMESTAMP);
        } else if (message.event === 'document_state_changed') {
            if (message.data && message.data.status === 'document.draft') {
                // NEW DOCUMENT WAS JUST CREATED. SEND IT OUT AND UPDATE FIREBASE!
                var noteToRecipients = null;
                if (message.data.metadata.noteToRecipients) {
                    noteToRecipients = message.data.metadata.noteToRecipients;
                }
                sendDocumentForSignature(message.data.id, noteToRecipients).then(function(response) {
                    var formData = {
                        createdDtTm: firebase.database.ServerValue.TIMESTAMP
                    };
                    if (response.status === 'retry') {
                        sendDocumentForSignature(message.data.id, noteToRecipients).then(function(response) {
                            if (response.status === 'retry') {
                                response.message = 'Access Token Not Refreshed After 2 Attempts';
                                var trackingKey = message.data.metadata.className + 'SentForSignatureError';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, null, null, null);
                            } else {
                                // SEND SUCCESSFUL...
                                var trackingKey = message.data.metadata.className + 'SentForSignature';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, message.data.metadata.className, formData, null);
                            }
                        });
                    } else {
                        // SEND SUCCESSFUL...
                        var trackingKey = message.data.metadata.className + 'SentForSignature';
                        pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, message.data.metadata.className, formData, null);
                    }
                });
            } else if (message.data && message.data.status === 'document.completed') {
                // EVERYONE HAS SIGNED THIS DOCUMENT. UPDATE FIREBASE, DOWNLOAD AND STAMP IT!
                console.log('Document has been completed by all parties');
                console.log('Policy Number: ' + message.data.metadata.policyNumber);
                console.log('Policy Key: ' + message.data.metadata.policyKey);
                console.log('Document ID: ' + message.data.id);
                var display = {
                    status: 'Complete',
                    step: message.data.metadata.formName + ' Signature',
                    text: 'Pending Broker Approval'
                };

                var trackingKey = message.data.metadata.className + 'Signed';
                var formData = {
                    completedDtTm: firebase.database.ServerValue.TIMESTAMP,
                    status: 'Complete'
                };

                // FIGURE OUT WHICH BUYERS TO SEND THIS TO FOR APPROVAL - Call a function to do this...

                // One-off - set "brokerActionRequired" flag to true
                db.child('policies').child(message.data.metadata.policyKey).child('brokerActionRequired').set(true);
                
                // If this policy has an active publicForm, set [message.data.metadata.className]Signed for that as well
                db.child('publicForms').orderByChild('spk').equalTo(message.data.metadata.policyKey).once('value', function(snap) {
                    if (snap.exists()) {
                        var updates = {};
                        updates['forms/' + message.data.metadata.className + '/completedDtTm'] = firebase.database.ServerValue.TIMESTAMP;
                        updates['forms/' + message.data.metadata.className + '/status'] = 'Complete';
                        updates['status/' + message.data.metadata.className + 'Signed'] = firebase.database.ServerValue.TIMESTAMP;
                        snap.ref.child(Object.keys(snap.val())[0]).update(updates);
                    }
                });

                // TODO - SEND A NOTIFICATION TO THE BROKER
                //db.child('notifications').child... "Completed Application has been uploaded for this Policy"
                pandadocService.setStatus(message.data.metadata.policyKey, display, trackingKey, message.data.metadata.className, formData, null);

                // Download PDF for Stamping
                pandadocService.downloadDocument(message.data.id, message.data.metadata.policyKey, message.data.metadata.formName).then(function(response) {
                    if (response.status === 'success') {
                        stampService.stampPandaDoc(response.data, message.data.metadata);
                    } else if (response.status === 'retry') {
                        // Access token refreshed. Trying again
                        pandadocService.downloadDocument(message.data.id, message.data.metadata.policyKey, message.data.metadata.formName).then(function(response) {
                            if (response.status === 'success') {
                                stampService.stampPandaDoc(response.data, message.data.metadata);
                            } else if (response.status === 'retry') {
                                // Access token refreshed. Failing out.
                                var trackingKey = message.data.metadata.className + 'AccessTokenError';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, null, null);
                            }
                        });
                    }
                });
            }
        }
        res.status('200').send('received');
    });
});

/**
 * Retrieve All Documents
 */
exports.listDocuments = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        const url = 'https://api.pandadoc.com/public/v1/documents';
        axios.get(url, {
            headers: {
                Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb'
            }
        }).then(function(response) {
            res.status('200').send({
                status: 'success',
                message: 'Document list retrieved',
                data: response.data
            });
        }).catch(function(err) {
            if (err.response.status === 401) {
                console.log('PandaDoc Authentication Error');
                res.status('400').send({
                    status: 'error',
                    message: 'PandaDoc Authentication Erorr',
                    data: {}
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'PandaDoc Error',
                    data: err.response.data
                });
            }
        });
    });
});

/**
 * Retrieve All Templates
 */
exports.listTemplates = functions.https.onRequest((req, res) => {
cors(req, res, () => {
        const url = 'https://api.pandadoc.com/public/v1/templates';
        axios.get(url, {
            headers: {
                Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb'
            }
        }).then(function(response) {
            res.status('200').send({
                status: 'success',
                message: 'Template list retrieved',
                data: response.data
            });
        }).catch(function(err) {
            if (err.response.status === 401) {
                console.log('PandaDoc Authentication Error');
                res.status('400').send({
                    status: 'error',
                    message: 'PandaDoc Authentication Erorr',
                    data: {}
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'PandaDoc Error',
                    data: err.response.data
                });
            }
        });
    });
});

/**
 * Create Document from Template
 * Pick templateId based on fields like insured.secondaryInsured and recipients
 */
exports.createDocumentFromTemplate = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var templateId = req.body.templateId;
        var documentName = req.body.documentName;
        var recipients = req.body.recipients;
        var fieldData = req.body.fieldData;
        var metaData = req.body.metaData;

        if (metaData.policyKey === 'LOOKUP' && metaData.LSHID) {
            // This is an auto-sign policy from public form. Get Private Policy Key using LSHID lookup
            firebase.database().ref().child('policies').orderByChild('LSHID').equalTo(metaData.LSHID).once('value', function(snap) {
                if (snap.exists()) {
                    snap.forEach(function(policy) {
                        metaData.policyKey = policy.key;
                    });
                }
                continueCreateDocumentFromTemplate(templateId, documentName, recipients, fieldData, metaData, true, res);
            });
        } else {
            continueCreateDocumentFromTemplate(templateId, documentName, recipients, fieldData, metaData, false, res);
        }
    });
});

function continueCreateDocumentFromTemplate(templateId, documentName, recipients, fieldData, metaData, fromLeadForm, res) {
    const url = 'https://api.pandadoc.com/public/v1/documents';
    axios.post(url, {
        "name": documentName,
        "template_uuid": templateId,
        "recipients": recipients,
        "fields": fieldData,
        "metadata": metaData
    }, {
        headers: {
            Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb',
            'Content-Type': 'application/json'
        }
    }).then(function(response) {
        var updates = {};
        updates[metaData.policyKey + '/forms/' + metaData.className + '/documentId'] = response.data.id;
        updates[metaData.policyKey + '/forms/' + metaData.className + '/status'] = 'Pending Signatures';
        updates[metaData.policyKey + '/forms/' + metaData.className + '/className'] = metaData.className;
        updates[metaData.policyKey + '/forms/' + metaData.className + '/formName'] = metaData.formName;
        updates[metaData.policyKey + '/forms/' + metaData.className + '/createdDtTm'] = firebase.database.ServerValue.TIMESTAMP;
        if (fromLeadForm) {
            // set the status display for the root policy
            updates[metaData.policyKey + '/status/applicationFinished'] = firebase.database.ServerValue.TIMESTAMP;
            updates[metaData.policyKey + '/status/display/step'] = 'Application Signatures';
            updates[metaData.policyKey + '/status/display/status'] = 'Pending';
            updates[metaData.policyKey + '/status/display/text'] = 'Pending Signatures';
        }
        // Update Policy Form with Pandadoc Document Id
        firebase.database().ref().child('policies').update(updates).then(function() {
            res.status('200').send({
                status: 'success',
                message: 'New Document Created',
                data: response.data
            });
        }).catch(function(err) {
            res.status('200').send({
                status: 'error',
                message: 'Failed to update e-signature document ID',
                data: err.message
            });
        });
    }).catch(function(err) {
        if (err.response.status === 401) {
            console.log('PandaDoc Authentication Error');
            res.status('400').send({
                status: 'error',
                message: 'PandaDoc Authentication Erorr',
                data: {}
            });
        } else {
            res.status('200').send({
                status: 'error',
                message: 'PandaDoc Error',
                data: err.response.data
            });
        }
    });
}

/**
 * Toggle "Interested/Not Interested" status on Buyer View page
 */
exports.toggleBuyerInterested = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var parentKey = req.body.pk;
        var policyGuid = req.body.pg;
        var buyerInterested = req.body.bi;
        var policyType = req.body.pt;

        console.log('parentKey = ' + parentKey);
        console.log('policyGuid = ' + policyGuid);
        console.log('buyerInterested = ' + buyerInterested);
        console.log('policyType = ' + policyType);

        // Reflect the buyer's interest back in either the original root policy, or the immediate supplier's version of the policy
        db.child(policyType).child(parentKey).child('buyers').orderByChild('publicGuid').equalTo(policyGuid).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(buyer) {
                    console.log('Setting interest for buyer index ' + buyer.key);
                    db.child(policyType).child(parentKey).child('buyers').child(buyer.key).update({
                        buyerInterested: buyerInterested
                    }).then(function(result) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Successfully set buyer interest'
                        });
                    });
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'No parent policy found to update'
                });
            }
        });
    });
});

/**
 * Mark a Policy as having been viewed by a buyer
 */
exports.buyerMarkViewed = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var parentKey = req.body.pk;
        var policyGuid = req.body.pg;
        var policyType = req.body.pt;

        // Mark that the buyer has viewed this Policy in either the original root policy, or the immediate supplier's version of the policy
        db.child(policyType).child(parentKey).child('buyers').orderByChild('publicGuid').equalTo(policyGuid).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(buyer) {
                    db.child(policyType).child(parentKey).child('buyers').child(buyer.key).update({
                        hasViewedPolicy: true,
                        viewedPolicyAt: firebase.database.ServerValue.TIMESTAMP
                    }).then(function(result) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Successfully set buyer view status'
                        });
                    });
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'No parent policy found to update'
                });
            }
        });
    });
});

/**
 * Buyer flag a policy (showing interest)
 */
exports.buyerFlagPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var publicGuid = req.body.publicGuid;
        var buyerOrgKey = req.body.buyerOrgKey;
        var buyerOrgType = req.body.buyerOrgType;
        var supplierOrgKey = req.body.supplierOrgKey;
        var supplierOrgType = req.body.supplierOrgType;
        var insuredLastName = req.body.insuredLastName;
        var policyNumber = req.body.policyNumber;

        var addFlag = true;
        // Mark policy as interested in privatePolicies node (for buyer to see)
        db.child('privatePolicies').child(publicGuid).once('value', function(snap) {
            if (snap.exists()) {
                var policy = snap.val();
                if (!policy.flaggedBy) {
                    policy.flaggedBy = [];
                }
                if (policy.flaggedBy.indexOf(buyerOrgKey) > -1) {
                    addFlag = false;
                    policy.flaggedBy.splice(buyerOrgKey, 1);
                } else {
                    policy.flaggedBy.push(buyerOrgKey);
                }
                db.child('privatePolicies/' + publicGuid).update(policy).then(function() {
                    db.child('orgs/' + buyerOrgType + '/' + buyerOrgKey + '/flaggedPolicies/' + publicGuid).update({
                        'flagged': addFlag ? 'true' : null,
                        'key': addFlag ? publicGuid : null,
                        'lastName': addFlag ? (insuredLastName ? insuredLastName : 'n/a') : null,
                        'policyNumber': addFlag ? (policyNumber ? policyNumber : 'n/a') : null
                    }).then(function() {
                        // Update the policy status to reflect interest
                        if (addFlag) {
                            //db.child('orgs/' + supplierOrgType + '/' + supplierOrgKey + '/flaggedPolicies)
                        }
                        res.status('200').send({
                            status: 'success',
                            message: addFlag ? 'This policy has been flagged for easy identification in the Marketplace View.' : 'This policy has been unflagged'
                        });
                    });
                });
            } else {
                console.log('Policy ' + req.body.publicGuid + ' does not exist');
                res.status('200').send({
                    status: 'error',
                    message: 'Policy does not exist....weird, right?'
                });
            }
        });
    });
});

/**
 * View a Document (from email link)
 */
exports.viewDocument = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var documentId = req.query.documentId;
        var recipientEmail = req.query.email;
        const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/session';
        axios.post(url, {
            recipient: recipientEmail,
            lifetime: 900
        }, {
            headers: {
                Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb',
                'Content-Type': 'application/json'
            }
        }).then(function(response) {
            res.redirect('https://app.pandadoc.com/s/' + response.data.id);
        })
        .catch(function(err) {
            if (err.response.status === 401) {
                console.log('PandaDoc Authentication Error');
                res.status('400').send({
                    status: 'error',
                    message: 'PandaDoc Authentication Erorr',
                    data: {}
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'PandaDoc Error - unable to generate a document link',
                    data: err.response.data
                });
            }
        });
    });
});

/**
 * Send a Document for Signature by ID
 */
function sendDocumentForSignature(documentId, noteToRecipients) {
    return new Promise(function(resolve, reject) {
        const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/send';
        axios.post(url, {
            message: (noteToRecipients) ? noteToRecipients : '',
            silent: false
        }, {
            headers: {
                Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb',
                'Content-Type': 'application/json'
            }
        }).then(function(response) {
            resolve({
                status: 'success',
                message: 'Document Sent',
                data: response.data
            });
        })
        .catch(function(err) {
            if (err.response.status === 401) {
                console.log('PandaDoc Authentication Error');
                res.status('400').send({
                    status: 'error',
                    message: 'PandaDoc Authentication Erorr',
                    data: {}
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'PandaDoc Error',
                    data: err.response.data
                });
            }
        });
    });
}

/**
 * Download a Document by ID
 * Save locally, stamp, then push to Firebase
 */
function downloadDocument(documentId, policyKey, formName) {
    return new Promise(function(resolve, reject) {
        const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/download';
        axios.get(url, {
            headers: {
                Authorization: 'API-Key 0da9a5184b29fa4bb612c3ee9f0f9a4a1dc922eb'
            },
            responseType: 'arraybuffer'
        }).then(function(response) {
            console.log(response.data.length);
            var file = formName + '-' + documentId + '.pdf';

            var today = new Date(Date.now());
            var month = today.getMonth() + 1;
            var day = today.getDate();
            var year = today.getFullYear();
            var dir = '/tmp/' + year + month + day;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            var writeStream = fs.createWriteStream(dir + '/' + file);

            writeStream.write(response.data, 'binary');

            writeStream.on('finish', function() {
                console.log('Downloaded file from PandaDoc...');
                console.log('Uploading to Firebase Storage...');
                // Upload file to Firebase Storage
                var bucket = storage.bucket();
                var uuid = UUID();
                var options = {
                    destination: 'policies/' + policyKey + '/requested/' + file,
                    resumable: false,
                    metadata: {
                        metadata: {
                            uploadedAt: moment().unix() * 1000,
                            uploadedBy: 'PandaDoc',
                            uploadedById: 'pandadoc',
                            uploadedByOrgKey: 'n/a',
                            policyKey: policyKey,
                            firebaseStorageDownloadTokens: uuid
                        }
                    }
                };

                var downloadURL = storage_root_url + '/o/policies%2F' + policyKey + '%2Frequested%2F' + file + '?alt=media&token=' + uuid;

                bucket.upload(dir + '/' + file, options, function(err, remoteFile) {
                    if (!err) {
                        console.log("Uploaded!");
                        resolve({
                            status: 'success',
                            message: 'File is in Google Storage',
                            data: {
                                downloadURL: downloadURL,
                                fileName: file
                            }
                        });
                    } else {
                        console.error("err: " + err);
                    }
                });
            });

            writeStream.end();

            writeStream.on('error', function(err) {
                console.log('WriteStream Error: ');
                console.log(err.message);
                resolve({
                    status: 'error',
                    message: 'WriteStream Failed: ' + err.message,
                    data: err
                });
            });
        }).catch(function(err) {
            if (err.response.status === 401) {
                console.log('PandaDoc Authentication Error');
                res.status('400').send({
                    status: 'error',
                    message: 'PandaDoc Authentication Erorr',
                    data: {}
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'PandaDoc Error',
                    data: err.response.data
                });
            }
        });
    });
}

/**
 * Update Document Status in Firebase
 */
function setStatus(policyKey, display, trackingKey, className, formData, blockchainData) {
    if (trackingKey) {
        db.child('policies').child(policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
    }
    if (display) {
        db.child('policies').child(policyKey).child('status').child('display').update(display);
    }
    if (formData) {
        db.child('policies').child(policyKey).child('forms').child(className).update(formData);
    }
    if (blockchainData) {
        db.child('policies').child(policyKey).child('forms').child(className).child('blockchain').update(blockchainData);
    }
}

/**
 * Validate provided document using Stampery
 */
exports.stampFile = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        const hook = functions_root_url + '/stamperyWebhooks';

        console.log('Getting hash for file at ' + data.downloadURL);

        getFileHash(data.downloadURL).then(function(h) {
            var stampHash = stampery.hash(h);
            console.log('Stamping hash ' + h);
            stampery.stamp(stampHash, hook).then(function(stamp) {
                console.log('Stamped successfully: ID ' + stamp.id);

                // Associate Stamp with Policy and Form
                db.child('stampery').child(stamp.id).update({
                    orgKey: data.orgKey ? data.orgKey : null,
                    policyKey: data.policyKey ? data.policyKey : null,
                    policyGuid: data.policyGuid ? data.policyGuid : null,
                    fileKey: data.fileKey ? data.fileKey : null,
                    fileType: data.fileType ? data.fileType : null,
                    policyNumber: data.policyNumber ? data.policyNumber : null,
                    formName: data.formName ? data.formName : null,
                    className: data.className ? data.className : null,
                    stampId: stamp.id ? stamp.id : null,
                    stampToken: stamp.token ? stamp.token : null,
                    stampTime: stamp.time ? stamp.time : null
                });

                console.log('Updating metadata for file:  ' + data.fileName);
                var bucket = storage.bucket();
                var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
                bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
                bucket.file(bucketPath).setMetadata({
                    metadata: {
                        stampId: stamp.id,
                        stampToken: stamp.token,
                        stampTime: stamp.time
                    }
                });

                var trackingKey = data.className + 'StampingStarted';
                var blockchainData = {
                    btc: {
                        status: 'pending',
                        statusDtTm: firebase.database.ServerValue.TIMESTAMP
                    },
                    eth: {
                        status: 'pending',
                        statusDtTm: firebase.database.ServerValue.TIMESTAMP
                    }
                };
                if (data.className === 'application' || data.className === 'medicalPrimary' || data.className === 'medicalSecondary') {
                    db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    db.child('policies').child(data.policyKey).child('forms').child(data.className).child('blockchain').update(blockchainData).then(function(response) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Stamping successful',
                            data: {}
                        });
                    });
                } else {
                    blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    if (data.policyGuid) {
                        db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).child('blockchain').update(blockchainData).then(function(response) {
                            res.status('200').send({
                                status: 'success',
                                message: 'Stamping successful',
                                data: {}
                            });
                        });
                    } else {
                        db.child('caseFiles').child(data.policyKey).child(data.fileKey).child('blockchain').update(blockchainData).then(function(response) {
                            res.status('200').send({
                                status: 'success',
                                message: 'Stamping successful',
                                data: {}
                            });
                        });
                    }
                }
            }).catch(function(err) {
                res.status('200').send({
                    status: 'error',
                    message: 'Stamping failed',
                    data: err
                });
            });
        });
    });
});

/**
 * Listen for Webhooks from Stampery
 */
exports.stamperyWebhooks = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var stampId = req.body.id;
        console.log('Stamp update for id ' + stampId);
        // Validate Stamp ID
        validateStampID(stampId).then(function(blockchainData) {
            // Look up Policy and Form for this Stamp ID
            db.child('stampery').child(stampId).once('value', function(snap) {
                var data = snap.val();
                // Update File Stamp Info
                if (data.className === 'application' || data.className === 'medicalPrimary' || data.className === 'medicalSecondary') {
                    if (blockchainData.btc === 'valid') {
                        var trackingKey = data.className + 'BTCStamped';
                        db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    }
                    if (blockchainData.eth === 'valid') {
                        var trackingKey = data.className + 'ETHStamped';
                        db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    }
                    db.child('policies').child(data.policyKey).child('forms').child(data.className).child('blockchain').update(blockchainData);
                } else {
                    if (blockchainData.btc === 'valid') {
                        var trackingKey = data.className + 'BTCStamped';
                        blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    }
                    if (blockchainData.eth === 'valid') {
                        var trackingKey = data.className + 'ETHStamped';
                        blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    }
                    // Verify that the file still exists
                    if (data.policyGuid) {
                        db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).once('value', function(snap) {
                            if (snap.exists()) {
                                db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).child('blockchain').update(blockchainData);
                            } else {
                                console.log('The file has been removed');
                            }
                        });
                    } else {
                        db.child('caseFiles').child(data.policyKey).child(data.fileKey).once('value', function(snap) {
                            if (snap.exists()) {
                                db.child('caseFiles').child(data.policyKey).child(data.fileKey).child('blockchain').update(blockchainData);
                            } else {
                                console.log('The file has been removed');
                            }
                        });
                    }
                }
            });
        });
    });
});

/**
 * Stamp a file using Stampery
 */
function stampPandaDoc(data, metadata) {
    //const hook = functions_root_url + '/stamperyWebhooks';

    //getFileHash(data.downloadURL).then(function(h) {
        //var stampHash = stampery.hash(h);
        //stampery.stamp(stampHash, hook).then(function(stamp) {
            console.log('Stamping skipped successfully:');

            // Associate Stamp with Policy and Form
            // db.child('stampery').child(stamp.id).update({
            //     policyKey: metadata.policyKey,
            //     policyNumber: metadata.policyNumber,
            //     formName: metadata.formName,
            //     className: metadata.className,
            //     fileType: 'requested',
            //     stampId: stamp.id,
            //     stampToken: stamp.token,
            //     stampTime: stamp.time
            // });

            console.log('Updating metadata for file:  ' + data.fileName);
            var bucket = storage.bucket();
            var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
            bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
            bucket.file(bucketPath).setMetadata({
                metadata: {
                    policyNumber: metadata.policyNumber,
                    formName: metadata.formName,
                    className: metadata.className
                }
            });
            // stampId: stamp.id,
            // stampToken: stamp.token,
            // stampTime: stamp.time

            var trackingKey = metadata.className + 'StampingStarted';
            var formData = {
                downloadURL: data.downloadURL
            };
            var blockchainData = {
                btc: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                },
                eth: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                }
            };
            pandadocService.setStatus(metadata.policyKey, null, trackingKey, metadata.className, formData, blockchainData);
        // }).catch(function(err) {
        //     console.log('ERROR: ' + err.message);
            
        //     // Bypassing Stampery for now...
        //     console.log('Updating metadata for file:  ' + data.fileName);
        //     var bucket = storage.bucket();
        //     var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
        //     bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
        //     bucket.file(bucketPath).setMetadata({
        //         metadata: {
        //             policyNumber: metadata.policyNumber,
        //             formName: metadata.formName,
        //             className: metadata.className
        //         }
        //     });

        //     var trackingKey = metadata.className + 'StampingStarted';
        //     var formData = {
        //         downloadURL: data.downloadURL
        //     };
        //     var blockchainData = {
        //         btc: {
        //             status: 'pending',
        //             statusDtTm: firebase.database.ServerValue.TIMESTAMP
        //         },
        //         eth: {
        //             status: 'pending',
        //             statusDtTm: firebase.database.ServerValue.TIMESTAMP
        //         }
        //     };
        //     pandadocService.setStatus(metadata.policyKey, null, trackingKey, metadata.className, formData, blockchainData);
        // });
    //});
}

/**
 * Given a Stampery stamp ID, check its validity in BTC and ETH blockchains
 */
function validateStampID(id) {
    var promise = new Promise(function(resolve, reject) {
        stampery.getById(id).then(function(result) {
            const stamp = result[0];
            var stampStatus = {
                btc: {
                    status: 'invalid',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                },
                eth: {
                    status: 'invalid',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                }
            };
            if (stamp && stamp.receipts && stamp.receipts.btc) {
                //console.log('Proving BTC network...');
                if (isNumber(stamp.receipts.btc)) {
                    stampStatus.btc.status = 'pending';
                } else {
                    try {
                        if (stampery.prove(stamp.receipts.btc)) {
                            stampStatus.btc.status = 'valid';
                        } else {
                            stampStatus.btc.status = 'pending';
                        }
                    } catch (error) {
                        //console.log('BTC Prove Error: ' + error.message);
                        stampStatus.btc.status = 'pending';
                    }
                }
            }
            if (stamp && stamp.receipts && stamp.receipts.eth) {
                //console.log('Proving ETH network...');
                if (isNumber(stamp.receipts.eth)) {
                    stampStatus.eth.status = 'pending';
                } else {
                    try {
                        if (stampery.prove(stamp.receipts.eth)) {
                            stampStatus.eth.status = 'valid';
                        } else {
                            stampStatus.eth.status = 'pending';
                        }
                    } catch (error) {
                        //console.log('ETH Prove Error: ' + error.message);
                        stampStatus.eth.status = 'pending';
                    }
                }
            }
            resolve(stampStatus);
        }).catch(function(err) {
            reject(err);
        });
    });
    return promise;
}

/**
 * Given any file path, check its validity in BTC and ETH blockchains
 */
function validateFile(downloadURL, token) {
    var promise = new Promise(function(resolve, reject) {
        getFileHash(downloadURL).then(function(h) {
            stampery.getByHash(h).then(function(res) {
                if (res.length > 0) {
                    var receipt = {};
                    res.some(function(result) {
                        if (result.token === token) {
                            receipt = result;
                        }
                    });
                    if (receipt.id) {
                        stampService.validateStampID(receipt.id).then(function(stampStatus) {
                            resolve(stampStatus);
                        });
                    } else {
                        resolve({
                            btc: 'invalid',
                            eth: 'invalid'
                        });
                    }
                } else {
                    resolve({
                        btc: 'invalid',
                        eth: 'invalid'
                    });
                }
            }).catch(function(err) {
                reject(err);
            });

        });
    });
    return promise;
}

/**
 * Get the SHA256 hash of a file (using ArrayBuffer of file)
 */
function getFileHash(downloadURL) {
    var promise = new Promise(function(resolve, reject) {
        // Get file metadata.md5Hash from Google Storage
        var bucket = storage.bucket();
        var bucketPath = downloadURL.replace(storage_root_url + '/o/', '');
        bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));

        // Get MD5 Hash from Firebase Storage Metadata
        bucket.file(bucketPath).getMetadata().then(function(metaData) {
            resolve(metaData[0].md5Hash);
        }).catch(function(err) {
            console.log('File not found...waiting and trying again');
            setTimeout(function() {
                bucket.file(bucketPath).getMetadata().then(function(metaData) {
                    resolve(metaData[0].md5Hash);
                });
            }, 2500);
        });
    });
    return promise;
}

/**
 * Determine if a given value is a number or not
 */
function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }