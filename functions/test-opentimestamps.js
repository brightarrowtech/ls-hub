const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const OpenTimestamps = require('opentimestamps');

var serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-ojpku-876bd98f9d.json");

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://lis-pipeline.firebaseio.com",
    storageBucket: "lis-pipeline.appspot.com"
});
var functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
var storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';

var db = firebase.database().ref();
var storage = firebase.storage();

db.child('orgs/buyer').orderByChild('email').equalTo('submissions@lari.io').once('value',function(snap) {
    snap.forEach(function(org) {
        console.log(org.key);
    });
});

// var data = {
//     downloadURL: 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com/o/policies%2F-LyWZGMVC_hc7Lp03oZd%2Frequested%2FApplication%20Form-MonDAEXo3bSwos79hczUiB.pdf?alt=media&token=9cefd5ef-4d97-4e57-a6aa-d8f7c5e89687'
// };

// getFileHash(data.downloadURL).then(function(h) {
//     console.log(h);
// });

// // const hash = Buffer.from('05c4f616a8e5310d19d938cfd769864d7f4ccdc2ca8b479b10af83564b097af9');
// // detached = DetachedTimestampFile.fromHash(new Ops.OpSHA256(), hash);

// // OpenTimestamps.stamp(detached).then( ()=>{
// //     const fileOts = detached.serializeToBytes();
// // });

// // const fileOts = Buffer.from('004f70656e54696d657374616d7073000050726f6f6600bf89e2e884e89294010805c4f616a8e5310d19d938cfd769864d7f4ccdc2ca8b479b10af83564b097af9f010e754bf93806a7ebaa680ef7bd0114bf408f010b573e8850cfd9e63d1f043fbb6fc250e08f10457cfa5c4f0086fb1ac8d4e4eb0e70083dfe30d2ef90c8e2e2d68747470733a2f2f616c6963652e6274632e63616c656e6461722e6f70656e74696d657374616d70732e6f7267','hex');
// // const detached = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);
// // const infoResult = OpenTimestamps.info(detached);
// // console.log(infoResult);

// function getFileHash(downloadURL) {
//     var promise = new Promise(function(resolve, reject) {
//         // Get file metadata.md5Hash from Google Storage
//         var bucket = storage.bucket();
//         var bucketPath = downloadURL.replace(storage_root_url + '/o/', '');
//         bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));

//         // Get MD5 Hash from Firebase Storage Metadata
//         bucket.file(bucketPath).getMetadata().then(function(metaData) {
//             resolve(metaData[0].md5Hash);
//         }).catch(function(err) {
//             console.log('File not found...waiting and trying again');
//             setTimeout(function() {
//                 bucket.file(bucketPath).getMetadata().then(function(metaData) {
//                     resolve(metaData[0].md5Hash);
//                 });
//             }, 2500);
//         });
//     });
//     return promise;
// }