const functions = require('firebase-functions');
const firebase = require('firebase-admin');
var pdfFiller = require('pdffiller');
const Promise = require('promise');

var serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: 'https://lis-pipeline-dev.firebaseio.com',
    storageBucket: 'lis-pipeline-dev.appspot.com'
});
var functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
var storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';

var storage = firebase.storage();

var form = 'application'; 
var data = {
    'insured.fullName': 'Stephen Bass',
    'insured.dateOfBirth': '1/1/1970',
    'insured.gender.name': 'M',
    'insured.ssn': '123-12-1234',
    'insured.address.address': '123 Nowhere Drive',
    'insured.address.city': 'Some City',
    'insured.address.state': 'SC',
    'insured.address.zip': '12345'
};
var policyNumber = 'Test1';
var policyKey = 'TestKey';
generatePDF(form, data, policyNumber, policyKey).then(function(result) {
    console.log(result);
});

function generatePDF(form, data, policyNumber, policyKey) {
    var promise = new Promise(function(resolve, reject) {

        // Get the fillable PDF form from 'tmp' if it exists, otherwise download from Storage

        // Fill the PDF
        var sourcePDF = 'test/' + form + '.pdf';
        var destinationPDF = 'test/' + form + '-' + policyNumber + '_complete.pdf';

        pdfFiller.fillForm(sourcePDF, destinationPDF, data, function(err) {
            if (err) throw err;

            // Push the filled PDF file back to Storage under the given Policy key

            // Respond with the download URL
            resolve('done');
        });
    });

    return promise;
}