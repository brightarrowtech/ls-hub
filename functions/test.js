const firebase = require('firebase-admin');
const Promise = require('promise');
const fs = require('fs');
const async = require('async');
const moment = require('moment');
const pipedrive = require('pipedrive');

var env = 'prod'; // 'dev', 'prod', 'demo'

pipedrive.Configuration.apiToken = 'ef66f46085806bbb021a9f2f045cd354c61c80eb';

var policy = {
    "-L8crXXb_c8NRSJUgRpn" : "ibroker",
    "-LqDOnfSHogC5fhH0L9j" : "supplier",
    "LSHID" : "L93440797",
    "actualCreatedBy" : "Marketing Lead Form",
    "actualCreatedById" : "marketing-lead-form",
    "alertId" : "1adf725f-e720-4af9-865d-13bb03aa3a06",
    "contacts" : [ {
        "emailAddress" : "itjustwerks123@gmail.com",
        "firstName" : "Test",
        "lastName" : "Pipedrive",
        "link" : "Submitted Lead Form",
        "phoneNumber" : "352 509 6878"
    } ],
    "createdAt" : 1593440797342,
    "createdBy" : "Brian Greenberg",
    "createdById" : "uKiBdw9iH4PbqZHy0XOZTSVSIO73",
    "createdByOrgKey" : "-L8crXXb_c8NRSJUgRpn",
    "createdByOrgType" : "ibroker",
    "createdOnPlatform" : "isubmit",
    "faceAmount" : "100000",
    "forms" : {
        "application" : {
        "className" : "application",
        "formName" : "Application Form",
        "publicGuid" : "fe8cb73e-980c-40ff-ae80-a56d3b9f730c",
        "recipients" : {
            "agent" : {
            "emailAddress" : "stephen@lshub.net",
            "firstName" : "Stephen",
            "fullName" : "Stephen Jass",
            "lastName" : "Jass"
            }
        },
        "status" : "In Progress"
        },
        "medicalPrimary" : {
        "className" : "medicalPrimary",
        "formName" : "Medical Questionnaire (Primary)",
        "publicGuid" : "dfb6e457-8e72-4812-bb40-da608b25eefe",
        "status" : "Start Now"
        }
    },
    "insured" : {
        "age" : 65,
        "dateOfBirth" : "12-12-1954",
        "firstName" : "Diane",
        "fullName" : "Diane Bruce",
        "healthStatus" : "Good",
        "lastName" : "Bruce"
    },
    "isLead" : true,
    "latestStatus" : "No updates have been posted here yet",
    "leadEstimate" : {
        "data" : {
        "estimate" : {
            "high" : "0",
            "low" : "0"
        }
        },
        "message" : "Policy estimate is zero",
        "status" : "notEligible"
    },
    "policyIsActive" : true,
    "publicNotes" : [ {
        "createdAt" : 1593444443000,
        "createdBy" : "Jennifer Faubert",
        "createdById" : "vmMoLLWXtFOqeBqdtTsdikwarY92",
        "createdByRole" : "iBroker",
        "note" : "Emailed. Will call."
    } ],
    "signingAgent" : {
        "emailAddress" : "stephen@lshub.net",
        "firstName" : "Stephen",
        "fullName" : "Stephen Jass",
        "lastName" : "Jass"
    },
    "status" : {
        "applicationInProgress" : true,
        "caseReceived" : true,
        "caseReceivedAt" : 1593440797342,
        "display" : {
        "status" : "In Progress",
        "step" : "Application",
        "text" : "Unqualified Lead"
        }
    },
    "statusSelection" : [ {
        "class" : "success",
        "status" : "On Track"
    } ],
    "trackingData" : [ {
        "key" : "Form Name",
        "value" : "Brian Greenberg - True Blue - Lead Form"
    }, {
        "key" : "Page URL",
        "value" : "https://www.truebluelifeinsurance.com/can-sell-life-insurance-policy/"
    }, {
        "key" : "Lead Submitted At",
        "value" : "06/29/2020 @ 7:26 am"
    }, {
        "key" : "Estimate",
        "value" : "Low: $0, High: $0"
    } ],
    "upline" : [ {
        "orgKey" : "-LqDOnfSHogC5fhH0L9j",
        "orgType" : "supplier"
    }, {
        "orgKey" : "-L8crXXb_c8NRSJUgRpn",
        "orgType" : "ibroker"
    } ]
}

async function createPipedriveLead(policy) {
    // // Organization = policy.createdBy
    // // Look up Organization...if result.length === 0, create Organization
    // let input = {
    //     term: policy.createdBy,
    //     start: 0,
    //     limit: 1
    // };
    // let orgID = 0;
    // let orgData = await pipedrive.OrganizationsController.findOrganizationsByName(input);
    // if (orgData.data && orgData.data.length > 0) {
    //     orgID = orgData.data[0].id;
    // } else {
    //     console.log('Org not found. Creating new Organization for Agent (policy.createdBy)');
    //     input = {
    //         body: {
    //             name: policy.createdBy
    //         }
    //     };
    //     let orgResponse = await pipedrive.OrganizationsController.addAnOrganization(input);
    //     orgID = orgResponse.data.id;
    // }

    // // Person = policy.insured
    // // Look up Person...if result.length === 0, create Person
    // input = {
    //     term: policy.insured.fullName,
    //     start: 0,
    //     limit: 2
    // };
    // let personID = 0;
    // let personData = await pipedrive.PersonsController.findPersonsByName(input);
    // if (personData.data && personData.data.length > 1) {
    //     console.log('More than one Insured Person was found for the provided search. Unable to associate');
    // } else if (personData.data && personData.data.length > 0) {
    //     personID = personData.data[0].id;
    // } else if (!personData.data || personData.data.length === 0) {
    //     console.log('Person not found. Creating new Person for insured (policy.insured)');
    //     input = {
    //         body: {
    //             name: policy.insured.fullName,
    //             org_id: orgID
    //         }
    //     };
    //     let personResponse = await pipedrive.PersonsController.addAPerson(input);
    //     personID = personResponse.data.id;
    // }

    // // Create new Deal in Pipeline (id: 17 - Lead Tracking / New)
    // let dealID = 0;
    // input = {
    //     body: {
    //         title: policy.insured.fullName + " - " + new Intl.NumberFormat('en-US', {style: 'currency', currency: 'USD', minimumFractionDigits: 0, maximumFractionDigits: 0}).format(policy.faceAmount),
    //         stage_id: 17,
    //         org_id: orgID,
    //         person_id: personID
    //     }
    // }
    // let newDeal = await pipedrive.DealsController.addADeal(input);
    // dealID = newDeal.data.id;

    // // Get dealFields:(key,name)
    // //   Loop to find "Policy Face Value" (id: 9a4cecdd9514f690812f5ff5ba45670b096c13a8)
    // input = {
    //     id: dealID,
    //     '9a4cecdd9514f690812f5ff5ba45670b096c13a8': parseInt(policy.faceAmount)
    // };
    // let updateDeal = await pipedrive.DealsController.updateADeal(input);

    // // Associate Policy Contact with Deal
    let dealID = 136;
    if (policy.contacts && policy.contacts.length > 0) {
        // Person = policy.contacts[0]
        // Look up Person (Contact) by email address...if result.length === 0, create Person
        let lookupValue = '';
        let contactPersonID = 0;
        let contactData = null;
        if (policy.contacts[0].emailAddress) {
            lookupValue = policy.contacts[0].emailAddress;
            input = {
                term: lookupValue,
                start: 0,
                limit: 2
            };
            contactData = await pipedrive.PersonsController.findPersonsByName(input);
            if (contactData.data && contactData.data.length > 1) {
                console.log('More than one Contact Person was found for the provided search. Unable to associate');
            } else if (contactData.data && contactData.data.length > 0) {
                contactPersonID = contactData.data[0].id;
            }
        }
        if (contactPersonID === 0) {
            lookupValue = policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName;
            input = {
                term: lookupValue,
                start: 0,
                limit: 2
            };
            contactData = await pipedrive.PersonsController.findPersonsByName(input);
            if (contactData.data && contactData.data.length > 1) {
                console.log('More than one Contact Person was found for the provided search. Unable to associate');
            } else if (contactData.data && contactData.data.length > 0) {
                contactPersonID = contactData.data[0].id;
            }
        }
        
        if (contactPersonID === 0) {
            console.log('Person not found. Creating new Person for contact (policy.contacts[0])');
            input = {
                body: {
                    name: policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName,
                    phone: [{
                        value: policy.contacts[0].phoneNumber.replace(/\D/g,''),
                        primary: true
                    }],
                    email: [{
                        value: policy.contacts[0].emailAddress,
                        primary: true
                    }]
                }
            };
            let contactResponse = await pipedrive.PersonsController.addAPerson(input);
            contactPersonID = contactResponse.data.id;
        } else {
            console.log('Contact found as existing Person. Updating to include email and phone');
            input = {
                id: contactPersonID,
                phone: [{
                    value: policy.contacts[0].phoneNumber.replace(/\D/g,''),
                    primary: true
                }],
                email: [{
                    value: policy.contacts[0].emailAddress,
                    primary: true
                }]
            };
            await pipedrive.PersonsController.updateAPerson(input);
        }
        if (contactPersonID !== 0 && contactPersonID !== insuredPersonID) {
            // Adding additional Participant to Deal (for Contact)
            input = {
                id: dealID,
                person_id: contactPersonID
            };
            await pipedrive.DealsController.addAParticipantToADeal(input);
        }
    }

    // // Add an initial Note to the Deal
    // input = {
    //     content: '<b>Insured DOB:</b> ' + policy.insured.dateOfBirth + ' (' + policy.insured.age + ' years old)<br><b>Health Status:</b> ' + policy.insured.healthStatus + '<br><b>LSHID:</b> ' + policy.LSHID + '<br><b>Low Estimate:</b> ' + policy.leadEstimate.data.estimate.low + '<br><b>High Estimate:</b> ' + policy.leadEstimate.data.estimate.high + '<br><b>Signing Agent:</b> ' + policy.signingAgent.fullName + ' (' + policy.signingAgent.emailAddress + ')',
    //     dealId: dealID
    // };
    // pipedrive.NotesController.addANote(input, function(error, response, context) {
    //     if (error) {
    //         console.log(error);
    //     } else {
    //         // Finished
    //         console.log('Deal ' + dealID + ' created');
    //     }
    // });
}

//createPipedriveLead(policy);

var serviceAccount = {};
if (env === 'demo') {
    serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-moong-5e8e33fd54");
} else if (env === 'prod') {
    serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-ojpku-876bd98f9d.json");
} else if (env === 'dev') {
    serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');
}

var functions_root_url = '';
var storage_root_url = '';

if (env === 'demo') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-demo.firebaseio.com",
        storageBucket: "lis-pipeline-demo.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline.firebaseio.com",
        storageBucket: "lis-pipeline.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-dev.firebaseio.com",
        storageBucket: "lis-pipeline-dev.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

var db = firebase.database().ref();
var storage = firebase.storage();

// SET BUYER GUIDS AS RECIPIENTS ON THEIR OWN PRIVATE POLICY COPIES
db.child('orgs/buyer').orderByChild('policies').startAt('a').once('value', function(snap) {
    if (snap.exists()) {
        let buyers = [];
        snap.forEach(function(buyerRef) {
            let buyer = buyerRef.val();
            let policies = Object.keys(buyer.policies);
            let buyerGuid = buyer.orgGuid;
            if (!buyerGuid) {
                console.log('Buyer ' + buyerRef.ref + ' has no orgGuid');
            } else {
                buyers.push({
                    buyerGuid: buyerGuid,
                    policies: policies
                });
            }
        });
        console.log('Processing ' + buyers.length + ' Buyers');
        async.eachOfSeries(buyers, function(buyer, key, done) {
            async.eachOfSeries(buyer.policies, function(policy, key2, done2) {
                db.child('privatePolicies/' + policy).update({
                    recipient: buyer.buyerGuid
                }).then(function() {
                    done2();
                });
            }, function(err) {
                console.log('Finished processing buyer ' + buyer.buyerGuid);
                done();
            });
        }, function(err) {
            console.log('Finished processing all buyers');
        });
    } else {
        console.log('No Buyers Found');
    }
});

// CHANGE A USER'S EMAIL ADDRESS AND PASSWORD
// let UID = 'mY04YYA7ToNQI5xq7E2d4FLaUV13';
// firebase.auth().updateUser(UID, {
//     email: 'colin@abacuslife.com',
//     emailVerified: true,
//     password: 'Abacus1'
// }).then(function(userRecord) {
//     console.log('Successfully updated user', userRecord.toJSON());
// }).catch(function(error) {
//     console.log('Error updating user:', error);
// });

// PULL A LIST OF ORPHAN LEADS
// db.child('leads').once('value', function(snap) {
//     if (snap.exists()) {
//         let leads = [];
//         snap.forEach(function(leadRef) {
//             let lead = leadRef.val();
//             leads.push({
//                 'Insured Name': lead.policyData.insured ? lead.policyData.insured.fullName : '',
//                 'Insured DOB': lead.policyData.insured ? lead.policyData.insured.dateOfBirth : '',
//                 'Insured Age': lead.policyData.insured ? lead.policyData.insured.age : '',
//                 'Face Amount': lead.policyData.faceAmount,
//                 'Health Status': lead.policyData.insured ? lead.policyData.insured.healthStatus : '',
//                 'Contact Name': lead.policyData.contacts ? (lead.policyData.contacts[0].firstName + ' ' + lead.policyData.contacts[0].lastName) : '',
//                 'Email': lead.policyData.contacts ? lead.policyData.contacts[0].emailAddress : '',
//                 'Phone': lead.policyData.contacts ? lead.policyData.contacts[0].phoneNumber : '',
//                 'Page URL': lead.pageURL,
//                 'Form ID': lead.pef
//             });
//             let leadCSV = JSON2CSV(leads);
//             try {
//                 fs.writeFileSync('./leads.csv', leadCSV);
//             } catch (err) {
//                 console.error(err);
//             }
//         });
//     }
// });

// PULL A LIST OF TRUE BLUE LEADS
// db.child('policies').orderByChild('createdById').equalTo('uKiBdw9iH4PbqZHy0XOZTSVSIO73').once('value', function(snap) {
//     if (snap.exists()) {
//         let leads = [];
//         snap.forEach(function(leadRef) {
//             let lead = leadRef.val();
//             leads.push({
//                 'LSH ID': lead.LSHID,
//                 'Submitted On': moment(lead.createdAt).format('MM/DD/YYYY'),
//                 'Insured Name': lead.insured ? lead.insured.fullName : '',
//                 'Insured DOB': lead.insured ? lead.insured.dateOfBirth : '',
//                 'Insured Age': lead.insured ? lead.insured.age : '',
//                 'Face Amount': lead.faceAmount,
//                 'Health Status': lead.insured ? lead.insured.healthStatus : '',
//                 'Estimated Offer': '',
//                 'Contact Name': lead.contacts ? (lead.contacts[0].firstName + ' ' + lead.contacts[0].lastName) : '',
//                 'Email': lead.contacts ? lead.contacts[0].emailAddress : '',
//                 'Phone': lead.contacts ? lead.contacts[0].phoneNumber : '',
//                 'Active': lead.policyIsActive ? 'Yes': 'No',
//                 'Lead Rejected': lead.isRejected ? 'Rejected' : ''
//             });
//         });
//         let leadCSV = JSON2CSV(leads);
//         console.log(leadCSV);
//     }
// });