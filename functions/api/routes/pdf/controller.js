const firebase   = require("firebase-admin");
const path       = require("path");
const fs = require('fs');
const Promise = require('promise');
const querystring = require('querystring');
const cors = require('cors')({ origin: true });
const UUID = require("uuid-v4");
const async = require('async');
const moment = require('moment');
const os = require("os");
const pdfFiller = require('pdffiller');

var access_token = '';
var refresh_token = '';
var code = '';
var client_id = '';
var client_secret = '';

var env = 'demo'; // 'dev', 'prod', 'demo'

// var serviceAccount = {};
// if (env === 'demo') {
//     serviceAccount = require(path.resolve(__dirname, "../../../certs/lis-pipeline-demo-firebase-adminsdk-zukgq-dc434bdf0d"));
// } else if (env === 'prod') {
//     serviceAccount = require(path.resolve(__dirname, "../../../certs/lis-pipeline-firebase-adminsdk-k6fyc-0088fff759"));
// } else if (env === 'dev') {
//     serviceAccount = require(path.resolve(__dirname, "../../../certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b"));
// }

var functions_root_url = '';
var storage_root_url = '';
if (env === 'demo') {
    // firebase.initializeApp({
    //     credential: firebase.credential.cert(serviceAccount),
    //     databaseURL: "https://lis-pipeline-demo.firebaseio.com",
    //     storageBucket: "lis-pipeline-demo.appspot.com"
    // });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    // firebase.initializeApp({
    //     credential: firebase.credential.cert(serviceAccount),
    //     databaseURL: "https://lis-pipeline.firebaseio.com",
    //     storageBucket: "lis-pipeline.appspot.com"
    // });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    // firebase.initializeApp({
    //     credential: firebase.credential.cert(serviceAccount),
    //     databaseURL: "https://lis-pipeline-dev.firebaseio.com",
    //     storageBucket: "lis-pipeline-dev.appspot.com"
    // });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

var db = firebase.database().ref();
var storage = firebase.storage();

function fillAndDownload(req, res) {
    // Convert PandaDoc data format to remove "value" node
    var fieldData = {};
    Object.keys(req.body.fieldData).forEach(function(item) {
        if (req.body.fieldData[item].value) {
            fieldData[item] = req.body.fieldData[item].value
        }
    });
    var policyNumber = req.body.metaData.policyNumber;
    var policyKey = req.body.metaData.policyKey;
    var formVersion = req.body.metaData.formVersion;
    var formName = req.body.metaData.formName;
    var className = req.body.metaData.className;
    var env = req.body.metaData.env ? req.body.metaData.env : 'prod';
    var fileName = className;
    fileName += (formVersion) ? '-' + formVersion + '.pdf' : '.pdf'

    generatePDF(formName, fileName, className, fieldData, policyNumber, policyKey, env).then(function(result) {
        if (result.status === 'success') {
            return res.status('200').send({
                status: 'success',
                message: 'The file was successfully filled and downloaded',
                data: result.downloadURL
            });
        } else {
            return res.status('200').send(result);
        }
    });
}

/**
 * Create a filled PDF document for a given form and data set
 */
function generatePDF(formName, fileName, className, data, policyNumber, policyKey, env) {
    var promise = new Promise(function(resolve, reject) {
        // Verify that we have the template downloaded to the server (from Firebase Storage)
        var bucket = storage.bucket();

        checkTemplate(fileName, bucket).then(function(result) {
            if (result.status === 'success') {
                var storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
                if (env === 'demo' || env === 'dev') {
                    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
                }

                // Fill the PDF
                var rand = Math.floor(Math.random()*90000) + 10000;
                var sourcePDF = path.resolve(os.tmpDir(), fileName);
                var file = className + '-' + policyNumber.replace(/ /g, '_').replace(/\//g, '_') + '-' + rand + '.pdf';
                var destinationPDF = path.resolve(os.tmpDir(), file);
                var shouldFlatten = false;

                console.log('Filling PDF with data...');

                pdfFiller.fillFormWithFlatten(sourcePDF, destinationPDF, data, shouldFlatten, function(err) {
                    if (err) {
                        resolve({
                            status: 'error',
                            message: err.message
                        });
                    }

                    console.log('File ready! Pushing to Firebase');

                    // Push the filled PDF file back to Storage under the given Policy key
                    var uuid = UUID();
                    var options = {
                        destination: 'policies/' + policyKey + '/' + file,
                        resumable: false,
                        metadata: {
                            metadata: {
                                policyKey: policyKey,
                                policyNumber: policyNumber,
                                formName: formName,
                                className: className,
                                firebaseStorageDownloadTokens: uuid
                            }
                        }
                    };

                    var downloadURL = storage_root_url + '/o/policies%2F' + policyKey + '%2F' + encodeURIComponent(file) + '?alt=media&token=' + uuid;

                    console.log('Uploading file:  ' + file);
                    bucket.upload(path.resolve(os.tmpDir(), file), options, function(err, remoteFile) {
                        if (!err) {
                            resolve({
                                status: 'success',
                                downloadURL: downloadURL
                            });
                        } else {
                            resolve({
                                status: 'error',
                                message: err.message
                            });
                        }
                    });
                });
            } else {
                resolve(result);
            }
        });
    });

    return promise;
}

/**
 * Check for existence of form template in server temp directory
 */
function checkTemplate(fileName, bucket) {
    var promise = new Promise(function(resolve, reject) {
        if (!fs.existsSync(path.resolve(os.tmpDir(), fileName))) {
            console.log('Template doesn\'t exist locally. Downloading from Firebase Storage');
            bucket.file('form-templates/' + fileName).download({
                destination: path.resolve(os.tmpDir(), fileName)
            }, function(err) {
                if (err) {
                    console.log('Template retrieval failed:');
                    console.log(err.message);
                    resolve({
                        status: 'error',
                        message: err.message,
                        fileName: fileName,
                        bucket: bucket
                    });
                } else {
                    // We have the template file locally...continue
                    console.log('Template has been retrieved successfully');
                    resolve({
                        status: 'success'
                    });
                }
            });
        } else {
            // We have the template file locally...continue
            resolve({
                status: 'success'
            });
        }
    });
    return promise;
}

module.exports = {
    fillAndDownload
}