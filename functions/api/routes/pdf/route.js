const router     = require("express").Router();
const controller = require("./controller");

router.post("/fillAndDownload", controller.fillAndDownload);

router.get('/fillAndDownload', function(req, res, next) {
    res.status('200').send('Nothing to see here');
});

module.exports = router;