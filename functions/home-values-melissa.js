const request = require('request');
const async = require('async');
const Papa = require('papaparse');
var fs = require('fs');

var NPNResume = '2739381';
var processRow = (NPNResume === '') ? true : false;
var fileName = 'test';
var results = [];
var rows = [];
var inputFile = 'zillow/' + fileName + '.csv';
var outputFile = 'zillow/' + fileName + '-data.csv';
var readStream = fs.createReadStream(inputFile);
var writeStream = fs.createWriteStream(outputFile, {flags:'a'});
Papa.parse(readStream, {
    header: true,
    skipEmptyLines: true,
    dynamicTyping: true,
    step: function(results) {
        if (NPNResume !== '' && !processRow) {
            if (results.data['NPN'] && results.data['NPN'].toString() === NPNResume) {
                processRow = true;
            }
        } else if (processRow) {
            rows.push(results.data);
        }
    },
    complete: function() {
        // PROCESS ROWS!
        async.forEachOfSeries(rows, function(row, key, callback) {
            if (row['Home Address 1'] !== null) {
                console.log('Processing NPN ' + row['NPN'] + ': ' + row['Home Address 1'] + ', ' + row['Home City'] + ', ' + row['Home State'] + ' ' + row['Home Zip']);
                request.get({
                    url: "https://www.melissa.com/v2/lookups/property/",
                    qs: {
                        'id': "kvCqzHOsBGyq14qU0PxMjP**nSAcwXpxhQ0PC2lXxuDAZ-**",
                        'freeForm': "",
                        'fmt': "json",
                        'mak': "",
                        'address': row['Home Address 1'],
                        'city': row['Home City'],
                        'state': row['Home State'],
                        'zip': row['Home Zip']
                    }
                }, function(err, response, body) {
                    body = JSON.parse(body);
                    row.MAK = body[0].MAK;
                    row.Latitude = body[0].Latitude;
                    row.Longitude = body[0].Longitude;
                    row.EstimatedValue = body[0].EstimatedValue;
                    row.YearAssessed = body[0].YearAssessed;
                    row.AssessedValueTotal = body[0].AssessedValueTotal;
                    row.TaxBilledAmount = body[0].TaxBilledAmount;
                    row.YearBuilt = body[0].YearBuilt;
                    row.AreaBuilding = body[0].AreaBuilding;
                    row.AreaLotAcres = body[0].AreaLotAcres;
                    row.BedroomsCount = body[0].BedroomsCount;
                    row.BathCount = body[0].BathCount;
                    writeStream.write(Papa.unparse([row], { header: false }) + '\r\n');
                    callback();
                });   
            } else {
                console.log('Processing NPN ' + row['NPN'] + ': No Home Address Provided');
                row.MAK = '';
                row.Latitude = '';
                row.Longitude = '';
                row.EstimatedValue = '';
                row.YearAssessed = '';
                row.AssessedValueTotal = '';
                row.TaxBilledAmount = '';
                row.YearBuilt = '';
                row.AreaBuilding = '';
                row.AreaLotAcres = '';
                row.BedroomsCount = '';
                row.BathCount = '';
                writeStream.write(Papa.unparse([row], { header: false }) + '\r\n');
                callback();
            }
        }, function(err) {
            if (err) {
                console.log(err);
            } else {
                writeStream.end();
                console.log('All done!');
            }
        });     
    },
    error: function(error) {
        console.log(error);
    }
});