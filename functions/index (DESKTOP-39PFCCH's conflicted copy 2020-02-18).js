const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const axios = require('axios');
const fs = require('fs');
const Promise = require('promise');
const querystring = require('querystring');
const cors = require('cors')({ origin: true });
const Stampery = require('stampery');
const forge = require('node-forge');
const UUID = require("uuid-v4");
const async = require('async');
const request = require('request');
const ZCRMRestClient = require('zcrmsdk');
const moment = require('moment');
const diff = require('deep-diff').diff;
const getRandomValues = require('get-random-values');
const os = require("os");

var access_token = '';
var refresh_token = '';
var code = '';
var client_id = '';
var client_secret = '';

var env = 'demo'; // 'dev', 'prod', 'demo'
var stampery = {};
var stampery_token = '';

if (env === 'dev' || env === 'demo') {
    stampery = new Stampery('9c76b54e-6e5c-44f9-b7f2-f857063e95ad');
    stampery_token = 'ced2f70e20c5e04';
} else {
    stampery = new Stampery('71dfcc47-b661-4071-afa7-f4ad7db42a23');
    stampery_token = '53b2c7efdc77d69';
}

var md = forge.md.sha256.create();

var serviceAccount = {};
if (env === 'demo') {
    serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-zukgq-dc434bdf0d");
} else if (env === 'prod') {
    serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-k6fyc-0088fff759.json");
} else if (env === 'dev') {
    serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');
}

var functions_root_url = '';
var storage_root_url = '';
if (env === 'demo') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-demo.firebaseio.com",
        storageBucket: "lis-pipeline-demo.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline.firebaseio.com",
        storageBucket: "lis-pipeline.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-dev.firebaseio.com",
        storageBucket: "lis-pipeline-dev.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

var db = firebase.database().ref();
var storage = firebase.storage();

var zohoOrgId = '664900183';
var zohoAuthToken = '7a9ad030c04d887241d8ce058634bde1';

if (env === 'prod') {
    zohoOrgId = '665881413';
    zohoAuthToken = '32a520ce9289f0543a8d28e5829f8ec6';
}

const zohoService = {};
var zohoRootUrl = 'https://subscriptions.zoho.com/api/v1';
var zohoRedirect = 'https://app.lshub.net';
if (env === 'demo') {
    zohoRedirect = 'https://demo.lshub.net';
} else if (env === 'dev') {
    zohoRedirect = 'http://localhost:3000';
}

const pandadocService = {
    downloadDocument: downloadDocument,
    setStatus: setStatus,
    getConfig: getConfig,
    setConfig: setConfig,
    refreshToken: refreshToken
}

const stampService = {
    stampPandaDoc: stampPandaDoc,
    validateStampID: validateStampID,
    validateFile: validateFile
};

function uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

/**
 * Set user's Access Domain (from the SAdmin User Management panel)
 */
exports.setCustomClaims = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var claims = req.body.claims;
        firebase.auth().setCustomUserClaims(uid, claims)
            .then(function() {
                db.child('users').child(uid).update(claims).then(function() {
                    res.status('200').send({
                        status: 'success',
                        message: 'Custom Claims successfuly updated'
                    });
                }).catch(function(err) {
                    res.status('200').send({
                        status: 'error',
                        message: 'Error setting Custom Claims on USERS node',
                        data: err
                    });
                });
            })
            .catch(function(error) {
                res.status('200').send({
                    status: 'error',
                    message: 'Error setting Custom Claims on AUTH object',
                    data: error
                });
            });
    });
})

/**
 * Mints a new custom JWT token for the provided UID
 */
exports.mintCustomToken = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        firebase.auth().verifyIdToken(data.token).then(function(decodedToken) {
            firebase.auth().createCustomToken(decodedToken.uid).then(function(newToken) {
                res.status('200').send({
                    status: 'success',
                    message: 'Custom token created',
                    data: newToken
                });
            }).catch(function(err) {
                res.status('200').send({
                    status: 'error',
                    message: 'Error minting custom token',
                    data: err
                });
            });
        });
    });
});

/**
 * Link Seller Registration with Policy from Lead Form
 */
exports.linkSellerWithPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var authUser = req.body.authUser;
        var sellerPolicyKey = req.body.sellerPolicyKey;
        //  1) Change policy createdBy and createdById to match the Seller
        //  2) Remove policy createdByOrgKey and createdByOrgType if present
        //  3) Add policy to Seller's "myPolicies" node under their user account
        //     Only policyKey is known at this point...
        var updates = {};
        updates['policies/' + sellerPolicyKey + '/createdBy'] = authUser.displayName;
        updates['policies/' + sellerPolicyKey + '/createdById'] = authUser.uid;
        updates['policies/' + sellerPolicyKey + '/createdByOrgKey'] = null;
        updates['policies/' + sellerPolicyKey + '/createdByOrgType'] = null;

        updates['users/' + authUser.uid + '/myPolicies/' + sellerPolicyKey] = {
            policyKey: sellerPolicyKey
        };
        
        db.update(updates).then(function() {
            res.status('200').send({
                status: 'success',
                message: 'Seller successfully associated with Policy',
                data: null
            });
        });
    });
});

/**
 * Close or Archive all of the Buyer copies of a Policy
 */
exports.closeOrArchiveCopies = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        db.child('privatePolicies').orderByChild('rootKey').equalTo(data.key).once('value', function(snap) {
            if (snap.exists()) {
                async.forEachOfSeries(Object.keys(snap.val()), function(policyCopyKey, key, callback) {
                    if (data.action === 'close') {
                        db.child('privatePolicies/' + policyCopyKey).update({
                            isClosed: true,
                            status: {
                                caseClosed: true,
                                caseClosedAt: firebase.database.ServerValue.TIMESTAMP,
                                display: {
                                    status: "Closed",
                                    text: "Closed",
                                    step: "Closed"
                                }
                            }
                        }).then(function(){
                            callback();
                        });
                    } else if (data.action === 'archive') {
                        db.child('privatePolicies/' + policyCopyKey).update({
                            policyIsActive: false,
                            isArchived: true,
                            status: {
                                caseArchived: true,
                                caseArchivedAt: firebase.database.ServerValue.TIMESTAMP
                            }
                        }).then(function(){
                            callback();
                        });
                    }
                }, function(err) {
                    if (err) {
                        res.status('200').send({
                            status: 'error',
                            message: 'Error closing Policy copies',
                            data: err
                        });
                    } else {
                        res.status('200').send({
                            status: 'success',
                            message: 'Policy copies have been updated',
                            data: null
                        });
                    }
                });
            } else {
                res.status('200').send({
                    status: 'success',
                    message: 'No Policy copies found',
                    data: data.key
                });
            }
        });                
    });
});

/**
 * Create new subscription for provided customer using hosted page
 */
exports.createSubscription = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var url = zohoRootUrl + '/hostedpages/newsubscription';
        var redirectUrl = zohoRedirect + '/profile';

        axios.post(url, {
                customer: req.body.customer,
                plan: req.body.plan,
                custom_fields: req.body.customFields,
                redirect_url: redirectUrl
            }, {
                headers: {
                    Authorization: 'Zoho-authtoken ' + zohoAuthToken,
                    'X-com-zoho-subscriptions-organizationid': zohoOrgId,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }).then(function(response) {
                if (response.data.code === 0) {
                    res.status('200').send({
                        status: 'redirect',
                        message: 'Hosted page created successfully',
                        data: response.data.hostedpage
                    });
                }
            })
            .catch(function(err) {
                console.log(err);
                res.status('200').send({
                    status: 'error',
                    message: err.message
                });
            });
    });
});

/** 
 * Get Hosted Page subscription details
 */
exports.getHostedPageDetails = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var url = zohoRootUrl + '/hostedpages/' + req.query.hostedPageId
        axios.get(url, {
                headers: {
                    Authorization: 'Zoho-authtoken ' + zohoAuthToken,
                    'X-com-zoho-subscriptions-organizationid': zohoOrgId,
                    'Content-Type': 'application/json;charset=UTF-8'
                }
            }).then(function(response) {
                console.log(response.data.data.subscription.subscription_id);
                res.status('200').send({
                    status: 'success',
                    message: 'Hosted page details retrieved',
                    data: {
                        subscriptionId: response.data.data.subscription.subscription_id,
                        planName: response.data.data.subscription.plan.name,
                        orgKey: response.data.data.subscription.custom_fields[0].value,
                        status: response.data.data.subscription.status
                    }
                });
            })
            .catch(function(err) {
                console.log(err);

            });
    });
});

/**
 * Notify LS Hub Admin of new Marketing Feature Requests
 */
exports.requestMarketingAccess = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;

        var recipients = [];
        recipients.push({
            email: 'stephen@lshub.net',
            template: 'marketing-access-request',
            subData: {
                orgType: data.orgType,
                orgName: data.orgName,
                orgPrimaryContact: data.orgPrimaryContact,
                orgPhone: data.orgPhone,
                orgEmail: data.orgEmail,
                userName: data.userName,
                userPhone: data.userPhone,
                userEmail: data.userEmail
            }
        });
        recipients.push({
            email: 'admin@lshub.net',
            template: 'marketing-access-request',
            subData: {
                orgType: data.orgType,
                orgName: data.orgName,
                orgPrimaryContact: data.orgPrimaryContact,
                orgPhone: data.orgPhone,
                orgEmail: data.orgEmail,
                userName: data.userName,
                userPhone: data.userPhone,
                userEmail: data.userEmail
            }
        });

        var formData = {
            env: env,
            recipients: recipients
        };
        // Step 4: Send email with digestData as payload
        var emailURL = 'https://api.lshub.net/email/send';
        request.post({
            url: emailURL,
            body: formData,
            json: true
        }, function(error, response, body) {
            if (error) {
                res.status(200).send({
                    status: 'error',
                    message: error.message
                });
            } else {
                // Update requesting org status as requested with timestamp...
                var updates = {};
                updates['marketingAccessRequestedAt'] = firebase.database.ServerValue.TIMESTAMP;
                updates['marketingAccessRequested'] = true;
                db.child('orgs').child(data.orgType).child(data.orgKey).update(updates).then(function() {
                    res.status(200).send({
                        status: 'success',
                        message: 'Marketing Access Request email sent successfully',
                        data: {}
                    });
                }).catch(function(err) {
                    res.status(200).send({
                        status: 'success',
                        message: 'Marketing Access Request email sent successfully',
                        data: {}
                    });
                });
            }
        });
    });
});

/** 
 * Post notifications to user orgs and/or accounts
 */
exports.sendInAppNotifications = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;

        var orgTypes = ['supplier', 'buyer', 'ibroker', 'broker', 'master'];
        if (data === undefined || !data.targets || data.targets.length === 0) {
            res.status('200').send({
                status: 'error',
                message: 'No Notification Targets provided',
                data: {}
            });
        } else {
            async.forEachOfSeries(data.targets, function(target, key, callback) {
                firebase.database().ref('notifications').push({
                    isUnread: true,
                    isUrgent: data.isUrgent ? data.isUrgent : null,
                    policyKey: data.policyKey ? data.policyKey : null,
                    LSHID: data.LSHID ? data.LSHID : null,
                    pid: data.pid ? data.pid : null,
                    bid: data.bid ? data.bid : null,
                    lvl: data.lvl ? data.lvl : null,
                    createdAt: firebase.database.ServerValue.TIMESTAMP,
                    createdBy: data.createdBy ? data.createdBy : null,
                    createdByRole: data.createdByRole ? data.createdByRole : null,
                    note: data.note ? data.note : null,
                    action: data.action ? data.action : null,
                    targetType: target.type,
                    targetId: target.id,
                    iBrokerBOFlag: target.iBrokerBOFlag ? target.iBrokerBOFlag : null,
                    subData: {
                        link: (data.notificationSubData && data.notificationSubData.link) ? data.notificationSubData.link : null,
                        linkRoot: (data.notificationSubData && data.notificationSubData.linkRoot) ? data.notificationSubData.linkRoot : null,
                        platform: (data.notificationSubData && data.notificationSubData.platform) ? data.notificationSubData.platform : null,
                        headerImg: (data.notificationSubData && data.notificationSubData.headerImg) ? data.notificationSubData.headerImg : null
                    }
                }).then(function(ref) {
                    ref.update({
                        notifyKey: ref.key
                    }).then(function() {
                        callback();
                    }).catch(function(e) {
                        callback(e);
                    });
                });
            }, function(err) {
                if (err) {
                    res.status('200').send({
                        status: 'error',
                        message: err.message,
                        data: err
                    });
                } else {
                    res.status('200').send({
                        status: 'success',
                        message: 'All notifications sent successfully',
                        data: {}
                    });
                }
            });
        }
    });
});

/**
 * Gets details about a public marketing lead form
 */
exports.getPublicEmbedForm = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        db.child('publicEmbedForms').child(data.pef).once('value', function(snap) {
            if (snap.exists()) {
                var form = snap.val();
                db.child('marketing/' + form.ok + '/forms/' + form.fid).once('value', function(formSnap) {
                    if (formSnap.exists()) {
                        var formDetails = formSnap.val();
                        res.status('200').send({
                            status: 'success',
                            message: 'Public Embed Form found',
                            data: {
                                contactNumber: (formDetails.contactNumber) ? formDetails.contactNumber : null,
                                followUpLink: (formDetails.followUpLink) ? formDetails.followUpLink : null,
                                accentColor: (formDetails.accentColor) ? formDetails.accentColor : '#009ada',
                                formShadow: (formDetails.formShadow) ? formDetails.formShadow : 'true',
                                formBorder: (formDetails.formBorder) ? formDetails.formBorder : 'false',
                                width: (formDetails.frameWidth) ? formDetails.frameWidth : '100%',
                                formName: (formDetails.formName) ? formDetails.formName : 'Sell Your Life Insurance Policy!'
                            }
                        });
                    } else {
                        res.status('200').send({
                            status: 'error',
                            message: 'Marketing Form not found for Org',
                            data: {}
                        });
                    }
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'Public Embed Form not found',
                    data: {}
                });
            }
        });
    });
});

// const server = require("./api/server");
// exports.api = functions.runWith({
//     memory: "2GB",
//     timeoutSeconds: 120
// }).https.onRequest(server);

/**
 * Creates a new Policy from the marketing lead form
 */
exports.createNewPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        // Get form data from the embedded form public settings
        console.log('Getting public embed form');
        db.child('publicEmbedForms').child(data.pef).once('value', function(snap) {
            if (snap.exists()) {
                var form = snap.val();
                console.log('Form exists: OK = ' + form.ok + ', FID = ' + form.fid);
                console.log('Getting private details');
                // Get form details from the private form settings - "OK" = Org Key, "FID" = "Form ID"
                db.child('marketing/' + form.ok + '/forms/' + form.fid).once('value', function(formSnap) {
                    if (formSnap.exists()) {
                        var formDetails = formSnap.val();
                        console.log('Details exist: leadsAssignedTo = ' + formDetails.leadsAssignedTo.userRef + ', assignedToName = ' + formDetails.leadsAssignedTo.name + ' formOrg = ' + formDetails.formOrg + ', formOrgType = ' + formDetails.formOrgType);
                        // Get current upline hierarchy for the user this form is being assigned to
                        //   Send assigned user to the server, along with current user orgKey and orgType
                        //   Server checks /users/[uid]/myUpline for selected user, and returns an array of all orgKey/orgType/[upline path] where the current user's Org is part of that Org's upline
                        var dataToSend = {
                            selectedUid: formDetails.leadsAssignedTo.userRef,
                            requestorOrgKey: formDetails.formOrg,
                            requestorOrgType: formDetails.formOrgType
                        };
                        console.log('Getting upline org chain');
                        doGetValidUplineOrgChains(dataToSend).then(function(response) {
                            if (response.status === 'success') {
                                console.log('Upline org chain found');
                                // TODO: If the selected user has more than one upline returned, prompt to select which upline org the policy should be associated with
                                //   For now, just submit to the first returned Org...
                                var policy = req.body.policyData;
                                var policyUpline = response.data[0];
                                policyUpline.forEach(function(org) {
                                    policy[org.orgKey] = org.orgType;
                                });

                                var applicationPublicGuid = uuidv4();

                                policy.forms = {
                                    application: {
                                        formName: 'Application Form',
                                        className: 'application',
                                        status: 'Pending 3rd Party',
                                        publicGuid: applicationPublicGuid,
                                        recipients: {
                                            insured: {},
                                            secondaryInsured: {},
                                            owner: {},
                                            agent: {}
                                        },
                                        thirdParty: {
                                            fullName: policy.contacts[0] ? policy.contacts[0].firstName + ' ' + policy.contacts[0].lastName : null,
                                            emailAddress: policy.contacts[0] ? policy.contacts[0].emailAddress : null,
                                            requestedAt: policy.contacts[0] ? firebase.database.ServerValue.TIMESTAMP : null
                                        }
                                    },
                                    medicalPrimary: {
                                        formName: 'Medical Questionnaire (Primary)',
                                        className: 'medicalPrimary',
                                        status: 'Start Now',
                                        publicGuid: uuidv4()
                                    }
                                };

                                policy.actualCreatedBy = 'Marketing Lead Form';
                                policy.actualCreatedById = 'marketing-lead-form';
                                policy.createdBy = formDetails.leadsAssignedTo.name;
                                policy.createdById = formDetails.leadsAssignedTo.userRef;
                                policy.createdByOrgKey = formDetails.formOrg ? formDetails.formOrg : null;
                                policy.createdByOrgType = formDetails.formOrgType ? formDetails.formOrgType : null;
                                policy.upline = policyUpline;

                                // set alertId to the profile alertId for the assigned user
                                policy.alertId = response.alertId ? response.alertId : null;

                                if (!formDetails.trackingData || formDetails.trackingData.length === 0) {
                                    formDetails.trackingData = [];
                                }
                                formDetails.trackingData.push({
                                    key: 'Form Name',
                                    value: formDetails.formName
                                });
                                formDetails.trackingData.push({
                                    key: 'Page URL',
                                    value: data.pageURL ? data.pageURL : 'n/a'
                                });

                                policy.trackingData = formDetails.trackingData;

                                // Get Agent profile info to add to Seller page
                                // TODO: move this to a db trigger when a new public form is submitted
                                // db.child('users/' + formDetails.leadsAssignedTo.userRef).once('value', function(snap) {
                                //     if (snap.exists()) {

                                //     } else {

                                //     }
                                // });

                                console.log('Policy Data:');
                                console.log(policy);
                                db.child('policies').push(policy).then(function(ref) {
                                    var policyKey = ref.key;
                                    console.log('Policy created. Key = ' + policyKey);
                                    console.log('Setting org access');
                                    async.forEachOfSeries(policyUpline, function(org, key, callback) {
                                        // Associate this Policy with all Upline Orgs
                                        db.child('orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + policyKey).set({
                                            'hasAccess': true
                                        }).then(function() {
                                            callback();
                                        });
                                    }, function(err) {
                                        console.log('Access set for all orgs');
                                        if (err) {
                                            console.log(err);
                                            res.status(200).send({
                                                status: 'error',
                                                message: err.message
                                            });
                                        } else {
                                            console.log('Saving policy to user node');
                                            // Save new policy reference to user's profile
                                            db.child('users/' + formDetails.leadsAssignedTo.userRef + '/myPolicies/' + policyKey).set({
                                                policyKey: policyKey,
                                                insured: {
                                                    firstName: policy.insured && policy.insured.firstName ? policy.insured.firstName : null,
                                                    lastName: policy.insured && policy.insured.lastName ? policy.insured.lastName : null
                                                }
                                            }).then(function() {
                                                console.log('User policy set. Creating 3rd Party Request form');

                                                // SET UP 3RD PARTY REQUEST FORM AND EMAIL CONTACT

                                                delete policy.adminFiles;
                                                delete policy.caseFiles;
                                                delete policy.createdBy;
                                                delete policy.createdById;
                                                delete policy.createdByOrgKey;
                                                delete policy.createdByOrgType;
                                                delete policy.actualCreatedBy;
                                                delete policy.actualCreatedById;
                                                delete policy.actualCreatedByOrgKey;
                                                delete policy.actualCreatedByOrgType;
                                                delete policy.policyIsActive;
                                                delete policy.private;
                                                delete policy.publicGuid;
                                                delete policy.submittedTo;
                                                delete policy.updateLog;
                                                delete policy.lastUpdateAt;
                                                delete policy.lastUpdatedBy;
                                                delete policy.lastUpdatedById;
                                                delete policy.latestStatus;
                                                delete policy.latestStatusAt;

                                                db.child('publicForms').child(applicationPublicGuid).update(policy).then(function(ref) {
                                                    console.log('Form created. Sending email');
                                                    // Create an email message to the recipient with link to [url]/view/application?pubId=[publicGuid]
                                                    if (policy.contacts[0] && policy.contacts[0].emailAddress) {
                                                        var recipients = [];
                                                        recipients.push({
                                                            email: policy.contacts[0].emailAddress,
                                                            template: 'embedded-form-submission',
                                                            subData: {
                                                                LSHID: policy.LSHID,
                                                                faceAmount: policy.faceAmount ? policy.faceAmount : 'Not Provided',
                                                                insured: policy.insured && policy.insured.firstName ? policy.insured.firstName : 'Not Provided',
                                                                name: policy.contacts[0].firstName ? policy.contacts[0].firstName : '',
                                                                formLink: data.linkRoot + '/view/application?pubId=' + applicationPublicGuid,
                                                                registerLink: data.linkRoot + '/register?spk=' + policyKey
                                                            }
                                                        });

                                                        var formData = {
                                                            env: env,
                                                            recipients: recipients
                                                        };
                                                        // Step 4: Send email with digestData as payload
                                                        var emailURL = 'https://api.lshub.net/email/send';
                                                        request.post({
                                                            url: emailURL,
                                                            body: formData,
                                                            json: true
                                                        }, function(error, response, body) {
                                                            if (error) {
                                                                res.status(200).send({
                                                                    status: 'error',
                                                                    message: error.message
                                                                });
                                                            } else {
                                                                console.log('Emails sent successfully');
                                                                res.status(200).send({
                                                                    status: 'success',
                                                                    message: 'New Policy created successfully. Key: ' + policyKey,
                                                                    data: {
                                                                        key: policyKey,
                                                                        formLink: data.linkRoot + '/view/application?pubId=' + applicationPublicGuid
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        res.status(200).send({
                                                            status: 'success',
                                                            message: 'New Policy created successfully. Key: ' + policyKey,
                                                            data: {
                                                                key: policyKey,
                                                                formLink: data.linkRoot + '/view/application?pubId=' + applicationPublicGuid
                                                            }
                                                        });
                                                    }
                                                }).catch(function(err) {
                                                    console.log(err);
                                                    res.status(200).send({
                                                        status: 'error',
                                                        message: err.message
                                                    });
                                                });
                                            });
                                        }
                                    });
                                });
                            } else {
                                console.log('Form details not found');
                                res.status(200).send({
                                    status: 'error',
                                    message: response.message
                                });
                            }
                        });
                    } else {
                        console.log('Form details not found');
                        res.status(200).send({
                            status: 'error',
                            message: 'Form details not found'
                        });
                    }
                });
            } else {
                console.log('Form ID not valid');
                res.status(200).send({
                    status: 'error',
                    message: 'Form ID not valid'
                });
            }
        });
    });
});

/** 
 * Monitor Policy objects for changes and create audit trail notifications
 */
exports.auditTrailPolicies = functions.database.ref('/policies/{pid}').onWrite(function(change, context) {
    var update = {};
    if (context.authType === 'ADMIN') {
        update.updatedBy = 'Server';
    } else if (context.authType === 'USER') {
        update.updatedBy = context.auth.uid;
    }
    update.updatedAt = firebase.database.ServerValue.TIMESTAMP;
    update.path = 'policies/' + context.params.pid;
    if (!change.before.exists()) {
        update.changes = [{
            kind: 'N',
            path: [''],
            lhs: null,
            rhs: 'New Policy Created'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    }
    if (!change.after.exists()) {
        update.changes = [{
            kind: 'D',
            path: [''],
            lhs: null,
            rhs: 'Policy Deleted'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    }
    const beforeData = change.before.val();
    const afterData = change.after.val();
    update.changes = diff(beforeData, afterData);
    return db.child('auditTrail').child(context.params.pid).push(update);
});

/** 
 * Monitor Private Policy objects for changes and create audit trail notifications
 */
exports.auditTrailPrivatePolicies = functions.database.ref('/privatePolicies/{pid}').onWrite(function(change, context) {
    var update = {};
    if (context.authType === 'ADMIN') {
        update.updatedBy = 'Server';
    } else if (context.authType === 'USER') {
        update.updatedBy = context.auth.uid;
    }
    update.updatedAt = firebase.database.ServerValue.TIMESTAMP;
    update.path = 'privatePolicies/' + context.params.pid;
    if (!change.before.exists()) {
        update.changes = [{
            kind: 'N',
            path: [''],
            lhs: null,
            rhs: 'New Policy Created'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    }
    if (!change.after.exists()) {
        update.changes = [{
            kind: 'D',
            path: [''],
            lhs: null,
            rhs: 'Policy Deleted'
        }];
        return db.child('auditTrail').child(context.params.pid).push(update);
    }
    const beforeData = change.before.val();
    const afterData = change.after.val();
    update.changes = diff(beforeData, afterData);
    return db.child('auditTrail').child(context.params.pid).push(update);
});

/**
 * For each new Notification, send email if marked isUrgent or if target is a user and their profile is set to receive emails for all notifications
 */
exports.notificationEmails = functions.database.ref('/notifications/{nid}').onCreate(function(snapshot, context) {
    const notification = snapshot.val();
    var sendEmail = false;
    var emailAddress = '';
    if (notification.isUnread) {
        if (notification.targetType === 'user') {
            // Check if the user has allNotifications enabled
            return db.child('users/' + notification.targetId + '/notificationEmails').once('value', function(snap) {
                if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                    sendEmail = true;
                    emailAddress = snap.val().emailAddress;
                    return continueNotificationEmail(sendEmail, emailAddress, notification);
                } else {
                    return false;
                }
            });
        } else if (notification.targetType !== 'user' && notification.targetType !== 'org') {
            // Check if the org has allNotifications enabled
            if (notification.iBrokerBOFlag && notification.iBrokerBOFlag === 'bo') {
                return db.child('orgs/' + notification.targetType + '/' + notification.targetId + '/backOfficeNotificationEmails').once('value', function(snap) {
                    if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                        sendEmail = true;
                        emailAddress = snap.val().emailAddress;
                        return continueNotificationEmail(sendEmail, emailAddress, notification);
                    } else {
                        return false;
                    }
                });
            } else {
                return db.child('orgs/' + notification.targetType + '/' + notification.targetId + '/notificationEmails').once('value', function(snap) {
                    if (snap.exists() && (snap.val().allEnabled || notification.isUrgent)) {
                        sendEmail = true;
                        emailAddress = snap.val().emailAddress;
                        return continueNotificationEmail(sendEmail, emailAddress, notification);
                    } else {
                        return false;
                    }
                });
            }
        } else {
            return continueNotificationEmail(sendEmail, emailAddress, notification);
        }
    } else {
        return false;
    }
});

function continueNotificationEmail(sendEmail, emailAddress, notification) {
    return new Promise(function(resolve, reject) {
        if (sendEmail) {
            var subData = notification.subData;

            // Determine auto-urgent action types
            if (notification.action === 'New Public Note' || notification.action === 'New Internal Note') {
                notification.isUrgent = true;
            }
            // If notification contains "bid" (buyer id) parameter, the policy link may need to be adjusted to include the original policy key and the correct target platform
            if (notification.bid) {
                // notification.subData.link
                if (subData.platform === 'iSubmit') {
                    // This notification was sent as an iBroker on iSubmit was viewing a Buyer's View, so it needs to target the iManager platform for the Buyer
                    subData = {
                        platform: 'iManager',
                        headerImg: 'imanager',
                        linkRoot: subData.linkRoot.replace('isubmit', 'imanager'),
                        link: subData.link.replace('isubmit', 'imanager')
                    };
                }
                if (notification.targetType === 'ibroker') {
                    // This notification was sent from someone on the iManager platform, but needs to target an iBroker on iSubmit
                    subData = {
                        platform: 'iSubmit',
                        headerImg: 'isubmit',
                        linkRoot: subData.linkRoot.replace('imanager', 'isubmit'),
                        link: subData.link.replace('imanager', 'isubmit')
                    };
                }
            }

            subData.action = notification.action;
            subData.note = notification.note;

            var recipients = [];

            recipients.push({
                email: emailAddress.trim(),
                template: 'new-event-notification',
                subData: subData
            });
            var formData = {
                env: env,
                recipients: recipients
            };
            // Step 4: Send email with digestData as payload
            var emailURL = 'https://api.lshub.net/email/send';
            request.post({
                url: emailURL,
                body: formData,
                json: true
            }, function(error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    console.log('Emails sent successfully');
                    resolve('done');
                }
            });
        } else {
            reject();
        }
    });
}

/** 
 * Get digest data for all users scheduled to receive digests today
 */
exports.checkDailyDigest = functions.pubsub.topic('check-daily-digest').onPublish(function(message) {
    return new Promise(function(resolve, reject) {
        var recipients = [];
        // Step 1: Get any users with digests set up in their Profile
        db.child('users').orderByChild('hasDigestEmail').equalTo(true).once('value', function(snap) {
            if (snap.exists()) {
                let digestCount = 0;
                let userObjArr = snap.val();
                let usersWithDigest = Object.keys(userObjArr).map(function(x) { return userObjArr[x]; });
                let todayDate = new Date();
                console.log(dateFormat(todayDate.toString(), 'yyyymmdd'));
                async.forEachOfSeries(usersWithDigest, function(user, key, callback) {
                    console.log('--- ' + dateFormat(user.digestEmail.nextRunDate, 'yyyymmdd') + ' --- ' + user.uid);
                    // Step 2: If the digest is active (subscribed = true) and the nextRunDate is today, process the digest
                    if (user.digestEmail && user.digestEmail.subscribed === true && dateFormat(user.digestEmail.nextRunDate, 'yyyymmdd') === dateFormat(todayDate.toString(), 'yyyymmdd')) {
                        var data = {
                            requestorUid: user.uid,
                            accessDomain: user.accessDomain,
                            requestorOrgType: user.myOrg ? user.myOrg.orgType : null,
                            requestorOrgKey: user.myOrg ? user.myOrg.orgKey : null,
                            lastRunDate: user.digestEmail.lastRunDate ? user.digestEmail.lastRunDate : 0
                        };
                        processDigest(data).then(function(digestData) {
                            console.log('Finished processing digest for ' + user.fullName);
                            digestCount = digestCount + 1;
                            user.digestEmail.emailAddress.split(',').forEach(function(recipientEmail) {
                                recipients.push({
                                    email: recipientEmail.trim(),
                                    template: 'digest-template',
                                    subData: {
                                        startDate: dateFormat(user.digestEmail.lastRunDate, 'mm/dd/yyyy'),
                                        endDate: dateFormat(todayDate.toString(), 'mm/dd/yyyy'),
                                        policies: digestData
                                    }
                                });
                            });

                            // Step 3: Update user digest nextRunDate to (today + 7 days)
                            var newRunDate = new Date(user.digestEmail.nextRunDate);
                            newRunDate.setDate(newRunDate.getDate() + 7);
                            db.child('users/' + user.uid + '/digestEmail').update({
                                nextRunDate: newRunDate,
                                lastRunDate: firebase.database.ServerValue.TIMESTAMP
                            }).then(function() {
                                callback();
                            });
                        });
                    } else {
                        callback();
                    }
                }, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(digestCount.toString() + ' digest emails ready to send.');
                        if (digestCount > 0) {
                            var formData = {
                                env: env,
                                recipients: recipients
                            };
                            // Step 4: Send email with digestData as payload
                            request.post({
                                url: 'https://api.lshub.net/email/send',
                                body: formData,
                                json: true
                            }, function(error, response, body) {
                                if (error) {
                                    reject(error);
                                } else {
                                    console.log('Emails sent successfully');
                                    resolve('done');
                                }
                            });
                        } else {
                            resolve('done');
                        }
                    }
                });
            } else {
                resolve();
            }
        });
    });
});

function dateFormat(dateToTest, format) {
    var testDate = new Date(dateToTest);
    var y = testDate.getFullYear();
    var m = testDate.getMonth() + 1;
    var d = testDate.getDate();
    if (format === 'yyyymmdd') {
        return '' + y + '-' + (m < 10 ? '0' : '') + m + '-' + (d < 10 ? '0' : '') + d;
    } else if (format === 'mm/dd/yyyy') {
        return '' + (m < 10 ? '0' : '') + m + '/' + (d < 10 ? '0' : '') + d + '/' + y;
    }
}

function processDigest(data) {
    return new Promise(function(resolve, reject) {
        let orgPolicies = [];

        db.child('orgs/' + data.requestorOrgType + '/' + data.requestorOrgKey + '/policies').once('value', function(snap) {
            let digestData = [];
            if (snap.exists()) {
                let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
                async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
                    orgPolicies.push(policyKey);
                    db.child('policies/' + policyKey).once('value', function(snap) {
                        if (snap.exists()) {
                            let policy = snap.val();
                            if (policy.policyIsActive) {
                                let newDigest = {
                                    LSHID: policy.LSHID,
                                    agentName: policy.createdBy,
                                    premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
                                    faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
                                    insuredName: policy.insured.fullName,
                                    insuredAge: policy.insured.age ? policy.insured.age : '',
                                    latestUpdate: policy.latestStatus ? policy.latestStatus : '',
                                    latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
                                    displayStatus: policy.status.display.text,
                                    link: data.accessDomain.replace(/_/g, '.') + '/policy?pid=' + snap.ref.key
                                };
                                newDigest.logSummary = [];
                                let miscUpdates = 0;
                                let fileUploads = 0;
                                let messages = 0;
                                let formActions = 0;
                                let buyerActions = 0;
                                if (policy.updateLog) {
                                    policy.updateLog.forEach(function(log) {
                                        if (log.updatedAt > data.lastRunDate) {
                                            if (log.type) {
                                                if (log.type === 'Policy Assigned') {
                                                    miscUpdates = miscUpdates + 1;
                                                } else if (log.type === 'File Uploaded') {
                                                    fileUploads = fileUploads + 1;
                                                } else if (log.type.indexOf('Application') > -1) {
                                                    formActions = formActions + 1;
                                                }
                                            } else {
                                                miscUpdates = miscUpdates + 1;
                                            }
                                        }
                                    });
                                }
                                if (policy.publicNotes) {
                                    policy.publicNotes.forEach(function(note) {
                                        if (note.createdAt > data.lastRunDate) {
                                            messages = messages + 1;
                                        }
                                    });
                                }
                                // TODO: Map Buyer Actions
                                newDigest.logSummary = {
                                    miscUpdates: miscUpdates,
                                    fileUploads: fileUploads,
                                    messages: messages,
                                    formActions: formActions,
                                    buyerActions: buyerActions
                                };
                                digestData.push(newDigest);
                            }
                            callback();
                        } else {
                            callback();
                        }
                    });
                }, function(err) {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        processUserDigest(data, orgPolicies, digestData).then(function(response) {
                            resolve(response);
                        });
                    }
                });
            } else {
                processUserDigest(data, orgPolicies, digestData).then(function(response) {
                    resolve(response);
                });
            }
        });
    });
}

function processUserDigest(data, orgPolicies, digestData) {
    return new Promise(function(resolve, reject) {
        db.child('policies').orderByChild('createdById').equalTo(data.requestorUid).once('value', function(snap) {
            if (snap.exists()) {
                let policyData = snap.val();
                Object.keys(policyData).forEach(function(policyKey) {
                    if (orgPolicies.indexOf(policyKey) < 0) {
                        let policy = policyData[policyKey];
                        if (policy.policyIsActive) {
                            let newDigest = {
                                LSHID: policy.LSHID,
                                agentName: policy.createdBy,
                                premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
                                faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
                                insuredName: policy.insured.fullName,
                                insuredAge: policy.insured.age ? policy.insured.age.toString() : '',
                                latestUpdate: policy.latestStatus ? policy.latestStatus : '',
                                latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
                                displayStatus: policy.status.display.text,
                                link: ''
                            };
                            newDigest.logSummary = [];
                            let miscUpdates = 0;
                            let fileUploads = 0;
                            let messages = 0;
                            let formActions = 0;
                            let buyerActions = 0;
                            if (policy.updateLog) {
                                policy.updateLog.forEach(function(log) {
                                    if (log.updatedAt > data.lastRunDate) {
                                        if (log.type) {
                                            if (log.type === 'Policy Assigned') {
                                                miscUpdates = miscUpdates + 1;
                                            } else if (log.type === 'File Uploaded') {
                                                fileUploads = fileUploads + 1;
                                            } else if (log.type.indexOf('Application') > -1) {
                                                formActions = formActions + 1;
                                            }
                                        } else {
                                            miscUpdates = miscUpdates + 1;
                                        }
                                    }
                                });
                            }
                            if (policy.publicNotes) {
                                policy.publicNotes.forEach(function(note) {
                                    if (note.createdAt > data.lastRunDate) {
                                        messages = messages + 1;
                                    }
                                });
                            }
                            // TODO: Map Buyer Actions
                            newDigest.logSummary = {
                                miscUpdates: miscUpdates,
                                fileUploads: fileUploads,
                                messages: messages,
                                formActions: formActions,
                                buyerActions: buyerActions
                            };
                            digestData.push(newDigest);
                        }
                    }
                });
                resolve(digestData);
            } else {
                resolve(digestData);
            }
        });
    });
}

/** 
 * Get an array of all downline seat users
 * Requires no input parameters
 * Uses the passed auth token to do lookup for currently logged in user
 */
exports.getFullDownline = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
            console.log('Unauthorized request');
            res.status(403).send('Unauthorized');
        }
        let idToken = req.headers.authorization.split('Bearer ')[1];
        firebase.auth().verifyIdToken(idToken).then((decodedIdToken) => {
            // Get the requestor's orgKey from their Profile
            db.child('users/' + decodedIdToken.uid + '/myOrg/orgKey').once('value', function(snap) {
                if (snap.exists()) {
                    getMemberOrg(snap.val()).then(function(fullList) {
                        if (fullList) {
                            res.status(200).send({
                                status: 'success',
                                message: 'Retrieved full downline',
                                data: fullList
                            });
                        } else {
                            res.status(200).send({
                                status: 'error',
                                message: 'No downline list retrieved'
                            });
                        }
                    });
                } else {
                    res.status(200).send({
                        status: 'error',
                        message: 'User has no Organization'
                    });
                }
            });
        }).catch((error) => {
            console.error('Error while verifying Firebase ID token:', error);
            res.status(403).send('Unauthorized');
        });
    });
});

function getMemberOrg(orgKey) {
    var newDownlineList = [];

    function getNextMemberOrg(orgKey) {
        function getNextOrg(nextOrgKey) {
            return new Promise(function(resolve, reject) {
                firebase.database().ref().child('orgs/seats/' + nextOrgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        var memberArr = [];
                        snap.forEach(function(member) {
                            memberArr.push(member.val());
                        });
                        resolve(memberArr);
                    } else {
                        console.log('No org seats found');
                        resolve('no-org');
                    }
                }).catch(function(err) {
                    console.log(err);
                    resolve(err);
                });
            });
        }

        function processMembers(membersArr) {
            return new Promise(function(resolve, reject) {
                if (membersArr.length > 0) {
                    async.forEachOfSeries(membersArr, function(member, key, callback) {
                        if (!member.isOwner) {
                            newDownlineList.push(member);
                            if (member.hasOrg) {
                                getNextMemberOrg(member.orgKey).then(function(list) {
                                    callback();
                                });
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            resolve(newDownlineList);
                        }
                    });
                } else {
                    resolve(newDownlineList);
                }
            });
        }

        return getNextOrg(orgKey).then(processMembers).then(function(list) {
            return list;
        });
    }
    return getNextMemberOrg(orgKey);
}

/**
 * Get an array of valid upline org chains for a selected user, from a requeting user's perspective
 */
exports.getValidUplineOrgChains = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var requestBody = req.body;

        doGetValidUplineOrgChains(requestBody).then(function(response) {
            if (response.status === 'success') {
                res.status('200').send({
                    status: 'success',
                    message: response.message,
                    data: response.data,
                    ownOrg: response.ownOrg
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: response.message,
                    data: response.data
                });
            }
        });
    });
});

function doGetValidUplineOrgChains(requestBody) {
    return new Promise(function(resolve,reject) {
        db.child('users').child(requestBody.selectedUid).once('value', function(snap) {
            if (snap.exists()) {
                var validUplineOrgChains = [];
                var alertId = snap.val().alertId;
                var myOrgEntry = {};
                if (snap.val().myOrg) {
                    myOrgEntry = {
                        orgKey: snap.val().myOrg.orgKey,
                        orgType: snap.val().myOrg.orgType
                    }
                }
                if (snap.val().myUpline) {
                    var uplines = snap.val().myUpline;

                    if (uplines.length === 1) {
                        getUplineOrgChain(uplines[0].orgKey, myOrgEntry).then(function(uplineOrgArr) {
                            var keepChain = false;
                            uplineOrgArr.forEach(function(org) {
                                if (org.orgKey === requestBody.requestorOrgKey && org.orgType === requestBody.requestorOrgType) {
                                    keepChain = true;
                                }
                            });
                            if (keepChain) {
                                validUplineOrgChains.push(uplineOrgArr);
                            }
                            resolve({
                                status: 'success',
                                message: 'Valid upline chain(s) found',
                                data: validUplineOrgChains,
                                ownOrg: myOrgEntry,
                                alertId: alertId
                            });
                        });
                    } else {
                        async.forEachOfSeries(uplines, function(upline, key, callback) {
                            getUplineOrgChain(upline.orgKey, myOrgEntry).then(function(uplineOrgArr) {
                                var keepChain = false;
                                uplineOrgArr.forEach(function(org) {
                                    if (org.orgKey === requestBody.requestorOrgKey && org.orgType === requestBody.requestorOrgType) {
                                        keepChain = true;
                                    }
                                });
                                if (keepChain) {
                                    validUplineOrgChains.push(uplineOrgArr);
                                }
                                callback();
                            });
                        }, function(err) {
                            if (err) {
                                resolve({
                                    status: 'error',
                                    message: err.message,
                                    data: err
                                });
                            } else {
                                resolve({
                                    status: 'success',
                                    message: 'Valid upline chain(s) found',
                                    data: validUplineOrgChains,
                                    ownOrg: myOrgEntry,
                                    alertId: alertId
                                });
                            }
                        });
                    }
                } else {
                    resolve({
                        status: 'success',
                        message: 'No upline found. Not a problem',
                        data: [],
                        ownOrg: myOrgEntry,
                        alertId: alertId
                    });
                }
            } else {
                resolve({
                    status: 'error',
                    message: 'User does not exist',
                    data: {}
                });
            }
        }).catch(function(err) {
            resolve({
                status: 'error',
                message: err.message,
                data: err
            });
        });
    });
}

function getUplineOrgChain(currentOrgKey, myOrgEntry) {
    uplineOrgArr = [];

    if (myOrgEntry.orgKey) {
        uplineOrgArr.push(myOrgEntry);
    }

    function getNextUplineOrg(orgKey) {
        function getNextOrg(nextOrgKey) {
            return new Promise(function(resolve, reject) {
                db.child('orgs/public/' + nextOrgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        var org = snap.val();
                        org.orgKey = nextOrgKey;
                        resolve(org);
                    } else {
                        console.log('No org found');
                        resolve('no-org');
                    }
                }).catch(function(err) {
                    console.log(err);
                    resolve(err);
                });
            });
        }

        function buildOrgChain(org) {
            if (org.uplineOrgKey) {
                // This is an upline chain org. Add it and continue looking up
                uplineOrgArr.push({
                    orgKey: org.orgKey,
                    orgType: org.orgType
                });
                return getNextUplineOrg(org.uplineOrgKey);
            } else if (org === 'no-org') {
                console.log('The user has no upline?');
            } else if (!org.orgKey) {
                console.log('An error occurred');
                console.log(org); // org = err at this point
            } else {
                uplineOrgArr.push({
                    orgKey: org.orgKey,
                    orgType: org.orgType
                });
            }
            return uplineOrgArr;
        }

        return getNextOrg(orgKey).then(buildOrgChain);

    }
    return getNextUplineOrg(currentOrgKey);
}

/**
 * Check if the provided user has been made an Admin (promoted) or removed as Admin (demoted) from any Orgs
 */
exports.checkForAdminRights = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var orgType = req.body.orgType;

        firebase.database().ref().child('orgs/' + orgType).orderByChild(uid).startAt('admin').once('value', function(data) {
            if (data.exists()) {
                var dataToSend = {};
                data.forEach(function(org) {
                    dataToSend[org.key] = org.val();
                    dataToSend[org.key].key = org.key;
                });
                res.status('200').send({
                    status: 'success',
                    data: dataToSend
                });
            } else {
                res.status('200').send({
                    status: 'empty',
                    data: 'No new Admin Orgs'
                });
            }
        });
    });
});

/**
 * Check if the email address being used to register (on iManager only) is already associated with a pending invitation
 */
exports.checkForPendingRegistration = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var email = req.body.email;
        firebase.database().ref().child('orgs/buyer').orderByChild('email').equalTo(email).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(org) {
                    res.status('200').send({
                        status: 'error',
                        data: org.val().orgKey
                    });
                });
            } else {
                firebase.database().ref().child('orgs/broker').orderByChild('email').equalTo(email).once('value', function(snap) {
                    if (snap.exists()) {
                        snap.forEach(function(org) {
                            res.status('200').send({
                                status: 'error',
                                data: org.val().orgKey
                            });
                        });
                    } else {
                        res.status('200').send({
                            status: 'success'
                        });
                    }
                });
            }
        });
    });
});

/**
 * Set policy permissions in iSumbmit to upline Org Hierarchy
 */
exports.setPolicyPermissions = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var uid = req.body.uid;
        var org = req.body.org;
        var policyKey = req.body.policyKey;

        // Verify that UID has access to Policy
        firebase.database().ref().child('users/' + uid).once('value', function(snap) {
            if (!snap.exists()) {
                res.status('200').send({
                    status: 'error',
                    message: 'User does not exist'
                });
            } else {
                verifyPolicyPermission(uid, policyKey, snap.val().myOrg).then(function(response) {
                    if (response.status === 'granted') {
                        firebase.database().ref().child('orgs/' + org.type + '/' + org.key + '/policies/' + policyKey).set({
                            'hasAccess': true
                        }).then(function() {
                            res.status('200').send({
                                status: 'success',
                                message: 'Policy associated with upline Org'
                            });
                        }).catch(function(err) {
                            res.status('200').send({
                                status: 'error',
                                message: err
                            });
                        });
                    } else {
                        res.status('200').send({
                            status: 'error',
                            message: 'User does not have permission to this Policy'
                        });
                    }
                });
            }
        });
    });
});

function verifyPolicyPermission(uid, policyKey, myOrg) {
    var promise = new Promise(function(resolve, reject) {
        if (myOrg) {
            firebase.database().ref().child('policies/' + policyKey + '/' + myOrg.orgKey).once('value', function(snap) {
                if (snap.exists()) {
                    resolve({
                        status: 'granted'
                    });
                } else {
                    resolve({
                        status: 'denied'
                    });
                }
            });
        } else {
            firebase.database().ref().child('policies/' + policyKey + '/createdById').once('value', function(snap) {
                if (snap.exists() && snap.val() === uid) {
                    resolve({
                        status: 'granted'
                    });
                } else {
                    resolve({
                        status: 'denied'
                    });
                }
            });
        }
    });
    return promise;
}

/**
 * Listen for Webhooks from PandaDoc
 */
exports.pandaDocWebhooks = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var message = req.body[0];

        if (message.event === 'recipient_completed') {
            console.log({
                status: 'signed',
                recipients: message.data.recipients,
                action_date: message.data.action_date,
                action_by: message.data.action_by
            });

            // determine role of the recipient that triggered this webhook
            var actionById = message.data.action_by.id;
            var actionByRole = '';
            message.data.recipients.forEach(function(recipient) {
                if (recipient.id === actionById) {
                    actionByRole = recipient.role;
                }
            });

            if (actionByRole === '') {
                // User may be logged into PandaDoc with an account using the same email as the recipient...ID will be different. Lookup by email
                var actionByEmail = message.data.action_by.email;
                message.data.recipients.forEach(function(recipient) {
                    if (recipient.email === actionByEmail) {
                        actionByRole = recipient.role;
                    }
                });
            }

            // Convert actionByRole to Firebase role node value
            //   Primary Insured = insured
            //   Secondary Insured = secondaryInsured
            //   Agent = agent
            //   Policy Owner = owner
            if (actionByRole === 'Primary Insured') {
                actionByRole = 'insured';
            } else if (actionByRole === 'Secondary Insured') {
                actionByRole = 'secondaryInsured';
            } else if (actionByRole === 'Agent') {
                actionByRole = 'agent';
            } else if (actionByRole === 'Policy Owner') {
                actionByRole = 'owner';
            } else if (actionByRole === 'Recipient') {
                actionByRole = 'recipient';
            }

            db.child('policies/' + message.data.metadata.policyKey + '/forms/' + message.data.metadata.className + '/recipients/' + actionByRole + '/signed').set(firebase.database.ServerValue.TIMESTAMP);
        } else if (message.event === 'document_state_changed') {
            if (message.data && message.data.status === 'document.draft') {
                // NEW DOCUMENT WAS JUST CREATED. SEND IT OUT AND UPDATE FIREBASE!
                var noteToRecipients = null;
                if (message.data.metadata.noteToRecipients) {
                    noteToRecipients = message.data.metadata.noteToRecipients;
                }
                sendDocumentForSignature(message.data.id, noteToRecipients).then(function(response) {
                    var formData = {
                        createdDtTm: firebase.database.ServerValue.TIMESTAMP
                    };
                    if (response.status === 'retry') {
                        sendDocumentForSignature(message.data.id, noteToRecipients).then(function(response) {
                            if (response.status === 'retry') {
                                response.message = 'Access Token Not Refreshed After 2 Attempts';
                                var trackingKey = message.data.metadata.className + 'SentForSignatureError';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, null, null, null);
                            } else {
                                // SEND SUCCESSFUL...
                                var trackingKey = message.data.metadata.className + 'SentForSignature';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, message.data.metadata.className, formData, null);
                            }
                        });
                    } else {
                        // SEND SUCCESSFUL...
                        var trackingKey = message.data.metadata.className + 'SentForSignature';
                        pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, message.data.metadata.className, formData, null);
                    }
                });
            } else if (message.data && message.data.status === 'document.completed') {
                // EVERYONE HAS SIGNED THIS DOCUMENT. UPDATE FIREBASE, DOWNLOAD AND STAMP IT!
                console.log('Document has been completed by all parties');
                console.log('Policy Number: ' + message.data.metadata.policyNumber);
                console.log('Policy Key: ' + message.data.metadata.policyKey);
                var display = {
                    status: 'Complete',
                    step: message.data.metadata.formName + ' Signature',
                    text: 'Pending Broker Approval'
                };

                var trackingKey = message.data.metadata.className + 'Signed';
                var formData = {
                    completedDtTm: firebase.database.ServerValue.TIMESTAMP,
                    status: 'Complete'
                };
                // One-off - set "brokerActionRequired" flag to true
                db.child('policies').child(message.data.metadata.policyKey).child('brokerActionRequired').set(true);
                // TODO - SEND A NOTIFICATION TO THE BROKER
                //db.child('notifications').child... "Completed Application has been uploaded for this Policy"
                pandadocService.setStatus(message.data.metadata.policyKey, display, trackingKey, message.data.metadata.className, formData, null);

                // Download PDF for Stamping
                pandadocService.downloadDocument(message.data.id, message.data.metadata.policyKey, message.data.metadata.formName).then(function(response) {
                    if (response.status === 'success') {
                        stampService.stampPandaDoc(response.data, message.data.metadata);
                    } else if (response.status === 'retry') {
                        // Access token refreshed. Trying again
                        pandadocService.downloadDocument(message.data.id, message.data.metadata.policyKey, message.data.metadata.formName).then(function(response) {
                            if (response.status === 'success') {
                                stampService.stampPandaDoc(response.data, message.data.metadata);
                            } else if (response.status === 'retry') {
                                // Access token refreshed. Failing out.
                                var trackingKey = message.data.metadata.className + 'AccessTokenError';
                                pandadocService.setStatus(message.data.metadata.policyKey, null, trackingKey, null, null);
                            }
                        });
                    }
                });
            }
        }
        res.status('200').send('received');
    });
});

/**
 * Retrieve All Documents
 */
exports.listDocuments = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents';
                axios.get(url, {
                        headers: {
                            Authorization: 'Bearer ' + access_token
                        }
                    }).then(function(response) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Document list retrieved',
                            data: response.data
                        });
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                res.status('200').send({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        }
                    });
            }
        });
    });
});

/**
 * Retrieve All Templates
 */
exports.listTemplates = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/templates';
                axios.get(url, {
                        headers: {
                            Authorization: 'Bearer ' + access_token
                        }
                    }).then(function(response) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Template list retrieved',
                            data: response.data
                        });
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                res.status('200').send({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        }
                    });
            }
        });
    });
});

/**
 * Create Document from Template
 * Pick templateId based on fields like insured.secondaryInsured and recipients
 */
exports.createDocumentFromTemplate = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var templateId = req.body.templateId;
        var documentName = req.body.documentName;
        var recipients = req.body.recipients;
        var fieldData = req.body.fieldData;
        var metaData = req.body.metaData;

        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents';
                axios.post(url, {
                        "name": documentName,
                        "template_uuid": templateId,
                        "recipients": recipients,
                        "fields": fieldData,
                        "metadata": metaData
                    }, {
                        headers: {
                            Authorization: 'Bearer ' + access_token,
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                        res.status('200').send({
                            status: 'success',
                            message: 'New Document Created',
                            data: response.data
                        });
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                res.status('200').send({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        } else {
                            res.status(err.response.status).send(err.response.data);
                        }
                    });
            }
        });
    });
});

/**
 * Toggle "Interested/Not Interested" status on Buyer View page
 */
exports.toggleBuyerInterested = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var parentKey = req.body.pk;
        var policyGuid = req.body.pg;
        var buyerInterested = req.body.bi;
        var policyType = req.body.pt;

        console.log('parentKey = ' + parentKey);
        console.log('policyGuid = ' + policyGuid);
        console.log('buyerInterested = ' + buyerInterested);
        console.log('policyType = ' + policyType);

        // Reflect the buyer's interest back in either the original root policy, or the immediate supplier's version of the policy
        db.child(policyType).child(parentKey).child('buyers').orderByChild('publicGuid').equalTo(policyGuid).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(buyer) {
                    console.log('Setting interest for buyer index ' + buyer.key);
                    db.child(policyType).child(parentKey).child('buyers').child(buyer.key).update({
                        buyerInterested: buyerInterested
                    }).then(function(result) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Successfully set buyer interest'
                        });
                    });
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'No parent policy found to update'
                });
            }
        });
    });
});

/**
 * Mark a Policy as having been viewed by a buyer
 */
exports.buyerMarkViewed = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var parentKey = req.body.pk;
        var policyGuid = req.body.pg;
        var policyType = req.body.pt;

        // Mark that the buyer has viewed this Policy in either the original root policy, or the immediate supplier's version of the policy
        db.child(policyType).child(parentKey).child('buyers').orderByChild('publicGuid').equalTo(policyGuid).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(buyer) {
                    db.child(policyType).child(parentKey).child('buyers').child(buyer.key).update({
                        hasViewedPolicy: true,
                        viewedPolicyAt: firebase.database.ServerValue.TIMESTAMP
                    }).then(function(result) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Successfully set buyer view status'
                        });
                    });
                });
            } else {
                res.status('200').send({
                    status: 'error',
                    message: 'No parent policy found to update'
                });
            }
        });
    });
});

/**
 * Buyer flag a policy (showing interest)
 */
exports.buyerFlagPolicy = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var publicGuid = req.body.publicGuid;
        var buyerOrgKey = req.body.buyerOrgKey;
        var buyerOrgType = req.body.buyerOrgType;
        var supplierOrgKey = req.body.supplierOrgKey;
        var supplierOrgType = req.body.supplierOrgType;
        var insuredLastName = req.body.insuredLastName;
        var policyNumber = req.body.policyNumber;

        var addFlag = true;
        // Mark policy as interested in privatePolicies node (for buyer to see)
        db.child('privatePolicies').child(publicGuid).once('value', function(snap) {
            if (snap.exists()) {
                var policy = snap.val();
                if (!policy.flaggedBy) {
                    policy.flaggedBy = [];
                }
                if (policy.flaggedBy.indexOf(buyerOrgKey) > -1) {
                    addFlag = false;
                    policy.flaggedBy.splice(buyerOrgKey, 1);
                } else {
                    policy.flaggedBy.push(buyerOrgKey);
                }
                db.child('privatePolicies/' + publicGuid).update(policy).then(function() {
                    db.child('orgs/' + buyerOrgType + '/' + buyerOrgKey + '/flaggedPolicies/' + publicGuid).update({
                        'flagged': addFlag ? 'true' : null,
                        'key': addFlag ? publicGuid : null,
                        'lastName': addFlag ? (insuredLastName ? insuredLastName : 'n/a') : null,
                        'policyNumber': addFlag ? (policyNumber ? policyNumber : 'n/a') : null
                    }).then(function() {
                        // Update the policy status to reflect interest
                        if (addFlag) {
                            //db.child('orgs/' + supplierOrgType + '/' + supplierOrgKey + '/flaggedPolicies)
                        }
                        res.status('200').send({
                            status: 'success',
                            message: addFlag ? 'This policy has been flagged for easy identification in the Marketplace View.' : 'This policy has been unflagged'
                        });
                    });
                });
            } else {
                console.log('Policy ' + req.body.publicGuid + ' does not exist');
                res.status('200').send({
                    status: 'error',
                    message: 'Policy does not exist....weird, right?'
                });
            }
        });
    });
});

/**
 * Gets Zillow home data for given address
 */
// exports.getZillowData = functions.https.onRequest((req, res) => {
//     cors(req, res, () => {
//         var data = req.body;
//         var fullAddress = {
//             address: data.address,
//             city: data.city,
//             state: data.state,
//             zip: data.zipCode
//         }
//         console.log(fullAddress);
//         //var zwsid = 'X1-ZWz1gxnb6gm0p7_am2o3';
//         var zwsid = 'X1-ZWz1hjhd3q0ykr_aui2x';
//         var zillow = new Zillow(zwsid);
//         // Use Node Zillow API here
//         var parameters = {
//             address: fullAddress.address,
//             citystatezip: fullAddress.zip
//         }
//         var reply = {};
//         zillow.get('GetDeepSearchResults', parameters).then(function(results) {
//             try {
//                 console.log(results.message.text);
//                 if (results.message.text.indexOf('Error') > -1) {
//                     reply.zpid = 'n/a';
//                     reply.lastSoldDate = 'n/a';
//                     reply.lastSoldPrice = 'n/a';
//                     reply.zestimate = 'n/a';
//                     reply.zillowLink = 'n/a';
//                     res.status('200').send({
//                         status: 'error',
//                         message: 'Address Not Found',
//                         data: reply
//                     });
//                 } else {
//                     var data = results.response.results.result[0];
//                     reply.zpid = data.zpid[0];
//                     if (data.lastSoldDate) {
//                         reply.lastSoldDate = data.lastSoldDate[0];
//                     } else {
//                         reply.lastSoldDate = 'n/a';
//                     }
//                     if (data.lastSoldPrice) {
//                         reply.lastSoldPrice = data.lastSoldPrice[0]['_'];
//                     } else {
//                         reply.lastSoldPrice = 'n/a';
//                     }
//                     if (data.zestimate[0] && data.zestimate[0].amount) {
//                         reply.zestimate = data.zestimate[0].amount[0]['_'];
//                     } else {
//                         reply.zestimate = 'n/a';
//                     }
//                     if (data.homedetails) {
//                         reply.zillowLink = data.homedetails[0];
//                     } else {
//                         reply.zillowLink = 'n/a';
//                     }
//                     res.status('200').send({
//                         status: 'success',
//                         message: 'Address Found',
//                         data: reply
//                     });
//                 }
//             } catch (error) {
//                 console.log('An error occurred: ' + error);
//                 res.status('200').send({
//                     status: 'error',
//                     message: error.message,
//                     data: error
//                 });
//             }
//         });
//     });
// });

/**
 * View a Document (from email link)
 */
exports.viewDocument = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var documentId = req.query.documentId;
        var recipientEmail = req.query.email;
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/session';
                axios.post(url, {
                        recipient: recipientEmail,
                        lifetime: 900
                    }, {
                        headers: {
                            Authorization: 'Bearer ' + access_token,
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                        res.redirect('https://app.pandadoc.com/s/' + response.data.id);
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                res.status('200').send({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        } else {
                            res.status('200').send({
                                status: 'error',
                                message: 'Unable to generate document link',
                                data: err.response.data
                            });
                        }
                    });
            }
        });
    });
});

/**
 * Send a Document for Signature by ID
 */
function sendDocumentForSignature(documentId, noteToRecipients) {
    var promise = new Promise(function(resolve, reject) {
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/send';
                axios.post(url, {
                        message: (noteToRecipients) ? noteToRecipients : '',
                        silent: false
                    }, {
                        headers: {
                            Authorization: 'Bearer ' + access_token,
                            'Content-Type': 'application/json'
                        }
                    }).then(function(response) {
                        resolve({
                            status: 'success',
                            message: 'Document Sent',
                            data: response.data
                        });
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                resolve({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        }
                    });
            }
        });
    });
    return promise;
}

/**
 * Download a Document by ID
 * Save locally, stamp, then push to Firebase
 */
function downloadDocument(documentId, policyKey, formName) {
    var promise = new Promise(function(resolve, reject) {
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/download';
                axios.get(url, {
                        headers: {
                            Authorization: 'Bearer ' + access_token
                        },
                        responseType: 'arraybuffer'
                    }).then(function(response) {
                        console.log(response.data.length);
                        var file = formName + '-' + documentId + '.pdf';

                        var today = new Date(Date.now());
                        var month = today.getMonth() + 1;
                        var day = today.getDate();
                        var year = today.getFullYear();
                        var dir = '/tmp/' + year + month + day;

                        if (!fs.existsSync(dir)) {
                            fs.mkdirSync(dir);
                        }

                        var writeStream = fs.createWriteStream(dir + '/' + file);

                        writeStream.write(response.data, 'binary');

                        writeStream.on('finish', function() {
                            console.log('Downloaded file from PandaDoc...');
                            console.log('Uploading to Firebase Storage...');
                            // Upload file to Firebase Storage
                            var bucket = storage.bucket();
                            var uuid = UUID();
                            var options = {
                                destination: 'policies/' + policyKey + '/requested/' + file,
                                resumable: false,
                                metadata: {
                                    metadata: {
                                        uploadedAt: moment().unix() * 1000,
                                        uploadedBy: 'PandaDoc',
                                        uploadedById: 'pandadoc',
                                        uploadedByOrgKey: 'n/a',
                                        policyKey: policyKey,
                                        firebaseStorageDownloadTokens: uuid
                                    }
                                }
                            };

                            var downloadURL = storage_root_url + '/o/policies%2F' + policyKey + '%2Frequested%2F' + file + '?alt=media&token=' + uuid;

                            bucket.upload(dir + '/' + file, options, function(err, remoteFile) {
                                if (!err) {
                                    console.log("Uploaded!");
                                    resolve({
                                        status: 'success',
                                        message: 'File is in Google Storage',
                                        data: {
                                            downloadURL: downloadURL,
                                            fileName: file
                                        }
                                    });
                                } else {
                                    console.error("err: " + err);
                                }
                            });
                        });

                        writeStream.end();

                        writeStream.on('error', function(err) {
                            console.log('WriteStream Error: ');
                            console.log(err.message);
                            resolve({
                                status: 'error',
                                message: 'WriteStream Failed: ' + err.message,
                                data: err
                            });
                        });
                    })
                    .catch(function(err) {
                        console.log('Error: ' + err.message);
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                resolve({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        }
                    });
            }
        });
    });
    return promise;
}

/**
 * Update Document Status in Firebase
 */
function setStatus(policyKey, display, trackingKey, className, formData, blockchainData) {
    if (trackingKey) {
        db.child('policies').child(policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
    }
    if (display) {
        db.child('policies').child(policyKey).child('status').child('display').update(display);
    }
    if (formData) {
        db.child('policies').child(policyKey).child('forms').child(className).update(formData);
    }
    if (blockchainData) {
        db.child('policies').child(policyKey).child('forms').child(className).child('blockchain').update(blockchainData);
    }
}

/**
 * Fetch PandaDoc Config Settings from Firebase
 */
function getConfig() {
    var promise = new Promise(function(resolve, reject) {
        db.child('pandadoc').child('config').once('value', function(snap) {
            if (snap.exists()) {
                resolve(snap.val());
            } else {
                console.log('No config found in Firebase');
                reject('error');
            }
        });
    });
    return promise;
}

/**
 * Update a PandaDoc Config Setting in Firebase
 */
function setConfig(key, value) {
    var promise = new Promise(function(resolve, reject) {
        db.child('pandadoc').child('config').child(key).set(value);
        resolve('success');
        if (1 === 3) {
            reject();
        }
    });
    return promise;
}

/**
 * Refresh the PandaDoc Access Token
 */
function refreshToken() {
    var promise = new Promise(function(resolve, reject) {
        const url = 'https://api.pandadoc.com/oauth2/access_token';
        const data = querystring.stringify({
            grant_type: 'refresh_token',
            client_id: client_id,
            client_secret: client_secret,
            refresh_token: refresh_token,
            scope: 'read+write'
        });
        axios.post(url, data).then(function(response) {
                // Update Tokens in Firebase
                pandadocService.setConfig('access_token', response.access_token);
                pandadocService.setConfig('refresh_token', response.refresh_token);
                resolve(response);
            })
            .catch(function(err) {
                console.log('Error: ' + err.response.status);
                reject(err);
            });
    });
    return promise;
}

/**
 * Validate provided document using Stampery
 */
exports.stampFile = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var data = req.body;
        const hook = functions_root_url + '/stamperyWebhooks';

        console.log('Getting hash for file at ' + data.downloadURL);

        getFileHash(data.downloadURL).then(function(h) {
            var stampHash = stampery.hash(h);
            console.log('Stamping hash ' + h);
            stampery.stamp(stampHash, hook).then(function(stamp) {
                console.log('Stamped successfully: ID ' + stamp.id);

                // Associate Stamp with Policy and Form
                db.child('stampery').child(stamp.id).update({
                    orgKey: data.orgKey ? data.orgKey : null,
                    policyKey: data.policyKey ? data.policyKey : null,
                    policyGuid: data.policyGuid ? data.policyGuid : null,
                    fileKey: data.fileKey ? data.fileKey : null,
                    fileType: data.fileType ? data.fileType : null,
                    policyNumber: data.policyNumber ? data.policyNumber : null,
                    formName: data.formName ? data.formName : null,
                    className: data.className ? data.className : null,
                    stampId: stamp.id ? stamp.id : null,
                    stampToken: stamp.token ? stamp.token : null,
                    stampTime: stamp.time ? stamp.time : null
                });

                console.log('Updating metadata for file:  ' + data.fileName);
                var bucket = storage.bucket();
                var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
                bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
                bucket.file(bucketPath).setMetadata({
                    metadata: {
                        stampId: stamp.id,
                        stampToken: stamp.token,
                        stampTime: stamp.time
                    }
                });

                var trackingKey = data.className + 'StampingStarted';
                var blockchainData = {
                    btc: {
                        status: 'pending',
                        statusDtTm: firebase.database.ServerValue.TIMESTAMP
                    },
                    eth: {
                        status: 'pending',
                        statusDtTm: firebase.database.ServerValue.TIMESTAMP
                    }
                };
                if (data.className === 'application' || data.className === 'medicalPrimary' || data.className === 'medicalSecondary') {
                    db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    db.child('policies').child(data.policyKey).child('forms').child(data.className).child('blockchain').update(blockchainData).then(function(response) {
                        res.status('200').send({
                            status: 'success',
                            message: 'Stamping successful',
                            data: {}
                        });
                    });
                } else {
                    blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    if (data.policyGuid) {
                        db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).child('blockchain').update(blockchainData).then(function(response) {
                            res.status('200').send({
                                status: 'success',
                                message: 'Stamping successful',
                                data: {}
                            });
                        });
                    } else {
                        db.child('caseFiles').child(data.policyKey).child(data.fileKey).child('blockchain').update(blockchainData).then(function(response) {
                            res.status('200').send({
                                status: 'success',
                                message: 'Stamping successful',
                                data: {}
                            });
                        });
                    }
                }
            }).catch(function(err) {
                res.status('200').send({
                    status: 'error',
                    message: 'Stamping failed',
                    data: err
                });
            });
        });
    });
});

/**
 * Listen for Webhooks from Stampery
 */
exports.stamperyWebhooks = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        var stampId = req.body.id;
        console.log('Stamp update for id ' + stampId);
        // Validate Stamp ID
        validateStampID(stampId).then(function(blockchainData) {
            // Look up Policy and Form for this Stamp ID
            db.child('stampery').child(stampId).once('value', function(snap) {
                var data = snap.val();
                // Update File Stamp Info
                if (data.className === 'application' || data.className === 'medicalPrimary' || data.className === 'medicalSecondary') {
                    if (blockchainData.btc === 'valid') {
                        var trackingKey = data.className + 'BTCStamped';
                        db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    }
                    if (blockchainData.eth === 'valid') {
                        var trackingKey = data.className + 'ETHStamped';
                        db.child('policies').child(data.policyKey).child('status').child(trackingKey).set(firebase.database.ServerValue.TIMESTAMP);
                    }
                    db.child('policies').child(data.policyKey).child('forms').child(data.className).child('blockchain').update(blockchainData);
                } else {
                    if (blockchainData.btc === 'valid') {
                        var trackingKey = data.className + 'BTCStamped';
                        blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    }
                    if (blockchainData.eth === 'valid') {
                        var trackingKey = data.className + 'ETHStamped';
                        blockchainData[trackingKey] = firebase.database.ServerValue.TIMESTAMP;
                    }
                    // Verify that the file still exists
                    if (data.policyGuid) {
                        db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).once('value', function(snap) {
                            if (snap.exists()) {
                                db.child('caseFiles').child(data.orgKey).child(data.policyGuid).child(data.fileKey).child('blockchain').update(blockchainData);
                            } else {
                                console.log('The file has been removed');
                            }
                        });
                    } else {
                        db.child('caseFiles').child(data.policyKey).child(data.fileKey).once('value', function(snap) {
                            if (snap.exists()) {
                                db.child('caseFiles').child(data.policyKey).child(data.fileKey).child('blockchain').update(blockchainData);
                            } else {
                                console.log('The file has been removed');
                            }
                        });
                    }
                }
            });
        });
    });
});

/**
 * Stamp a file using Stampery
 */
function stampPandaDoc(data, metadata) {
    const hook = functions_root_url + '/stamperyWebhooks';

    getFileHash(data.downloadURL).then(function(h) {
        var stampHash = stampery.hash(h);
        stampery.stamp(stampHash, hook).then(function(stamp) {
            console.log('Stamped successfully:');

            // Associate Stamp with Policy and Form
            db.child('stampery').child(stamp.id).update({
                policyKey: metadata.policyKey,
                policyNumber: metadata.policyNumber,
                formName: metadata.formName,
                className: metadata.className,
                fileType: 'requested',
                stampId: stamp.id,
                stampToken: stamp.token,
                stampTime: stamp.time
            });

            console.log('Updating metadata for file:  ' + data.fileName);
            var bucket = storage.bucket();
            var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
            bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
            bucket.file(bucketPath).setMetadata({
                metadata: {
                    policyNumber: metadata.policyNumber,
                    formName: metadata.formName,
                    className: metadata.className,
                    stampId: stamp.id,
                    stampToken: stamp.token,
                    stampTime: stamp.time
                }
            });

            var trackingKey = metadata.className + 'StampingStarted';
            var formData = {
                downloadURL: data.downloadURL
            };
            var blockchainData = {
                btc: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                },
                eth: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                }
            };
            pandadocService.setStatus(metadata.policyKey, null, trackingKey, metadata.className, formData, blockchainData);
        }).catch(function(err) {
            console.log('ERROR: ' + err.message);
            
            // Bypassing Stampery for now...
            console.log('Updating metadata for file:  ' + data.fileName);
            var bucket = storage.bucket();
            var bucketPath = data.downloadURL.replace(storage_root_url + '/o/', '');
            bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));
            bucket.file(bucketPath).setMetadata({
                metadata: {
                    policyNumber: metadata.policyNumber,
                    formName: metadata.formName,
                    className: metadata.className
                }
            });

            var trackingKey = metadata.className + 'StampingStarted';
            var formData = {
                downloadURL: data.downloadURL
            };
            var blockchainData = {
                btc: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                },
                eth: {
                    status: 'pending',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                }
            };
            pandadocService.setStatus(metadata.policyKey, null, trackingKey, metadata.className, formData, blockchainData);
        });
    });
}

/**
 * Given a Stampery stamp ID, check its validity in BTC and ETH blockchains
 */
function validateStampID(id) {
    var promise = new Promise(function(resolve, reject) {
        stampery.getById(id).then(function(result) {
            const stamp = result[0];
            var stampStatus = {
                btc: {
                    status: 'invalid',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                },
                eth: {
                    status: 'invalid',
                    statusDtTm: firebase.database.ServerValue.TIMESTAMP
                }
            };
            if (stamp && stamp.receipts && stamp.receipts.btc) {
                //console.log('Proving BTC network...');
                if (isNumber(stamp.receipts.btc)) {
                    stampStatus.btc.status = 'pending';
                } else {
                    try {
                        if (stampery.prove(stamp.receipts.btc)) {
                            stampStatus.btc.status = 'valid';
                        } else {
                            stampStatus.btc.status = 'pending';
                        }
                    } catch (error) {
                        //console.log('BTC Prove Error: ' + error.message);
                        stampStatus.btc.status = 'pending';
                    }
                }
            }
            if (stamp && stamp.receipts && stamp.receipts.eth) {
                //console.log('Proving ETH network...');
                if (isNumber(stamp.receipts.eth)) {
                    stampStatus.eth.status = 'pending';
                } else {
                    try {
                        if (stampery.prove(stamp.receipts.eth)) {
                            stampStatus.eth.status = 'valid';
                        } else {
                            stampStatus.eth.status = 'pending';
                        }
                    } catch (error) {
                        //console.log('ETH Prove Error: ' + error.message);
                        stampStatus.eth.status = 'pending';
                    }
                }
            }
            resolve(stampStatus);
        }).catch(function(err) {
            reject(err);
        });
    });
    return promise;
}

/**
 * Given any file path, check its validity in BTC and ETH blockchains
 */
function validateFile(downloadURL, token) {
    var promise = new Promise(function(resolve, reject) {
        getFileHash(downloadURL).then(function(h) {
            stampery.getByHash(h).then(function(res) {
                if (res.length > 0) {
                    var receipt = {};
                    res.some(function(result) {
                        if (result.token === token) {
                            receipt = result;
                        }
                    });
                    if (receipt.id) {
                        stampService.validateStampID(receipt.id).then(function(stampStatus) {
                            resolve(stampStatus);
                        });
                    } else {
                        resolve({
                            btc: 'invalid',
                            eth: 'invalid'
                        });
                    }
                } else {
                    resolve({
                        btc: 'invalid',
                        eth: 'invalid'
                    });
                }
            }).catch(function(err) {
                reject(err);
            });

        });
    });
    return promise;
}

/**
 * Get the SHA256 hash of a file (using ArrayBuffer of file)
 */
function getFileHash(downloadURL) {
    var promise = new Promise(function(resolve, reject) {
        // Get file metadata.md5Hash from Google Storage
        var bucket = storage.bucket();
        var bucketPath = downloadURL.replace(storage_root_url + '/o/', '');
        bucketPath = decodeURIComponent(bucketPath.substr(0, bucketPath.indexOf('?')).replace(/%2F/g, '/'));

        // Get MD5 Hash from Firebase Storage Metadata
        bucket.file(bucketPath).getMetadata().then(function(metaData) {
            resolve(metaData[0].md5Hash);
        }).catch(function(err) {
            console.log('File not found...waiting and trying again');
            setTimeout(function() {
                bucket.file(bucketPath).getMetadata().then(function(metaData) {
                    resolve(metaData[0].md5Hash);
                });
            }, 2500);
        });
    });
    return promise;
}

/**
 * Determine if a given value is a number or not
 */
function isNumber(n) { return !isNaN(parseFloat(n)) && !isNaN(n - 0) }