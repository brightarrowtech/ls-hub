const axios = require('axios');
const fs = require('fs');
const Promise = require('promise');

const pandadocService = {
    downloadDocument: downloadDocument,
    getConfig: getConfig,
    refreshToken: refreshToken
}

// PandaDoc config var globals
var access_token = '';
var refresh_token = '';
var code = '';
var client_id = '';
var client_secret = '';

var documentId = ''; // req.query.docId
var formName = ''; // req.query.fn
pandadocService.downloadDocument(documentId, formName).then(function(response) {
    console.log(response);
});

/**
 * Download a completed form from PandaDoc API
 */
function downloadDocument(documentId, formName) {
    var promise = new Promise(function(resolve, reject) {
        pandadocService.getConfig().then(function(config) {
            if (config !== 'error') {
                access_token = config.access_token;
                refresh_token = config.refresh_token;
                code = config.code;
                client_id = config.client_id;
                client_secret = config.client_secret;

                const url = 'https://api.pandadoc.com/public/v1/documents/' + documentId + '/download';
                axios.get(url, {
                        headers: {
                            Authorization: 'Bearer ' + access_token
                        }
                    }).then(function(response) {
                        console.log(response.data); // I get an error if I don't include this console.log, which is odd...
                        var file = formName + '-' + documentId + '.pdf';
                        var writeStream = fs.createWriteStream('/tmp/' + file);
                        writeStream.write(response.data, 'binary', function() {
                            console.log('Downloaded file from PandaDoc');
                            // ===============================
                            // THE FILE HERE IS MISSING DETAIL
                            // ===============================
                            resolve({
                                status: 'success',
                                message: 'File Downloaded',
                                data: file
                            });
                        });
                        writeStream.on('error', function(err) {
                            console.log('WriteStream Error: ' + err.message);
                            reject({
                                status: 'error',
                                message: 'WriteStream Failed: ' + err.message,
                                data: err
                            });
                        });
                    })
                    .catch(function(err) {
                        if (err.response.status === 401) {
                            console.log('Refreshing Access Token');
                            pandadocService.refreshToken().then(function(response) {
                                console.log('Access Token has been refreshed');
                                resolve({
                                    status: 'retry',
                                    message: 'Access Token has been refreshed',
                                    data: {}
                                });
                            });
                        } else {
                            reject({
                                status: 'error',
                                message: 'Download Failed: ' + err.message,
                                data: err
                            });
                        }
                    });
            }
        });
    });
    return promise;
}

/**
 * Fetch PandaDoc Config Settings from Firebase
 */
function getConfig() {
    var promise = new Promise(function(resolve, reject) {
        // Get current PandaDoc config from database
        var config = {
            access_token: 'most recent valid token',
            refresh_token: 'most recent valid refresh token',
            code: 'code from manual authorization',
            client_id: 'client id from developer console application',
            client_secret: 'client secret from developer console application'
        };
        resolve(config);
    });
    return promise;
}

/**
 * Refresh the PandaDoc Access Token
 */
function refreshToken() {
    var promise = new Promise(function(resolve, reject) {
        const url = 'https://api.pandadoc.com/oauth2/access_token';
        const data = querystring.stringify({
            grant_type: 'refresh_token',
            client_id: client_id,
            client_secret: client_secret,
            refresh_token: refresh_token,
            scope: 'read+write'
        });
        axios.post(url, data).then(function(response) {
                // Update Tokens in Firebase, then...
                resolve(response);
            })
            .catch(function(err) {
                console.log('Error: ' + err.response.status);
                reject(err);
            });
    });
    return promise;
}