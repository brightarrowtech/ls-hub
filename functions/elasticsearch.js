const firebase = require('firebase-admin');
const moment = require('moment');
const fs = require('fs');
const { Client } = require('@elastic/elasticsearch');
const esb = require('elastic-builder');

//const client = new Client({ node: 'http://localhost:9200' });
const client = new Client({
    cloud: {
        id: 'lshub-elasticsearch:dXMtd2VzdDEuZ2NwLmNsb3VkLmVzLmlvJDQwZmI1NjBjNmFlMzQ1ZWM4YTk3MzE3ODBkYzdhMDk1JDJiNDFiZTVmMjhmNDQ5M2FiYWEzYTVhYzMwMzU5YzRl'
    },
    auth: {
        apiKey: {
            "api_key": "zTAW5S2CR8y3ludkPGIujg",
            "id": "dSdx1ngBfFj-5b2yhnMM",
            "name": "lsh-elastic-key"
        }
    }
});

if (!Array.prototype.flatMap) {
    function flatMap (f, ctx) {
      return this.reduce
        ( (r, x, i, a) =>
            r.concat(f.call(ctx, x, i, a))
        , []
        )
    }
    Array.prototype.flatMap = flatMap
}

const env = 'prod'; // 'dev', 'prod', 'demo'
const indexName = (env === 'prod') ? 'policies' : 'dev_policies';

var serviceAccount = {};
if (env === 'demo') {
    serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-moong-5e8e33fd54");
} else if (env === 'prod') {
    serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-ojpku-876bd98f9d.json");
} else if (env === 'dev') {
    serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');
}

var functions_root_url = '';
var storage_root_url = '';
if (env === 'demo') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-demo.firebaseio.com",
        storageBucket: "lis-pipeline-demo.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline.firebaseio.com",
        storageBucket: "lis-pipeline.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-dev.firebaseio.com",
        storageBucket: "lis-pipeline-dev.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

const database = firebase.database().ref();

;(async () => {
   
    //testSearch('b73fc692-a8c4-4bc1-b6b5-71ea34147c1b');
    //search(teamUID);
    //count(teamUID);
    rebuildIndex().then(function() {
        populateIndex('ALL', 'public').then(function() {
            populateIndex('ALL', 'private').then(function() {
                process.exit();
            });
        }); //public or private
    });
    //listIndicies();
    

    async function populateIndex(total, which) {
        let private = false;
        if (which === 'private') {
            private = true;
        }
        console.log('================================');
        console.log('Beginning data migration for ' + which + ' policies');
        let result = await doMigration('add', total, private); // Migrate policies to the new index
        console.log('Finished migrating ' + which + ' policies');
        return result;
    }

    async function listIndicies() {
        let indicies = await client.cat.indices({
            v: true,
            s: 'index'
        });
        console.log(indicies.body);
    }

    async function rebuildIndex() {
        console.log('================================');
        console.log('Rebuilding Policies Index...');
        await client.indices.delete({
            index: indexName
        });
        let result = await client.indices.create({
            index: indexName,
            body: {
                settings: {
                    analysis: {
                        normalizer: {
                            lsh_lowercase: {
                                type: 'custom',
                                char_filter: [],
                                filter: ['lowercase', 'asciifolding']
                            }
                        }
                    }
                },
                mappings: {
                    properties: {
                        customAll: {
                            type: 'wildcard'
                        },
                        policyKey: {
                            type: 'keyword',
                            normalizer: 'lsh_lowercase'
                        },
                        fullName: {
                            type: 'keyword',
                            normalizer: 'lsh_lowercase',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            },
                            boost: 2,
                            copy_to: 'customAll'
                        },
                        displayStatus: {
                            type: 'keyword',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        policyType: {
                            type: 'keyword',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        healthStatus: {
                            type: 'keyword',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        supplier: {
                            type: 'keyword',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        les: {
                            type: 'nested',
                            properties: {
                                source: {
                                    type: 'keyword'
                                },
                                type: {
                                    type: 'keyword'
                                },
                                description: {
                                    type: 'keyword'
                                },
                                lshYears: {
                                    type: 'short'
                                }
                            }
                        },

                        isActive: { type: 'boolean' },
                        isClosed: { type: 'boolean' },
                        isArchived: { type: 'boolean' },
                        isLead: { type: 'boolean' },
                        hasInterest: { type: 'boolean' },
                        imInterested: { type: 'boolean' },
                        isPendingAcceptance: { type: 'boolean' },
                        isAccepted: { type: 'boolean' },
                        isPrivate: { type: 'boolean' },

                        gender: {
                            type: 'keyword',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        carrier: {
                            type: 'keyword',
                            normalizer: 'lsh_lowercase',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            },
                            copy_to: 'customAll'
                        },
                        createdAt: { 
                            type: 'date',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        faceAmount: { 
                            type: 'integer',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        annualPremiumAmount: { 
                            type: 'integer',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        age: { 
                            type: 'short',
                            copy_to: 'customAll',
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        },
                        status: {
                            type: 'nested',
                            properties: {
                                progress: {
                                    type: 'nested',
                                    properties: {
                                        activity: {
                                            type: 'keyword'
                                        },
                                        activityAt: {
                                            type: 'date',
                                            fields: {
                                                sort: {
                                                    type: 'keyword',
                                                    ignore_above: 256
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        access: {
                            type: 'nested',
                            properties: {
                                orgType: {
                                    type: 'keyword'
                                },
                                orgKey: {
                                    type: 'keyword'
                                },
                                orgGuid:  {
                                    type: 'keyword'
                                }
                            }
                        },
                        createdBy: { 
                            type: 'nested',
                            properties: {
                                createdByName: { 
                                    type: 'text',
                                    fields: {
                                        sort: {
                                            type: 'keyword',
                                            ignore_above: 256
                                        }
                                    }
                                },
                                createdByID: { type: 'keyword' },
                                createdByOrgKey: { type: 'keyword' },
                                createdByOrgType: { type: 'keyword' }
                            } 
                        },
                        indexUpdatedDtTm: { 
                            type: 'date',
                            index: false 
                        },
                        id: { 
                            type: 'keyword', 
                            index: false,
                            fields: {
                                sort: {
                                    type: 'keyword',
                                    ignore_above: 256
                                }
                            }
                        }
                    }
                }
            }
        }, { ignore: [400] });
                
        console.log('Finished Rebuilding Policies Index');
        return result
    }

    async function processChunk(private, lastID, idx, total, action, max) {
        console.log('Processing page ' + idx + ', starting at policyKey ' + lastID);
        return new Promise(async function(resolve) {
            let policiesArr = {};
            let batchSize = 100;
            let policyNode = 'policies';
            if (private) {
                policyNode = 'privatePolicies';
            }
            if (lastID) {
                policiesArr = await database.child(policyNode).orderByKey().startAt(lastID).limitToFirst(batchSize).once('value');
            } else {
                policiesArr = await database.child(policyNode).orderByKey().limitToFirst(batchSize).once('value');
            }
            let policiesToProcess = [];
            let records = [];
            let found = 0;
            if (policiesArr.exists()) {
                policiesArr.forEach(function(policyRef) {
                    found++;
                    if (max === 'ALL' || found < max) {
                        let policyData = policyRef.val();
                        policyData.policyKey = policyRef.key;
                        if (policyData.LSHID && policyData.policyKey !== lastID) {
                            policiesToProcess.push(policyData);
                        } else {
                            lastID = policyData.policyKey;
                        }
                    }
                });
                if (policiesToProcess.length > 0) {
                    policiesToProcess.forEach(function(policy) {
                        let record = {};
                        
                        let nameToIndex = '';
                        if (policy.insured && policy.insured.firstName && (policy.insured.lastName || policy.insured.lastNameInitial)) {
                            nameToIndex = policy.insured.firstName + ' ' + policy.insured.lastName;
                            if (private) {
                                nameToIndex = policy.insured.firstName + ' ' + policy.insured.lastNameInitial + '.';
                            }
                        }

                        record.policyKey = policy.policyKey;
                        record.LSHID = policy.LSHID;
                        record.fullName = nameToIndex;
                        record.gender = (policy.insured && policy.insured.gender) ? policy.insured.gender[0].id : '';
                        record.carrier = policy.carrier ? policy.carrier : '';
                        record.createdAt = moment(policy.createdAt).format();
                        record.faceAmount = policy.faceAmount ? policy.faceAmount : 0;
                        record.annualPremiumAmount = policy.annualPremiumAmount ? policy.annualPremiumAmount : 0;
                        record.age = (policy.insured && policy.insured.age) ? policy.insured.age : 0;
                        record.policyType = policy.policyType ? policy.policyType : '';
                        record.status = [];
                        if (policy.status) {
                            Object.keys(policy.status).forEach(function(key) {
                                if (policy.status[key] === true) {
                                    if (key === 'receivedFromSupplier') {
                                        if (!record.isAccepted) {
                                            record.isPendingAcceptance = true;
                                        }
                                    } else if (key === 'acceptedCase') {
                                        record.isAccepted = true;
                                        record.isPendingAcceptance = false;
                                    }
                                    record.status.push({
                                        progress: {
                                            activity: key,
                                            activityAt: moment(policy.status[key + 'At']).format()
                                        }
                                    });
                                }
                            });
                        }
                        record.displayStatus = (policy.status && policy.status.display && policy.status.display.text) ? policy.status.display.text : '';
                        
                        record.isActive = policy.policyIsActive ? policy.policyIsActive : false;
                        record.isClosed = policy.isClosed ? policy.isClosed : false;
                        record.isArchived = policy.isArchived ? policy.isArchived : false;
                        record.isLead = policy.isLead;
                        record.isPrivate = private;
                        record.hasInterest = false;
                        if (policy.buyers) {
                            for (let index = 0; index < policy.buyers.length; index++) {
                                let buyer = policy.buyers[index];
                                if (buyer.buyerInterested) {
                                    record.hasInterest = true;
                                    break;
                                }
                            }
                        }
                        record.imInterested = policy.buyerInterested ? policy.buyerInterested : false;

                        record.healthStatus = (policy.insured && policy.insured.healthStatus) ? policy.insured.healthStatus : null;
                        record.supplier = policy.supplier;
                        record.les = [];
                        if (policy.les) {
                            policy.les.forEach(function(le) {
                                if (le.showLE) {
                                    record.les.push({
                                        source: le.source,
                                        description: le.description,
                                        lshYears: le.lshYears,
                                        type: le.type[0]
                                    });
                                }
                            });
                        }

                        record.access = [];
                        if (!private) {
                            if (policy.upline) {
                                policy.upline.forEach(function(upline) {
                                    record.access.push(upline);
                                });
                            } else {
                                console.log('No upline for policy ' + policy.policyKey);
                                record.access.push({
                                    "orgKey": "-L8crXXb_c8NRSJUgRpn",
                                    "orgType": "backoffice"
                                });
                            }
                            record.access.push({
                                "orgKey": (env === "prod") ? "-LH-BIGkYe_M_-C4gBEC" : "-LH-0PWxLXMhRFtJvwW6",
                                "orgType": "master"
                            });
                        } else {
                            record.access.push({
                                "orgGuid": policy.recipient
                            });
                            record.access.push({
                                "orgKey": (env === "prod") ? "-LH-BIGkYe_M_-C4gBEC" : "-LH-0PWxLXMhRFtJvwW6",
                                "orgType": "master"
                            });
                        }
                        record.createdBy = [{
                            createdByName: policy.createdBy,
                            createdByID: policy.createdById,
                            createdByOrgKey: policy.createdByOrgKey,
                            createdByOrgType: policy.createdByOrgType
                        }];

                        record.id = policy.policyKey;
                        record.indexUpdatedDtTm = moment().format();
        
                        records.push(record);
                        total++;
                        lastID = policy.policyKey;
                    });
                    if (records.length > 0) {
                        let body = {};
                        if (action === 'remove') {
                            body = records.flatMap(doc => [{ delete: { _id: doc.id }}]);
                        } else {
                            body = records.flatMap(doc => [{ index: { _index: indexName, _type: '_doc', _id: doc.id }}, doc]);
                        }

                        let { body: bulkResponse } = await client.bulk({
                            index: indexName,
                            refresh: true,
                            body
                        });

                        if (action === 'add' && bulkResponse.errors) {
                            bulkResponse.items.forEach(function(item) {
                                if (item.index.error) {
                                    console.log(item.index.error);
                                }
                            });
                        }
                    }
                    resolve({
                        lastID: lastID,
                        total: total
                    });
                } else if (found === batchSize) {
                    resolve({
                        lastID: lastID,
                        total: total
                    });
                } else {
                    resolve({
                        lastID: 'STOP',
                        total: total
                    });
                }
            } else {
                resolve({
                    lastID: 'STOP',
                    total: total
                });
            }
        });
    }

    async function doMigration(action, max, private) {
        let lastID = null;
        let finished = false;
        let idx = 1;
        let total = 0;
        while (!finished) {
            let result = await processChunk(private, lastID, idx, total, action, max);
            total = result.total;
            lastID = result.lastID;
            idx++;
            if (lastID === 'STOP' || (max !== 'ALL' && total >= max)) {
                finished = true;
            }
        }
        console.log('Migrated ' + total + ' policies');
      }


    async function count(teamUID) {
        let requestBody = {};

        requestBody = esb.requestBodySearch().query(esb.matchQuery('teamUID',teamUID)).from(0).size(0);

        const result = await client.search({
            index: 'contacts',
            body: requestBody.toJSON()
        });

        console.log(result.body.hits.total.value + ' Matches');
        process.exit();
    }

    async function testSearch(teamUID) {
        let mustQuery = [];
        mustQuery.push(
            esb.termQuery('teamUID',teamUID)
        );

        let requestBody = esb.requestBodySearch()
            .query(
                esb.boolQuery()
                    .must(mustQuery)
            )
            .from(0)
            .size(1)
            .trackTotalHits(true)
            .toJSON();

        client.search({
            index: 'contacts',
            body: requestBody
        }).then(function(results) {
            // RETURN RESULTS
            let teamContactCount = results.body.hits.total.value;
            console.log(teamContactCount);
        });
    }

    async function search(teamUID) {
        let searchObj = {
            sort: {
                field: 'invitedBy.invitedByName',
                direction: 'desc'
            },
            paging: {
                from: 0,
                size:10
            },
            teamUID: teamUID
        };

        let mustQuery = [];
        let mustNotQuery = [];
        
        // TEAM UID
        mustQuery.push(
            esb.matchQuery('teamUID',searchObj.teamUID)
        );
        
        // ADD GROUP FILTER
        if (searchObj.groupFilter && searchObj.groupFilter.group) {
            mustQuery.push(
                esb.nestedQuery()
                    .path('groups')
                    .query(
                        esb.matchQuery('groups.groupUID',searchObj.groupFilter.group.groupUID)
                    )
            );
        }

        // ADD ANY STAGE FILTERS
        if (searchObj.stageFilters && searchObj.stageFilters.stages) {
            let shouldArr = [];
            searchObj.stageFilters.stages.forEach(function(stage) {
                shouldArr.push(
                    esb.nestedQuery()
                        .path('stages')
                        .query(
                            esb.boolQuery()
                                .must([
                                    esb.matchQuery('stages.stageUID',stage.stageUID),
                                    esb.matchQuery('stages.groupUID',searchObj.groupFilter.group.groupUID)
                                ])
                        )
                );
            });
            mustQuery.push(
                esb.boolQuery().should(shouldArr)
            )
        } else if (searchObj.stageFilters && searchObj.stageFilters.noStageSet) {
            if (searchObj.groupFilter && searchObj.groupFilter.group) {
                // Limit search to no stages set within the selected Group only
                mustQuery.push(
                    esb.matchQuery('hasNoStage',searchObj.groupFilter.group.groupUID)
                );
            } else {
                // Search where no stages is set on any Group
                mustQuery.push(
                    esb.existsQuery('hasNoStage')
                );
            }
        }

        // ADD ANY GROUP TAG FILTERS
        if (searchObj.tagFilters && searchObj.tagFilters.tags) {
            let nestedQ = [];
            searchObj.tagFilters.tags.forEach(function(tag) {
                nestedQ.push(
                    esb.boolQuery()
                        .must([
                            esb.nestedQuery()
                            .path('groupTags.tag')
                            .query(
                                esb.matchPhraseQuery('groupTags.tag.name',tag.tagName)
                            ),
                            esb.nestedQuery()
                                .path('groupTags')
                                .query(
                                    esb.boolQuery().must([
                                        esb.matchQuery('groupTags.tagSetUID',tag.tagSetUID),
                                        esb.matchQuery('groupTags.groupUID',searchObj.groupFilter.group.groupUID)
                                    ])
                                )
                        ])
                );
            });
            if (searchObj.tagFilters.joiner === 'AND') {
                mustQuery.push(
                    esb.boolQuery().must(nestedQ)
                )
            } else if (searchObj.tagFilters.joiner === 'OR') {
                mustQuery.push(
                    esb.boolQuery().should(nestedQ)
                )
            }
        }

        // ADD ANY GLOBAL TAG FILTERS
        if (searchObj.globalTagFilters && searchObj.globalTagFilters.tags) {
            let nestedQ = [];
            searchObj.globalTagFilters.tags.forEach(function(tag) {
                nestedQ.push(
                    esb.boolQuery()
                        .must([
                            esb.nestedQuery()
                            .path('globalTags.tag')
                            .query(
                                esb.matchPhraseQuery('globalTags.tag.name',tag.tagName)
                            ),
                            esb.nestedQuery()
                                .path('globalTags')
                                .query(
                                    esb.boolQuery().must([
                                        esb.matchQuery('globalTags.tagSetUID',tag.tagSetUID),
                                        esb.matchQuery('globalTags.groupUID','GLOBAL')
                                    ])
                                )
                        ])
                );
            });
            if (searchObj.globalTagFilters.joiner === 'AND') {
                mustQuery.push(
                    esb.boolQuery().must(nestedQ)
                )
            } else if (searchObj.globalTagFilters.joiner === 'OR') {
                mustQuery.push(
                    esb.boolQuery().should(nestedQ)
                )
            }
        }

        // ADD ANY SEARCH FILTER, GLOBAL OR IN SPECIFIC FIELDS
        if (searchObj.searchFilter) {
            let shouldArr = [];
            if (!searchObj.searchFilter.fields) {
                searchObj.searchFilter.fields = ['fullName'];
            }
            searchObj.searchFilter.fields.forEach(function(field) {
                if (field.indexOf('.') > -1) {
                    // Nested field...split and build query
                    let fieldArr = field.split('.');
                    if (searchObj.searchFilter.isExact) {
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.matchPhraseQuery(field, searchObj.searchFilter.text)
                                )
                        )
                    } else {
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.matchQuery(field, '*' + searchObj.searchFilter.text + '*').operator('or').fuzziness('AUTO')
                                )
                        )
                        shouldArr.push(
                            esb.nestedQuery()
                                .path(fieldArr[0])
                                .query(
                                    esb.wildcardQuery(field, '*' + searchObj.searchFilter.text + '*')
                                )
                        )
                    }
                } else {
                    // Top level field...add to query
                    if (searchObj.searchFilter.isExact) {
                        shouldArr.push(
                            esb.matchPhraseQuery(field, searchObj.searchFilter.text)
                        )
                    } else {
                        shouldArr.push(
                            esb.matchQuery(field, '*' + searchObj.searchFilter.text + '*').operator('or').fuzziness('AUTO')
                        )
                        shouldArr.push(
                            esb.wildcardQuery(field, '*' + searchObj.searchFilter.text + '*')
                        )
                    }
                }
            });
            mustQuery.push(
                esb.boolQuery()
                    .should(shouldArr)
            );
        }

        // DEAL WITH SORTING
        let sortArr = [];
        if (searchObj.searchFilter) {
            sortArr.push('_score');
        }
        if (searchObj.sort) {
            if (searchObj.sort.field.indexOf('.') > -1) {
                // Nested sort
                sortArr.push(
                    esb.sort(searchObj.sort.field + '.sort', searchObj.sort.direction).nested({ 
                        path: searchObj.sort.field.split('.')[0], 
                        filter: esb.wildcardQuery(searchObj.sort.field, '*')
                    })
                );
            } else {
                sortArr.push(esb.sort(searchObj.sort.field + '.sort', searchObj.sort.direction).toJSON());
            }
        }

        // BUILD REQUEST
        let requestBody = esb.requestBodySearch()
            .query(
                esb.boolQuery()
                    .must(mustQuery)
                    .mustNot(mustNotQuery)
            )
            .from(searchObj.paging.from)
            .size(searchObj.paging.size)
            .toJSON();
        requestBody.sort = sortArr;

        console.log(requestBody.sort[0])

        // DO SEARCH
        const result = await client.search({
            index: 'contacts',
            body: requestBody
        });

        // RETURN RESULTS
        console.log(result.body.hits.total.value + ' Results');
        result.body.hits.hits.forEach(function(hit) {
            console.log(hit._source.fullName);
            console.log(hit._source.invitedBy);
        });
        process.exit();
    }

})();