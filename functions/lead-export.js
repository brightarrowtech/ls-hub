const firebase = require('firebase-admin');
const Promise = require('promise');
const fs = require('fs');
const async = require('async');
const moment = require('moment');

var env = 'prod'; // 'dev', 'prod', 'demo'

var serviceAccount = {};
if (env === 'demo') {
    serviceAccount = require("./certs/lis-pipeline-demo-firebase-adminsdk-moong-5e8e33fd54");
} else if (env === 'prod') {
    serviceAccount = require("./certs/lis-pipeline-firebase-adminsdk-ojpku-876bd98f9d.json");
} else if (env === 'dev') {
    serviceAccount = require('./certs/lis-pipeline-dev-firebase-adminsdk-75864-c6ce282c4b');
}

var functions_root_url = '';
var storage_root_url = '';

if (env === 'demo') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-demo.firebaseio.com",
        storageBucket: "lis-pipeline-demo.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-demo.appspot.com';
} else if (env === 'prod') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline.firebaseio.com",
        storageBucket: "lis-pipeline.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline.appspot.com';
} else if (env === 'dev') {
    firebase.initializeApp({
        credential: firebase.credential.cert(serviceAccount),
        databaseURL: "https://lis-pipeline-dev.firebaseio.com",
        storageBucket: "lis-pipeline-dev.appspot.com"
    });
    functions_root_url = 'https://us-central1-lis-pipeline-dev.cloudfunctions.net';
    storage_root_url = 'https://firebasestorage.googleapis.com/v0/b/lis-pipeline-dev.appspot.com';
}

var db = firebase.database().ref();
var storage = firebase.storage();

db.child('policies').orderByChild('createdById').equalTo('uKiBdw9iH4PbqZHy0XOZTSVSIO73').once('value', function(snap) {
    if (snap.exists()) {
        let leads = [];
        snap.forEach(function(leadRef) {
            let lead = leadRef.val();
            leads.push({
                'LSH ID': lead.LSHID,
                'Submitted On': moment(lead.createdAt).format('MM/DD/YYYY'),
                'Insured Name': lead.insured ? lead.insured.fullName : '',
                'Insured DOB': lead.insured ? lead.insured.dateOfBirth : '',
                'Insured Age': lead.insured ? lead.insured.age : '',
                'Face Amount': lead.faceAmount,
                'Health Status': lead.insured ? lead.insured.healthStatus : '',
                'Estimated Offer': '',
                'Contact Name': lead.contacts ? (lead.contacts[0].firstName + ' ' + lead.contacts[0].lastName) : '',
                'Email': lead.contacts ? lead.contacts[0].emailAddress : '',
                'Phone': lead.contacts ? lead.contacts[0].phoneNumber : '',
                'Active': lead.policyIsActive ? 'Yes': 'No',
                'Lead Rejected': lead.isRejected ? 'Rejected' : ''
            });
        });
        let leadCSV = JSON2CSV(leads);
        console.log(leadCSV);
    }
});

function JSON2CSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;

    var str = '';
    var line = '';

    var head = array[0];

    for (var index in array[0]) {
        var value = index + "";
        line += '"' + value.replace(/"/g, '""') + '",';
    }

    line = line.slice(0, -1);
    str += line + '\r\n';

    for (var i = 0; i < array.length; i++) {
        var line = '';

        for (var index in array[i]) {
            var value = array[i][index] + "";
            line += '"' + value.replace(/"/g, '""') + '",';
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }
    return str;
}

// db.child('policies').once('value', function(snap) {
//     if (snap.exists()) {
//         let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
//         async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
//             db.child('policies/' + policyKey).once('value', function(snap) {
//                 if (snap.exists()) {
//                     let policy = snap.val();
//                     if (policy.private && policy.private.les) {
//                         var updates = {};
//                         updates['les'] = policy.private.les;
//                         updates['private/les'] = null;
//                         db.child('policies/' + policyKey).update(updates).then(function() {
//                             console.log('Updated policy ' + policyKey);
//                             callback();
//                         });
//                     } else {
//                         callback();
//                     }
//                 } else {
//                     callback();
//                 }
//             });
//         }, function(err) {
//             console.log('Finished');
//         });
//     }
// });

// //console.log('Checking for org-level Policies');
// db.child('orgs/' + requestBody.requestorOrgType + '/' + requestBody.requestorOrgKey + '/policies').once('value', function(snap) {
//     if (snap.exists()) {
//         let policyKeys = Object.keys(snap.val()).map(function(x) { return x; });
//         let digestData = {
//             startDate: "",
//             endDate: "",
//             policies: []
//         };
//         //console.log(policyKeys.length + ' Policies found');
//         async.forEachOfSeries(policyKeys, function(policyKey, key, callback) {
//             orgPolicies.push(policyKey);
//             db.child('policies/' + policyKey).once('value', function(snap) {
//                 if (snap.exists()) {
//                     let policy = snap.val();
//                     if (policy.policyIsActive) {
//                         let newDigest = {
//                             LSHID: policy.LSHID,
//                             agentName: policy.createdBy,
//                             premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
//                             faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
//                             insuredName: policy.insured.fullName,
//                             insuredAge: policy.insured.age ? policy.insured.age : '',
//                             latestUpdate: policy.latestStatus ? policy.latestStatus : '',
//                             latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
//                             displayStatus: policy.status.display.text,
//                             link: ''
//                         };
//                         newDigest.logSummary = [];
//                         let miscUpdates = 0;
//                         let fileUploads = 0;
//                         let messages = 0;
//                         let formActions = 0;
//                         let buyerActions = 0;
//                         if (policy.updateLog) {
//                             policy.updateLog.forEach(function(log) {
//                                 if (log.type) {
//                                     if (log.type === 'Policy Assigned') {
//                                         miscUpdates = miscUpdates + 1;
//                                     } else if (log.type === 'File Uploaded') {
//                                         fileUploads = fileUploads + 1;
//                                     } else if (log.type.indexOf('Application') > -1) {
//                                         formActions = formActions + 1;
//                                     }
//                                 } else {
//                                     miscUpdates = miscUpdates + 1;
//                                 }
//                             });
//                         }
//                         if (policy.publicNotes) {
//                             policy.publicNotes.forEach(function(note) {
//                                 messages = messages + 1;
//                             });
//                         }
//                         // TODO: Map Buyer Actions
//                         newDigest.logSummary = {
//                             miscUpdates: miscUpdates,
//                             fileUploads: fileUploads,
//                             messages: messages,
//                             formActions: formActions,
//                             buyerActions: buyerActions
//                         };
//                         digestData.policies.push(newDigest);
//                     }
//                     callback();
//                 } else {
//                     callback();
//                 }
//             });
//         }, function(err) {
//             if (err) {
//                 console.log(err);
//             } else {
//                 //console.log(digestData);
//                 //console.log('Checking for user-level Policies');
//                 db.child('policies').orderByChild('createdById').equalTo(requestBody.requestorUid).once('value', function(snap) {
//                     if (snap.exists()) {
//                         let policyData = snap.val();
//                         Object.keys(policyData).forEach(function(policyKey) {
//                             if (orgPolicies.indexOf(policyKey) < 0) {
//                                 let policy = policyData[policyKey];
//                                 if (policy.policyIsActive) {
//                                     let newDigest = {
//                                         LSHID: policy.LSHID,
//                                         agentName: policy.createdBy,
//                                         premium: policy.annualPremiumAmount ? policy.annualPremiumAmount.toString() : 'n/a',
//                                         faceValue: policy.faceAmount ? policy.faceAmount.toString() : 'n/a',
//                                         insuredName: policy.insured.fullName,
//                                         insuredAge: policy.insured.age ? policy.insured.age.toString() : '',
//                                         latestUpdate: policy.latestStatus ? policy.latestStatus : '',
//                                         latestStatus: policy.statusSelection ? policy.statusSelection[0].status : 'On Track',
//                                         displayStatus: policy.status.display.text,
//                                         link: ''
//                                     };
//                                     newDigest.logSummary = [];
//                                     let miscUpdates = 0;
//                                     let fileUploads = 0;
//                                     let messages = 0;
//                                     let formActions = 0;
//                                     let buyerActions = 0;
//                                     if (policy.updateLog) {
//                                         policy.updateLog.forEach(function(log) {
//                                             if (log.type) {
//                                                 if (log.type === 'Policy Assigned') {
//                                                     miscUpdates = miscUpdates + 1;
//                                                 } else if (log.type === 'File Uploaded') {
//                                                     fileUploads = fileUploads + 1;
//                                                 } else if (log.type.indexOf('Application') > -1) {
//                                                     formActions = formActions + 1;
//                                                 }
//                                             } else {
//                                                 miscUpdates = miscUpdates + 1;
//                                             }
//                                         });
//                                     }
//                                     if (policy.publicNotes) {
//                                         policy.publicNotes.forEach(function(note) {
//                                             messages = messages + 1;
//                                         });
//                                     }
//                                     // TODO: Map Buyer Actions
//                                     newDigest.logSummary = {
//                                         miscUpdates: miscUpdates,
//                                         fileUploads: fileUploads,
//                                         messages: messages,
//                                         formActions: formActions,
//                                         buyerActions: buyerActions
//                                     };
//                                     digestData.policies.push(newDigest);
//                                 }
//                             } else {
//                                 console.log('(policy already processed at org-level)');
//                             }
//                         });
//                         // console.log('Finished');
//                         // console.log(digestData);
//                     }
//                 });
//             }
//         });
//     }
// });