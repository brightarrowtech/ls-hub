# Notifications
## Data Structure
    var data = {
        targets: [{
            type: [orgType],
            id: [orgKey]
        }],
        policyKey: vm.policyKey],
        LSHID: vm.policy.LSHID,
        createdBy: vm.currentUser.profile.displayName,
        createdByRole: vm.currentUser.profile.role,
        note: 'What the notification should say',
        action: [See Next Section],
        isUnread: boolean,
        isUrgent: boolean
    };

## Action Types
* 'New Public Note'
* 'New Internal Note'
* 'Offer Received'
* 'Status Updated'
* 'New File Uploaded'
* 'Action Required'
* 'New Policy Created'
* 'Status Updated'

# URL Parameters
## iManager
### /bdn/policy
* pid: Policy GUID. Identifies the Policy copy that was sent to the Buyer. Found under "publicPolicies" node in db.
* bid: Buyer GUID. Public identifier for the Buyer's organization. Links Buyers to Policies, but not to User accounts.
* lvl: Level. If a Buyer is distributing to sub-Buyers, management of those views is at "lvl=2".
* pk: Policy Key. If a Buyer is managing a sub-Buyer's view (lvl=2), the original publicPolicy GUID is referenced as "pk". If the Policy creator is managing the first-level Buyer views, "pk" refers to the Policy Key under "policies" node.

### /policy
* pid: Policy Key. Identifies the original Policy as the creator sees it. Found under "policies" node in db.
