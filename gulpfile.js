/* jshint node:true */
'use strict';

var gulp = require('gulp');
var ignore = require('gulp-ignore');
var util = require('gulp-util');
var babel = require('gulp-babel');
var uglifyEs = require('gulp-uglify-es').default;
var karma = require('karma').server;
var argv = require('yargs').argv;
var modRewrite = require('connect-modrewrite');
var wiredep = require('gulp-wiredep');
var $ = require('gulp-load-plugins')();

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.green(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.green(msg));
    }
}

gulp.task('styles', function() {
    return gulp.src('app/styles/main.less')
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.autoprefixer({ browsers: ['last 1 version'] }))

    .pipe(gulp.dest('.tmp/styles'));
});

gulp.task('jshint', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe($.jshint())
        //.pipe($.jshint.reporter('jshint-stylish'))
        //.pipe($.jshint.reporter('fail'));
});

gulp.task('jscs', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe($.jscs());
});

gulp.task('html', gulp.series('styles', function() {
    var lazypipe = require('lazypipe');
    var cssChannel = lazypipe()
        .pipe($.csso)
        .pipe($.replace, 'bower_components/bootstrap/fonts', 'fonts');

    gulp.src('bower_components/iCheck/skins/square/blue*.png')
        .pipe(gulp.dest('dist/styles'));

    return gulp.src('app/**/*.html')
        .pipe($.if('*.js', $.ngAnnotate({ add: true })))
        .pipe(ignore.exclude(["**/*.map"]))
        .pipe($.if('*.js', uglifyEs().on('error', util.log)))
        .pipe($.if('*.css', cssChannel()))
        .pipe($.useref())
        .pipe($.if('*.html', $.minifyHtml({ conditionals: true, loose: true })))
        .pipe(gulp.dest('dist'));
}));

gulp.task('images', function() {
    return gulp.src('app/images/**/*')
        // .pipe($.cache($.imagemin({
        //   progressive: true,
        //   interlaced: true
        // })))
        .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function() {
    return gulp.src(require('main-bower-files')().concat('app/fonts/**/*')
            .concat('bower_components/font-awesome/fonts/*'))
        .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
        .pipe($.flatten())
        .pipe(gulp.dest('dist/fonts'))
        .pipe(gulp.dest('.tmp/fonts'));
});

gulp.task('extras', function() {
    gulp.src('app/scripts/vendor/propeller.min.js')
        .pipe(gulp.dest('dist/scripts/vendor'));
    return gulp.src('app/scripts/vendor/nouislider-angular.js')
        .pipe(gulp.dest('dist/scripts/vendor'));
    //gulp.src('app/styles/fonts/roboto/**/*')
    //    .pipe(gulp.dest('dist/styles/fonts/roboto'));
    // return gulp.src([
    //     'app/*.*',
    //     '!app/*.html',
    //     'node_modules/apache-server-configs/dist/.htaccess'
    // ], {
    //     dot: true
    // }).pipe(gulp.dest('dist'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

//'!\\.\\w+$ /index.html [L]'

gulp.task('connect', gulp.series('styles', function() {
    var serveStatic = require('serve-static');
    var serveIndex = require('serve-index');
    var app = require('connect')()
        .use(modRewrite([
            '^[^\\.]*$ /index.html [L]'
        ]))
        .use(require('connect-livereload')({ port: 35729 }))
        .use(serveStatic('.tmp'))
        .use(serveStatic('app'))
        // paths to bower_components should be relative to the current file
        // e.g. in app/index.html you should use ../bower_components
        .use('/bower_components', serveStatic('bower_components'))
        .use(serveIndex('app'));

    require('http').createServer(app)
        .listen(3000)
        .on('listening', function() {
            log('Started connect web server on http://localhost:3000');
            require('opn')('http://isubmit-local.lshub.net:3000');
        }); 
}));

// inject bower components
gulp.task('wiredep', function() {
    log('Injecting custom scripts to index.html');

    return gulp
        .src('app/index.html')
        .pipe( $.inject(gulp.src([
            './app/**/*.module.js',
            './app/**/*.js',
            '!./app/**/*.spec.js'
        ]), {relative: true}) )
        .pipe(gulp.dest('app'));
    //var wiredep = require('wiredep').stream;
    // var exclude = [
    //     //'bootstrap',
    //     //'jquery',
    //     'es5-shim',
    //     'json3',
    //     'angular-scenario'
    // ];

    // gulp.src('app/styles/*.less')
    //     .pipe(wiredep())
    //     .pipe(gulp.dest('app/styles'));

    // gulp.src('app/*.html')
    //     .pipe(wiredep({ exclude: exclude }))
    //     .pipe(gulp.dest('app'));

    // gulp.src('test/*.js')
    //     .pipe(wiredep({ exclude: exclude, devDependencies: true }))
    //     .pipe(gulp.dest('test'));
});

gulp.task('watch', function() {
    log('Starting Watch...');
    return new Promise(function(resolve) {
        $.livereload.listen();

        // watch for changes
        gulp.watch([
            'app/**/*.html',
            'app/scripts/**/*.js',
            'app/images/**/*'
        ]).on('change', $.livereload.changed);

        gulp.watch('app/styles/**/*.less', gulp.series('styles', function(done) {
            $.livereload.changed('.tmp/styles/**/*.css');
            done();
        }));
        gulp.watch('bower.json', gulp.series('wiredep'));
        resolve();
    });
});

gulp.task('serve', gulp.series('wiredep', 'watch', 'fonts', 'connect', function(done) {
    //require('opn')('http://isubmit-local.lshub.net:3000');
    log('Finished starting');
    done();
}));

gulp.task('test', function(done) {
    karma.start({
        configFile: __dirname + '/test/karma.conf.js',
        singleRun: true
    }, done);
});

gulp.task('builddist', gulp.series('html', 'images', 'fonts', 'styles', 'extras', function() {
    return gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true }));
}));

gulp.task('build', gulp.series('clean', 'builddist', function(done) {
    done();
}));

gulp.task('docs', function() {
    return gulp.src('app/scripts/**/**')
        .pipe($.ngdocs.process())
        .pipe(gulp.dest('./docs'));
});