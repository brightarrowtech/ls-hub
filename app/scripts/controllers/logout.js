(function() {
    'use strict';

    /* @ngInject */
    function LogoutCtrl($scope, $state, Auth, brandingDomain) {
        var vm = this;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        if (vm.subdomain.indexOf('isubmit') > -1) {
            vm.otherDomain = vm.subdomain.replace('isubmit', 'imanager');
            vm.otherDomainDisp = 'iManager';
        } else {
            vm.otherDomain = vm.subdomain.replace('imanager', 'isubmit');
            vm.otherDomainDisp = 'iSubmit';
        }

        Auth.$signOut().then(function() {});

        vm.login = function() {
            $state.go('login');
        }
    }

    angular.module('yapp')
        .controller('LogoutCtrl', LogoutCtrl);
})();