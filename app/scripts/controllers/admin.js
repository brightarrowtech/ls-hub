(function() {
    'use strict';

    /* @ngInject */
    function AdminCtrl(stdData, $scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert) {
        var vm = this;
        vm.isSAdmin = false;
        vm.sortCol = 'user.lastName';
        vm.sortReverse = false;
        vm.sortColPolicy = 'policy.policyNumber';
        vm.sortReversePolicy = false;
        vm.showUserManagementPanel = true;
        vm.showBuyers = true;
        vm.showSuppliers = true;
        vm.suppliers = [];
        vm.carriers = [];

        vm.roles = stdData.allRoles;
        vm.fileCategories = stdData.fileCategories;

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.role.indexOf('Admin') > -1) {
                vm.isSAdmin = true;
                loadPage();
            } else {
                $state.go('inventory');
            }
        });

        function loadPage() {
            var root = firebase.database().ref();
            vm.users = $firebaseArray(root.child('users'));
            vm.users.$loaded().then(function(users) {
                root.child('policies').orderByChild('isArchived').equalTo(true).once('value', function(snap) {
                    vm.archivedPolicies = [];
                    snap.forEach(function(policy) {
                        var thisPolicy = policy.val();
                        thisPolicy.policyKey = policy.key;
                        vm.archivedPolicies.push(thisPolicy);
                    });
                    vm.buyers = $firebaseArray(root.child('buyers'));
                    vm.buyers.$loaded().then(function(buyers) {
                        vm.buyerOrgs = $firebaseArray(root.child('orgs/buyer'));
                        vm.buyerOrgs.$loaded().then(function(buyerOrgs) {
                            vm.supplierOrgs = $firebaseArray(root.child('orgs/supplier'));
                            vm.supplierOrgs.$loaded().then(function(supplierOrgs) {
                                console.log('Supplier Orgs loaded');
                            });
                        });
                    });
                    vm.carriers = $firebaseArray(root.child('carriers'));
                    vm.carriers.$loaded().then(function(carriers) {
                        console.log('Carriers loaded');
                    });
                    // vm.suppliers = $firebaseArray(root.child('suppliers'));
                    // vm.suppliers.$loaded().then(function(suppliers) {
                    //     $scope.$apply();
                    // });
                });
            });
        }

        vm.bindAccessDomain = function(accessDomain) {
            return accessDomain.replace(/_/g, '.');
        }

        vm.setAccessDomain = function(user) {
            sweetAlert.swal({
                title: "Access Domain",
                text: "Please enter the domain the user " + user.displayName + " should have access to",
                type: "question",
                input: "text",
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: "Don't Change",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Update",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then(
                function(accessDomain) {
                    if (accessDomain.value) {
                        var data = {
                            uid: user.uid,
                            claims: {
                                accessDomain: accessDomain.value.replace(/\./g, '_')
                            }
                        };
                        authService.setCustomClaims(data).then(function(response) {
                            if (response.status === 'success') {
                                sweetAlert.swal({
                                    title: "Success",
                                    text: "",
                                    type: "success",
                                    timer: 1000
                                }).then(function() {});
                                $scope.$apply();
                            }
                        });
                    }
                }
            )
        }

        vm.addBuyer = function() {
            sweetAlert.swal({
                title: "Buyer Name",
                text: "Please provide the name of the Buyer you wish to add",
                type: "question",
                input: "text",
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Save",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then(
                function(buyer) {
                    if (buyer.value) {
                        var newBuyer = {};
                        newBuyer.name = buyer.value;
                        vm.buyers.$add(newBuyer).then(function() {
                            $scope.$apply();
                        });
                    }
                }
            )
        }

        vm.updateBuyer = function(buyer) {
            buyer.editInfo = false;
            vm.editInfo = false;
            delete buyer.editInfo;
            vm.buyers.$save(buyer).then(function() {
                vm.showDetail = true;
                $scope.$apply();
            });
        }

        vm.deleteBuyer = function(buyer, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This action cannot be un-done",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        vm.buyers.$remove(buyer).then(function() {
                            sweetAlert.swal({
                                title: "Deleted!",
                                text: "",
                                type: "success",
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                timer: 1000
                            }).then(function() {
                                $scope.$apply();
                            });
                        });
                    }
                }
            )
        }

        vm.clickBuyer = function($event, buyer) {
            if ($event.target.tagName === 'BUTTON' || $event.target.tagName === 'INPUT') {
                $event.stopPropagation();
            } else if (!vm.editInfo) {
                if (vm.showDetail === buyer.name) {
                    vm.showDetail = undefined;
                } else {
                    vm.showDetail = buyer.name;
                }
            }
        }

        vm.addCarrier = function() {
            sweetAlert.swal({
                title: "Carrier Name",
                text: "Please provide the name of the Carrier you wish to add",
                type: "question",
                input: "text",
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Save",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then(
                function(carrier) {
                    var newCarrier = {};
                    newCarrier.name = carrier.value;
                    vm.carriers.$add(newCarrier).then(function() {
                        $scope.$apply();
                    });
                }
            )
        }

        vm.deleteCarrier = function(carrier, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This action cannot be un-done",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        vm.carriers.$remove(carrier).then(function() {
                            sweetAlert.swal({
                                title: "Deleted!",
                                text: "",
                                type: "success",
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                timer: 1000
                            }).then(function() {
                                $scope.$apply();
                            });
                        });
                    }
                }
            )
        }

        vm.addSupplier = function() {
            sweetAlert.swal({
                title: "Supplier Name",
                text: "Please provide the name of the Supplier you wish to add",
                type: "question",
                input: "text",
                reverseButtons: true,
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Save",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }).then(
                function(supplier) {
                    var newSupplier = {};
                    newSupplier.name = supplier;
                    vm.suppliers.$add(newSupplier).then(function() {
                        $scope.$apply();
                    });
                }
            )
        }

        vm.deleteSupplier = function(supplier, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This action cannot be un-done",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        vm.suppliers.$remove(supplier).then(function() {
                            sweetAlert.swal({
                                title: "Deleted!",
                                text: "",
                                type: "success",
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                timer: 1000
                            }).then(function() {
                                $scope.$apply();
                            });
                        });
                    }
                }
            )
        }

        vm.restorePolicy = function(policyKey, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This policy will appear in Inventory immediately",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes, restore it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        var root = firebase.database().ref();
                        root.child('policies/' + policyKey).update({ isArchived: false });
                        vm.archivedPolicies.splice(index, 1);
                        $scope.$apply();
                        sweetAlert.swal({
                            title: "Restored!",
                            text: "",
                            type: "success",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "OK",
                            timer: 1000
                        }).then(function() {});
                    }
                }
            )
        }

        vm.deletePolicy = function(policyKey, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This action cannot be un-done",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        var root = firebase.database().ref();
                        root.child('policies/' + policyKey).remove();
                        vm.archivedPolicies.splice(index, 1);
                        $scope.$apply();
                        sweetAlert.swal({
                            title: "Deleted!",
                            text: "",
                            type: "success",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "OK",
                            timer: 1000
                        }).then(function() {});
                    }
                }
            )
        }

        vm.updateOrg = function(org, type) {
            if (type === 'buyer') {
                vm.buyerOrgs.$save(org).then(function() {
                    sweetAlert.swal({
                        title: "Success",
                        text: "",
                        type: "success",
                        timer: 1000
                    }).then(function() {});    
                });
            } else if (type === 'supplier') {
                vm.supplierOrgs.$save(org).then(function() {
                    sweetAlert.swal({
                        title: "Success",
                        text: "",
                        type: "success",
                        timer: 1000
                    }).then(function() {});    
                });
            }
        }

        vm.updateUser = function(user) {
            if (user.myOrg) {
                sweetAlert.swal({
                    title: "Org Alert!",
                    text: "This user also has an associated Org (ID: " + user.myOrg.orgKey + "). You might want to also de-authorize it to prevent Org-level notifications from going out.",
                    type: "warning"
                }).then(
                    function() {
                        vm.users.$save(user).then(function() {
                            sweetAlert.swal({
                                title: "Success",
                                text: "",
                                type: "success",
                                timer: 1000
                            }).then(function() {});
                        });
                    }
                )
            } else {
                vm.users.$save(user).then(function() {
                    sweetAlert.swal({
                        title: "Success",
                        text: "",
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                });
            }
        }

        vm.updateUserRole = function(user) {
            var data = {
                uid: user.uid,
                claims: {
                    role: user.role
                }
            };
            authService.setCustomClaims(data).then(function(response) {
                if (response.status === 'success') {
                    sweetAlert.swal({
                        title: "Success",
                        text: "",
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                    $scope.$apply();
                }
            });
        }

        vm.viewPolicy = function(pid) {
            $state.go('policy', { 'pid': pid });
        }

        vm.impersonateUser = function(uid) {
            var data = {
                admin: currentAuth.uid,
                uid: uid,
                active: true
            };
            authService.impersonateUser(data).then(function(response) {
                if (response.status === 'success') {
                    sweetAlert.swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                    vm.impersonatingUid = uid;
                    $scope.$apply();
                }
            });
        }
    }
    angular.module('yapp')
        .controller('AdminCtrl', AdminCtrl);
})();