(function() {
    'use strict';

    /* @ngInject */
    function PackViewCtrl($window, ENV, stdData, toastr, $scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, policyService, currentAuth, sweetAlert, notifyService, apiService, brandingDomain, sharedService) {
        var vm = this;

        // URL parsing to determine platform
        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        // Import standard data to data model
        vm.yesNo = stdData.yesNo;

        // Set some initial vars
        vm.hidePage = true;


        // Establish global Firebase RTDB root reference
        var root = firebase.database().ref();

        /**

██████╗  █████╗  ██████╗ ███████╗    ██╗      ██████╗  █████╗ ██████╗ 
██╔══██╗██╔══██╗██╔════╝ ██╔════╝    ██║     ██╔═══██╗██╔══██╗██╔══██╗
██████╔╝███████║██║  ███╗█████╗      ██║     ██║   ██║███████║██║  ██║
██╔═══╝ ██╔══██║██║   ██║██╔══╝      ██║     ██║   ██║██╔══██║██║  ██║
██║     ██║  ██║╚██████╔╝███████╗    ███████╗╚██████╔╝██║  ██║██████╔╝
╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
                                                                      
         * Description: Initial page load functions
         * 
         * authService.getCurrentUser() - Get the logged in user and determine if their account is in good standing
         * loadPage() - Get the Policy based on ID's in querystring params
         * 
         */

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }
            vm.userRole = vm.currentUser.profile.role;
            // Get and check permissions here...
            vm.user = $firebaseObject(root.child('users/' + vm.currentUser.auth.uid));
            vm.user.$loaded().then(function(user) {
                loadPage();
            });
        });

        function loadPage() {
            if ($state.params.pkid) {
                vm.packViewKey = $state.params.pkid;
                //vm.packViewRef = root.child('packViews').child(vm.packViewKey);
                vm.packViewRef = root.child('policies').child(vm.packViewKey);
                vm.packViewRef.once('value', function(snap) {
                    if (snap.exists()) {
                        vm.packView = snap.val();

                        // Validate access
                        if ((vm.platform === 'imanager' || vm.currentUser.profile.role === 'iBroker') && (!vm.currentUser.profile.myOrg || (vm.currentUser.profile.myOrg && !vm.packView[vm.currentUser.profile.myOrg.orgKey]))) {
                            // on iManager, everyone who will be viewing a Policy will have an Org
                            // and everyone's Org will be listed on the policy as having access (created by their Org)
                            vm.policy = undefined;
                            sweetAlert.swal({
                                title: "Error",
                                text: "The link you provided is not valid, or you do not have access to view this Policy.",
                                type: "error"
                            }).then(function() {
                                $state.go('inventory');
                            });
                        } else {
                            vm.hidePage = false;

                            vm.userOrgType = authService.determineUserOrgType(vm.currentUser.profile.role);

                            jQuery('body').off('ifUnchecked', 'input');
                            jQuery('body').off('ifChecked', 'input');
                            //adjustStatusImage();
                            updateLabels();
                            processNotifications();
                        }
                    } else {
                        sweetAlert.swal({
                            title: "Policy not found",
                            text: "Sorry, we couldn't find a Policy using that link. Sending you to your Inventory.",
                            type: "error",
                            showCancelButton: false
                        }).then(function(result) {
                            $state.go('inventory');
                        });
                    }
                }).catch(function(e) {
                    if (e.code === 'PERMISSION_DENIED') {
                        sweetAlert.swal({
                            title: "Permission Denied",
                            text: "You do not have access to view this Policy.",
                            type: "error",
                            showCancelButton: false
                        }).then(function(result) {
                            $state.go('inventory');
                        });
                    }
                });
            } else {
                vm.hidePage = false;
                //$state.go('inventory');
            }
        }

        /**
         * 
         * Description: Functions related to the Policy iteself
         * 
         * vm.editPolicy()
         * vm.editExtendedPolicy()
         * vm.resetApplicationForm()
         * vm.archivePolicy()
         * vm.saveLatestUpdate()
         * 
         */

        vm.editPolicy = function() {
            if (vm.policy.status.applicationFinished) {
                vm.showEditInstructions = true;
            } else if (vm.platform === 'imanager') {
                $state.go('caseIntake', { pid: vm.policyKey });
            } else {
                vm.loadForm(vm.policy.forms['application']);
            }
        }

        vm.editExtendedPolicy = function() {
            $state.go('application', { pid: vm.policyKey });
        }

        vm.resetApplicationForm = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "The previously submitted Application Form will only be recoverable by contacting LS Hub support. Are you sure you wish to proceed?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    if (!vm.policy.archivedForms) {
                        vm.policy.archivedForms = {};
                    }
                    vm.policy.archivedForms['application-' + moment()] = sharedService.cloneObject(vm.policy.forms['application']);
                    delete vm.policy.forms['application'].createdDtTm;
                    delete vm.policy.forms['application'].documentId;
                    delete vm.policy.forms['application'].publicGuid;
                    delete vm.policy.forms['application'].signMethod;
                    vm.policy.forms['application'].status = 'In Progress';
                    delete vm.policy.status.applicationFinished;
                    delete vm.policy.status.applicationSentForSignature;
                    vm.policy.status.display = {
                        status: 'In Progress',
                        step: 'Application',
                        text: 'Started'
                    };
                    var updates = {};
                    updates['archivedForms'] = vm.policy.archivedForms;
                    updates['forms'] = vm.policy.forms;
                    updates['status'] = vm.policy.status;
                    vm.policyRef.update(updates).then(function(ref) {
                        vm.showEditInstructions = false;
                        vm.toast('success', 'Application Form has been re-opened for editing');
                    });
                }
            });
        }

        vm.archivePolicy = function() {
            if (vm.isCreator) {
                sweetAlert.swal({
                    title: "Are you sure?",
                    text: "Deleted Policies will be archived for some time, but will be purged when resources are needed. See Administrator for Policy Restoration details.",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (!result.dismiss) {
                        vm.policy.isArchived = true;
                        vm.policy.publicNotes.forEach(note => {
                            delete note.$$hashKey;
                        });
                        vm.policy.private.adminNotes.forEach(note => {
                            delete note.$$hashKey;
                        });
                        vm.policyRef.update(vm.policy).then(function(ref) {
                            vm.toast('success', 'Policy has been Archived');
                            setTimeout(function() {
                                $state.go('inventory');
                            }, 1000);
                        });
                    }
                });
            } else {
                sweetAlert.swal({
                    title: "Sorry",
                    text: "You have to be the Policy creator to delete it.",
                    type: "error",
                    showCancelButton: false
                }).then(function(result) {});
            }
        }

        vm.saveLatestUpdate = function() {
            vm.latestStatusUpdated = false;
            vm.latestStatusAt = moment().unix() * 1000;
            if (!vm.policy.updateLog) {
                vm.policy.updateLog = [];
            }
            vm.policy.updateLog.push({
                action: 'Status Updated',
                updatedAt: firebase.database.ServerValue.TIMESTAMP,
                updatedBy: vm.currentUser.profile.displayName,
                updatedById: vm.currentUser.auth.uid
            });
            var updates = {};
            updates['latestStatusAt'] = firebase.database.ServerValue.TIMESTAMP;
            updates['latestStatus'] = vm.policy.latestStatus;
            updates['updateLog'] = vm.policy.updateLog;
            var data = {
                targets: [],
                policyKey: vm.policyKey,
                createdBy: vm.currentUser.profile.displayName,
                createdByRole: vm.currentUser.profile.role,
                note: vm.policy.statusSelection[0].status + ' - ' + vm.policy.latestStatus,
                LSHID: vm.policy.LSHID,
                action: 'Status Updated'
            };
            vm.policyRef.update(updates).then(function() {
                vm.toast('success', 'Latest Update successfully posted');
                sendNotification(data);
                $scope.$apply();
            }).catch(function(err) {
                vm.toast('error', err.message);
            });
        }

        /**

██████╗ ██╗   ██╗██╗   ██╗███████╗██████╗     ███████╗███████╗██╗     ███████╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗
██╔══██╗██║   ██║╚██╗ ██╔╝██╔════╝██╔══██╗    ██╔════╝██╔════╝██║     ██╔════╝██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║
██████╔╝██║   ██║ ╚████╔╝ █████╗  ██████╔╝    ███████╗█████╗  ██║     █████╗  ██║        ██║   ██║██║   ██║██╔██╗ ██║
██╔══██╗██║   ██║  ╚██╔╝  ██╔══╝  ██╔══██╗    ╚════██║██╔══╝  ██║     ██╔══╝  ██║        ██║   ██║██║   ██║██║╚██╗██║
██████╔╝╚██████╔╝   ██║   ███████╗██║  ██║    ███████║███████╗███████╗███████╗╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║
╚═════╝  ╚═════╝    ╚═╝   ╚══════╝╚═╝  ╚═╝    ╚══════╝╚══════╝╚══════╝╚══════╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
                                                                                                                      
         * Description: Functions related to Buyer selection
         * 
         * vm.clickBuyer()
         * vm.updateBuyer()
         * vm.submitToBuyers()
         * continueSendToBuyers()
         * 
         */

        vm.clickBuyer = function($event, buyer) {
            if ($event.target.tagName === 'INPUT') {
                $event.stopPropagation();
            } else if (!vm.editInfo) {
                if (vm.showDetail === buyer.buyerName) {
                    vm.showDetail = undefined;
                } else {
                    vm.showDetail = buyer.buyerName;
                }
            }
        }

        vm.updateBuyer = function(buyer, index) {
            delete buyer.editInfo;
            delete buyer.$$hashKey;
            vm.policyRef.child('buyers/' + index).update(buyer).then(function() {
                // For each fileCategory, check if there are matching files in vm.caseFiles. If so, add them to buyer's sharedCaseFiles node.
                var updates = {};
                if (vm.caseFiles) {
                    vm.caseFiles.forEach(function(caseFile) {
                        if (!caseFile.hiddenFromBuyers && buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(caseFile.category) > -1) {
                            var caseFileCopy = sharedService.cloneObject(caseFile);
                            delete caseFileCopy.$id;
                            delete caseFileCopy.$priority;
                            delete caseFileCopy.$$hashKey;
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = caseFileCopy;
                        } else {
                            // File is hidden or they don't have access to this category, set to null in case it exists
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = null;
                        }
                    });
                }
                root.update(updates).then(function() {
                    vm.toast('success', 'Buyer Updated');
                    vm.showSave = false;
                    $scope.$apply();
                });
            });
        }

        vm.submitToBuyers = function() {
            console.log(vm.selectedBuyers);
            var selectedBuyersPlural = (vm.selectedBuyers.length > 1) ? 's' : '';
            sweetAlert.swal({
                title: "Confirm Action",
                text: "The " + vm.selectedBuyers.length + " selected Buyer" + selectedBuyersPlural + " will be notified about this Policy by email, and it will show up in their Available Policies when they log into their iManager account. Click OK to proceed, or Cancel to adjust Buyer list",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#00B200",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    continueSendToBuyers();
                }
            });
        }

        function continueSendToBuyers() {
            vm.sendingToBuyers = true;

            vm.policy.status.outForOffers = true;
            vm.policy.status.outForOffersAt = firebase.database.ServerValue.TIMESTAMP;

            if (!vm.policy.buyers) {
                vm.policy.buyers = [];
            }

            vm.policyCopy = sharedService.cloneObject(vm.policy);

            // Prep the Policy Copy for distribution
            vm.policyCopy.rootKey = vm.policyKey;
            vm.policyCopy.parentKey = vm.policyKey;

            delete vm.policyCopy.status;
            delete vm.policyCopy.$$conf;
            delete vm.policyCopy.$id;
            delete vm.policyCopy.$priority;
            delete vm.policyCopy.$resolved;
            delete vm.policyCopy.createdBy;
            delete vm.policyCopy.createdAt;
            delete vm.policyCopy.createdById;
            delete vm.policyCopy.createdByOrgKey;
            delete vm.policyCopy.private;
            delete vm.policyCopy.caseFiles;
            delete vm.policyCopy.upline;
            delete vm.policyCopy.submittedTo;
            delete vm.policyCopy.updateLog;
            delete vm.policyCopy.lastUpdatedAt;
            delete vm.policyCopy.lastUpdatedBy;
            delete vm.policyCopy.lastUpdatedById;
            delete vm.policyCopy.forms.application;
            delete vm.policyCopy.buyers;
            delete vm.policyCopy.forms.medicalPrimary;
            delete vm.policyCopy.forms.medicalSecondary;
            delete vm.policyCopy.currentBene;
            delete vm.policyCopy.publicNotes;
            delete vm.policyCopy.archivedForms;
            delete vm.policyCopy.status;
            delete vm.policyCopy.policyConfirmed;
            delete vm.policyCopy.policyConfirmedAt;
            delete vm.policyCopy.policyConfirmedBy;
            delete vm.policyCopy.policyConfirmedById;

            // Set initial status for new Policy copy
            vm.policyCopy.status = {
                display: {
                    status: "Received",
                    step: "Received",
                    text: "Received"
                }
            };
            vm.policyCopy.status.receivedFromSupplier = true;
            vm.policyCopy.status.receivedFromSupplierAt = firebase.database.ServerValue.TIMESTAMP;


            // Set new created by/at info that next level will see
            vm.policyCopy.createdBy = vm.currentUser.profile.displayName;
            vm.policyCopy.createdById = vm.currentUser.auth.uid;
            vm.policyCopy.createdAt = firebase.database.ServerValue.TIMESTAMP;
            vm.policyCopy.buyerInterested = true;
            if (vm.currentUser.org) {
                vm.policyCopy.createdByOrgKey = vm.currentUser.org.$id;
                vm.policyCopy.createdByOrgName = vm.currentUser.org.name;
                vm.policyCopy.createdByOrgType = vm.currentUser.org.orgType;
            }

            // Keep insured.dateOfBirth, insured.gender, insured.firstName first initial of insured.lastName
            var primaryInsuredDataToKeep = {
                dateOfBirth: (vm.policy.insured && vm.policy.insured.dateOfBirth) ? vm.policy.insured.dateOfBirth : '',
                gender: (vm.policy.insured && vm.policy.insured.gender) ? vm.policy.insured.gender : '',
                firstName: (vm.policy.insured && vm.policy.insured.firstName) ? vm.policy.insured.firstName : '',
                lastNameInitial: (vm.policy.insured && vm.policy.insured.lastName) ? vm.policy.insured.lastName.charAt(0) : '',
                lastName: (vm.policy.insured && vm.policy.insured.lastName) ? vm.policy.insured.lastName : '',
                age: (vm.policy.insured && vm.policy.insured.age) ? vm.policy.insured.age : '',
                bmi: (vm.policy.insured && vm.policy.insured.bmi) ? vm.policy.insured.bmi : '',
                height: (vm.policy.insured && vm.policy.insured.height) ? vm.policy.insured.height : '',
                weight: (vm.policy.insured && vm.policy.insured.weight) ? vm.policy.insured.weight : '',
                bmiHistory: vm.policy.insured.bmiHistory ? vm.policy.insured.bmiHistory : null
            };
            delete vm.policyCopy.insured;
            vm.policyCopy.insured = primaryInsuredDataToKeep;

            if (vm.policy.insured.secondaryInsured && vm.policy.insured.secondaryInsured[0] === 'Yes') {
                var secondaryInsuredDataToKeep = {
                    dateOfBirth: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dateOfBirth) ? vm.policy.secondaryInsured.dateOfBirth : '',
                    gender: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.gender) ? vm.policy.secondaryInsured.gender : '',
                    firstName: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.firstName) ? vm.policy.secondaryInsured.firstName : '',
                    lastNameInitial: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.lastName) ? vm.policy.secondaryInsured.lastName.charAt(0) : '',
                    lastName: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.lastName) ? vm.policy.secondaryInsured.lastName : '',
                    age: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.age) ? vm.policy.secondaryInsured.age : '',
                    bmi: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.bmi) ? vm.policy.secondaryInsured.bmi : '',
                    height: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.height) ? vm.policy.secondaryInsured.height : '',
                    weight: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.weight) ? vm.policy.secondaryInsured.weight : '',
                    bmiHistory: vm.policy.secondaryInsured.bmiHistory ? vm.policy.secondaryInsured.bmiHistory : null
                };
                delete vm.policyCopy.secondaryInsured;
                vm.policyCopy.secondaryInsured = secondaryInsuredDataToKeep;
            } else {
                delete vm.policyCopy.secondaryInsured;
            }

            // Prep object for email notifications
            var recipients = [];

            $window.async.forEachOfSeries(vm.selectedBuyers, function(buyer, key, callback) {
                // Skip if Buyer already exists
                if (!vm.policyCopy[buyer.orgGuid]) {
                    // Add reference to new policy to determine buyer access
                    vm.policyCopy[buyer.orgGuid] = "buyer|" + (moment().unix() * 1000).toString();

                    // Generate new GUID for this Buyer/Policy copy
                    let policyGuid = uuidv4();

                    // Add private buyer specific data to the Policy
                    vm.policy.buyers.push({
                        orgGuid: buyer.orgGuid,
                        buyerName: buyer.name ? buyer.name : '',
                        publicGuid: policyGuid,
                        buyerPhone: buyer.phone ? buyer.phone : '',
                        buyerFax: buyer.fax ? buyer.fax : '',
                        buyerEmail: buyer.email ? buyer.email : '',
                        buyerAddress: buyer.address ? buyer.address : null,
                        fileCategoryAccess: buyer.fileCategoryAccess ? buyer.fileCategoryAccess : null,
                        buyerInterested: true,
                        buyerNotes: buyer.notes ? buyer.notes : '',
                        addedAt: moment().unix() * 1000,
                        addedBy: vm.currentUser.profile.displayName,
                        addedById: vm.currentUser.auth.uid
                    });

                    // Add selected Buyer to email notifications object
                    if (buyer.email) {
                        recipients.push({
                            email: buyer.email,
                            template: 'buyer-new-policy-notification',
                            validate: {
                                checkPath: 'orgs/public/' + buyer.orgKey,
                                checkKey: 'status',
                                checkVal: 'active',
                                resendLink: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + buyer.orgKey + '&ic='
                            },
                            subData: {
                                link: 'imanager.' + vm.domain + '.' + vm.tld + '/bdn/policy?pid=' + policyGuid + '&bid=' + buyer.orgGuid,
                                name: buyer.name,
                                email: buyer.email,
                                faceAmount: vm.policyCopy.faceAmount,
                                carrier: vm.policyCopy.carrier,
                                supplierName: vm.currentUser.org.name,
                                supplierEmail: vm.currentUser.org.email,
                                supplierPhone: vm.currentUser.org.phone
                            }
                        });
                    } else {
                        vm.toast('error', 'No email available for Buyer ' + buyer.name);
                    }

                    // Add default access to each case file
                    let buyerCaseFiles = [];
                    if (vm.caseFiles) {
                        vm.caseFiles.forEach(function(caseFile) {
                            if (buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(caseFile.category) > -1 && !caseFile.hiddenFromBuyers) {
                                var caseFileCopy = sharedService.cloneObject(caseFile);
                                delete caseFileCopy.$id;
                                delete caseFileCopy.$priority;
                                delete caseFileCopy.$$hashKey;
                                buyerCaseFiles.push(caseFileCopy);
                            }
                        });
                    }

                    let accessIndicator = {
                        'hasAccess': true,
                        'fromOrg': vm.policyCopy.createdByOrgKey ? vm.policyCopy.createdByOrgKey : null
                    };

                    var updates = {};
                    updates['privatePolicies/' + policyGuid] = vm.policyCopy;
                    updates['orgs/' + buyer.orgType + '/' + buyer.orgKey + '/policies/' + policyGuid] = accessIndicator;
                    updates['sharedCaseFiles/' + policyGuid] = buyerCaseFiles;
                    root.update(updates).then(function() {
                        callback();
                    }).catch(function(err) {
                        callback(err);
                    });
                }
            }, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Done adding all buyers');
                    vm.policyCopy = null;

                    // Save the new buyer list to the server
                    if (vm.policy.buyers) {
                        vm.policy.buyers.forEach(buyer => {
                            delete buyer.$$hashKey;
                        });
                    } else {
                        vm.policy.buyers = null;
                    }
                    vm.policyRef.child('buyers').update(vm.policy.buyers).then(function(ref) {
                        // Send email notifications to all Buyers
                        apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                            console.log(response);
                            if (response.status === 'success') {
                                vm.toast('success', 'All Buyers have been emailed');
                            } else {
                                vm.toast('error', 'Some emails failed, but the Policy is available to all Buyers');
                            }
                            // Update status & buyer list                        
                            vm.policy.status.display = {
                                status: 'Out for Offers',
                                step: 'Out for Offers',
                                text: 'Out for Offers'
                            };
                            vm.policy.status.outForOffers = true;
                            vm.policy.status.outForOffersAt = firebase.database.ServerValue.TIMESTAMP;
                            vm.policyRef.child('status').update(vm.policy.status).then(function(response) {
                                vm.pickBuyers = false;
                                vm.confirmDocumentAccess = false;
                                vm.selectedBuyers = [];
                                vm.sendToAllBuyers = false;
                            }).catch(function(err) {
                                console.log(err);
                                vm.sendingToBuyers = false;
                            });
                        });
                    }).catch(function(err) {
                        console.log(err);
                        vm.sendingToBuyers = false;
                    });
                }
            });
        }

        /**

██████╗  ██████╗ ██╗     ██╗ ██████╗██╗   ██╗    ███████╗██╗██╗     ███████╗███████╗
██╔══██╗██╔═══██╗██║     ██║██╔════╝╚██╗ ██╔╝    ██╔════╝██║██║     ██╔════╝██╔════╝
██████╔╝██║   ██║██║     ██║██║      ╚████╔╝     █████╗  ██║██║     █████╗  ███████╗
██╔═══╝ ██║   ██║██║     ██║██║       ╚██╔╝      ██╔══╝  ██║██║     ██╔══╝  ╚════██║
██║     ╚██████╔╝███████╗██║╚██████╗   ██║       ██║     ██║███████╗███████╗███████║
╚═╝      ╚═════╝ ╚══════╝╚═╝ ╚═════╝   ╚═╝       ╚═╝     ╚═╝╚══════╝╚══════╝╚══════╝
                                                                                    
         * Description: Functions supporting all Case File actions
         * 
         * $scope.myFiles() - Used for Case Files filtering in UI
         * vm.formUpload()
         * vm.loadForm() - For Requested Forms, clicking the icon will either load the in progress form, or open the completed form to view/download
         * vm.toggleFormDetails() - For Requested Forms, show/hide the tracking details
         * vm.fileChanged()
         * vm.doUpload()
         * vm.updateFileMetadata()
         * vm.downloadAll()
         * vm.confirmFileCategories()
         * vm.removeFile()
         * 
         */

        $scope.myFiles = function(file) {
            if (vm.isAdmin || file.visibleToSupply) {
                return true;
            } else {
                return false;
            }
        };

        vm.formUpload = function(type, className, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                vm.newFiles = [];
                vm.newFiles.push({
                    name: $event.target.files[i].name,
                    type: $event.target.files[i].type,
                    date: $event.target.files[i].lastModified,
                    file: $event.target.files[i]
                });
            }
            $scope.$apply();
            vm.doUpload(type, vm.newFiles, className);
        }

        vm.loadForm = function(form) {
            if (form.className === 'medicalPrimary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyKey, recipient: 'Primary Insured' });
            } else if (form.className === 'medicalSecondary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyKey, recipient: 'Secondary Insured' });
            } else if (form.className === 'application' && (form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('application', { pid: vm.policyKey });
            } else if (form.status === 'Complete') {
                window.open(form.downloadURL, '_blank');
            } else if (form.status === 'Pending Upload' && vm.platform === 'isubmit') {
                window.open(form.downloadURL, '_blank');
            } else if (form.status === 'Pending Upload' && vm.platform === 'imanager') {
                jQuery('#manual-file-' + form.className).trigger('click');
            } else {
                var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
                if (ENV === 'demo') {
                    server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
                } else if (ENV === 'dev') {
                    server = 'http://localhost:5000/lis-pipeline-dev/us-central1';
                }
                window.open(
                    server + '/viewDocument?documentId=' + form.documentId + '&email=' + encodeURIComponent(vm.currentUser.profile.email),
                    '_blank'
                );
            }
        }

        vm.toggleFormDetails = function(form, index, event) {
            if (vm.showFormDetails === index) {
                // Close the detail view
                vm.showFormDetails = -1;
                angular.element(document.querySelector('#' + form.className + '-container')).removeClass('show-detail');
            } else {
                vm.showFormDetails = index;
                angular.element(document.querySelectorAll('.show-detail')).removeClass('show-detail');
                angular.element(document.querySelector('#' + form.className + '-container')).addClass('show-detail');
            }
            event.stopPropagation();
        }

        vm.fileChanged = function(type, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                if (type === 'case') {
                    vm.newCaseFiles.push({
                        name: $event.target.files[i].name,
                        type: $event.target.files[i].type,
                        date: $event.target.files[i].lastModified,
                        file: $event.target.files[i]
                    });
                }
            }
            $scope.$apply();
            if (type === 'case') {
                vm.doUpload(type, vm.newCaseFiles, 'caseFile');
            }
        }

        vm.doUpload = function(type, files, className) {
            vm.filesToLoad = [];
            files.forEach(function(file) {
                vm.filesToLoad.push(file);
            });
            async.forEachOfSeries(vm.filesToLoad, function(fileObj, key, callback) {
                var metadata = {
                    contentType: fileObj.file.type,
                    customMetadata: {
                        'uploadedAt': moment(),
                        'uploadedBy': vm.currentUser.profile.displayName,
                        'uploadedById': vm.currentUser.auth.uid,
                        'uploadedByOrgKey': vm.currentUser.org ? vm.currentUser.org.$id : 'n/a',
                        'policyKey': vm.policyKey,
                        'policyGuid': null,
                        'policyNumber': vm.policy.policyNumber,
                        'formName': null,
                        'className': className
                    }
                };

                var storage = $firebaseStorage(vm.storageRef.child(type).child(fileObj.file.name));
                var uploadTask = storage.$put(fileObj.file, metadata);

                uploadTask.$progress(function(snapshot) {
                    if (snapshot.totalBytes > 0) {
                        fileObj.percentUploaded = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    } else {
                        fileObj.percentUploaded = 100;
                    }
                });

                uploadTask.$complete(function(snapshot) {
                    fileObj.link = snapshot.downloadURL;
                    fileObj.uploadedAt = firebase.database.ServerValue.TIMESTAMP;
                    fileObj.uploadedBy = vm.currentUser.profile.displayName;
                    fileObj.uploadedById = vm.currentUser.auth.uid;
                    fileObj.uploadedByOrgKey = vm.currentUser.org ? vm.currentUser.org.$id : null
                    if (!vm.isAdmin) {
                        fileObj.visibleToSupply = true;
                        fileObj.category = 'From ' + vm.currentUser.profile.displayName;
                    }
                    fileObj.hiddenFromBuyers = true;
                    fileObj.fromSupplier = true;

                    if (!vm.policy.updateLog) {
                        vm.policy.updateLog = [];
                    }

                    vm.policy.updateLog.push({
                        action: 'File uploaded to ' + type + ' tab: ' + fileObj.file.name,
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        updatedBy: vm.currentUser.profile.displayName,
                        updatedById: vm.currentUser.auth.uid
                    });

                    // Send notifications about this new file
                    var data = {
                        targets: [],
                        policyKey: vm.policyKey,
                        createdBy: vm.currentUser.profile.displayName,
                        createdByRole: vm.currentUser.profile.role,
                        note: 'New File has been uploaded: ' + fileObj.file.name,
                        LSHID: vm.policy.LSHID,
                        action: 'New File Uploaded'
                    };
                    sendNotification(data);

                    if (type === 'case') {
                        delete fileObj.$$hashKey;
                        vm.caseFiles.$add(fileObj).then(function(ref) {
                            vm.newCaseFiles.splice(fileObj, 1);
                            // STAMP THIS FILE
                            var stampData = {
                                orgKey: null,
                                policyKey: vm.policyKey,
                                policyGuid: null,
                                policyNumber: vm.policy.policyNumber,
                                className: className,
                                formName: null,
                                fileName: fileObj.file.name,
                                fileKey: ref.key,
                                fileType: type,
                                downloadURL: snapshot.downloadURL
                            };
                            policyService.stampFile(stampData).then(function(response) {
                                if (response.status === 'success') {
                                    var updates = {};
                                    updates['updateLog'] = vm.policy.updateLog;
                                    vm.policyRef.update(updates).then(function(ref) {
                                        callback();
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback();
                                    });
                                } else {
                                    console.log(response);
                                    callback();
                                }
                            });
                        });
                    } else if (type === 'requested') {
                        vm.policy.forms[className].downloadURL = fileObj.link;
                        vm.policy.forms[className].status = 'Complete';
                        vm.policy.forms[className].completedDtTm = firebase.database.ServerValue.TIMESTAMP;
                        vm.policy.status[className + 'Signed'] = true;
                        vm.policy.status[className + 'SignedAt'] = moment().unix() * 1000;

                        vm.policy.status.display.status = 'Complete';
                        vm.policy.status.display.text = 'Pending Broker Review';

                        vm.policy.brokerActionRequired = true;

                        // STAMP THIS FILE
                        var stampData = {
                            orgKey: null,
                            policyKey: vm.policyKey,
                            policyGuid: null,
                            policyNumber: vm.policy.policyNumber,
                            className: className,
                            formName: null,
                            fileName: fileObj.file.name,
                            fileKey: null,
                            fileType: type,
                            downloadURL: snapshot.downloadURL
                        };
                        policyService.stampFile(stampData).then(function(response) {
                            if (response.status === 'success') {
                                var updates = {};

                                updates['status'] = vm.policy.status;
                                updates['brokerActionRequired'] = true;
                                updates['updateLog'] = vm.policy.updateLog;
                                updates['forms/' + className] = vm.policy.forms[className];

                                vm.policyRef.update(updates).then(function(ref) {
                                    vm.toast('success', 'File uploaded to the server');

                                    adjustStatusImage();

                                    vm.newAdminNote = 'Completed Application has been uploaded for this Policy';
                                    vm.saveNewAdminNote();
                                    callback();
                                }).catch(function(err) {
                                    console.log(err);
                                    callback();
                                });
                            } else {
                                console.log(response);
                                callback();
                            }
                        });
                    }
                }, fileObj);
            }, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    vm.toast('success', 'All files uploaded to the server');
                }
            });
        }

        vm.updateFileMetadata = function(type, file, index) {
            var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
            var meta = {
                category: file.category
            };
            vm.caseFiles.$save(file).then(function(ref) {
                storage.$updateMetadata(meta).then(function(updatedMeta) {
                    vm.toast('success', 'File category updated');
                });
            });
        }

        vm.downloadAll = function(type) {
            var files = vm.caseFiles;

            if (!files || files.length === 0) {
                vm.toast('error', 'No files to download');
            } else {
                var zip = new JSZip();
                var count = 0;
                var zipFilename = vm.policy.policyNumber + ".zip";

                files.forEach(function(file) {
                    var filename = file.name;
                    // loading a file and add it in a zip file
                    JSZipUtils.getBinaryContent(file.link, function(err, data) {
                        if (err) {
                            throw err;
                        }
                        zip.file(filename, data, { binary: true });
                        count++;
                        if (count == files.length) {
                            zip.generateAsync({ type: "blob" })
                                .then(function(zipFile) {
                                    saveAs(zipFile, zipFilename);
                                });
                        }
                    });
                });
            }
        }

        vm.confirmFileCategories = function(buyer, index) {
            vm.selectedBuyers[index].fileCategoriesConfirmed = !vm.selectedBuyers[index].fileCategoriesConfirmed;
            if (vm.selectedBuyers[index].fileCategoriesConfirmed) {
                vm.fileCategoriesConfirmedCount++;
            } else {
                vm.fileCategoriesConfirmedCount--;
            }
            if (vm.fileCategoriesConfirmedCount === vm.selectedBuyers.length) {
                vm.allCategoriesConfirmed = true;
            } else {
                vm.allCategoriesConfirmed = false;
            }
        }

        vm.removeFile = function(type, file, index) {
            if (vm.isIMO || vm.isAdmin || file.uploadedById === vm.currentUser.auth.uid) {
                sweetAlert.swal({
                    title: "Are you sure?",
                    text: "Do you really want to remove this file? It will also be removed for any Buyers who currently have access to it",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (!result.dismiss) {
                        var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
                        var uploadTask = storage.$delete().then(function() {
                            if (!vm.policy.updateLog) {
                                vm.policy.updateLog = [];
                            }

                            vm.policy.updateLog.push({
                                action: 'File removed from ' + type + ' tab: ' + file.name,
                                updatedAt: firebase.database.ServerValue.TIMESTAMP,
                                updatedBy: vm.currentUser.profile.displayName,
                                updatedById: vm.currentUser.auth.uid
                            });

                            if (type === 'case') {
                                var fileId = file.$id;
                                vm.caseFiles.$remove(file).then(function() {
                                    // Also remove this file from any buyer who had access to it
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + fileId] = null;
                                        });
                                    }
                                    root.update(updates).then(function() {
                                        vm.policyRef.child('updateLog').update(vm.policy.updateLog).then(function() {
                                            vm.toast('success', 'File removed');
                                        });
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        }).catch(function(err) {
                            if (type === 'case') {
                                var fileId = file.$id;
                                vm.caseFiles.$remove(file).then(function() {
                                    // Also remove this file from any buyer who had access to it
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + fileId] = null;
                                        });
                                    }
                                    root.update(updates).then(function() {
                                        vm.policyRef.child('updateLog').update(vm.policy.updateLog).then(function() {
                                            vm.toast('success', 'File removed');
                                        });
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        });
                    }
                });
            }
        }

        /**

██████╗ ██╗   ██╗██████╗ ██╗     ██╗ ██████╗        ██╗    ██████╗ ██████╗ ██╗██╗   ██╗ █████╗ ████████╗███████╗    ███╗   ██╗ ██████╗ ████████╗███████╗███████╗
██╔══██╗██║   ██║██╔══██╗██║     ██║██╔════╝       ██╔╝    ██╔══██╗██╔══██╗██║██║   ██║██╔══██╗╚══██╔══╝██╔════╝    ████╗  ██║██╔═══██╗╚══██╔══╝██╔════╝██╔════╝
██████╔╝██║   ██║██████╔╝██║     ██║██║           ██╔╝     ██████╔╝██████╔╝██║██║   ██║███████║   ██║   █████╗      ██╔██╗ ██║██║   ██║   ██║   █████╗  ███████╗
██╔═══╝ ██║   ██║██╔══██╗██║     ██║██║          ██╔╝      ██╔═══╝ ██╔══██╗██║╚██╗ ██╔╝██╔══██║   ██║   ██╔══╝      ██║╚██╗██║██║   ██║   ██║   ██╔══╝  ╚════██║
██║     ╚██████╔╝██████╔╝███████╗██║╚██████╗    ██╔╝       ██║     ██║  ██║██║ ╚████╔╝ ██║  ██║   ██║   ███████╗    ██║ ╚████║╚██████╔╝   ██║   ███████╗███████║
╚═╝      ╚═════╝ ╚═════╝ ╚══════╝╚═╝ ╚═════╝    ╚═╝        ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═╝  ╚═╝   ╚═╝   ╚══════╝    ╚═╝  ╚═══╝ ╚═════╝    ╚═╝   ╚══════╝╚══════╝
                                                                                                                                                                
         * Description: Functions supporting the Public and Private/Admin Messaging panels
         * 
         * vm.saveNewPublicNote()
         * vm.removePublicNote()
         * vm.saveNewAdminNote()
         * vm.removeAdminNote()
         * 
         */

        vm.saveNewPublicNote = function() {
            if (!vm.policy.publicNotes) {
                vm.policy.publicNotes = [];
            }
            vm.policy.publicNotes.push({
                createdBy: vm.currentUser.profile.displayName,
                createdById: vm.currentUser.auth.uid,
                createdByRole: vm.currentUser.profile.role,
                createdAt: moment().unix() * 1000,
                note: vm.newPublicNote
            });
            vm.policy.publicNotes.forEach(note => {
                delete note.$$hashKey;
            });
            vm.policyRef.child('publicNotes').update(vm.policy.publicNotes).then(function() {
                vm.toast('success', 'Public Note saved');
                var data = {
                    targets: [],
                    policyKey: vm.policyKey,
                    createdBy: vm.currentUser.profile.displayName,
                    createdByRole: vm.currentUser.profile.role,
                    note: vm.newPublicNote,
                    LSHID: vm.policy.LSHID,
                    action: 'New Public Note',
                    isUrgent: true
                };
                if (vm.notifyDirection[0] === 'Everyone') {
                    // Add all policy chain Orgs that are not your own
                    for (let i = 0; i < (vm.policy.upline.length); i++) {
                        if (vm.policy.upline[i].orgKey) {
                            if ((vm.currentUser.profile.myOrg && vm.policy.upline[i].orgKey !== vm.currentUser.profile.myOrg.orgKey) || !vm.currentUser.profile.myOrg) {
                                data.targets.push({
                                    type: vm.policy[vm.policy.upline[i].orgKey],
                                    id: vm.policy.upline[i].orgKey
                                });
                            }
                        }
                    }
                    // Add the policy Creator if the policy was not created by an Org member and is not self
                    if (!vm.policy.createdByOrgKey && vm.policy.createdById !== vm.currentUser.auth.uid) {
                        data.targets.push({
                            type: 'user',
                            id: vm.policy.createdById
                        });
                    }
                } else if (vm.notifyDirection[0] === 'Upline') {
                    // Add all policy chain Orgs that are not your own and are higher than your own
                    let foundMyOrg = false;
                    // If you don't have an Org, you must be the Policy Creator. Check that, and set foundMyOrg to true so notification goes to all policy upline orgs
                    if (!vm.currentUser.profile.myOrg && vm.policy.createdById === vm.currentUser.auth.uid) {
                        foundMyOrg = true;
                    }
                    // Otherwise, Policy upline hierarchy is added in order, so loop and set foundMyOrg when found, then send to the rest above that
                    for (let i = 0; i < (vm.policy.upline.length); i++) {
                        if (vm.policy.upline[i].orgKey) {
                            if (foundMyOrg) {
                                data.targets.push({
                                    type: vm.policy[vm.policy.upline[i].orgKey],
                                    id: vm.policy.upline[i].orgKey
                                });
                            }
                            if ((vm.currentUser.profile.myOrg && vm.policy.upline[i].orgKey === vm.currentUser.profile.myOrg.orgKey)) {
                                foundMyOrg = true;
                            }
                        }
                    }
                } else if (vm.notifyDirection[0] === 'Downline') {
                    // Add all policy chain Orgs that are not your own and are higher than your own
                    let foundMyOrg = false;
                    // If you don't have an Org, you must be the Policy Creator. Check that, and set foundMyOrg to true so notification goes to all policy upline orgs
                    if (!vm.currentUser.profile.myOrg && vm.policy.createdById === vm.currentUser.auth.uid) {
                        foundMyOrg = true;
                    }
                    // Otherwise, Policy upline hierarchy is added in order, so loop REVERSE and set foundMyOrg when found, then send to the rest above that
                    for (let i = (vm.policy.upline.length - 1); i >= 0; i--) {
                        if (vm.policy.upline[i].orgKey) {
                            if (foundMyOrg) {
                                data.targets.push({
                                    type: vm.policy[vm.policy.upline[i].orgKey],
                                    id: vm.policy.upline[i].orgKey
                                });
                            }
                            if ((vm.currentUser.profile.myOrg && vm.policy.upline[i].orgKey === vm.currentUser.profile.myOrg.orgKey)) {
                                foundMyOrg = true;
                            }
                        }
                    }
                    // Add the policy Creator if the policy was not created by an Org member and is not self
                    if (!vm.policy.createdByOrgKey && vm.policy.createdById !== vm.currentUser.auth.uid) {
                        data.targets.push({
                            type: 'user',
                            id: vm.policy.createdById
                        });
                    }
                }
                sendNotification(data);
                vm.newPublicNote = '';
            });
        }

        vm.removePublicNote = function(index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this note?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.policy.publicNotes[index].isDeleted = true;
                    vm.policy.publicNotes.forEach(function(note) {
                        delete note.$$hashKey;
                    });
                    vm.policyRef.child('publicNotes').update(vm.policy.publicNotes).then(function() {
                        console.log('Note removed');
                        vm.toast('success', 'Public Note removed');
                    });
                }
            });
        }

        vm.saveNewAdminNote = function() {
            if (!vm.policy.private) {
                vm.policy.private = {};
            }
            if (!vm.policy.private.adminNotes) {
                vm.policy.private.adminNotes = [];
            }
            vm.policy.private.adminNotes.push({
                createdBy: vm.currentUser.profile.displayName,
                createdById: vm.currentUser.auth.uid,
                createdByRole: vm.currentUser.profile.role,
                createdAt: moment().unix() * 1000,
                note: vm.newAdminNote
            });
            vm.policy.private.adminNotes.forEach(note => {
                delete note.$$hashKey;
            });
            vm.policyRef.child('private/adminNotes').update(vm.policy.private.adminNotes).then(function() {
                var data = {
                    targets: [],
                    policyKey: vm.policyKey,
                    createdBy: vm.currentUser.profile.displayName,
                    createdByRole: vm.currentUser.profile.role,
                    note: vm.newAdminNote,
                    LSHID: vm.policy.LSHID,
                    action: 'New Internal Note',
                    isUrgent: true
                };
                data.targets.push({
                    type: vm.currentUser.org.orgType,
                    id: vm.currentUser.org.$id,
                    iBrokerBOFlag: (vm.notifyEntity && vm.notifyEntity[0] === 'Back Office') ? 'bo' : (vm.notifyEntity && vm.notifyEntity[0] === 'iBroker') ? 'ibroker' : null
                });
                sendNotification(data);
                vm.newAdminNote = '';
            });
        }

        vm.removeAdminNote = function(index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this note?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.policy.private.adminNotes[index].isDeleted = true;
                    vm.policy.private.adminNotes.forEach(note => {
                        delete note.$$hashKey;
                    });
                    vm.policyRef.child('private/adminNotes').update(vm.policy.private.adminNotes).then(function() {
                        vm.toast('success', 'Internal Note removed');
                    });
                }
            });
        }

        /**

███╗   ██╗ ██████╗ ████████╗██╗███████╗██╗ ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗███████╗
████╗  ██║██╔═══██╗╚══██╔══╝██║██╔════╝██║██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
██╔██╗ ██║██║   ██║   ██║   ██║█████╗  ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║███████╗
██║╚██╗██║██║   ██║   ██║   ██║██╔══╝  ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║ ╚████║╚██████╔╝   ██║   ██║██║     ██║╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝  ╚═══╝ ╚═════╝    ╚═╝   ╚═╝╚═╝     ╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                                                                                 
         * Description: Functions related to Policy Notifications
         * 
         * processNotifications() - Clear notifications targeted to the current user or user's Org for the current Policy
         * sendNotification() - Calls the NotifyService to actually send each notification to each target
         * 
         */

        function processNotifications() {
            // Query for Pack View notifications for the current user
            root.child('notifications').orderByChild('packViewKey').equalTo(vm.packViewKey).once('value', function(packViewNotifications) {
                if (packViewNotifications.exists()) {
                    let notificationsArr = [];
                    var clearedCount = 0;
                    let notifications = packViewNotifications.val();
                    Object.keys(notifications).forEach(x => {
                        if (notifications[x].isUnread) {
                            notificationsArr.push(notifications[x]);
                        }
                    });
                    async.forEachOfSeries(notificationsArr, function(notification, key, callback) {
                        //let notification = p.val();
                        if (notification.isUnread) {
                            if ((notification.targetType === 'user' && notification.targetId === vm.currentUser.auth.uid) || (notification.targetType !== 'user' && vm.currentUser.profile.myOrg && notification.targetId === vm.currentUser.profile.myOrg.orgKey)) {
                                var processNotification = true;
                                if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                    processNotification = false;
                                } else if (vm.currentUser.profile.role === 'Back Office' && notification.createdByRole !== 'iBroker') {
                                    processNotification = false;
                                }
                                if (processNotification) {
                                    // Clear this notification
                                    root.child('notifications/' + notification.notifyKey).update({
                                        isUnread: false,
                                        readBy: vm.currentUser.auth.uid,
                                        readAt: firebase.database.ServerValue.TIMESTAMP
                                    }).then(function() {
                                        clearedCount++;
                                        callback();
                                    });
                                } else {
                                    callback();
                                }
                            } else {
                                callback();
                            }
                        } else {
                            callback();
                        }
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else if (clearedCount > 0) {
                            vm.toast('success', 'Notifications cleared');
                        }
                    });
                }
            });
        }

        function sendNotification(data) {
            if (!data.targets || data.targets.length === 0) {
                // Add targets to notify
                // Add policy creator if not self and only if there is no creating org (creator will be included with the org notification)
                if (vm.policy.createdById !== vm.currentUser.auth.uid && !vm.policy.createdByOrgKey) {
                    data.targets.push({
                        type: 'user',
                        id: vm.policy.createdById
                    });
                }
                // Add org upline, not including own org, unless you are SAdmin or Back Office
                vm.policy.upline.forEach(function(uplineOrg) {
                    if ((vm.currentUser.profile.myOrg && uplineOrg.orgKey !== vm.currentUser.profile.myOrg.orgKey) || vm.currentUser.profile.role === 'SAdmin' || vm.currentUser.profile.role === 'Back Office') {
                        data.targets.push({
                            type: uplineOrg.orgType ? uplineOrg.orgType : 'org',
                            id: uplineOrg.orgKey
                        });
                    }
                });
            }
            if (data.targets && data.targets.length > 0) {
                data.notificationSubData = {
                    link: vm.subdomain + '.' + vm.domain + '.' + vm.tld + '/policy?pid=' + vm.policyKey,
                    linkRoot: vm.subdomain + '.' + vm.domain + '.' + vm.tld,
                    platform: vm.subdomain.indexOf('isubmit') > -1 ? 'iSubmit' : 'iManager',
                    headerImg: vm.subdomain.indexOf('isubmit') > -1 ? 'isubmit' : 'imanager'
                };
                notifyService.addNotification(data).then(function() {
                    vm.toast('success', 'Notifications sent');
                });
            }
        }

        /**

██████╗ ███████╗ ██████╗ ██████╗ ██████╗ ██████╗     ██████╗ ███████╗ ██████╗ ██╗   ██╗███████╗███████╗████████╗███████╗
██╔══██╗██╔════╝██╔════╝██╔═══██╗██╔══██╗██╔══██╗    ██╔══██╗██╔════╝██╔═══██╗██║   ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝
██████╔╝█████╗  ██║     ██║   ██║██████╔╝██║  ██║    ██████╔╝█████╗  ██║   ██║██║   ██║█████╗  ███████╗   ██║   ███████╗
██╔══██╗██╔══╝  ██║     ██║   ██║██╔══██╗██║  ██║    ██╔══██╗██╔══╝  ██║▄▄ ██║██║   ██║██╔══╝  ╚════██║   ██║   ╚════██║
██║  ██║███████╗╚██████╗╚██████╔╝██║  ██║██████╔╝    ██║  ██║███████╗╚██████╔╝╚██████╔╝███████╗███████║   ██║   ███████║
╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝     ╚═╝  ╚═╝╚══════╝ ╚══▀▀═╝  ╚═════╝ ╚══════╝╚══════╝   ╚═╝   ╚══════╝
                                                                                                                        
         * Description: iManager Record Request Functions
         * 
         * vm.requestMedicalSummary()
         * vm.requestIllustration()
         * vm.requestMedicalRecords()
         * 
         */

        vm.requestMedicalSummary = function() {
            firebase.database().ref().child('policies/' + vm.policyKey + '/caseFiles').orderByChild('category').equalTo('Medical Records').once('value', function(snap) {
                if (!snap.exists()) {
                    sweetAlert.swal({
                        title: "Missing Medical Records",
                        text: "Please make sure that you have uploaded at least one set of Medical Records to the 'Case Files' tab and categorized that file as 'Medical Records'",
                        type: "error"
                    }).then(function() {});
                } else {
                    vm.policy.status.medicalSummaryRequested = true;
                    updateStatus();
                }
            });
        }

        vm.requestIllustration = function() {
            sweetAlert.swal({
                title: "Confirm Action",
                html: "This will send a message to LS Hub's Back Office Support requesting new or updated Illustrations for this Policy. Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    sweetAlert.swal({
                        title: "Add Instructions",
                        html: "Provide any details you want us to know about this request",
                        type: "question",
                        input: "textarea",
                        showCancelButton: true,
                        cancelButtonText: "Cancel",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Send",
                        closeOnConfirm: false
                    }).then(function(result) {
                        if (result.value) {
                            vm.newPublicNote = "The Policy creator has requested new or updated Illustrations for this Policy with the following note: " + result.value;
                            vm.saveNewPublicNote();
                            sweetAlert.swal({
                                title: "Illustrations Requested",
                                text: "You will receive a notification regarding this order soon",
                                type: "success",
                                timer: 1500
                            }).then(function() {});
                        }
                    });
                }
            });
        }

        vm.requestMedicalRecords = function() {
            sweetAlert.swal({
                title: "Confirm Action",
                html: "This will send a message to LS Hub's Back Office Support requesting new or updated Medical Records for this Policy. Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    sweetAlert.swal({
                        title: "Add Instructions",
                        html: "Provide any details you want us to know about this request",
                        type: "question",
                        input: "textarea",
                        showCancelButton: true,
                        cancelButtonText: "Cancel",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Send",
                        closeOnConfirm: false
                    }).then(function(result) {
                        if (result.value) {
                            vm.newPublicNote = "The Policy creator has requested new or updated Medical Records for this Policy with the following note: " + result.value;
                            vm.saveNewPublicNote();
                            sweetAlert.swal({
                                title: "Medical Records Requested",
                                text: "You will receive a notification regarding this order soon",
                                type: "success",
                                timer: 1500
                            }).then(function() {});
                        }
                    });
                }
            });
        }

        /**

██████╗  ██████╗ ██╗     ██╗ ██████╗██╗   ██╗    ███████╗██╗  ██╗██████╗ ███████╗███╗   ██╗███████╗███████╗███████╗
██╔══██╗██╔═══██╗██║     ██║██╔════╝╚██╗ ██╔╝    ██╔════╝╚██╗██╔╝██╔══██╗██╔════╝████╗  ██║██╔════╝██╔════╝██╔════╝
██████╔╝██║   ██║██║     ██║██║      ╚████╔╝     █████╗   ╚███╔╝ ██████╔╝█████╗  ██╔██╗ ██║███████╗█████╗  ███████╗
██╔═══╝ ██║   ██║██║     ██║██║       ╚██╔╝      ██╔══╝   ██╔██╗ ██╔═══╝ ██╔══╝  ██║╚██╗██║╚════██║██╔══╝  ╚════██║
██║     ╚██████╔╝███████╗██║╚██████╗   ██║       ███████╗██╔╝ ██╗██║     ███████╗██║ ╚████║███████║███████╗███████║
╚═╝      ╚═════╝ ╚══════╝╚═╝ ╚═════╝   ╚═╝       ╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝╚═╝  ╚═══╝╚══════╝╚══════╝╚══════╝
                                                                                                                                                                                                                   
         * Description: Functions related to the "Expenses" section of the Policy (back office only)
         * 
         * vm.addNewExpense()
         * vm.saveExpense()
         * vm.editExpense()
         * vm.cancelExpense()
         * vm.removeExpense()
         * 
         */

        vm.addNewExpense = function() {
            vm.addExpense = true;
            vm.newExpenseDate = moment().format('MM-DD-YYYY');
        }

        vm.saveExpense = function() {
            if (vm.newExpenseItem && vm.newExpenseAmount) {
                if (!vm.policy.private.expenses) {
                    vm.policy.private.expenses = [];
                    vm.policy.private.totalExpenses = 0;
                }
                var expense = {
                    item: vm.newExpenseItem,
                    amount: parseFloat(vm.newExpenseAmount),
                    dateOfExpense: vm.newExpenseDate,
                    description: vm.newExpenseDescription ? vm.newExpenseDescription : ''
                };

                if (!vm.policy.updateLog) {
                    vm.policy.updateLog = [];
                }

                if (vm.editingExpenseIdx > -1) {
                    // update the existing Expense
                    vm.policy.private.totalExpenses -= vm.policy.private.expenses[vm.editingExpenseIdx].amount;
                    vm.policy.private.expenses[vm.editingExpenseIdx] = expense;
                    vm.policy.private.totalExpenses += expense.amount;
                    vm.policy.updateLog.push({
                        action: 'Updated an existing Expense',
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        updatedBy: vm.currentUser.profile.displayName,
                        updatedById: vm.currentUser.auth.uid
                    });
                } else {
                    // push the new Expense
                    vm.policy.private.expenses.push(expense);
                    vm.policy.private.totalExpenses += expense.amount;
                    vm.policy.updateLog.push({
                        action: 'Added a new Expense',
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        updatedBy: vm.currentUser.profile.displayName,
                        updatedById: vm.currentUser.auth.uid
                    });
                }

                vm.policy.private.expenses.forEach(function(expense) {
                    delete expense.$$hashKey;
                });

                var updates = {};
                updates['private/expenses'] = vm.policy.private.expenses;
                updates['private/totalExpenses'] = vm.policy.private.totalExpenses;
                updates['updateLog'] = vm.policy.updateLog;

                vm.policyRef.update(updates).then(function() {
                    vm.cancelExpense();
                    vm.toast('success', 'Policy Updated');
                });
            } else {
                sweetAlert.swal({
                    title: "Error",
                    text: "You must provide at least an Item and Amount to save an Expense",
                    type: "warning"
                }).then(function() {});
            }
        }

        vm.editExpense = function(expense) {
            var idx = vm.policy.private.expenses.indexOf(expense);
            if (idx > -1) {
                vm.editingExpenseIdx = idx;
            }
            vm.newExpenseItem = expense.item;
            vm.newExpenseAmount = expense.amount;
            vm.newExpenseDate = expense.dateOfExpense;
            vm.newExpenseDescription = expense.description;
            vm.addExpense = true;
        }

        vm.cancelExpense = function() {
            vm.addExpense = false;
            vm.newExpenseItem = '';
            vm.newExpenseAmount = '';
            vm.newExpenseDate = '';
            vm.newExpenseDescription = '';
            vm.editingExpenseIdx = undefined;
            jQuery('#newExpense .placeHolder.active').each(function(ele) { jQuery(this).removeClass('active'); });
        }

        vm.removeExpense = function(expense) {
            sweetAlert.swal({
                title: "Confirm Action",
                text: "Are you sure you want to remove this Expense?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    var idx = vm.policy.private.expenses.indexOf(expense);
                    if (idx > -1) {
                        vm.policy.private.expenses.splice(idx, 1);
                        vm.policy.private.totalExpenses -= expense.amount;
                        vm.policy.private.expenses.forEach(function(expense) {
                            delete expense.$$hashKey;
                        });
                        var updates = {};
                        updates['private/expenses'] = vm.policy.private.expenses;
                        updates['private/totalExpenses'] = vm.policy.private.totalExpenses;
                        if (!vm.policy.updateLog) {
                            vm.policy.updateLog = [];
                        }
                        vm.policy.updateLog.push({
                            action: 'Removed an Expense',
                            updatedAt: firebase.database.ServerValue.TIMESTAMP,
                            updatedBy: vm.currentUser.profile.displayName,
                            updatedById: vm.currentUser.auth.uid
                        });
                        updates['updateLog'] = vm.policy.updateLog;
                        vm.policyRef.update(updates).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    }
                }
            });
        }

        /**
         
 █████╗  ██████╗████████╗██╗ ██████╗ ███╗   ██╗    ██████╗ ███████╗ ██████╗ ██╗   ██╗███████╗███████╗████████╗███████╗
██╔══██╗██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║    ██╔══██╗██╔════╝██╔═══██╗██║   ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝
███████║██║        ██║   ██║██║   ██║██╔██╗ ██║    ██████╔╝█████╗  ██║   ██║██║   ██║█████╗  ███████╗   ██║   ███████╗
██╔══██║██║        ██║   ██║██║   ██║██║╚██╗██║    ██╔══██╗██╔══╝  ██║▄▄ ██║██║   ██║██╔══╝  ╚════██║   ██║   ╚════██║
██║  ██║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║    ██║  ██║███████╗╚██████╔╝╚██████╔╝███████╗███████║   ██║   ███████║
╚═╝  ╚═╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝    ╚═╝  ╚═╝╚══════╝ ╚══▀▀═╝  ╚═════╝ ╚══════╝╚══════╝   ╚═╝   ╚══════╝
                                                                                                                      
         * Description: Functions related to requests for action on the Policy - these are generally urgent and trigger email notifications
         * 
         * vm.requestBackOfficeProcessing() - Adds Policy to Back Office queue for processing
         * vm.requestBrokerReview() - Adds Policy to Broker queue for review after Back Office processing
         * vm.setActionRequired() - Turns Agent, Broker, or Back Office request flags on or off
         * 
         */

        vm.requestBackOfficeProcessing = function() {
            sweetAlert.swal({
                title: "Process this Policy?",
                text: "This Policy will be sent to the Back Office for processing and will likely result in charges. Are you sure it's ready to send?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.dismiss) {
                    // do nothing
                } else {
                    vm.policy.boActionRequired = true;
                    vm.policy.status.sentToBackOffice = true;
                    vm.policy.status.sentToBackOfficeAt = firebase.database.ServerValue.TIMESTAMP;
                    // Add default Admin Note here for Back Office
                    vm.newAdminNote = 'This Policy has been reviewed and is ready for processing. Thank you!';
                    vm.saveNewAdminNote();
                    vm.policy.status.display.text = 'In Progress';
                    updateStatus();
                }
            });
        }

        vm.requestBrokerReview = function() {
            sweetAlert.swal({
                title: "Finished with this Policy?",
                html: "This Policy will be sent to the Broker for review and submission to buyers. Please make sure all applicable documents have been uploaded.<br><br>Are you sure it's ready to send?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.dismiss) {
                    // do nothing
                } else {
                    vm.policy.status.submittedToBroker = true;
                    vm.policy.status.submittedToBrokerAt = firebase.database.ServerValue.TIMESTAMP;
                    vm.newAdminNote = 'The Back Office has finished all requested due diligence for this Policy. It is ready for review and submission to the Buyer Network. Thank you!';
                    vm.saveNewAdminNote();
                    vm.policy.status.display.text = 'Pending Broker Review';
                    updateStatus();
                }
            });
        }

        vm.setActionRequired = function(type, status) {
            if (type === 'agent' && status === 'on') {
                sweetAlert.swal({
                    title: "Add a Public Note",
                    text: "Please add a Note so the Agent knows what to do",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.actionRequired = true;
                        // Save Public Note
                        vm.newPublicNote = response.value;
                        vm.notifyDirection = ['Downline'];
                        vm.saveNewPublicNote();
                        updateStatus();
                    } else {
                        vm.policy.actionRequired = false;
                        $scope.$apply();
                    }
                });
            } else if (type === 'back-office' && status === 'on') {
                sweetAlert.swal({
                    title: "Add an Internal Note",
                    text: "Please add a Note so the Back Office knows what you need",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.boActionRequired = true;
                        // Save Private Note
                        vm.newAdminNote = response.value;
                        vm.saveNewAdminNote();
                        updateStatus();
                    } else {
                        vm.policy.boActionRequired = false;
                        $scope.$apply();
                    }
                });
            } else if (type === 'broker' && status === 'on') {
                sweetAlert.swal({
                    title: "Add an Internal Note",
                    text: "Please add a Note so the Broker knows what you need",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.brokerActionRequired = true;
                        // Save Private Note
                        vm.newAdminNote = response.value;
                        vm.saveNewAdminNote();
                        updateStatus();
                    } else {
                        vm.policy.brokerActionRequired = false;
                        $scope.$apply();
                    }
                });
            } else if (type === 'back-office' && status === 'off') {
                vm.policy.boActionRequired = false;
                updateStatus();
            } else if (type === 'agent' && status === 'off') {
                vm.policy.actionRequired = false;
                updateStatus();
            } else if (type === 'broker' && status === 'off') {
                vm.policy.brokerActionRequired = false;
                updateStatus();
            }
        }

        /**

███████╗███████╗████████╗     ██████╗ ███╗   ██╗    ██████╗ ███████╗██╗  ██╗ █████╗ ██╗     ███████╗     ██████╗ ███████╗
██╔════╝██╔════╝╚══██╔══╝    ██╔═══██╗████╗  ██║    ██╔══██╗██╔════╝██║  ██║██╔══██╗██║     ██╔════╝    ██╔═══██╗██╔════╝
███████╗█████╗     ██║       ██║   ██║██╔██╗ ██║    ██████╔╝█████╗  ███████║███████║██║     █████╗      ██║   ██║█████╗  
╚════██║██╔══╝     ██║       ██║   ██║██║╚██╗██║    ██╔══██╗██╔══╝  ██╔══██║██╔══██║██║     ██╔══╝      ██║   ██║██╔══╝  
███████║███████╗   ██║       ╚██████╔╝██║ ╚████║    ██████╔╝███████╗██║  ██║██║  ██║███████╗██║         ╚██████╔╝██║     
╚══════╝╚══════╝   ╚═╝        ╚═════╝ ╚═╝  ╚═══╝    ╚═════╝ ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝          ╚═════╝ ╚═╝     
                                                                                                                         
         * Description: Functions related to the "Set On Behalf Of" Feature

         * vm.setOnBehalfOf() - Retrieves the user's downline members to choose from
         * setMemberArr() - Formats retrieved downline for use in the <select> list
         * vm.continueSetOnBehalfOf() - Validate the selected user and determine proper upline
         * finalizeSetOnBehalfOf() - Update the Policy
         * 
         */

        vm.setOnBehalfOf = function() {
            vm.settingCreatedOnBehalfOf = true;
            vm.gettingDownlineList = true;
            vm.seatMembers = [];
            if (vm.currentUser.org) {
                // Step 1: Prompt to pick a user from /orgs/seats/[orgKey], showing "displayName" and "email" fields from that list
                policyService.getFullDownline().then(function(response) {
                    if (response.status === 'success') {
                        setMemberArr(response);
                    } else {
                        authService.setAuthHeader().then(function() {
                            policyService.getFullDownline().then(function(response) {
                                if (response.status === 'success') {
                                    setMemberArr(response);
                                } else {
                                    console.log(err);
                                }
                            }).catch(function(err) {
                                console.log(err);
                            });
                        });
                    }
                }).catch(function(err) {
                    console.log(err);
                });
            }
        }

        function setMemberArr(response) {
            response.data.forEach(function(member) {
                if (member.userRef !== vm.currentUser.auth.uid) {
                    vm.seatMembers.push({
                        name: member.displayName,
                        company: member.company,
                        role: member.role,
                        userRef: member.userRef
                    });
                }
            });
            vm.gettingDownlineList = false;
            $scope.$apply();
        }

        vm.continueSetOnBehalfOf = function() {
            // Step 2: Send selected user to the server, along with current user orgKey and orgType
            //         Server checks /users/[uid]/myUpline for selected user, and returns an array of all orgKey/orgType/[upline path] where the current user's Org is part of that Org's upline
            var dataToSend = {
                selectedUid: vm.onBehalfOfPick.userRef,
                requestorOrgKey: vm.currentUser.org.$id,
                requestorOrgType: vm.currentUser.org.orgType
            };
            policyService.getValidUplineOrgChains(dataToSend).then(function(response) {
                if (response.status === 'success') {
                    // Step 3: If the selected user has more than one upline returned, prompt to select which upline org the policy should be associated with
                    if (response.data.length > 1) {
                        // TODO: Set up a test for this prompt
                        finalizeSetOnBehalfOf(response.data[0], response.ownOrg);
                    } else {
                        finalizeSetOnBehalfOf(response.data[0], response.ownOrg);
                    }
                } else {
                    console.log(response.message);
                    vm.toast('error', 'Problem getting upline org chain');
                }
            });
        }

        function finalizeSetOnBehalfOf(upline, ownOrg) {
            var updates = {};
            // Step 4: Add each orgKey in upline to the policy with the orgType as the value
            upline.forEach(function(org) {
                vm.policy[org.orgKey] = org.orgType;
                updates[org.orgKey] = org.orgType;
            });
            // Step 5: Using the selected Org and upline path, set relevant fields on vm.policy
            vm.policy.actualCreatedBy = vm.policy.createdBy;
            vm.policy.actualCreatedById = vm.policy.createdById;
            vm.policy.actualCreatedByOrgKey = vm.policy.createdByOrgKey ? vm.policy.createdByOrgKey : null;
            vm.policy.actualCreatedByOrgType = vm.policy.createdByOrgType ? vm.policy.createdByOrgType : null;
            vm.policy.createdBy = vm.onBehalfOfPick.name;
            vm.policy.createdById = vm.onBehalfOfPick.userRef;
            vm.policy.createdByOrgKey = ownOrg.orgKey ? ownOrg.orgKey : null;
            vm.policy.createdByOrgType = ownOrg.orgType ? ownOrg.orgType : null;
            vm.policy.upline = upline;
            // Step 6: Push new action to updateLog (add action = "Assigned the Policy to another user's behalf")
            if (!vm.policy.updateLog) {
                vm.policy.updateLog = [];
            }
            vm.policy.updateLog.push({
                action: 'Set Policy as Submitted on behalf of ' + vm.onBehalfOfPick.displayName,
                updatedAt: firebase.database.ServerValue.TIMESTAMP,
                updatedBy: vm.currentUser.profile.displayName,
                updatedById: vm.currentUser.auth.uid
            });
            updates['createdBy'] = vm.policy.createdBy;
            updates['createdById'] = vm.policy.createdById;
            updates['createdByOrgKey'] = vm.policy.createdByOrgKey;
            updates['createdByOrgType'] = vm.policy.createdByOrgType;
            updates['actualCreatedBy'] = vm.policy.actualCreatedBy;
            updates['actualCreatedById'] = vm.policy.actualCreatedById;
            updates['actualCreatedByOrgKey'] = vm.policy.actualCreatedByOrgKey;
            updates['actualCreatedByOrgType'] = vm.policy.actualCreatedByOrgType;
            updates['upline'] = vm.policy.upline;
            updates['updateLog'] = vm.policy.updateLog;
            vm.policyRef.update(updates).then(function(ref) {
                // Step 7: Add policy for each upline to 'orgs/[orgType]/[orgKey]/policies', setting hasAccess to true
                async.forEachOfSeries(upline, function(org, key, callback) {
                    firebase.database().ref().child('orgs/' + org.orgType + '/' + org.orgKey + '/policies/' + vm.policyKey).update({
                        hasAccess: true
                    }).then(function() {
                        callback();
                    }).catch(function(err) {
                        callback(err);
                    });
                }, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        vm.toast('success', 'Policy has been updated to reflect new submission user');
                    }
                });
            });
        }

        /**

██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗     ███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗    ██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝    █████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗    ██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║  ██║███████╗███████╗██║     ███████╗██║  ██║    ██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝    ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                                                                                                              
         * Description: Miscellaneous functions to support other features
         * 
         * vm.toast() - Call the toastr service to display popup notification
         * updateLabels() - Handle the jvFloat labels, iCheck inputs and Flatpickr date pickers
         * vm.showMessage() - SWAL popup response to clicking "?" icons in UI
         * vm.calculateAge() - Determines age in years giving a date of birth
         * uuidv4() - Returns an adequately random GUID for Buyer Policy copies
         * vm.onSelectCallback() - Function called after updating a <select> in the UI
         * adjustStatusImage() - Updates the Policy status image in the top right of the UI
         * updateStatus() - Updates the Policy "status" node, including the display text
         * vm.goTo() - Navigate to a new route
         * 
         */

        function adjustStatusImage() {
            vm.statusImage = 'Timeline';
            if (vm.policy.status.applicationSigned) {
                vm.statusImage += '_1';
            }
            if (vm.policy.status.illustrationsReceived) {
                if (vm.policy.status.illustrationsOrdered) {
                    vm.statusImage += '_2B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.illustrationsReceived = false;
                }
            } else if (vm.policy.status.illustrationsOrdered) {
                vm.statusImage += '_2A';
            }
            if (vm.policy.status.medicalsReceived) {
                if (vm.policy.status.medicalsOrdered) {
                    vm.statusImage += '_3B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.medicalsReceived = false;
                }
            } else if (vm.policy.status.medicalsOrdered) {
                vm.statusImage += '_3A';
            }
            if (vm.policy.status.medicalSummaryReceived) {
                if (!vm.policy.status.medicalSummaryRequested && vm.platform === 'isubmit') {
                    vm.policy.status.medicalSummaryReceived = false;
                }
            }
            if (vm.policy.status.outForOffers) {
                if (vm.policy.status.illustrationsReceived && vm.policy.status.medicalsReceived) {
                    if (vm.policy.status.offerInterest) {
                        vm.statusImage = 'Timeline_4Int';
                    } else if (vm.policy.status.offerNoInterest) {
                        vm.statusImage = 'Timeline_4NoInt';
                    } else {
                        vm.statusImage = 'Timeline_4A';
                    }
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.outForOffers = false;
                }
            }
            if (vm.policy.status.offerReceived) {
                if (vm.policy.status.outForOffers) {
                    vm.statusImage = 'Timeline_4B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.offerReceived = false;
                }
            }
        }

        function updateStatus() {
            vm.policy.status.display.text = 'Started';
            if (vm.policy.status.applicationSigned) {
                vm.policy.status.display.text = 'In Progress';
            }
            if (vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Out for Offers';
            }
            if (vm.policy.status.offerNoInterest) {
                vm.policy.status.display.text = 'Case Rejected';
            }
            if (vm.policy.status.offerReceived) {
                vm.policy.status.display.text = 'Offer Received';
            }
            Object.keys(vm.policy.forms).forEach(function(form) {
                if (vm.policy.forms[form].status === 'Pending Signatures') {
                    vm.policy.status.display.text = 'Pending Signatures';
                } else if (vm.policy.forms[form].status === 'Pending 3rd Party') {
                    vm.policy.status.display.text = 'Pending 3rd Party';
                } else if (vm.policy.forms[form].status === 'Needs Review') {
                    vm.policy.status.display.text = 'Pending Agent Review';
                }
            });
            var updates = {};
            if (vm.platform === 'isubmit') {
                if (vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Agent Action Required';
                    updates['actionRequired'] = true;
                } else {
                    updates['actionRequired'] = null;
                }
                if (vm.policy.boActionRequired) {
                    vm.policy.status.display.text = 'Back Office Action Required';
                    delete vm.policy.status.submittedToBroker;
                    updates['boActionRequired'] = true;
                } else {
                    updates['boActionRequired'] = null;
                }
                if (vm.policy.brokerActionRequired) {
                    vm.policy.status.display.text = 'Broker Action Required';
                    updates['brokerActionRequired'] = true;
                } else {
                    updates['brokerActionRequired'] = null;
                }
                if (vm.policy.status.submittedToBroker && !vm.policy.status.outForOffers && !vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Pending Broker Review';
                }
                if (!vm.policy.status.sentToBackOffice && !vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Pending Broker Review';
                }
            }
            updates['status'] = vm.policy.status;
            vm.policyRef.update(updates).then(function(ref) {
                vm.toast('success', 'Status updated');
            });
        }

        vm.goTo = function(route) {
            $state.go(route);
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    defaultDate: "today",
                    maxDate: new Date(),
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.policy[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = vm.policy[parts[0]][parts[1]];
                        } else if (parts.length === 3) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]];
                        } else if (parts.length === 4) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]][parts[3]];
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('body').on('ifChecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'statusSelection') {
                        delete vm.policy.statusSelection[0].$$hashKey;
                        vm.policy.latestStatusAt = moment().unix() * 1000;
                        if (!vm.policy.updateLog) {
                            vm.policy.updateLog = [];
                        }
                        vm.policy.updateLog.push({
                            action: 'Status Updated',
                            updatedAt: firebase.database.ServerValue.TIMESTAMP,
                            updatedBy: vm.currentUser.profile.displayName,
                            updatedById: vm.currentUser.auth.uid
                        });
                        var updates = {};
                        updates['latestStatusAt'] = firebase.database.ServerValue.TIMESTAMP;
                        updates['statusSelection'] = vm.policy.statusSelection;
                        updates['updateLog'] = vm.policy.updateLog;
                        var data = {
                            targets: [],
                            policyKey: vm.policyKey,
                            createdBy: vm.currentUser.profile.displayName,
                            createdByRole: vm.currentUser.profile.role,
                            note: vm.policy.statusSelection[0].status + ' - ' + vm.policy.latestStatus,
                            LSHID: vm.policy.LSHID,
                            action: 'Status Updated'
                        };
                        vm.policyRef.update(updates).then(function() {
                            vm.toast('success', 'Policy Status successfully set');
                            sendNotification(data);
                        }).catch(function(err) {
                            vm.toast('error', err.message);
                        });
                    } else if (event.target.name === 'visibleToSupply') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        sweetAlert.swal({
                            title: "Confirm Action",
                            html: "This will remove access to this file from any Buyers who would otherwise be able to see files of this category. Are you sure?",
                            type: "question",
                            showCancelButton: true,
                            cancelButtonText: "No",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }).then(function(result) {
                            if (!result.dismiss) {
                                vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function() {
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        // TODO: Remove existing access to this file from any existing Buyers
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.caseFiles[fileIdx].$id] = null;
                                        });
                                        root.update(updates).then(function() {
                                            sweetAlert.swal({
                                                title: "Success",
                                                text: "File access has been removed from all Buyers",
                                                type: "success",
                                                timer: 1000
                                            }).then(function() {});
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    }
                                    vm.toast('success', 'Policy Updated');
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        });
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = true;
                        vm.policyRef.child('policyIsActive').set(true).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        if (event.target.name === 'policyActionRequired') {
                            vm.setActionRequired('agent', 'on');
                        } else if (event.target.name === 'policyBOActionRequired') {
                            vm.setActionRequired('back-office', 'on');
                        } else if (event.target.name === 'policyBrokerActionRequired') {
                            vm.setActionRequired('broker', 'on');
                        }
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = vm.buyers;
                    } else if (event.target.className.indexOf('status-check') >= 0) {
                        vm.policy.status[event.target.name] = true;
                        vm.policy.status[event.target.name + 'At'] = moment().unix() * 1000;
                        adjustStatusImage();
                        updateStatus();
                        var data = {
                            targets: [],
                            policyKey: vm.policyKey,
                            createdBy: vm.currentUser.profile.displayName,
                            createdByRole: vm.currentUser.profile.role,
                            note: event.target.getAttribute('notificationText') + ' for this Policy',
                            LSHID: vm.policy.LSHID,
                            action: event.target.name === 'offerReceived' ? 'Offer Received' : 'Status Updated'
                        };
                        sendNotification(data);
                    }
                    $scope.$apply();
                });
                jQuery('body').on('ifUnchecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'visibleToSupply') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        if (!vm.caseFiles[fileIdx].category) {
                            sweetAlert.swal({
                                title: "No File Category",
                                text: "Please categorize this file first so we know how to distribute it to Buyers",
                                type: "warning"
                            }).then(function() {
                                vm.caseFiles[fileIdx].hiddenFromBuyers = true;
                                $scope.$apply();
                            });
                        } else {
                            // Check all Buyers to see if any of them have access to this file's category
                            var buyersWithAccess = [];
                            var buyerAccessMessage = 'The following Buyers will be able to view this file:<br><br>';
                            if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                vm.policy.buyers.forEach(buyer => {
                                    delete buyer.$$hashKey;
                                    if (buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(vm.caseFiles[fileIdx].category) > -1) {
                                        buyersWithAccess.push(buyer);
                                        buyerAccessMessage += buyer.buyerName + '<br>';
                                    }
                                });
                            }
                            buyerAccessMessage += '<br>Do you want to continue?';
                            if (buyersWithAccess.length > 0) {
                                sweetAlert.swal({
                                    title: "Confirm Action",
                                    html: buyerAccessMessage,
                                    type: "question",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonColor: "#00B200",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false
                                }).then(function(result) {
                                    if (!result.dismiss) {
                                        // Update /sharedCaseFiles/[each buyer's PolicyGuid] in a single transaction
                                        var updates = {};
                                        var caseFileCopy = sharedService.cloneObject(vm.caseFiles[fileIdx]);
                                        delete caseFileCopy.$id;
                                        delete caseFileCopy.$priority;
                                        delete caseFileCopy.$$hashKey;
                                        buyersWithAccess.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.caseFiles[fileIdx].$id] = caseFileCopy;
                                        });

                                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                                            root.update(updates).then(function() {
                                                sweetAlert.swal({
                                                    title: "Success",
                                                    text: "File access has been granted to all selected Buyers",
                                                    type: "success",
                                                    timer: 1000
                                                }).then(function() {});
                                            }).catch(function(err) {
                                                console.log(err);
                                            });
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    } else {
                                        sweetAlert.swal({
                                            title: "Success",
                                            text: "No changes have been made",
                                            type: "success",
                                            timer: 1000
                                        }).then(function() {
                                            vm.caseFiles[fileIdx].hiddenFromBuyers = true;
                                        });
                                    }
                                });
                            } else {
                                vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                                    vm.toast('success', 'Policy Updated');
                                });
                            }
                        }
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = false;
                        vm.policyRef.child('policyIsActive').set(false).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        if (event.target.name === 'policyActionRequired') {
                            vm.setActionRequired('agent', 'off');
                        } else if (event.target.name === 'policyBOActionRequired') {
                            vm.setActionRequired('back-office', 'off');
                        } else if (event.target.name === 'policyBrokerActionRequired') {
                            vm.setActionRequired('broker', 'off');
                        }
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        vm.policy.secondaryInsured.isDeceased = false;
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        updateStatus();
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = [];
                    } else if (event.target.className.indexOf('status-check') >= 0) {
                        vm.policy.status[event.target.name] = false;
                        delete vm.policy.status[event.target.name + 'At'];
                        adjustStatusImage();
                        updateStatus();
                    }
                    $scope.$apply();
                });
                jQuery('.tabs').tabslet();
                $scope.$apply();
            }, 0);
        }

        vm.showMessage = function(type) {
            if (type === 'Out for Offers') {
                sweetAlert.swal({
                    title: "Out for Offers",
                    text: "If you have already sent this Policy out to Buyers by another means, you can check this box to update the Policy Status to reflect this. Either way, you can always use the \"Send to Buyers\" button to distribute Policies through the iManager platform",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Confirm Document Access') {
                sweetAlert.swal({
                    title: "Confirm Document Access",
                    text: "Buyers can be set up with Default Document Category access, but you may want to adjust that for any given Policy. Document access is granted by File Category, and can optionally be overridden per Document to block it from all Buyers.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Latest Update') {
                sweetAlert.swal({
                    title: "Latest Policy Update",
                    text: "This section shows the most recent status update from the Broker. This information is also included in the Digest Email you can subscribe to in your Profile",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.onSelectCallback = function(input) {
            jQuery('.placeHolder-' + input).addClass('active');
        }
    }
    angular.module('yapp')
        .controller('PackViewCtrl', PackViewCtrl);
})();