(function() {
    'use strict';

    /* @ngInject */
    function MedicalQCtrl(stdData, ENV, apiService, notifyService, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, pandadocService, brandingDomain, sharedService) {
        var vm = this;
        vm.showPage = 1;
        vm.pageIsDirty = false;

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.yesNo = stdData.yesNo;
        vm.genders = stdData.genders;
        vm.doYouLiveInOptions = [
            'Assisted Living Facility',
            'Skilled Nursing Facility',
            'Nursing Home',
            'Other'
        ];
        vm.activitiesNeedingAssistance = [
            'Meal Planning',
            'Taking Medications',
            'Shopping',
            'Walking',
            'Bathing',
            'Dressing'
        ];
        vm.heartDisorders = [
            'High blood pressure',
            'Atrial fibrillation',
            'Irregular Pulse or Arrhythmia Other than AFIB',
            'Coronary Artery Disease',
            'Angina (chest pain from heart disease)',
            'Heart Attack(s)',
            'Heart Valve Disease',
            'Heart Failure',
            'Other'
        ];
        vm.circulatoryDisorders = [
            'Stroke',
            'TIA or Mini-Stroke',
            'Aneurysm of an Artery',
            'Arterial Blockage in the Neck, Abdomen or Legs',
            'Venous Disease (blood clots, deep vein thrombosis or embolism)',
            'Other'
        ];
        vm.cancers = [
            'Tumor or Malignancy',
            'Leukemia',
            'Lymphoma',
            'Multiple Myeloma',
            'Blood Cancers (MPNs)',
            'Myelodyplastic Syndrome',
            'Other Cancerous Disorder'
        ];
        vm.neurologicalDisorders = [
            'Parkinson\'s Disease',
            'Multiple Sclerosis',
            'ALS (Lou Gehrig’s Disease)',
            'Loss of Consciousness',
            'Convulsions or Epilepsy',
            'Poor Vision',
            'Chronic Pain',
            'Sleep Apnea',
            'Other'
        ];
        vm.mentalNervousDisorders = [
            'Memory or Cognitive Impairment Without Dementia',
            'Alzheimer\'s or Other Type of Dementia',
            'Depression',
            'Schizophrenia',
            'Other'
        ];
        vm.digestiveDisorders = [
            'Diabetes',
            'Liver (not due to infection)',
            'Colon or Rectum',
            'Small Intestine',
            'Esophagus or Stomach',
            'GI Bleeding (upper or lower)',
            'Other'
        ];
        vm.infectionsDiseases = [
            'Hepatitis',
            'Pneumonia',
            'Sepsis (blood infection)',
            'Shingles',
            'Urinary Tract Infection',
            'MRSA',
            'Other'
        ];
        vm.respiratoryDisorders = [
            'Asthma',
            'COPD, Emphysema or Chronic Bronchitis',
            'Shortness of Breath at Rest or with Minimal Exertion',
            'Chronic Lung Infection',
            'Other'
        ];
        vm.genitourinaryDisorders = [
            'Prostate',
            'Bladder',
            'Kidney Disease, Impaired Function or Failure',
            'Urine Abnormalities',
            'Other'
        ];
        vm.bloodAbnormalities = [
            'Anemia',
            'High Cholesterol or Triglycerides',
            'Abnormalities of Platelets, White or Red Blood Cells',
            'Abnormal Bruising, Bleeding or Clotting',
            'Disorder of the Spleen, Bone Marrow or Lymph Nodes',
            'Other'
        ];
        vm.boneJointNerveAbnormalities = [
            'Paralysis or Significant Physical Impairment',
            'Gout',
            'Numbness in Extremities',
            'Problems with Balance or Walking Injury or Accidental Fall',
            'Degenerative Arthritis',
            'Rheumatoid Arthritis',
            'Osteoporosis',
            'Fracture of Hip, Vertebra or Other Bone',
            'Other'
        ];
        vm.immuneDisorders = [
            'HIV',
            'Autoimmune Disease',
            'Systemic Lupus',
            'Connective Tissue Disease',
            'Other'
        ];
        vm.alcoholUse = [
            'Alcoholism or Alcohol Abuse',
            'Illegal Drug Use',
            'Marijuana',
            'Prescription Drug Abuse',
            'Ever been advised by a medical professional to reduce or eliminate alcohol or drug use, including prescription drugs'
        ];

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }
            var root = firebase.database().ref();
            vm.user = $firebaseObject(root.child('users/' + vm.currentUser.auth.uid));
            vm.user.$loaded().then(function(user) {
                loadPage();
            });
        });

        function loadPage() {
            var root = firebase.database().ref();
            if ($state.params.pid) {
                /**
                 * LOADING AN EXISTING POLICY
                 */
                vm.policyKey = $state.params.pid;
                vm.policy = $firebaseObject(root.child('policies').child(vm.policyKey));
                vm.policy.$loaded().then(function(policy) {
                    if ($state.params.recipient) {
                        vm.recipient = $state.params.recipient;

                        if (vm.recipient === 'Primary Insured') {
                            vm.formName = 'medicalPrimary';
                            vm.form = vm.policy.forms.medicalPrimary;
                            if (!vm.form.agent) {
                                vm.form.agent = {
                                    fullName: vm.currentUser.profile.fullName
                                };
                            }
                            if (!vm.form.recipients) {
                                vm.form.recipients = {
                                    agent: {
                                        emailAddress: vm.currentUser.profile.email,
                                        firstName: vm.currentUser.profile.firstName,
                                        lastName: vm.currentUser.profile.lastName
                                    },
                                    recipient: {
                                        firstName: vm.policy.forms.application.recipients.insured.firstName,
                                        lastName: vm.policy.forms.application.recipients.insured.lastName,
                                        emailAddress: vm.policy.forms.application.recipients.insured.emailAddress ? vm.policy.forms.application.recipients.insured.emailAddress : ''
                                    }
                                };
                            }
                            if (!vm.form.publicGuid) {
                                vm.form.publicGuid = uuidv4();
                            }
                            if (!vm.form.data) {
                                vm.policy.forms.medicalPrimary.status = 'In Progress';
                                vm.saveForm('init');
                            } else {
                                checkForThirdPartyData();
                            }
                        } else if (vm.recipient === 'Secondary Insured') {
                            vm.formName = 'medicalSecondary';
                            vm.form = vm.policy.forms.medicalSecondary;
                            if (!vm.form.agent) {
                                vm.form.agent = {
                                    fullName: vm.currentUser.profile.fullName
                                };
                            }
                            if (!vm.form.recipients) {
                                vm.form.recipients = {
                                    agent: {
                                        emailAddress: vm.currentUser.profile.email,
                                        firstName: vm.currentUser.profile.firstName,
                                        lastName: vm.currentUser.profile.lastName
                                    },
                                    recipient: {
                                        firstName: vm.policy.forms.application.recipients.secondaryInsured.firstName,
                                        lastName: vm.policy.forms.application.recipients.secondaryInsured.lastName,
                                        emailAddress: vm.policy.forms.application.recipients.secondaryInsured.emailAddress ? vm.policy.forms.application.recipients.secondaryInsured.emailAddress : ''
                                    }
                                };
                            }
                            if (!vm.form.publicGuid) {
                                vm.form.publicGuid = uuidv4();
                            }
                            if (!vm.form.data) {
                                vm.policy.forms.medicalSecondary.status = 'In Progress';
                                vm.saveForm('init');
                            } else {
                                checkForThirdPartyData();
                            }
                        }
                    }
                });
            } else {
                $state.go('inventory');
            }
        }

        function checkForThirdPartyData() {
            if (vm.form.status === 'Pending 3rd Party' || vm.form.status === 'Needs Review') {
                // Go get whatever data the 3rd party has provided so far and merge it into the Policy
                var root = firebase.database().ref();
                root.child('publicForms').child(vm.form.publicGuid).once('value', function(snap) {
                    if (snap.exists()) {
                        var unwatch = vm.policy.$watch(function() {
                            vm.form = vm.policy.forms[vm.form.className];
                        });
                        root.child('policies').child(vm.policyKey).child('forms/' + vm.form.className).update(snap.val()).then(function() {
                            sweetAlert.swal({
                                title: "Saved!",
                                text: "",
                                type: "success",
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                timer: 1000
                            }).then(function(result) {
                                vm.pageIsDirty = false;
                                updateLabels();
                            });
                        }).catch(function(err) {
                            console.log(err.message);
                        });
                    }
                });
            } else {
                updateLabels();
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.medical .page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.medical .placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.medical .pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.form[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = (vm.form[parts[0]]) ? vm.form[parts[0]][parts[1]] : '';
                        } else if (parts.length === 3) {
                            compStr = (vm.form[parts[0]][parts[1]]) ? vm.form[parts[0]][parts[1]][parts[2]] : '';
                        } else if (parts.length === 4) {
                            compStr = (vm.form[parts[0]][parts[1]][parts[2]]) ? vm.form[parts[0]][parts[1]][parts[2]][parts[3]] : '';
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('input').on('ifChecked', function(event) {
                    vm.pageIsDirty = true;
                    $scope.$apply();
                });
                jQuery('input').on('ifUnchecked', function(event) {
                    vm.pageIsDirty = true;
                    $scope.$apply();
                });
                document.getElementById('scrollContainer').scrollTo(0, 0);
                $scope.$apply();
            }, 10);
        }

        vm.goToPage = function(page) {
            if (page > vm.showPage && vm.pageIsDirty) {
                vm.saveForm('nextPage');
            } else {
                vm.showPage = page;
                if (window.history.scrollRestoration === 'auto') {
                    window.history.scrollRestoration = 'manual';
                }
                updateLabels();
            }
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        if (route === 'policy') {
                            $state.go('policy', { pid: vm.policyKey });
                        } else {
                            $state.go(route);
                        }
                    } else {
                        vm.saveForm('close');
                    }
                });
            } else {
                if (route === 'policy') {
                    $state.go('policy', { pid: vm.policyKey });
                } else {
                    $state.go(route);
                }
            }
        }

        vm.saveForm = function(actionAfterSave) {
            var root = firebase.database().ref();

            vm.actionAfterSave = actionAfterSave;

            if (actionAfterSave !== 'init') {
                if (!vm.form.data) {
                    vm.form.data = {};
                }

                vm.form.data.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;
                vm.form.data.lastUpdatedBy = vm.currentUser.profile.displayName;
                vm.form.data.lastUpdatedById = vm.currentUser.auth.uid;

                if (!vm.form.data.updateLog) {
                    vm.form.data.updateLog = [];
                }

                vm.form.data.updateLog.push({
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    updatedBy: vm.currentUser.profile.displayName,
                    updatedById: vm.currentUser.auth.uid
                });

                if (vm.form.data.recipient.gender[0].$$hashKey) {
                    delete vm.form.data.recipient.gender[0].$$hashKey;
                }
                if (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.gender) {
                    delete vm.form.data.recipient.siblingOne.gender[0].$$hashKey;
                }
                if (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.gender) {
                    delete vm.form.data.recipient.siblingTwo.gender[0].$$hashKey;
                }
                if (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.gender) {
                    delete vm.form.data.recipient.siblingThree.gender[0].$$hashKey;
                }
                if (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.gender) {
                    delete vm.form.data.recipient.siblingFour.gender[0].$$hashKey;
                }
                if (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.gender) {
                    delete vm.form.data.recipient.spouse.gender[0].$$hashKey;
                }
            } else {
                // Pre-load known data into form data object
                vm.form.data = {};
                if (vm.recipient === 'Primary Insured') {
                    vm.form.data.recipient = {
                        fullName: (vm.policy.insured.fullName) ? vm.policy.insured.fullName : '',
                        ssn: (vm.policy.insured.ssn) ? vm.policy.insured.ssn : '',
                        dateOfBirth: (vm.policy.insured.dateOfBirth) ? vm.policy.insured.dateOfBirth : '',
                        gender: (vm.policy.insured.gender) ? vm.policy.insured.gender : ''
                    };
                } else if (vm.recipient === 'Secondary Insured') {
                    vm.form.data.recipient = {
                        fullName: (vm.policy.secondaryInsured.fullName) ? vm.policy.secondaryInsured.fullName : '',
                        ssn: (vm.policy.secondaryInsured.ssn) ? vm.policy.secondaryInsured.ssn : '',
                        dateOfBirth: (vm.policy.secondaryInsured.dateOfBirth) ? vm.policy.secondaryInsured.dateOfBirth : '',
                        gender: (vm.policy.secondaryInsured.gender) ? vm.policy.secondaryInsured.gender : ''
                    };
                }
                vm.form.data.carrier = vm.policy.carrier ? vm.policy.carrier : '';
                vm.form.data.policyNumber = vm.policy.policyNumber ? vm.policy.policyNumber : '';
            }

            root.child('policies/' + vm.policyKey + '/forms/' + vm.formName).update(vm.form).then(function(ref) {
                if (vm.form.status === 'Pending 3rd Party') {
                    var root = firebase.database().ref();

                    var publicCopy = sharedService.cloneObject(vm.form);

                    var publicGuid = vm.form.publicGuid;
                    var className = vm.form.className;

                    delete publicCopy.agent;
                    delete publicCopy.recipients;
                    delete publicCopy.status;
                    delete publicCopy.publicGuid;
                    delete publicCopy.className;

                    root.child('publicForms').child(vm.form.publicGuid).update(publicCopy).then(function() {
                        publicCopy = null;
                        continueSave();
                    });
                } else {
                    continueSave();
                }
            }).catch(function(err) {
                if (err) {
                    console.log(err.message);
                }
            });
        }

        function continueSave() {
            if (vm.actionAfterSave === 'goToPolicy') {
                $state.go('policy', { 'pid': vm.policyKey });
            } else {
                sweetAlert.swal({
                    title: "Saved!",
                    text: "",
                    type: "success",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "OK",
                    timer: 1000
                }).then(function(result) {
                    vm.pageIsDirty = false;
                    if (vm.actionAfterSave === 'close') {
                        $state.go('policy', { pid: vm.policyKey });
                    } else if (vm.actionAfterSave === 'nextPage') {
                        vm.showPage++;
                        updateLabels();
                    } else if (vm.actionAfterSave === 'init') {
                        updateLabels();
                    } else if (vm.actionAfterSave === 'requestThirdParty') {
                        continueSendToThirdParty();
                    }
                });
            }
        }

        vm.submitForm = function() {
            // TODO: Validate any required fields

            vm.submittingForm = true;

            // Update policy status to "Application Finished"
            if (vm.recipient === 'Primary Insured') {
                vm.policy.status.primaryMedicalQuestionnaireFinished = firebase.database.ServerValue.TIMESTAMP;
            } else if (vm.recipient === 'Secondary Insured') {
                vm.policy.status.secondaryMedicalQuestionnaireFinished = firebase.database.ServerValue.TIMESTAMP;
            }

            vm.policy.forms[vm.formName].status = 'Pending Signatures';

            if (vm.signMethod === 'esign') {
                vm.policy.forms[vm.formName].signMethod = 'E-Signature';
                if (vm.form.recipients.recipient && !vm.form.recipients.recipient.emailAddress) {
                    sweetAlert.swal({
                        title: "Uh Oh!",
                        text: "Recipient Email cannot be blank when using e-signature",
                        type: "error"
                    }).then(function() {});
                }
            } else if (vm.signMethod = 'printAndSign') {
                vm.policy.forms[vm.formName].signMethod = 'Print and Sign';
            }

            var recipients = [];

            recipients.push({
                email: vm.form.recipients.recipient.emailAddress ? vm.form.recipients.recipient.emailAddress : '',
                first_name: vm.form.recipients.recipient.firstName,
                last_name: vm.form.recipients.recipient.lastName,
                role: 'Recipient'
            });

            var templateId = 'Xuhix3EbFqqkdmWKh4ja76'; // Medical Form - Production
            if (ENV === 'demo') {
                templateId = '4JWLwH33idXmt3EtipAydX'; // Medical Form - Demo
            } else if (ENV === 'dev') {
                templateId = 'wXJwDYZDeqTFfX78Eah3Hg'; // Medical Form - Dev
            }

            var data = {
                templateId: templateId,
                documentName: vm.recipient + " Medical Questionnaire Form - " + vm.policy.insured.lastName,
                recipients: recipients,
                fieldData: {
                    "carrier": { "value": (vm.form.data.carrier) ? vm.form.data.carrier : '' },
                    "policyNumber": { "value": (vm.form.data.policyNumber) ? vm.form.data.policyNumber : '' },
                    "recipient.fullName": { "value": (vm.form.data.recipient.fullName) ? vm.form.data.recipient.fullName : '' },
                    "recipient.gender.name": { "value": (vm.form.data.recipient.gender) ? vm.form.data.recipient.gender[0].name : '' },
                    "recipient.dateOfBirth": { "value": (vm.form.data.recipient.dateOfBirth) ? vm.form.data.recipient.dateOfBirth : '' },
                    "recipient.ssn": { "value": (vm.form.data.recipient.ssn) ? vm.form.data.recipient.ssn : '' },
                    "recipient.timesWakingAtNight": { "value": (vm.form.data.recipient.timesWakingAtNight) ? vm.form.data.recipient.timesWakingAtNight : '' },
                    "recipient.height": { "value": (vm.form.data.recipient.height) ? vm.form.data.recipient.height : '' },
                    "recipient.weight": { "value": (vm.form.data.recipient.weight) ? vm.form.data.recipient.weight : '' },
                    "recipient.weightChangeDetails": { "value": (vm.form.data.recipient.weightChangeDetails) ? vm.form.data.recipient.weightChangeDetails : '' },
                    "recipient.yearsSmoked": { "value": (vm.form.data.recipient.yearsSmoked) ? vm.form.data.recipient.yearsSmoked : '' },
                    "recipient.cigarettesPerDay": { "value": (vm.form.data.recipient.cigarettesPerDay) ? vm.form.data.recipient.cigarettesPerDay : '' },
                    "recipient.yearsSinceQuitting": { "value": (vm.form.data.recipient.yearSinceQuitting) ? vm.form.data.recipient.yearSinceQuitting : '' },
                    "recipient.alcoholicBeverages": { "value": (vm.form.data.recipient.alcoholicBeverages) ? vm.form.data.recipient.alcoholicBeverages[0] : '' },
                    "recipient.alcoholicBeveragesDetails": { "value": (vm.form.data.recipient.alcoholicBeveragesDetails) ? vm.form.data.recipient.alcoholicBeveragesDetails : '' },
                    "recipient.livesInFacilityDuration": { "value": (vm.form.data.recipient.livesInFacilityDuration) ? vm.form.data.recipient.livesInFacilityDuration : '' },
                    "recipient.livesInFacilityType": { "value": (vm.form.data.recipient.livesInFacilityType) ? vm.form.data.recipient.livesInFacilityType[0] : '' },
                    "recipient.assistanceNeededDetails": { "value": (vm.form.data.recipient.assistanceNeededDetails) ? vm.form.data.recipient.assistanceNeededDetails : '' },
                    "recipient.drives": { "value": (vm.form.data.recipient.drives) ? vm.form.data.recipient.drives[0] : '' },
                    "recipient.reasonNotDriving": { "value": (vm.form.data.recipient.reasonNotDriving) ? vm.form.data.recipient.reasonNotDriving : '' },
                    "recipient.notFollowingDrInstruction": { "value": (vm.form.data.recipient.notFollowingDrInstruction) ? vm.form.data.recipient.notFollowingDrInstruction[0] : '' },
                    "recipient.reasonNotFollowingDrInstruction": { "value": (vm.form.data.recipient.reasonNotFollowingDrInstruction) ? vm.form.data.recipient.reasonNotFollowingDrInstruction : '' },
                    "recipient.regularExerciseDetails": { "value": (vm.form.data.recipient.regularExerciseDetails) ? vm.form.data.recipient.regularExerciseDetails : '' },
                    "recipient.everSmokedCigarettes": { "value": (vm.form.data.recipient.everSmokedCigarettes) ? vm.form.data.recipient.everSmokedCigarettes[0] : '' },
                    "recipient.otherTobacco": { "value": (vm.form.data.recipient.otherTobacco) ? vm.form.data.recipient.otherTobacco[0] : '' },
                    "recipient.otherTobaccoDetails": { "value": (vm.form.data.recipient.otherTobaccoDetails) ? vm.form.data.recipient.otherTobaccoDetails : '' },
                    "recipient.pcpFrequency": { "value": (vm.form.data.recipient.pcpFrequency) ? vm.form.data.recipient.pcpFrequency : '' },
                    "recipient.specialistFrequency": { "value": (vm.form.data.recipient.specialistFrequency) ? vm.form.data.recipient.specialistFrequency : '' },
                    "recipient.bloodPressure": { "value": (vm.form.data.recipient.bloodPressure) ? vm.form.data.recipient.bloodPressure : '' },
                    "recipient.bloodSugar": { "value": (vm.form.data.recipient.bloodSugar) ? vm.form.data.recipient.bloodSugar : '' },
                    "recipient.cholesterol": { "value": (vm.form.data.recipient.cholesterol) ? vm.form.data.recipient.cholesterol : '' },
                    "recipient.ejectionFraction": { "value": (vm.form.data.recipient.ejectionFraction) ? vm.form.data.recipient.ejectionFraction : '' },
                    "recipient.bloodAbnormalities": { "value": (vm.form.data.recipient.bloodAbnormalities) ? vm.form.data.recipient.bloodAbnormalities.join(', ') : '' },
                    "recipient.boneJointNerveAbnormalities": { "value": (vm.form.data.recipient.boneJointNerveAbnormalities) ? vm.form.data.recipient.boneJointNerveAbnormalities.join(', ') : '' },
                    "recipient.cancers": { "value": (vm.form.data.recipient.cancers) ? vm.form.data.recipient.cancers.join(', ') : '' },
                    "recipient.circulatoryDisorders": { "value": (vm.form.data.recipient.circulatoryDisorders) ? vm.form.data.recipient.circulatoryDisorders.join(', ') : '' },
                    "recipient.digestiveDisorders": { "value": (vm.form.data.recipient.digestiveDisorders) ? vm.form.data.recipient.digestiveDisorders.join(', ') : '' },
                    "recipient.genitourinaryDisorders": { "value": (vm.form.data.recipient.genitourinaryDisorders) ? vm.form.data.recipient.genitourinaryDisorders.join(', ') : '' },
                    "recipient.heartDisorders": { "value": (vm.form.data.recipient.heartDisorders) ? vm.form.data.recipient.heartDisorders.join(', ') : '' },
                    "recipient.immuneDisorders": { "value": (vm.form.data.recipient.immuneDisorders) ? vm.form.data.recipient.immuneDisorders.join(', ') : '' },
                    "recipient.infectiousDiseases": { "value": (vm.form.data.recipient.infectiousDiseases) ? vm.form.data.recipient.infectiousDiseases.join(', ') : '' },
                    "recipient.mentalNervousDisorders": { "value": (vm.form.data.recipient.mentalNervousDisorders) ? vm.form.data.recipient.mentalNervousDisorders.join(', ') : '' },
                    "recipient.neurologicalDisorders": { "value": (vm.form.data.recipient.neurologicalDisorders) ? vm.form.data.recipient.neurologicalDisorders.join(', ') : '' },
                    "recipient.alcoholUse": { "value": (vm.form.data.recipient.alcoholUse) ? vm.form.data.recipient.alcoholUse.join(', ') : '' },
                    "recipient.otherAccidentOrTreatment": { "value": (vm.form.data.recipient.otherAccidentOrTreatment) ? vm.form.data.recipient.otherAccidentOrTreatment : '' },
                    "recipient.otherConditions": { "value": (vm.form.data.recipient.otherConditions) ? vm.form.data.recipient.otherConditions : '' },
                    "recipient.respiratoryDisorders": { "value": (vm.form.data.recipient.respiratoryDisorders) ? vm.form.data.recipient.respiratoryDisorders.join(', ') : '' },
                    "recipient.father.livingAge": { "value": (vm.form.data.recipient.father && vm.form.data.recipient.father.livingAge) ? vm.form.data.recipient.father.livingAge : '' },
                    "recipient.father.deceasedAge": { "value": (vm.form.data.recipient.father && vm.form.data.recipient.father.deceasedAge) ? vm.form.data.recipient.father.deceasedAge : '' },
                    "recipient.father.causeOfDeath": { "value": (vm.form.data.recipient.father && vm.form.data.recipient.father.causeOfDeath) ? vm.form.data.recipient.father.causeOfDeath : '' },
                    "recipient.mother.livingAge": { "value": (vm.form.data.recipient.mother && vm.form.data.recipient.mother.livingAge) ? vm.form.data.recipient.mother.livingAge : '' },
                    "recipient.mother.deceasedAge": { "value": (vm.form.data.recipient.mother && vm.form.data.recipient.mother.deceasedAge) ? vm.form.data.recipient.mother.deceasedAge : '' },
                    "recipient.mother.causeOfDeath": { "value": (vm.form.data.recipient.mother && vm.form.data.recipient.mother.causeOfDeath) ? vm.form.data.recipient.mother.causeOfDeath : '' },
                    "recipient.siblingOne.livingAge": { "value": (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.livingAge) ? vm.form.data.recipient.siblingOne.livingAge : '' },
                    "recipient.siblingOne.deceasedAge": { "value": (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.deceasedAge) ? vm.form.data.recipient.siblingOne.deceasedAge : '' },
                    "recipient.siblingOne.causeOfDeath": { "value": (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.causeOfDeath) ? vm.form.data.recipient.siblingOne.causeOfDeath : '' },
                    "recipient.siblingOne.gender.name": { "value": (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.gender) ? vm.form.data.recipient.siblingOne.gender[0].name : '' },
                    "recipient.siblingTwo.livingAge": { "value": (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.livingAge) ? vm.form.data.recipient.siblingTwo.livingAge : '' },
                    "recipient.siblingTwo.deceasedAge": { "value": (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.deceasedAge) ? vm.form.data.recipient.siblingTwo.deceasedAge : '' },
                    "recipient.siblingTwo.causeOfDeath": { "value": (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.causeOfDeath) ? vm.form.data.recipient.siblingTwo.causeOfDeath : '' },
                    "recipient.siblingTwo.gender.name": { "value": (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.gender) ? vm.form.data.recipient.siblingTwo.gender[0].name : '' },
                    "recipient.siblingThree.livingAge": { "value": (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.livingAge) ? vm.form.data.recipient.siblingThree.livingAge : '' },
                    "recipient.siblingThree.deceasedAge": { "value": (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.deceasedAge) ? vm.form.data.recipient.siblingThree.deceasedAge : '' },
                    "recipient.siblingThree.causeOfDeath": { "value": (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.causeOfDeath) ? vm.form.data.recipient.siblingThree.causeOfDeath : '' },
                    "recipient.siblingThree.gender.name": { "value": (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.gender) ? vm.form.data.recipient.siblingThree.gender[0].name : '' },
                    "recipient.siblingFour.livingAge": { "value": (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.livingAge) ? vm.form.data.recipient.siblingFour.livingAge : '' },
                    "recipient.siblingFour.deceasedAge": { "value": (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.deceasedAge) ? vm.form.data.recipient.siblingFour.deceasedAge : '' },
                    "recipient.siblingFour.causeOfDeath": { "value": (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.causeOfDeath) ? vm.form.data.recipient.siblingFour.causeOfDeath : '' },
                    "recipient.siblingFour.gender.name": { "value": (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.gender) ? vm.form.data.recipient.siblingFour.gender[0].name : '' },
                    "recipient.spouse.livingAge": { "value": (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.livingAge) ? vm.form.data.recipient.spouse.livingAge : '' },
                    "recipient.spouse.deceasedAge": { "value": (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.deceasedAge) ? vm.form.data.recipient.spouse.deceasedAge : '' },
                    "recipient.spouse.causeOfDeath": { "value": (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.causeOfDeath) ? vm.form.data.recipient.spouse.causeOfDeath : '' },
                    "recipient.spouse.gender.name": { "value": (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.gender) ? vm.form.data.recipient.spouse.gender[0].name : '' },
                    "recipient.diagnosisOne.dateOfDiagnosis": { "value": (vm.form.data.recipient.diagnosisOne) ? vm.form.data.recipient.diagnosisOne.dateOfDiagnosis : '' },
                    "recipient.diagnosisOne.dateOfTreatment": { "value": (vm.form.data.recipient.diagnosisOne) ? vm.form.data.recipient.diagnosisOne.dateOfTreatment : '' },
                    "recipient.diagnosisOne.diagnosis": { "value": (vm.form.data.recipient.diagnosisOne) ? vm.form.data.recipient.diagnosisOne.diagnosis : '' },
                    "recipient.diagnosisOne.treatmentReceived": { "value": (vm.form.data.recipient.diagnosisOne) ? vm.form.data.recipient.diagnosisOne.treatmentReceived : '' },
                    "recipient.diagnosisOne.results": { "value": (vm.form.data.recipient.diagnosisOne) ? vm.form.data.recipient.diagnosisOne.results : '' },
                    "recipient.diagnosisTwo.dateOfDiagnosis": { "value": (vm.form.data.recipient.diagnosisTwo) ? vm.form.data.recipient.diagnosisTwo.dateOfDiagnosis : '' },
                    "recipient.diagnosisTwo.dateOfTreatment": { "value": (vm.form.data.recipient.diagnosisTwo) ? vm.form.data.recipient.diagnosisTwo.dateOfTreatment : '' },
                    "recipient.diagnosisTwo.diagnosis": { "value": (vm.form.data.recipient.diagnosisTwo) ? vm.form.data.recipient.diagnosisTwo.diagnosis : '' },
                    "recipient.diagnosisTwo.treatmentReceived": { "value": (vm.form.data.recipient.diagnosisTwo) ? vm.form.data.recipient.diagnosisTwo.treatmentReceived : '' },
                    "recipient.diagnosisTwo.results": { "value": (vm.form.data.recipient.diagnosisTwo) ? vm.form.data.recipient.diagnosisTwo.results : '' },
                    "recipient.diagnosisThree.dateOfDiagnosis": { "value": (vm.form.data.recipient.diagnosisThree) ? vm.form.data.recipient.diagnosisThree.dateOfDiagnosis : '' },
                    "recipient.diagnosisThree.dateOfTreatment": { "value": (vm.form.data.recipient.diagnosisThree) ? vm.form.data.recipient.diagnosisThree.dateOfTreatment : '' },
                    "recipient.diagnosisThree.diagnosis": { "value": (vm.form.data.recipient.diagnosisThree) ? vm.form.data.recipient.diagnosisThree.diagnosis : '' },
                    "recipient.diagnosisThree.treatmentReceived": { "value": (vm.form.data.recipient.diagnosisThree) ? vm.form.data.recipient.diagnosisThree.treatmentReceived : '' },
                    "recipient.diagnosisThree.results": { "value": (vm.form.data.recipient.diagnosisThree) ? vm.form.data.recipient.diagnosisThree.results : '' },
                    "recipient.diagnosisFour.dateOfDiagnosis": { "value": (vm.form.data.recipient.diagnosisFour) ? vm.form.data.recipient.diagnosisFour.dateOfDiagnosis : '' },
                    "recipient.diagnosisFour.dateOfTreatment": { "value": (vm.form.data.recipient.diagnosisFour) ? vm.form.data.recipient.diagnosisFour.dateOfTreatment : '' },
                    "recipient.diagnosisFour.diagnosis": { "value": (vm.form.data.recipient.diagnosisFour) ? vm.form.data.recipient.diagnosisFour.diagnosis : '' },
                    "recipient.diagnosisFour.treatmentReceived": { "value": (vm.form.data.recipient.diagnosisFour) ? vm.form.data.recipient.diagnosisFour.treatmentReceived : '' },
                    "recipient.diagnosisFour.results": { "value": (vm.form.data.recipient.diagnosisFour) ? vm.form.data.recipient.diagnosisFour.results : '' },
                    "recipient.medicationOne.name": { "value": (vm.form.data.recipient.medicationOne) ? vm.form.data.recipient.medicationOne.name : '' },
                    "recipient.medicationOne.howLong": { "value": (vm.form.data.recipient.medicationOne) ? vm.form.data.recipient.medicationOne.howLong : '' },
                    "recipient.medicationOne.forWhat": { "value": (vm.form.data.recipient.medicationOne) ? vm.form.data.recipient.medicationOne.forWhat : '' },
                    "recipient.medicationOne.dosageAndFrequency": { "value": (vm.form.data.recipient.medicationOne) ? vm.form.data.recipient.medicationOne.dosageAndFrequency : '' },
                    "recipient.medicationTwo.name": { "value": (vm.form.data.recipient.medicationTwo) ? vm.form.data.recipient.medicationTwo.name : '' },
                    "recipient.medicationTwo.howLong": { "value": (vm.form.data.recipient.medicationTwo) ? vm.form.data.recipient.medicationTwo.howLong : '' },
                    "recipient.medicationTwo.forWhat": { "value": (vm.form.data.recipient.medicationTwo) ? vm.form.data.recipient.medicationTwo.forWhat : '' },
                    "recipient.medicationTwo.dosageAndFrequency": { "value": (vm.form.data.recipient.medicationTwo) ? vm.form.data.recipient.medicationTwo.dosageAndFrequency : '' },
                    "recipient.medicationThree.name": { "value": (vm.form.data.recipient.medicationThree) ? vm.form.data.recipient.medicationThree.name : '' },
                    "recipient.medicationThree.howLong": { "value": (vm.form.data.recipient.medicationThree) ? vm.form.data.recipient.medicationThree.howLong : '' },
                    "recipient.medicationThree.forWhat": { "value": (vm.form.data.recipient.medicationThree) ? vm.form.data.recipient.medicationThree.forWhat : '' },
                    "recipient.medicationThree.dosageAndFrequency": { "value": (vm.form.data.recipient.medicationThree) ? vm.form.data.recipient.medicationThree.dosageAndFrequency : '' },
                    "recipient.medicationFour.name": { "value": (vm.form.data.recipient.medicationFour) ? vm.form.data.recipient.medicationFour.name : '' },
                    "recipient.medicationFour.howLong": { "value": (vm.form.data.recipient.medicationFour) ? vm.form.data.recipient.medicationFour.howLong : '' },
                    "recipient.medicationFour.forWhat": { "value": (vm.form.data.recipient.medicationFour) ? vm.form.data.recipient.medicationFour.forWhat : '' },
                    "recipient.medicationFour.dosageAndFrequency": { "value": (vm.form.data.recipient.medicationFour) ? vm.form.data.recipient.medicationFour.dosageAndFrequency : '' },
                    "pcp.name": { "value": (vm.form.data.pcp) ? vm.form.data.pcp.name : '' },
                    "pcp.phoneNumber": { "value": (vm.form.data.pcp) ? vm.form.data.pcp.phoneNumber : '' },
                    "pcp.address.address": { "value": (vm.form.data.pcp && vm.form.data.pcp.address) ? vm.form.data.pcp.address.address : '' },
                    "pcp.address.city": { "value": (vm.form.data.pcp && vm.form.data.pcp.address) ? vm.form.data.pcp.address.city : '' },
                    "pcp.address.state": { "value": (vm.form.data.pcp && vm.form.data.pcp.address) ? vm.form.data.pcp.address.state : '' },
                    "pcp.address.zip": { "value": (vm.form.data.pcp && vm.form.data.pcp.address) ? vm.form.data.pcp.address.zip : '' },
                    "pcp.dateOfLastVisit": { "value": (vm.form.data.pcp) ? vm.form.data.pcp.dateOfLastVisit : '' },
                    "pcp.lastVisitReason": { "value": (vm.form.data.pcp) ? vm.form.data.pcp.lastVisitReason : '' },
                    "specOne.name": { "value": (vm.form.data.specOne) ? vm.form.data.specOne.name : '' },
                    "specOne.phoneNumber": { "value": (vm.form.data.specOne) ? vm.form.data.specOne.phoneNumber : '' },
                    "specOne.specialty": { "value": (vm.form.data.specOne) ? vm.form.data.specOne.specialty : '' },
                    "specOne.address.address": { "value": (vm.form.data.specOne && vm.form.data.specOne.address) ? vm.form.data.specOne.address.address : '' },
                    "specOne.address.city": { "value": (vm.form.data.specOne && vm.form.data.specOne.address) ? vm.form.data.specOne.address.city : '' },
                    "specOne.address.state": { "value": (vm.form.data.specOne && vm.form.data.specOne.address) ? vm.form.data.specOne.address.state : '' },
                    "specOne.address.zip": { "value": (vm.form.data.specOne && vm.form.data.specOne.address) ? vm.form.data.specOne.address.zip : '' },
                    "specOne.dateOfLastVisit": { "value": (vm.form.data.specOne) ? vm.form.data.specOne.dateOfLastVisit : '' },
                    "specOne.lastVisitReason": { "value": (vm.form.data.specOne) ? vm.form.data.specOne.lastVisitReason : '' },
                    "specTwo.name": { "value": (vm.form.data.specTwo) ? vm.form.data.specTwo.name : '' },
                    "specTwo.phoneNumber": { "value": (vm.form.data.specTwo) ? vm.form.data.specTwo.phoneNumber : '' },
                    "specTwo.specialty": { "value": (vm.form.data.specTwo) ? vm.form.data.specTwo.specialty : '' },
                    "specTwo.address.address": { "value": (vm.form.data.specTwo && vm.form.data.specTwo.address) ? vm.form.data.specTwo.address.address : '' },
                    "specTwo.address.city": { "value": (vm.form.data.specTwo && vm.form.data.specTwo.address) ? vm.form.data.specTwo.address.city : '' },
                    "specTwo.address.state": { "value": (vm.form.data.specTwo && vm.form.data.specTwo.address) ? vm.form.data.specTwo.address.state : '' },
                    "specTwo.address.zip": { "value": (vm.form.data.specTwo && vm.form.data.specTwo.address) ? vm.form.data.specTwo.address.zip : '' },
                    "specTwo.dateOfLastVisit": { "value": (vm.form.data.specTwo) ? vm.form.data.specTwo.dateOfLastVisit : '' },
                    "specTwo.lastVisitReason": { "value": (vm.form.data.specTwo) ? vm.form.data.specTwo.lastVisitReason : '' },
                    "specThree.name": { "value": (vm.form.data.specThree) ? vm.form.data.specThree.name : '' },
                    "specThree.phoneNumber": { "value": (vm.form.data.specThree) ? vm.form.data.specThree.phoneNumber : '' },
                    "specThree.specialty": { "value": (vm.form.data.specThree) ? vm.form.data.specThree.specialty : '' },
                    "specThree.address.address": { "value": (vm.form.data.specThree && vm.form.data.specThree.address) ? vm.form.data.specThree.address.address : '' },
                    "specThree.address.city": { "value": (vm.form.data.specThree && vm.form.data.specThree.address) ? vm.form.data.specThree.address.city : '' },
                    "specThree.address.state": { "value": (vm.form.data.specThree && vm.form.data.specThree.address) ? vm.form.data.specThree.address.state : '' },
                    "specThree.address.zip": { "value": (vm.form.data.specThree && vm.form.data.specThree.address) ? vm.form.data.specThree.address.zip : '' },
                    "specThree.dateOfLastVisit": { "value": (vm.form.data.specThree) ? vm.form.data.specThree.dateOfLastVisit : '' },
                    "specThree.lastVisitReason": { "value": (vm.form.data.specThree) ? vm.form.data.specThree.lastVisitReason : '' }
                },
                metaData: {
                    policyNumber: vm.policy.policyNumber,
                    policyKey: vm.policyKey,
                    formName: (vm.recipient === 'Primary Insured') ? 'Medical Questionnaire (Primary)' : 'Medical Questionnaire (Secondary)',
                    className: (vm.recipient === 'Primary Insured') ? 'medicalPrimary' : 'medicalSecondary',
                    formVersion: '',
                    noteToRecipients: (vm.form.noteToRecipient) ? vm.form.noteToRecipient : '',
                    env: ENV
                }
            };

            vm.policy.status.display = {
                step: 'Medical Form Signatures',
                status: 'Pending',
                text: 'Pending Signatures'
            };

            vm.policy.$save(status.display).then(function() {
                if (vm.signMethod === 'printAndSign') {
                    apiService.fillPDF(data).then(function(response) {
                        if (response.status === 'success') {
                            var root = firebase.database().ref();
                            vm.form.status = 'Pending Upload';
                            vm.downloadURL = response.data;
                            root.child('policies/' + vm.policyKey + '/forms/' + vm.formName).update({
                                downloadURL: response.data,
                                status: 'Pending Upload'
                            }).then(function(ref) {
                                sweetAlert.swal({
                                    title: 'Success',
                                    text: 'Your Medical Questionnaire has been successfully created. The filled-in form will be opened in a new tab after you click "OK".',
                                    type: 'success'
                                }).then(function(result) {
                                    // Save and go to policy page
                                    vm.submittingForm = false;
                                    window.open(vm.downloadURL, '_blank');
                                    vm.saveForm('goToPolicy');
                                });
                            }).catch(function(err) {
                                sweetAlert.swal({
                                    title: 'Error',
                                    text: err.message,
                                    type: 'error'
                                }).then(function(result) {
                                    vm.submittingForm = false;
                                    $scope.$apply();
                                });
                            });
                        } else {
                            sweetAlert.swal({
                                title: 'Error',
                                text: response.message,
                                type: 'error'
                            }).then(function(result) {
                                vm.submittingForm = false;
                                $scope.$apply();
                            });
                        }
                    });
                } else if (vm.signMethod === 'esign') {
                    pandadocService.createDocumentFromTemplate(data).then(function(response) {
                        if (response.status === 'success') {
                            if (response.data && response.data.id) {
                                vm.form.status = 'Pending Signatures';
                                // Store the new PandaDoc document ID in Firebase NOW (not after the first webhook arrives!!!)
                                var root = firebase.database().ref();
                                root.child('policies/' + vm.policyKey + '/forms/' + vm.formName).update({
                                    documentId: response.data.id,
                                    status: 'Pending Signatures'
                                }).then(function(ref) {
                                    sweetAlert.swal({
                                        title: 'Success',
                                        text: 'Your Medical Questionnaire has been successfully submitted for e-signature',
                                        type: 'success'
                                    }).then(function(result) {
                                        // Save and go to policy page
                                        vm.saveForm('close');
                                    }).catch(function(err) {
                                        sweetAlert.swal({
                                            title: 'Error',
                                            text: err.message,
                                            type: 'error'
                                        }).then(function(result) {
                                            vm.submittingForm = false;
                                        });
                                    });;
                                });
                            }
                        } else {
                            vm.submittingForm = false;
                            sweetAlert.swal({
                                title: 'Error Submitting Form',
                                text: response.type + ' - Contact LS Hub Support',
                                type: 'error'
                            }).then(function(result) {});
                        }
                    });
                }
            }).catch(function(err) {
                console.log(err.message);
            });
        }

        vm.removeThirdParty = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This will prevent the 3rd party from accessing this form. You can request 3rd party completion again, but the current link will no longer work.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, remove access!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    var root = firebase.database().ref();
                    root.child('publicForms').child(vm.form.publicGuid).remove().then(function(ref) {
                        root.child('policies').child(vm.policyKey).child('forms/' + vm.form.className).update({
                            publicGuid: uuidv4(),
                            status: 'In Progress',
                            thirdParty: null
                        }).then(function(ref) {
                            sweetAlert.swal({
                                title: "Success",
                                text: "",
                                type: "success",
                                timer: 2500
                            }).then(function(result) {});
                        });
                    });
                }
            });
        }

        vm.approveThirdParty = function() {
            vm.justApprovedThirdParty = true;
            var root = firebase.database().ref();
            root.child('publicForms').child(vm.form.publicGuid).remove().then(function(ref) {
                root.child('policies').child(vm.policyKey).child('forms/' + vm.form.className).update({
                    publicGuid: uuidv4(),
                    status: 'In Progress'
                }).then(function(ref) {
                    // Set policy status back...
                    var statusText = 'In Progress';
                    if (!vm.policy.status.applicationSigned) {
                        statusText = 'Started';
                    }
                    root.child('policies').child(vm.policyKey).child('status/display').update({
                        text: statusText
                    }).then(function() {
                        sweetAlert.swal({
                            title: "Success",
                            text: "",
                            type: "success",
                            timer: 2500
                        }).then(function(result) {
                            vm.form.status = 'In Progress';
                            $scope.$apply();
                        });
                    }).catch(function(err) {
                        console.log(err.message);
                    });
                });
            });
        }

        vm.sendToThirdParty = function() {
            if (!vm.thirdParty || !vm.thirdParty.firstName || !vm.thirdParty.emailAddress) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Name and Email are required to send this form out for 3rd party completion.",
                    type: "error"
                }).then(function() {});
            } else {
                if (vm.pageIsDirty) {
                    sweetAlert.swal({
                        title: "Save Changes?",
                        text: "You have unsaved changes. Do you want to save first?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "No",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Yes, Save!",
                        closeOnConfirm: false
                    }).then(function(result) {
                        if (result.dismiss) {
                            continueSendToThirdParty();
                        } else {
                            vm.saveForm('requestThirdParty');
                        }
                    });
                }
            }
        }

        function continueSendToThirdParty() {
            // Copy any current form data to publicForms/[publicGuid] in Firebase
            var root = firebase.database().ref();

            vm.sendingToThirdParty = true;

            var publicGuid = vm.form.publicGuid;
            var className = vm.form.className;

            delete vm.form.agent;
            delete vm.form.recipients;
            delete vm.form.status;
            delete vm.form.publicGuid;
            delete vm.form.className;

            vm.form.alertId = vm.currentUser.profile.alertId;

            root.child('publicForms').child(publicGuid).update(vm.form).then(function(ref) {
                root.child('policies').child(vm.policyKey).child('forms/' + className).update({
                    status: 'Pending 3rd Party',
                    thirdParty: {
                        fullName: vm.thirdParty.firstName + ' ' + vm.thirdParty.lastName,
                        emailAddress: vm.thirdParty.emailAddress,
                        requestedAt: firebase.database.ServerValue.TIMESTAMP
                    }
                }).then(function(response) {
                    root.child('policies').child(vm.policyKey).child('status/display').update({
                        text: 'Pending 3rd Party'
                    }).then(function(response) {
                        // Create an email message to the recipient with link to [url]/view/application?pubId=[publicGuid]
                        var linkRoot = (ENV === 'dev') ? 'localhost:3000' : vm.subdomain + '.' + vm.domain + '.' + vm.tld;
                        var recipients = [];
                        recipients.push({
                            email: vm.thirdParty.emailAddress,
                            template: 'third-party-form-request',
                            subData: {
                                name: vm.thirdParty.firstName,
                                link: linkRoot + '/view/medical?pubId=' + publicGuid
                            }
                        });
                        notifyService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                            if (response.status === 'success') {
                                vm.sendingToThirdParty = false;
                                sweetAlert.swal({
                                    title: "Success",
                                    text: "The requested 3rd party has been emailed a link to this Policy.",
                                    type: "success",
                                    timer: 2500
                                }).then(function(result) {
                                    // Go back to the policy
                                    $state.go('policy', { pid: vm.policyKey });
                                });
                            } else {
                                vm.sendingToThirdParty = false;
                                console.log(response.message);
                                sweetAlert.swal({
                                    title: "Uh Oh!",
                                    text: "Error sending email to 3rd party recipient!",
                                    type: "error"
                                }).then(function() {});
                            }
                        });
                    }).catch(function(err) {
                        console.log(err);
                        vm.sendingToThirdParty = false;
                    });
                }).catch(function(err) {
                    console.log(err);
                    vm.sendingToThirdParty = false;
                });
            }).catch(function(err) {
                console.log(err);
                vm.sendingToThirdParty = false;
            });
        }
    }
    angular.module('yapp')
        .controller('MedicalQCtrl', MedicalQCtrl);
})();