(function(angular) {
    'use strict';

    /* @ngInject */
    function LoginCtrl(apiService, stdData, ENV, VER, $scope, $state, authService, Auth, currentAuth, sweetAlert, brandingDomain, verifyAccounts) {
        var vm = this;
        vm.version = VER;
        vm.user = {};
        vm.newUser = {};
        vm.resetting = false;
        vm.buyerInvite = {};
        vm.registeringUser = false;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        vm.accessDomain = brandingDomainArr[3];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
            vm.otherDomain = vm.subdomain.replace('isubmit', 'imanager');
            vm.otherDomainDisp = 'iManager';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
            vm.otherDomain = vm.subdomain.replace('imanager', 'isubmit');
            vm.otherDomainDisp = 'iSubmit';
        }

        setTimeout(function() {
            jQuery('.float-me').jvFloat();
            jQuery('.placeHolder').filter(function() {
                return this.value && this.value.length !== 0;
            }).addClass('active');
            jQuery('.pseudo-placeHolder').filter(function() {
                if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                    return true;
                }
            }).addClass('active');
        }, 0);

        vm.roles = stdData.isubmitRoles;
        if (vm.roles.indexOf('IMO') === -1) {
            vm.roles.push('IMO');
        }

        if (vm.subdomain.indexOf('imanager') > -1) {
            vm.roles = stdData.imanagerRoles;
        }

        var redirectParams = [];
        var hasRedirect = false;
        var redirectTo = '';
        Object.keys($state.params).forEach((key, index) => {
            if ($state.params[key]) {
                if (key === 'redirect') {
                    hasRedirect = true;
                    redirectTo = $state.params[key];
                } else {
                    redirectParams.push({
                        key: key,
                        value: $state.params[key]
                    });
                }
            }
        });

        if (currentAuth) {
            if (verifyAccounts && !currentAuth.emailVerified) {
                firebase.auth().currentUser.getToken(true).then(function() {
                    firebase.auth().currentUser.reload().then(function() {
                        if ($state.params.spk) {
                            authService.linkSellerWithPolicy(firebase.auth().currentUser,$state.params.spk).then(function(result) {
                                if (!firebase.auth().currentUser.emailVerified) {
                                    $state.go('pending');
                                } else {
                                    authService.getCurrentUser(firebase.auth().currentUser, true).then(function(user) {
                                        continueUserCheck(user, firebase.auth().currentUser);
                                    });
                                }       
                            });
                        } else {
                            if (!firebase.auth().currentUser.emailVerified) {
                                $state.go('pending');
                            } else {
                                authService.getCurrentUser(firebase.auth().currentUser, true).then(function(user) {
                                    continueUserCheck(user, firebase.auth().currentUser);
                                });
                            }
                        }
                    });
                });
            } else {
                if ($state.params.spk) {
                    authService.linkSellerWithPolicy(firebase.auth().currentUser,$state.params.spk).then(function(result) {
                        authService.getCurrentUser(currentAuth, true).then(function(user) {
                            continueUserCheck(user, currentAuth);
                        });
                    });
                } else {
                    authService.getCurrentUser(currentAuth, true).then(function(user) {
                        continueUserCheck(user, currentAuth);
                    });
                }
            }
        } else {
            continueUserCheck(null, null);
        }

        function continueUserCheck(user, currentAuth) {
            if (user && !user.profile.isAuthorized) {
                $state.go('not-authorized');
            } else if ($state.params.spk) {
                vm.sellerPolicyKey = $state.params.spk;
                vm.roles = ['Seller', 'Seller Representative'];
                vm.newUser.role = 'Seller';
            } else if ($state.params.ref) {
                vm.refKey = $state.params.ref;
                if (vm.refKey === 'back-office') {
                    // This registration is from a back office team member
                    vm.roles = ['Back Office'];
                } else if (vm.refKey.indexOf('pending') === 0) {
                    // This is coming from the pending "Check Again" link
                    // The user already has an account and is logged in
                    // Grab the "refKey" if it exists on the User's Profile
                    if (user && user.profile && user.profile.refKey) {
                        vm.refKey = user.profile.refKey;
                        // Continue as if this were a regular Referral Org association
                        linkOrgReferral(vm.refKey, user, currentAuth);
                    } else {
                        // Attempt to route to Profile with init: true
                        $state.go('profile', { 'init': 'true' });
                    }
                } else {
                    // This registration is from an Org referral
                    linkOrgReferral(vm.refKey, user, currentAuth);
                }
            } else if ($state.params.bok) {
                Auth.$signOut().then(function() {
                    if (!$state.params.ic) {
                        sweetAlert.swal({
                            title: "Uh Oh!",
                            text: "The provided Registration Link is expired or invalid",
                            type: "error"
                        }).then(function() {
                            $state.go('login');
                        });
                    } else {
                        vm.buyerOrgKey = $state.params.bok;
                        vm.invitationCode = $state.params.ic;
                        var root = firebase.database().ref();
                        root.child('orgs/public').child(vm.buyerOrgKey).once('value', function(snap) {
                            if (snap.exists()) {
                                vm.buyerOrgName = snap.val().name;
                                if (snap.val().inviteCode !== $state.params.ic) {
                                    sweetAlert.swal({
                                        title: "Uh Oh!",
                                        text: "The provided Registration Link is expired or invalid",
                                        type: "error"
                                    }).then(function() {
                                        vm.buyerOrgName = null;
                                        $state.go('login', { 'bok': null, 'ic': null });
                                    });
                                } else {
                                    vm.buyerInvite = {
                                        orgKey: vm.buyerOrgKey,
                                        orgType: snap.val().orgType
                                    };
                                    vm.showPage = true;
                                    $scope.$apply();
                                }
                            } else {
                                sweetAlert.swal({
                                    title: "Uh Oh!",
                                    text: "The provided Registration Link is expired or invalid",
                                    type: "error"
                                }).then(function() {
                                    vm.buyerOrgName = null;
                                    $state.go('login');
                                });
                            }
                        });
                    }
                });
            } 
            if (user && !$state.params.bok) {
                if ((user.profile.myOrg && user.profile.myOrg.checkKey && user.profile.myOrg.status === 'pending') || (!user.profile.myOrg && user.profile.refKey)) {
                    var refKey = (vm.refKey) ? vm.refKey : (user.profile.myOrg && user.profile.myOrg.checkKey) ? user.profile.myOrg.checkKey : user.profile.refKey;
                    // If logging in by a referral link and not already a member of the referring org, join to referring Org
                    if (vm.platform === 'imanager') {
                        linkOrgReferral(refKey, user, currentAuth);
                    } else {
                        if (!user.profile.myUpline) {
                            linkOrgReferral(refKey, user, currentAuth);
                        } else {
                            var doLink = true;
                            user.profile.myUpline.forEach(function(upline) {
                                if (upline.orgKey === refKey) {
                                    doLink = false;
                                }
                            });
                            if (doLink) {
                                linkOrgReferral(refKey, user, currentAuth);
                            }
                        }
                    }
                } else {
                    if (!user.profile.viewedProfile) {
                        $state.go('profile', { 'init': 'true' });
                    } else if (hasRedirect) {
                        var path = {};
                        redirectParams.forEach(param => {
                            path[param.key] = param.value
                        });
                        $state.go(redirectTo, path);
                    } else {
                        $state.go('inventory');
                    }
                }
            } else if (!$state.params.bok && !$state.params.spk) {
                vm.showPage = true;
            }
        }

        function linkOrgReferral(refKey, user, currentAuth) {
            if (user && user.profile && user.profile.myOrg && user.profile.myOrg.status === 'pending') {
                // Still pending admin approval...check if approved
                authService.checkForAdminRights(currentAuth).then(function(result) {
                    if (result.status === 'promoted') {
                        sweetAlert.swal({
                            title: "Congratulations!",
                            text: "You have been promoted to Admin for the Organization '" + result.message + "'. Please head to your Profile to check it out.",
                            type: "info"
                        }).then(function() {
                            if (vm.platform === 'imanager') {
                                if (!user.profile.viewedProfile) {
                                    $state.go('profile', { 'init': 'true' });
                                } else if (hasRedirect) {
                                    var path = {};
                                    redirectParams.forEach(param => {
                                        path[param.key] = param.value
                                    });
                                    $state.go(redirectTo, path);
                                } else {
                                    $state.go('profile');
                                }
                                return currentAuth;
                            }
                        });
                    } else {
                        console.log(result);
                        $state.go('profile');
                    }
                });
            } else {
                var root = firebase.database().ref();
                root.child('orgs/public').child(refKey).once('value', function(snap) {
                    if (snap.exists()) {
                        vm.invitedBy = snap.val().name;
                        vm.invitedByType = snap.val().orgType;
                        if (snap.val().seatsRemaining > 0) {
                            if (user) {
                                // // Confirm joining new Upline Org
                                // sweetAlert.swal({
                                //     title: "Join Upline Organization?",
                                //     text: "You are about to join " + vm.invitedBy + ". Do you wish to continue?",
                                //     type: "info",
                                //     showCancelButton: true,
                                //     cancelButtonText: "No",
                                //     confirmButtonText: "Yes"
                                // }).then(function(result) {
                                //     if (!result.dismiss) {
                                        if (vm.platform === 'isubmit') {
                                            authService.addUpline(currentAuth, refKey, vm.invitedBy, vm.invitedByType, false).then(function(result) {
                                                if (result === 'success') {
                                                    $state.go('profile');
                                                } else {
                                                    sweetAlert.swal({
                                                        title: "Uh Oh!",
                                                        text: "Unable to add this Upline - " + result,
                                                        type: "error"
                                                    }).then(function() {});
                                                }
                                                return currentAuth;
                                            });
                                        } else if (vm.platform === 'imanager') {
                                            authService.joinOrg(currentAuth, refKey, vm.invitedByType).then(function(result) {
                                                if (result === 'success') {
                                                    $state.go('profile', { 'init': 'true' });
                                                    return currentAuth;
                                                } else {
                                                    sweetAlert.swal({
                                                        title: "Uh Oh!",
                                                        text: "Unable to join this Organization - " + result,
                                                        type: "error"
                                                    }).then(function() {
                                                        vm.loggingIn = false;
                                                        $scope.$apply();
                                                    });
                                                }
                                            });
                                        }
                                    // } else if (!user.profile.viewedProfile) {
                                    //     $state.go('profile', { 'init': 'true' });
                                    // } else {
                                    //     $state.go('inventory');
                                    // }
                                // });
                            } else {
                                vm.showPage = true;
                                $scope.$apply();
                            }
                        } else {
                            sweetAlert.swal({
                                title: "Uh Oh!",
                                text: "The Referral Code provided is invalid",
                                type: "error"
                            }).then(function() {
                                $state.go('login');
                            });
                        }
                    } else if (user) {
                        $state.go('profile');
                    }
                }).catch(function(err) {
                    console.log(err);
                });
            }
        }

        vm.resetPassword = function(user) {
            vm.resetting = true;
            if (!user.email) {
                vm.resetting = false;
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Email cannot be blank",
                    type: "error"
                }).then(function() {});
            } else {
                setTimeout(function() {
                    vm.resetting = false;
                    authService.resetPassword(user.email).then(function() {
                        $state.go('login', { 'ref': vm.refKey });
                    }).catch(function(error) {
                        console.error("Error: ", error);
                    });
                }, 2000);
            }
        }

        vm.register = function(newUser) {
            if (!newUser.firstName || !newUser.lastName) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Name cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!newUser.email) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Email cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!newUser.password) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Password cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!newUser.role) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Please select a Role",
                    type: "error"
                }).then(function() {});
            } else if (newUser.password !== newUser.confirmPassword) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Passwords do not match",
                    type: "error"
                }).then(function() {});
            } else if (!newUser.tosAgreement) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "You must agree to the Terms of Service",
                    type: "error"
                }).then(function() {});
            } else if (!newUser.phone) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Phone number cannot be blank",
                    type: "error"
                }).then(function() {});
            } else {
                newUser.accessDomain = vm.accessDomain;
                if (!newUser.company) {
                    newUser.company = '';
                }
                vm.registeringUser = true;
                return authService.checkForPendingRegistration(vm.subdomain, newUser.email).then(function(response) {
                    if (response.status === 'success' || vm.buyerOrgKey) {
                        return authService.register(newUser)
                            .then(function(authUser) {
                                var isReferral = (vm.refKey && vm.refKey !== 'back-office') ? true : false;
                                authService.createUserProfile(newUser, authUser, isReferral, vm.refKey, vm.subdomain, vm.buyerInvite, vm.sellerPolicyKey).then(function(authUser) {
                                    if (!vm.refKey && !vm.sellerPolicyKey && vm.subdomain.indexOf('imanager') < 0) {
                                        // This is a registration that is not a referral, seller or a buyer registration. Add Atlas Life Settlements as Upline
                                        authService.addUpline(authUser, '-L8crXXb_c8NRSJUgRpn', 'LS Hub', 'backoffice', true).then(function(result) {
                                            return vm.login(newUser, true);
                                        });
                                    //} else if (!vm.refKey && vm.subdomain.indexOf('imanager') > -1) {
                                        // Buyer registrations do not have an upline...they are the top of their own buyer network.
                                    //    return vm.login(newUser, true);
                                    } else {
                                        return vm.login(newUser, true);
                                    }
                                });
                            })
                            .catch(function(error) {
                                sweetAlert.swal({
                                    title: "Uh Oh!",
                                    text: error.message,
                                    type: "error"
                                }).then(function() {
                                    vm.registeringUser = false;
                                    $scope.$apply();
                                });
                            });
                    } else {
                        sweetAlert.swal({
                            title: "Oops!",
                            text: "There is a pending invitation to register with this email address. Please check your email and use the provided link to complete the registration process.",
                            type: "info",
                            showCancelButton: true,
                            cancelButtonText: 'Resend Invite'
                        }).then(function(action) {
                            if (action.dismiss === 'cancel') {
                                console.log('Resending invitation email');
                                var recipients = [];
                                recipients.push({
                                    email: newUser.email,
                                    template: 'resend-invitation',
                                    validate: {
                                        checkPath: 'orgs/public/' + response.data,
                                        checkKey: 'status',
                                        checkVal: 'active',
                                        resendLink: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + response.data + '&ic='
                                    },
                                    subData: {
                                        link: 'imanager.' + vm.domain + '.' + vm.tld + '/buyers?tab=3',
                                        supplierName: 'n/a',
                                        supplierPhone: 'n/a',
                                        supplierEmail: 'n/a'
                                    }
                                });
                                apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                                    if (response.status === 'success') {
                                        sweetAlert.swal({
                                            title: "Success",
                                            text: "Registration Invitation has been resent!",
                                            type: "success"
                                        }, function() {
                                            vm.registeringUser = false;
                                            $scope.$apply();
                                        });
                                    } else {
                                        sweetAlert.swal({
                                            title: "Sorry!",
                                            text: "Email Invitation was not resent. Please contact Support",
                                            type: "error"
                                        }, function() {
                                            vm.registeringUser = false;
                                            $scope.$apply();
                                        });
                                    }
                                });
                            } else {
                                vm.registeringUser = false;
                                $scope.$apply();
                            }
                        });
                    }
                });
            }
            return false;
        }

        function continueLogin(user, authUser) {
            if (vm.refKey || (user.profile.myOrg && user.profile.myOrg.checkKey && user.profile.myOrg.status === 'pending') || (!user.profile.myOrg && user.profile.refKey)) {
                var refKey = (vm.refKey) ? vm.refKey : (user.profile.myOrg && user.profile.myOrg.checkKey) ? user.profile.myOrg.checkKey : user.profile.refKey;
                // If logging in by a referral link and not already a member of the referring org, join to referring Org
                if (vm.platform === 'imanager') {
                    linkOrgReferral(refKey, user, authUser);
                } else {
                    if (!user.profile.myUpline) {
                        linkOrgReferral(refKey, user, authUser);
                    } else {
                        var doLink = true;
                        user.profile.myUpline.forEach(function(upline) {
                            if (upline.orgKey === refKey) {
                                doLink = false;
                            }
                        });
                        if (doLink) {
                            linkOrgReferral(refKey, user, authUser);
                        } else {
                            $state.go('profile');
                        }
                    }
                }
            } else if (vm.buyerOrgKey && vm.invitationCode) {
                // If logging in with a Buyer Invitation code...
                // 1) The invitation code and buyer org are already confirmed
                // 2) Update the User's "myOrg" node with the invited Buyer Org
                authService.joinInvitedOrg(authUser, vm.buyerInvite).then(function(response) {
                    console.log(response);
                    if (!user.profile.viewedProfile) {
                        $state.go('profile', { 'init': 'true' });
                    } else {
                        $state.go('profile');
                    }
                });
            } else if (vm.newUser) {
                if (!user.profile.viewedProfile) {
                    $state.go('profile', { 'init': 'true' });
                } else if (hasRedirect) {
                    var path = {};
                    redirectParams.forEach(param => {
                        path[param.key] = param.value
                    });
                    $state.go(redirectTo, path);
                } else {
                    $state.go('profile');
                }
                return authUser;
            } else {
                if (!user.profile.viewedProfile) {
                    $state.go('profile', { 'init': 'true' });
                } else if (hasRedirect) {
                    var path = {};
                    redirectParams.forEach(param => {
                        path[param.key] = param.value
                    });
                    $state.go(redirectTo, path);
                } else {
                    $state.go('inventory');
                }
                return authUser;
            }
        }

        vm.login = function(user, fromRegistration) {
            if (!user.email) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Email cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!user.password) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Password cannot be blank",
                    type: "error"
                }).then(function() {});
            } else {
                vm.loggingIn = true;
                return authService.login(user)
                    .then(function(authUser) {
                        if (vm.sellerPolicyKey) {
                            authService.linkSellerWithPolicy(authUser,vm.sellerPolicyKey).then(function(result) {
                                console.log('User linked with policy successfully');
                            });
                        }
                        if (fromRegistration && verifyAccounts) {
                            // Send verification email and redirect to Pending page
                            authService.verifyEmail().then(function(response) {
                                $state.go('pending');
                            });
                        } else {
                            // Validate that the user has access to the current domain
                            authService.checkCustomClaims(authUser.uid, 'accessDomain', vm.accessDomain).then(function(result) {
                                if (result.status === 'error') {
                                    sweetAlert.swal({
                                        title: "Access Denied",
                                        html: "You do not have access to the current domain. Please check your URL and make sure you're logging into the right location.<br><br>Are you trying to log into the <a href='http://" + vm.otherDomain + "." + vm.domain + "." + vm.tld + "'>" + vm.otherDomainDisp + " Platform</a>?",
                                        type: "error"
                                    }).then(function() {
                                        vm.loggingIn = false;
                                        $scope.$apply();
                                    });
                                } else {
                                    // Check for new Admin rights
                                    authService.checkForAdminRights(authUser).then(function(result) {
                                        if (result.status === 'promoted') {
                                            sweetAlert.swal({
                                                title: "Congratulations!",
                                                text: "You have been promoted to Admin for the Organization '" + result.message + "'. Please head to your Profile to check it out.",
                                                type: "info"
                                            }).then(function() {
                                                // Get the user object
                                                authService.getCurrentUser(authUser, true).then(function(user) {
                                                    if (!user.profile.isAuthorized) {
                                                        $state.go('not-authorized');
                                                    } else if (vm.refKey) {
                                                        // If logging in by a referral link, join to referring Org
                                                        if (vm.platform === 'isubmit') {
                                                            authService.addUpline(authUser, vm.refKey, vm.invitedBy, vm.invitedByType, false).then(function() {
                                                                if (!user.profile.viewedProfile) {
                                                                    $state.go('profile', { 'init': 'true' });
                                                                } else if (hasRedirect) {
                                                                    var path = {};
                                                                    redirectParams.forEach(param => {
                                                                        path[param.key] = param.value
                                                                    });
                                                                    $state.go(redirectTo, path);
                                                                } else {
                                                                    $state.go('profile');
                                                                }
                                                                return authUser;
                                                            });
                                                        } else if (vm.platform === 'imanager') {
                                                            if (!user.profile.viewedProfile) {
                                                                $state.go('profile', { 'init': 'true' });
                                                            } else if (hasRedirect) {
                                                                var path = {};
                                                                redirectParams.forEach(param => {
                                                                    path[param.key] = param.value
                                                                });
                                                                $state.go(redirectTo, path);
                                                            } else {
                                                                $state.go('profile');
                                                            }
                                                            return authUser;
                                                        }
                                                    } else if (vm.newUser) {
                                                        if (!user.profile.viewedProfile) {
                                                            $state.go('profile', { 'init': 'true' });
                                                        } else if (hasRedirect) {
                                                            var path = {};
                                                            redirectParams.forEach(param => {
                                                                path[param.key] = param.value
                                                            });
                                                            $state.go(redirectTo, path);
                                                        } else {
                                                            $state.go('profile');
                                                        }
                                                        return authUser;
                                                    } else {
                                                        if (!user.profile.viewedProfile) {
                                                            $state.go('profile', { 'init': 'true' });
                                                        } else if (hasRedirect) {
                                                            var path = {};
                                                            redirectParams.forEach(param => {
                                                                path[param.key] = param.value
                                                            });
                                                            $state.go(redirectTo, path);
                                                        } else {
                                                            $state.go('inventory');
                                                        }
                                                        return authUser;
                                                    }
                                                });
                                            });
                                        } else {
                                            authService.getCurrentUser(authUser, true).then(function(user) {
                                                if (!user.profile.isAuthorized) {
                                                    $state.go('not-authorized');
                                                } else if (user.profile.myUpline) {
                                                    // Validate that the member is STILL a member of each upline org (that they haven't been manually removed)
                                                    async.forEachOfSeries(user.profile.myUpline, function(upline, key, callback) {
                                                        authService.validateUpline(user.auth.uid, upline, key).then(function(response) {
                                                            console.log(response);
                                                            callback();
                                                        });
                                                    }, function(err) {
                                                        if (err) {
                                                            console.log(err);
                                                        }
                                                        continueLogin(user, authUser);
                                                    });
                                                } else {
                                                    continueLogin(user, authUser);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    })
                    .catch(function(error) {
                        sweetAlert.swal({
                            title: "Uh Oh!",
                            text: error.message,
                            type: "error"
                        }).then(function() {
                            vm.loggingIn = false;
                            $scope.$apply();
                        });
                    });
            }
        }
    }

    angular.module('yapp')
        .controller('LoginCtrl', LoginCtrl);

})(window.angular);