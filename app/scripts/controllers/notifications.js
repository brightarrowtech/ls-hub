(function() {
    'use strict';

    /* @ngInject */
    function NotificationsCtrl($fancyModal, ENV, toastr, $state, $scope, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain) {
        var vm = this;
        vm.sortCol = 'createdAt';
        vm.sortReverse = true;
        vm.userNotifications = [];
        vm.orgNotifications = [];
        vm.buyerNotifications = [];
        vm.notifications = [];

        var root = firebase.database().ref();

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                    vm.isAdmin = true;
                }
                // GET NOTIFICATIONS
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.auth.uid).once('value', function(userNotifications) {
                    vm.userNotificationCount = 0;
                    if (userNotifications.exists()) {
                        userNotifications.forEach(function(n) {
                            vm.notifications.push(n.val());
                        });
                    }
                    if (vm.currentUser.org && vm.currentUser.org.orgGuid) {
                        // Get Buyer Notifications
                        root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.orgGuid).once('value', function(orgNotifications) {
                            if (orgNotifications.exists()) {
                                orgNotifications.forEach(function(n) {
                                    let notification = n.val();
                                    var processNotification = true;
                                    if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                        processNotification = false;
                                    } else if (vm.currentUser.profile.role === 'Back Office' && notification.createdByRole !== 'iBroker') {
                                        processNotification = false;
                                    }
                                    if (processNotification) {
                                        vm.buyerNotifications.push(notification);
                                    }
                                });
                                vm.notifications = vm.notifications.concat(vm.buyerNotifications);
                                getOrgNotifications();
                            } else {
                                getOrgNotifications();
                            }
                        }).catch(function(err) {
                            console.log('Error retrieving buyer-level notifications');
                            console.log(err);
                            getOrgNotifications();
                        });
                    } else {
                        getOrgNotifications();
                    }
                }).catch(function(err) {
                    console.log('Error retrieving user-level notifications');
                    console.log(err);
                    getOrgNotifications();
                });
            }
        });

        function getOrgNotifications() {
            // Get non-buyer Org notifications
            if (vm.currentUser.org) {
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.$id).once('value', function(orgNotifications) {
                    if (orgNotifications.exists()) {
                        orgNotifications.forEach(function(n) {
                            let notification = n.val();
                            var processNotification = true;
                            if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                processNotification = false;
                            } else if (vm.currentUser.profile.role === 'Back Office' && notification.createdByRole !== 'iBroker') {
                                processNotification = false;
                            }
                            if (processNotification) {
                                vm.orgNotifications.push(notification);
                            }
                        });
                        vm.notifications = vm.notifications.concat(vm.orgNotifications);
                        $scope.$apply();
                    } else {
                        $scope.$apply();
                    }
                }).catch(function(err) {
                    console.log('Error retrieving org-level notifications');
                    console.log(err);
                    $scope.$apply();
                });
            } else {
                $scope.$apply();
            }
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        vm.goTo = function(route, n) {
            if (n.policyKey && n.pid && n.bid && n.lvl) {
                $state.go('buyerPolicy', { pid: n.pid, bid: n.bid, pk: n.policyKey, lvl: n.lvl });
            } else if (n.policyKey && n.pid && n.bid) {
                $state.go('buyerPolicy', { pid: n.pid, bid: n.bid, pk: n.policyKey });
            } else if (!n.policyKey && n.pid && n.bid) {
                $state.go('buyerPolicy', { pid: n.pid, bid: n.bid });
            } else if (n.policyKey) {
                $state.go('policy', { pid: n.policyKey });
            } else {
                $state.go(route);
            }
        }
    }
    angular.module('yapp')
        .controller('NotificationsCtrl', NotificationsCtrl);
})();