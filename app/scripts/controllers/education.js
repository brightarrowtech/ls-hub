(function() {
    'use strict';

    /* @ngInject */
    function EducationCtrl($window, notifyService, stdData, policyService, $fancyModal, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain) {
        var vm = this;
        vm.pageIsDirty = false;
        vm.form = {
            trackingData: []
        };
        vm.checkingOrg = true;
        vm.myOrgUplineName = 'n/a';
        vm.isBdn = false;
        vm.seatOrgLevels = [];
        vm.iteration = 0;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        //vm.subdomain = vm.subdomain.replace(/demo/g,'local');
        vm.invitationLinkRoot = '//' + vm.subdomain + '.' + vm.domain + '.' + vm.tld;
        // if (ENV === 'demo') {
        //     vm.invitationLinkRoot += ':3000';
        // }

        vm.yesNo = stdData.yesNo;
        if (vm.platform === 'isubmit') {
            vm.roles = stdData.isubmitRoles;
        } else {
            vm.roles = stdData.imanagerRoles;
        }

        var root = firebase.database().ref();

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                loadPage();
            }
        });

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function loadPage() {
            vm.forms = $firebaseArray(root.child('marketing/' + vm.currentUser.org.$id + '/forms'));
            // resetFormFields().then(function() {
            //     vm.updateForm();
            // });
        }

    }

    angular.module('yapp')
        .controller('EducationCtrl', EducationCtrl);
})();