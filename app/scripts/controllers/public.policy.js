(function() {
    'use strict';

    /* @ngInject */
    function PublicPolicyCtrl(stdData, toastr, $scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, sweetAlert, notifyService, apiService, brandingDomain, sharedService) {
        var vm = this;
        vm.newAdminFiles = [];
        vm.newCaseFiles = [];
        vm.statusImage = 'Timeline';
        vm.showPage = 1;
        vm.isCreator = false;
        vm.pageIsDirty = false;
        vm.selectedBuyers = [];

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.fileCategories = stdData.fileCategories;
        vm.states = stdData.states;
        vm.genders = stdData.genders;
        vm.maritalStatus = stdData.maritalStatus;
        vm.types = stdData.policyTypes;
        vm.yesNo = stdData.yesNo;

        loadPage();

        function loadPage() {
            var root = firebase.database().ref();

            if ($state.params.pubId) {
                /**
                 * LOADING AN EXISTING POLICY
                 */
                //vm.policyPubId = $state.params.pubId;
                root.child('buyerPolicies/' + $state.params.pubId).once('value', function(snap) {
                    if (snap.exists()) {
                        vm.policyGuid = snap.val().policyGuid;
                        console.log(vm.policyGuid);
                        vm.policy = $firebaseObject(root.child('privatePolicies').child(vm.policyGuid));
                        vm.policy.$loaded().then(function() {
                            vm.storage = firebase.storage();
                            vm.policyKey = vm.policy.policyKey;
                            vm.storageRef = vm.storage.ref('policies/' + vm.policyKey);
                            if (!vm.policy.caseFiles) {
                                vm.policy.caseFiles = [];
                            }
                            // adjustStatusImage();
                            // updateLabels();
                            $scope.$apply();
                        });
                    } else {
                        $state.go('no-access');
                    }
                });
            } else {
                $state.go('no-access');
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    maxDate: new Date(),
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.policy[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = vm.policy[parts[0]][parts[1]];
                        } else if (parts.length === 3) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]];
                        } else if (parts.length === 4) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]][parts[3]];
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('input').on('ifChecked', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = true;
                        vm.savePrivate();
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = true;
                        vm.policy.$save(vm.policy.policyIsActive).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        if (event.target.name === 'policyActionRequired') {
                            vm.setActionRequired('agent')
                        } else if (event.target.name === 'policyBOActionRequired') {
                            vm.setActionRequired('back-office')
                        }
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = vm.buyers;
                    }
                    $scope.$apply();
                });
                jQuery('input').on('ifUnchecked', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = false;
                        vm.savePrivate();
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = false;
                        vm.policy.$save(vm.policy.policyIsActive).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        vm.policy.secondaryInsured.isDeceased = false;
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        updateStatus();
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = [];
                    }
                    $scope.$apply();
                });
                jQuery('.tabs').tabslet();
                $scope.$apply();
            }, 0);
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function sendNotification(data) {
            // Add targets to notify
            // Add policy creator if not self and only if there is no creating org (creator will be included with the org notification)
            // if (vm.policy.createdById !== vm.currentUser.auth.uid && !vm.policy.createdByOrgKey) {
            //     data.targets.push({
            //         type: 'user',
            //         id: vm.policy.createdById
            //     });
            // }
            // // Add org upline, not including own org, unless you are SAdmin or Back Office
            // vm.policy.upline.forEach(function(uplineOrg) {
            //     if (uplineOrg.orgKey !== vm.currentUser.profile.myOrg.orgKey || vm.currentUser.profile.role === 'SAdmin' || vm.currentUser.profile.role === 'Back Office') {
            //         data.targets.push({
            //             type: 'org',
            //             id: uplineOrg.orgKey
            //         });
            //     }
            // });
            // notifyService.addNotification(data).then(function() {
            //     vm.toast('success', 'Notifications sent');
            // });
        }

        vm.setActionRequired = function(type) {
            if (type === 'agent') {
                sweetAlert.swal({
                    title: "Add a Public Note",
                    text: "Please add a Note so the Agent knows what they need to do",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.actionRequired = true;
                        // Save Public Note
                        vm.newPublicNote = response.value;
                        vm.saveNewPublicNote();
                        updateStatus();
                    } else {
                        vm.policy.actionRequired = false;
                        $scope.$apply();
                    }
                });
            } else if (type === 'back-office') {
                sweetAlert.swal({
                    title: "Add an Internal Note",
                    text: "Please add a Note so the Back Office knows what they need to do",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.boActionRequired = true;
                        // Save Private Note
                        vm.newAdminNote = response.value;
                        vm.saveNewAdminNote();
                        updateStatus();
                    } else {
                        vm.policy.boActionRequired = false;
                        $scope.$apply();
                    }
                });
            }
        }

        vm.showDetails = function(form, index, event) {
            if (vm.showFormDetails === index) {
                // Close the detail view
                vm.showFormDetails = -1;
                angular.element(document.querySelector('#' + form.className + '-container')).removeClass('show-detail');
            } else {
                vm.showFormDetails = index;
                angular.element(document.querySelectorAll('.show-detail')).removeClass('show-detail');
                angular.element(document.querySelector('#' + form.className + '-container')).addClass('show-detail');
            }
            event.stopPropagation();
        }

        vm.cancelByReload = function() {
            $state.reload();
            updateLabels();
        }

        vm.savePrivate = function() {
            vm.policy.$save(vm.policy.private).then(function(ref) {
                vm.toast('success', 'Policy Updated');
                vm.privateIsDirty = false;
            });
        }

        vm.editPolicy = function() {
            if (vm.policy.status.applicationFinished) {
                vm.showEditInstructions = true;
            } else {
                vm.loadForm(vm.policy.forms['application']);
            }
        }

        vm.submitToBuyers = function() {
            var selectedBuyersPlural = (vm.selectedBuyers.length > 1) ? 's' : '';
            sweetAlert.swal({
                title: "Confirm Action",
                text: "The " + vm.selectedBuyers.length + " selected Buyer" + selectedBuyersPlural + " will receive an email with a link to this Policy. Are you sure you wish to proceed?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    sweetAlert.swal({
                        title: "Success",
                        text: "",
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                }
            });
        }

        vm.resetApplicationForm = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "The previously submitted Application Form will only be recoverable by contacting LS Hub support. Are you sure you wish to proceed?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    if (!vm.policy.archivedForms) {
                        vm.policy.archivedForms = {};
                    }
                    vm.policy.archivedForms['application-' + moment()] = sharedService.cloneObject(vm.policy.forms['application']);
                    delete vm.policy.forms['application'].createdDtTm;
                    delete vm.policy.forms['application'].documentId;
                    delete vm.policy.forms['application'].publicGuid;
                    delete vm.policy.forms['application'].signMethod;
                    vm.policy.forms['application'].status = 'In Progress';
                    delete vm.policy.status.applicationFinished;
                    delete vm.policy.status.applicationSentForSignature;
                    vm.policy.status.display = {
                        status: 'In Progress',
                        step: 'Application',
                        text: 'Started'
                    };
                    vm.policy.$save().then(function(ref) {
                        vm.showEditInstructions = false;
                        vm.toast('success', 'Application Form has been re-opened for editing');
                    });
                }
            });
        }

        vm.loadForm = function(form) {
            if (form.className === 'medicalPrimary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyKey, recipient: 'Primary Insured' });
            } else if (form.className === 'medicalSecondary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyKey, recipient: 'Secondary Insured' });
            } else if (form.className === 'application' && (form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('application', { pid: vm.policyKey });
            } else if (form.status === 'Complete') {
                window.open(form.downloadURL, '_blank');
            } else if (form.status === 'Pending Upload') {
                window.open(form.downloadURL, '_blank');
            } else {
                window.open(
                    'https://us-central1-lis-pipeline.cloudfunctions.net/viewDocument?documentId=' + form.documentId + '&email=' + encodeURIComponent(vm.currentUser.profile.email),
                    '_blank'
                );
            }
        }

        function adjustStatusImage() {
            vm.statusImage = 'Timeline';
            if (vm.policy.status.applicationSigned) {
                vm.statusImage += '_1';
            }
            if (vm.policy.status.illustrationsReceived) {
                if (vm.policy.status.illustrationsOrdered) {
                    vm.statusImage += '_2B';
                } else {
                    vm.policy.status.illustrationsReceived = false;
                }
            } else if (vm.policy.status.illustrationsOrdered) {
                vm.statusImage += '_2A';
            }
            if (vm.policy.status.medicalsReceived) {
                if (vm.policy.status.medicalsOrdered) {
                    vm.statusImage += '_3B';
                } else {
                    vm.policy.status.medicalsReceived = false;
                }
            } else if (vm.policy.status.medicalsOrdered) {
                vm.statusImage += '_3A';
            }
            if (vm.policy.status.medicalSummaryReceived) {
                if (!vm.policy.status.medicalSummaryRequested) {
                    vm.policy.status.medicalSummaryReceived = false;
                }
            }
            if (vm.policy.status.outForOffers) {
                if (vm.policy.status.illustrationsReceived && vm.policy.status.medicalsReceived) {
                    if (vm.policy.status.offerInterest) {
                        vm.statusImage = 'Timeline_4Int';
                    } else if (vm.policy.status.offerNoInterest) {
                        vm.statusImage = 'Timeline_4NoInt';
                    } else {
                        vm.statusImage = 'Timeline_4A';
                    }
                } else {
                    vm.policy.status.outForOffers = false;
                }
            }
            if (vm.policy.status.offerReceived) {
                if (vm.policy.status.outForOffers) {
                    vm.statusImage = 'Timeline_4B';
                } else {
                    vm.policy.status.offerReceived = false;
                }
            }
        }

        function updateStatus() {
            vm.policy.status.display.text = 'Started';
            if (vm.policy.status.applicationSigned) {
                vm.policy.status.display.text = 'In Progress';
            }
            if (vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Out for Offers';
            }
            if (vm.policy.status.offerNoInterest) {
                vm.policy.status.display.text = 'Case Rejected';
            }
            if (vm.policy.status.offerReceived) {
                vm.policy.status.display.text = 'Offer Received';
            }
            Object.keys(vm.policy.forms).forEach(function(form) {
                if (vm.policy.forms[form].status === 'Pending Signatures') {
                    vm.policy.status.display.text = 'Pending Signatures';
                } else if (vm.policy.forms[form].status === 'Pending 3rd Party') {
                    vm.policy.status.display.text = 'Pending 3rd Party';
                } else if (vm.policy.forms[form].status === 'Needs Review') {
                    vm.policy.status.display.text = 'Pending Agent Review';
                }
            });
            if (vm.policy.actionRequired) {
                vm.policy.status.display.text = 'Agent Action Required';
            }
            if (vm.policy.boActionRequired) {
                vm.policy.status.display.text = 'Back Office Action Required';
                delete vm.policy.status.submittedToBroker;
            }
            if (vm.policy.status.submittedToBroker && !vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Pending Broker Review';
            }
            vm.policy.$save(vm.policy.status).then(function(ref) {
                vm.toast('success', 'Status updated');
            });
        }

        vm.goToPage = function(page) {
            if (page > vm.showPage && vm.pageIsDirty) {
                vm.savePolicy('nextPage');
            } else {
                vm.showPage = page;
                updateLabels();
            }
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        $state.go(route);
                    } else {
                        vm.savePolicy('close');
                    }
                });
            } else {
                $state.go(route);
            }
        }

        vm.formUpload = function(type, className, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                vm.newFiles = [];
                vm.newFiles.push({
                    name: $event.target.files[i].name,
                    type: $event.target.files[i].type,
                    date: $event.target.files[i].lastModified,
                    file: $event.target.files[i]
                });
            }
            $scope.$apply();
            vm.doUpload(type, vm.newFiles, className);
        }

        vm.fileChanged = function(type, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                if (type === 'admin') {
                    vm.newAdminFiles.push({
                        name: $event.target.files[i].name,
                        type: $event.target.files[i].type,
                        date: $event.target.files[i].lastModified,
                        file: $event.target.files[i]
                    });
                } else if (type === 'case') {
                    vm.newCaseFiles.push({
                        name: $event.target.files[i].name,
                        type: $event.target.files[i].type,
                        date: $event.target.files[i].lastModified,
                        file: $event.target.files[i]
                    });
                }
            }
            $scope.$apply();
            if (type === 'admin') {
                vm.doUpload(type, vm.newAdminFiles, null);
            } else if (type === 'case') {
                vm.doUpload(type, vm.newCaseFiles, null);
            }
        }

        vm.doUpload = function(type, files, className) {
            // files.forEach(function(fileObj) {
            //     var metadata = {
            //         contentType: fileObj.file.type,
            //         customMetadata: {
            //             'uploadedAt': moment(),
            //             'uploadedBy': vm.currentUser.profile.displayName,
            //             'uploadedById': vm.currentUser.auth.uid
            //         }
            //     };

            //     var storage = $firebaseStorage(vm.storageRef.child(type).child(fileObj.file.name));
            //     var uploadTask = storage.$put(fileObj.file, metadata);

            //     uploadTask.$progress(function(snapshot) {
            //         var percentUploaded = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            //     });

            //     uploadTask.$complete(function(snapshot) {
            //         fileObj.link = snapshot.downloadURL;
            //         fileObj.uploadedAt = firebase.database.ServerValue.TIMESTAMP;
            //         fileObj.uploadedBy = vm.currentUser.profile.displayName;
            //         fileObj.uploadedById = vm.currentUser.auth.uid;

            //         if (!vm.policy.updateLog) {
            //             vm.policy.updateLog = [];
            //         }

            //         vm.policy.updateLog.push({
            //             action: 'File uploaded to ' + type + ' tab: ' + fileObj.file.name,
            //             updatedAt: firebase.database.ServerValue.TIMESTAMP,
            //             updatedBy: vm.currentUser.profile.displayName,
            //             updatedById: vm.currentUser.auth.uid
            //         });

            //         var data = {
            //             targets: [],
            //             policyKey: vm.policyKey,
            //             createdBy: vm.currentUser.profile.displayName,
            //             createdByRole: vm.currentUser.profile.role,
            //             note: 'New File has been uploaded: ' + fileObj.file.name,
            //             action: 'New File Uploaded'
            //         };
            //         sendNotification(data);

            //         if (type === 'admin') {
            //             if (!vm.policy.adminFiles) {
            //                 vm.policy.adminFiles = [];
            //             }
            //             vm.policy.adminFiles.push(fileObj);
            //             vm.newAdminFiles.splice(fileObj, 1);
            //             if (vm.newAdminFiles && vm.newAdminFiles.length === 0) {
            //                 vm.policy.$save(vm.policy.adminFiles).then(function(ref) {
            //                     console.log('Files saved to server');

            //                     vm.policy.$save(vm.policy.updateLog).then(function(ref) {
            //                         console.log('Log history updated');
            //                     });
            //                 });
            //             }
            //         } else if (type === 'case') {
            //             if (!vm.policy.caseFiles) {
            //                 vm.policy.caseFiles = [];
            //             }
            //             vm.policy.caseFiles.push(fileObj);
            //             vm.newCaseFiles.splice(fileObj, 1);
            //             if (vm.newCaseFiles && vm.newCaseFiles.length === 0) {
            //                 vm.policy.$save(vm.policy.caseFiles).then(function(ref) {
            //                     console.log('Files saved to server');

            //                     vm.policy.$save(vm.policy.updateLog).then(function(ref) {
            //                         console.log('Log history updated');
            //                     });
            //                 });
            //             }
            //         } else if (type === 'requested') {
            //             vm.policy.forms[className].downloadURL = fileObj.link;
            //             vm.policy.forms[className].status = 'Complete';
            //             vm.policy.forms[className].completedDtTm = firebase.database.ServerValue.TIMESTAMP;
            //             vm.policy.status[className + 'Signed'] = true;
            //             vm.policy.$save(vm.policy.forms[className]).then(function(ref) {
            //                 console.log('File saved to server');

            //                 vm.policy.status.display.status = 'In Progress';
            //                 vm.policy.status.display.text = 'In Progress';

            //                 vm.policy.$save(vm.policy.status).then(function(ref) {
            //                     console.log('Policy status updated');

            //                     vm.policy.$save(vm.policy.updateLog).then(function(ref) {
            //                         console.log('Log history updated');

            //                         $scope.$apply();
            //                     });
            //                 });
            //             });
            //         }
            //     });
            // });
        }

        vm.updateFileMetadata = function(type, file, index) {
            var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
            var meta = {
                category: file.category
            };
            vm.policy.adminFiles[index].category = file.category;
            vm.policy.$save(vm.policy.adminFiles).then(function(ref) {
                storage.$updateMetadata(meta).then(function(updatedMeta) {
                    vm.toast('success', 'File category updated');
                });
            });
        }

        vm.downloadAll = function(type) {
            var files = vm.policy.caseFiles;
            if (type === 'admin') {
                files = vm.policy.adminFiles;
            }

            if (!files || files.length === 0) {
                vm.toast('error', 'No files to download');
            } else {
                var zip = new JSZip();
                var count = 0;
                var zipFilename = vm.policy.policyNumber + ".zip";

                files.forEach(function(file) {
                    var filename = file.name;
                    // loading a file and add it in a zip file
                    JSZipUtils.getBinaryContent(file.link, function(err, data) {
                        if (err) {
                            throw err;
                        }
                        zip.file(filename, data, { binary: true });
                        count++;
                        if (count == files.length) {
                            zip.generateAsync({ type: "blob" })
                                .then(function(zipFile) {
                                    saveAs(zipFile, zipFilename);
                                });
                        }
                    });
                });
            }
        }

        vm.removeFile = function(type, file, index) {
            // if (vm.isIMO || vm.isAdmin || file.uploadedById === vm.currentUser.auth.uid) {
            //     sweetAlert.swal({
            //         title: "Are you sure?",
            //         text: "Do you really want to remove this file?",
            //         type: "warning",
            //         showCancelButton: true,
            //         cancelButtonText: "No",
            //         confirmButtonColor: "#B20000",
            //         confirmButtonText: "Yes, delete it!",
            //         closeOnConfirm: false
            //     }).then(function(result) {
            //         if (!result.dismiss) {
            //             var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
            //             var uploadTask = storage.$delete().then(function() {
            //                 if (!vm.policy.updateLog) {
            //                     vm.policy.updateLog = [];
            //                 }

            //                 vm.policy.updateLog.push({
            //                     action: 'File removed from ' + type + ' tab: ' + file.name,
            //                     updatedAt: firebase.database.ServerValue.TIMESTAMP,
            //                     updatedBy: vm.currentUser.profile.displayName,
            //                     updatedById: vm.currentUser.auth.uid
            //                 });

            //                 vm.policy.$save(vm.policy.updateLog).then(function(ref) {
            //                     console.log('Log history updated');
            //                 });

            //                 if (type === 'admin') {
            //                     vm.policy.adminFiles.splice(index, 1);
            //                     vm.policy.$save(vm.policy.adminFiles).then(function(ref) {
            //                         if (!vm.policy.adminFiles || vm.policy.adminFiles.length === 0) {
            //                             vm.policy.adminFiles = [];
            //                         }
            //                         vm.toast('success', 'File removed');
            //                     });
            //                 } else if (type === 'case') {
            //                     vm.policy.caseFiles.splice(index, 1);
            //                     vm.policy.$save(vm.policy.caseFiles).then(function(ref) {
            //                         if (!vm.policy.caseFiles || vm.policy.caseFiles.length === 0) {
            //                             vm.policy.caseFiles = [];
            //                         }
            //                         vm.toast('success', 'File removed');
            //                     });
            //                 }
            //             });
            //         }
            //     });
            // }
        }

        vm.archivePolicy = function() {
            // if (vm.isIMO || vm.isAdmin || file.uploadedById === vm.currentUser.auth.uid) {
            //     sweetAlert.swal({
            //         title: "Are you sure?",
            //         text: "Deleted Policies will be archived for some time, but will be purged when resources are needed. See Administrator for Policy Restoration details.",
            //         type: "warning",
            //         showCancelButton: true,
            //         cancelButtonText: "No",
            //         confirmButtonColor: "#B20000",
            //         confirmButtonText: "Yes, delete it!",
            //         closeOnConfirm: false
            //     }).then(function(result) {
            //         if (!result.dismiss) {
            //             vm.policy.isArchived = true;
            //             vm.policy.$save(vm.policy).then(function(ref) {
            //                 vm.toast('success', 'Policy has been Archived');
            //                 setTimeout(function() {
            //                     $state.go('inventory');
            //                 }, 1000);
            //             });
            //         }
            //     });
            // }
        }

        vm.onSelectCallback = function(input) {
            vm.pageIsDirty = true;
            jQuery('.placeHolder-' + input).addClass('active');
            if (input === 'type' && vm.policy.type !== 'Survivorship Universal Life') {
                vm.policy.secondaryInsured = {};
            } else if (input === 'category') {

            }
            updateLabels();
        }

        vm.removePublicNote = function(index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this note?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.policy.publicNotes[index].isDeleted = true;
                    vm.policy.$save(vm.policy.publicNotes).then(function() {
                        console.log('Note removed');
                        vm.toast('success', 'Public Note removed');
                    });
                }
            });
        }

        vm.saveNewPublicNote = function() {
            // if (!vm.policy.publicNotes) {
            //     vm.policy.publicNotes = [];
            // }
            // vm.policy.publicNotes.push({
            //     createdBy: vm.currentUser.profile.displayName,
            //     createdById: vm.currentUser.auth.uid,
            //     createdByRole: vm.currentUser.profile.role,
            //     createdAt: firebase.database.ServerValue.TIMESTAMP,
            //     note: vm.newPublicNote
            // });
            // vm.policy.$save(vm.policy.publicNotes).then(function() {
            //     console.log('Note saved');
            //     vm.toast('success', 'Public Note saved');
            //     var data = {
            //         targets: [],
            //         policyKey: vm.policyKey,
            //         createdBy: vm.currentUser.profile.displayName,
            //         createdByRole: vm.currentUser.profile.role,
            //         note: vm.newPublicNote,
            //         action: 'New Public Note'
            //     };
            //     sendNotification(data);
            //     vm.newPublicNote = '';
            // });
        }

        vm.removeAdminNote = function(index) {
            // sweetAlert.swal({
            //     title: "Are you sure?",
            //     text: "Do you really want to remove this note?",
            //     type: "warning",
            //     showCancelButton: true,
            //     cancelButtonText: "No",
            //     confirmButtonColor: "#B20000",
            //     confirmButtonText: "Yes, delete it!",
            //     closeOnConfirm: false
            // }).then(function(result) {
            //     if (!result.dismiss) {
            //         vm.policy.private.adminNotes.splice(index, 1);
            //         vm.policy.$save(vm.policy.private.adminNotes).then(function() {
            //             console.log('Note removed');
            //             vm.toast('success', 'Internal Note removed');
            //         });
            //     }
            // });
        }

        vm.saveNewAdminNote = function() {
            // if (!vm.policy.private) {
            //     vm.policy.private = {};
            // }
            // if (!vm.policy.private.adminNotes) {
            //     vm.policy.private.adminNotes = [];
            // }
            // vm.policy.private.adminNotes.push({
            //     createdBy: vm.currentUser.profile.displayName,
            //     createdById: vm.currentUser.auth.uid,
            //     createdByRole: vm.currentUser.profile.role,
            //     createdAt: firebase.database.ServerValue.TIMESTAMP,
            //     note: vm.newAdminNote
            // });
            // if (vm.policyKey === 'NEW') {
            //     vm.savePolicy('');
            // } else {
            //     vm.policy.$save(vm.policy.private.adminNotes).then(function() {
            //         console.log('Note saved');
            //         var data = {
            //             targets: [],
            //             policyKey: vm.policyKey,
            //             createdBy: vm.currentUser.profile.displayName,
            //             createdByRole: vm.currentUser.profile.role,
            //             note: vm.newAdminNote,
            //             action: 'New Internal Note'
            //         };
            //         sendNotification(data);
            //         vm.newAdminNote = '';
            //     });
            // }
        }

        vm.savePolicy = function(actionAfterSave) {
            var root = firebase.database().ref();

            vm.actionAfterSave = actionAfterSave;

            if (vm.policyKey !== 'NEW') {
                vm.policy.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;
                vm.policy.lastUpdatedBy = vm.currentUser.profile.displayName;
                vm.policy.lastUpdatedById = vm.currentUser.auth.uid;

                if (!vm.policy.updateLog) {
                    vm.policy.updateLog = [];
                }

                vm.policy.updateLog.push({
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    updatedBy: vm.currentUser.profile.displayName,
                    updatedById: vm.currentUser.auth.uid
                });

                // Convert number text to numbers for sorting
                if (vm.policy.faceAmount) {
                    vm.policy.faceAmount = parseInt(vm.policy.faceAmount);
                }
                if (vm.policy.premium) {
                    vm.policy.premium = parseInt(vm.policy.premium);
                }
                if (vm.policy.estimatedCoiPremium) {
                    vm.policy.estimatedCoiPremium = parseInt(vm.policy.estimatedCoiPremium);
                }

                // Calculate Insured Age
                if (vm.policy.insured.dateOfBirth) {
                    vm.policy.insured.age = vm.calculateAge(vm.policy.insured.dateOfBirth);
                }

                vm.policy.$save().then(function(ref) {
                    vm.toast('success', 'Form updated');
                    vm.pageIsDirty = false;
                    if (vm.actionAfterSave === 'close') {
                        $state.go('inventory');
                    } else if (vm.actionAfterSave === 'nextPage') {
                        vm.showPage++;
                        $scope.$apply();
                        updateLabels();
                    }
                });
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        vm.requestBrokerReview = function() {
            var data = {
                targets: [],
                policyKey: vm.policyKey,
                createdBy: vm.currentUser.profile.displayName,
                createdByRole: vm.currentUser.profile.role,
                note: 'The Back Office has submitted the Policy for Broker Review',
                action: 'Action Required'
            };
            data.targets.push({
                type: 'ibroker',
                id: '-L8crXXb_c8NRSJUgRpn'
            });
            notifyService.addNotification(data).then(function() {
                vm.policy.status.submittedToBroker = true;
                vm.policy.status.display.text = 'Pending Broker Review';
                updateStatus();
            });
        }

        vm.requestMedicalSummary = function() {
            firebase.database().ref().child('policies/' + vm.policyKey + '/adminFiles').orderByChild('category').equalTo('Medical Records').once('value', function(snap) {
                if (!snap.exists()) {
                    sweetAlert.swal({
                        title: "Missing Medical Records",
                        text: "Please make sure that you have uploaded at least one set of Medical Records to the 'Back-end Files' tab and categorized that file as 'Medical Records'",
                        type: "error"
                    }).then(function() {});
                } else {
                    vm.policy.status.medicalSummaryRequested = true;
                    updateStatus();
                }
            });
        }
    }

    angular.module('yapp')
        .controller('PublicPolicyCtrl', PublicPolicyCtrl);
})();