(function() {
    'use strict';

    /* @ngInject */
    function BuyersCtrl(stdData, $fancyModal, zohoService, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain, apiService) {
        var vm = this;
        vm.pageIsDirty = false;
        vm.form = {};
        vm.tab = 1;
        var root = firebase.database().ref();

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        vm.yesNo = stdData.yesNo;
        vm.fileCategories = stdData.fileCategories;

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                    vm.isAdmin = true;
                }
                vm.userOrgType = authService.determineUserOrgType(vm.currentUser.profile.role);
                vm.showManageBuyers = true;
                if (vm.currentUser.org) {
                    vm.buyers = $firebaseArray(firebase.database().ref('orgs/' + vm.userOrgType + '/' + vm.currentUser.org.$id + '/buyers'));
                    vm.buyers.$loaded().then(function(org) {}).catch(function(err) {
                        console.log(err);
                    });
                    vm.suppliers = $firebaseArray(firebase.database().ref('orgs/' + vm.userOrgType + '/' + vm.currentUser.org.$id + '/linkedTo'));
                    vm.suppliers.$loaded().then(function(org) {}).catch(function(err) {
                        console.log(err);
                    });
                    vm.supplierInvites = $firebaseArray(firebase.database().ref('orgs/' + vm.userOrgType + '/' + vm.currentUser.org.$id + '/invitedTo'));
                    vm.supplierInvites.$loaded().then(function(org) {
                        if (org.length > 0) {
                            vm.toast('success', 'You have Supplier Invitations waiting!');
                        }
                    }).catch(function(err) {
                        console.log(err);
                    });
                }
                jQuery('.tabs').tabslet({
                    active: ($state.params && $state.params.tab) ? $state.params.tab : '1'
                });
            }
        });

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        vm.acceptInvite = function(supplier) {
            sweetAlert.swal({
                title: "Agreement",
                html: "By accepting this Invitation, you acknowledge the following User Agreement:<br><br><textarea readonly style='width:100%;height:125px;font-size:0.9em;text-align:justify;padding:0px 5px;line-height:1.5em'>The User shall indemnify, defend and hold harmless Life Settlement Hub LLC (\"the Company\"), its affiliates, their respective owners, directors, officers, managers, employees and agents, and their respective successors and assigns (each, a “Company Indemnified Party”) from any and all damages, claims, losses, Liabilities, judgments, settlements, awards, judgments, fines, deficiencies, taxes and other charges (together with interest and penalties thereon, if any) and costs and expenses (including reasonable attorneys’ fees and costs of investigation) incurred by or asserted against any Company Indemnified Party, directly or indirectly, arising from or related to (i) any breach of any representation or warranty made by the User contained in this Agreement, any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, (ii) any breach of any covenant or agreement of the User contained in this Agreement. any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, and (iii) any violation of any applicable law or regulation by (a) the User in connection with this Agreement, (b) any affiliate or agent of the User in connection with any agreement between any affiliate or agent of the User and the Company or any of its affiliates, (c) the User or any of its affiliates or their agents in connection with any agreement between the User or any of its affiliates and any other user of the System or (d) the User or any of its affiliates or agents in connection with any transaction contemplated hereby or any agreement between the User or any of its affiliates and any affiliate or agent of the User or any other user of the System.</textarea><br><br>Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.dismiss) {
                    vm.toast('error', 'Accept Invitation action cancelled');
                } else {
                    // Move supplier from "invitedTo" to "linkedTo" node in database
                    if (vm.currentUser.org) {
                        if (!vm.currentUser.org.linkedTo) {
                            vm.currentUser.org.linkedTo = {};
                        }
                        vm.currentUser.org.linkedTo[supplier.$id] = {
                            email: supplier.email,
                            fax: supplier.fax,
                            orgName: supplier.orgName,
                            orgType: supplier.orgType,
                            phone: supplier.phone
                        };
                        vm.currentUser.org.$save(vm.currentUser.org.linkedTo).then(function() {
                            root.child('orgs/' + vm.currentUser.org.orgType + '/' + vm.currentUser.org.$id + '/invitedTo/' + supplier.$id).remove().then(function() {
                                sweetAlert.swal({
                                    title: "Success",
                                    text: "You are now connected with this Supplier!",
                                    type: "success",
                                    timer: 1500
                                }).then(function(result) {
                                    jQuery('.tabs').trigger('show', '#tab-2');
                                });
                            });
                        });
                    }
                }
            });
        }

        vm.declineInvite = function(supplier) {
            sweetAlert.swal({
                title: "Decline this Supplier?",
                html: "This Supplier may continue to send you cases, but you will not have access to them anymore. If you want to reconnect with this supplier, they will have to resend this invitation. Are you sure you want to decline?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {

                    // TODO: Make this a reality

                    sweetAlert.swal({
                        title: "Success",
                        text: "You are no longer connected with this Supplier",
                        type: "success"
                    }).then(function(result) {});
                }
            });
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.addSupplier = function() {
            sweetAlert.swal({
                title: "Agreement",
                html: "By adding this Supplier to your account, you acknowledge the following User Agreement:<br><br><textarea readonly style='width:100%;height:125px;font-size:0.9em;text-align:justify;padding:0px 5px;line-height:1.5em'>The User shall indemnify, defend and hold harmless Life Settlement Hub LLC (\"the Company\"), its affiliates, their respective owners, directors, officers, managers, employees and agents, and their respective successors and assigns (each, a “Company Indemnified Party”) from any and all damages, claims, losses, Liabilities, judgments, settlements, awards, judgments, fines, deficiencies, taxes and other charges (together with interest and penalties thereon, if any) and costs and expenses (including reasonable attorneys’ fees and costs of investigation) incurred by or asserted against any Company Indemnified Party, directly or indirectly, arising from or related to (i) any breach of any representation or warranty made by the User contained in this Agreement, any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, (ii) any breach of any covenant or agreement of the User contained in this Agreement. any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, and (iii) any violation of any applicable law or regulation by (a) the User in connection with this Agreement, (b) any affiliate or agent of the User in connection with any agreement between any affiliate or agent of the User and the Company or any of its affiliates, (c) the User or any of its affiliates or their agents in connection with any agreement between the User or any of its affiliates and any other user of the System or (d) the User or any of its affiliates or agents in connection with any transaction contemplated hereby or any agreement between the User or any of its affiliates and any affiliate or agent of the User or any other user of the System.</textarea><br><br>Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (result.dismiss) {
                    vm.toast('error', 'Add Supplier action cancelled');
                } else {
                    vm.toast('success', 'Moving on');
                }
            });
        }

        vm.addBuyer = function(orgType) {
            var orgTypeLower = orgType.toLowerCase();
            
            // bug fix here
            
            sweetAlert.swal({
                title: "Agreement",
                html: "By adding this " + orgType + " to your account, you acknowledge the following User Agreement:<br><br><textarea readonly style='width:100%;height:125px;font-size:0.9em;text-align:justify;padding:0px 5px;line-height:1.5em'>The User shall indemnify, defend and hold harmless Life Settlement Hub LLC (\"the Company\"), its affiliates, their respective owners, directors, officers, managers, employees and agents, and their respective successors and assigns (each, a “Company Indemnified Party”) from any and all damages, claims, losses, Liabilities, judgments, settlements, awards, judgments, fines, deficiencies, taxes and other charges (together with interest and penalties thereon, if any) and costs and expenses (including reasonable attorneys’ fees and costs of investigation) incurred by or asserted against any Company Indemnified Party, directly or indirectly, arising from or related to (i) any breach of any representation or warranty made by the User contained in this Agreement, any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, (ii) any breach of any covenant or agreement of the User contained in this Agreement. any agreement between the Company and any affiliate or agent of the User, or any agreement between the User and any other user of the System, and (iii) any violation of any applicable law or regulation by (a) the User in connection with this Agreement, (b) any affiliate or agent of the User in connection with any agreement between any affiliate or agent of the User and the Company or any of its affiliates, (c) the User or any of its affiliates or their agents in connection with any agreement between the User or any of its affiliates and any other user of the System or (d) the User or any of its affiliates or agents in connection with any transaction contemplated hereby or any agreement between the User or any of its affiliates and any affiliate or agent of the User or any other user of the System.</textarea><br><br>Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (result.dismiss) {
                    vm.toast('error', 'Add Buyer action cancelled');
                } else {
                    sweetAlert.swal({
                        title: orgType + " Email",
                        text: "Provide the email address of the " + orgType + " you wish to add.",
                        type: "question",
                        input: "text",
                        reverseButtons: true,
                        showCancelButton: true,
                        cancelButtonText: "Cancel",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Save",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }).then(function(buyerEmail) {
                        if (buyerEmail.value) {
                            vm.buyerEmail = buyerEmail.value;
                            // Check to make sure this Buyer doesn't already exist
                            root.child('orgs/' + orgTypeLower).orderByChild('email').equalTo(vm.buyerEmail).once('value', function(snap) {
                                if (snap.exists()) {
                                    var existingBuyer = snap.val()[Object.keys(snap.val())[0]];
                                    // Prompt user to confirm Name to link to existing Org
                                    sweetAlert.swal({
                                        title: orgType + " Found!",
                                        text: "There is already a " + orgType + " on our system using that email address. Please confirm whether you want to add \"" + existingBuyer.name + "\" to your list of " + orgType + "s",
                                        type: "question",
                                        reverseButtons: true,
                                        showCancelButton: true,
                                        cancelButtonText: "No",
                                        confirmButtonColor: "#00B200",
                                        confirmButtonText: "Yes, add " + orgType + "!",
                                        closeOnConfirm: false,
                                        showLoaderOnConfirm: true
                                    }).then(function(addBuyer) {
                                        if (!addBuyer.dismiss) {
                                            var updateObj = {};
                                            if (vm.currentUser.org) {
                                                updateObj[vm.currentUser.org.$id] = {
                                                    orgName: vm.currentUser.org.name,
                                                    orgType: vm.currentUser.org.orgType,
                                                    phone: vm.currentUser.org.phone ? vm.currentUser.org.phone : '',
                                                    fax: vm.currentUser.org.fax ? vm.currentUser.org.fax : '',
                                                    address: vm.currentUser.org.address ? vm.currentUser.org.address : null,
                                                    email: vm.currentUser.org.email ? vm.currentUser.org.email : ''
                                                }
                                            }

                                            var recipients = [];
                                            recipients.push({
                                                email: existingBuyer.email,
                                                template: 'new-supplier-invitation',
                                                validate: {
                                                    checkPath: 'orgs/public/' + existingBuyer.orgKey,
                                                    checkKey: 'status',
                                                    checkVal: 'active',
                                                    resendLink: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + existingBuyer.orgKey + '&ic='
                                                },
                                                subData: {
                                                    name: existingBuyer.name,
                                                    link: 'imanager.' + vm.domain + '.' + vm.tld + '/buyers?tab=3',
                                                    supplierName: vm.currentUser.org ? vm.currentUser.org.name : 'n/a',
                                                    supplierPhone: vm.currentUser.org ? vm.currentUser.org.phone : 'n/a',
                                                    supplierEmail: vm.currentUser.org ? vm.currentUser.org.email : 'n/a'
                                                }
                                            });
                                            apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                                                if (response.status === 'success') {
                                                    vm.toast('success', 'Existing ' + orgType + ' has been emailed an Invitation');
                                                } else {
                                                    vm.toast('error', 'Email Invitation was not sent');
                                                }
                                            });
                                            root.child('orgs/' + orgTypeLower + '/' + existingBuyer.orgKey + '/invitedTo').update(updateObj).then(function() {
                                                vm.buyers.$add({
                                                    orgGuid: existingBuyer.orgGuid,
                                                    createdAt: firebase.database.ServerValue.TIMESTAMP,
                                                    createdById: vm.currentUser.auth.uid,
                                                    email: existingBuyer.email,
                                                    phone: existingBuyer.phone ? existingBuyer.phone : '',
                                                    fax: existingBuyer.fax ? existingBuyer.fax : '',
                                                    address: existingBuyer.address ? existingBuyer.address : null,
                                                    orgType: existingBuyer.orgType,
                                                    orgKey: existingBuyer.orgKey,
                                                    status: existingBuyer.status,
                                                    name: existingBuyer.name
                                                }).then(function() {
                                                    sweetAlert.swal({
                                                        title: orgType + " Added!",
                                                        text: "",
                                                        type: "success",
                                                        confirmButtonColor: "#00B200",
                                                        confirmButtonText: "OK",
                                                        timer: 1000
                                                    }).then(function() {
                                                        $scope.$apply();
                                                    });
                                                });
                                            });
                                        } else {
                                            sweetAlert.swal({
                                                title: "Not Added!",
                                                text: "Please try again using a different email address. Thank you!",
                                                type: "success",
                                                confirmButtonColor: "#00B200",
                                                confirmButtonText: "OK"
                                            }).then(function() {
                                                $scope.$apply();
                                            });
                                        }
                                    });
                                } else {
                                    sweetAlert.swal({
                                        title: orgType + " Name",
                                        text: "Please provide the name of the " + orgType + " you wish to add.",
                                        type: "question",
                                        input: "text",
                                        reverseButtons: true,
                                        showCancelButton: true,
                                        cancelButtonText: "Cancel",
                                        confirmButtonColor: "#00B200",
                                        confirmButtonText: "Save",
                                        closeOnConfirm: false,
                                        showLoaderOnConfirm: true
                                    }).then(function(buyerName) {
                                        if (buyerName.value) {
                                            var newBuyer = {
                                                name: buyerName.value,
                                                email: vm.buyerEmail,
                                                orgType: orgTypeLower,
                                                createdAt: firebase.database.ServerValue.TIMESTAMP,
                                                createdById: vm.currentUser.auth.uid,
                                                orgGuid: uuidv4(),
                                                status: 'active',
                                                isApproved: true,
                                                plan: 'free'
                                            };
                                            newBuyer.invitedTo = {};
                                            if (vm.currentUser.org) {
                                                newBuyer.invitedTo[vm.currentUser.org.$id] = {
                                                    orgName: vm.currentUser.org.name,
                                                    orgType: vm.currentUser.org.orgType,
                                                    phone: vm.currentUser.org.phone ? vm.currentUser.org.phone : '',
                                                    fax: vm.currentUser.org.fax ? vm.currentUser.org.fax : '',
                                                    address: vm.currentUser.org.address ? vm.currentUser.org.address : null,
                                                    email: vm.currentUser.org.email ? vm.currentUser.org.email : ''
                                                }
                                            }
                                            root.child('orgs/' + orgTypeLower).push(newBuyer).then(function(ref) {
                                                newBuyer.orgKey = ref.key;
                                                var inviteCode = uuidv4();

                                                // Send email invitation to new Buyer
                                                var recipients = [];
                                                recipients.push({
                                                    email: newBuyer.email,
                                                    template: 'buyer-invitation',
                                                    subData: {
                                                        name: newBuyer.name,
                                                        link: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + newBuyer.orgKey + '&ic=' + inviteCode
                                                    }
                                                });
                                                apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                                                    if (response.status === 'success') {
                                                        vm.toast('success', 'New ' + orgType + ' has been emailed an Invitation to iManager');
                                                    } else {
                                                        vm.toast('error', 'Email Invitation was not sent');
                                                    }
                                                });

                                                var updates = {};
                                                updates['orgs/' + orgTypeLower + '/' + ref.key + '/orgKey'] = ref.key;
                                                updates['orgs/public/' + ref.key] = {
                                                    name: newBuyer.name,
                                                    orgType: newBuyer.orgType,
                                                    status: 'invited',
                                                    inviteCode: inviteCode
                                                };

                                                root.update(updates).then(function() {
                                                    vm.buyers.$add({
                                                        orgGuid: newBuyer.orgGuid,
                                                        createdAt: firebase.database.ServerValue.TIMESTAMP,
                                                        createdById: vm.currentUser.auth.uid,
                                                        email: newBuyer.email,
                                                        orgType: newBuyer.orgType,
                                                        orgKey: newBuyer.orgKey,
                                                        status: newBuyer.status,
                                                        name: newBuyer.name
                                                    }).then(function() {
                                                        sweetAlert.swal({
                                                            title: orgType + " Added!",
                                                            text: "",
                                                            type: "success",
                                                            confirmButtonColor: "#00B200",
                                                            confirmButtonText: "OK",
                                                            timer: 1000
                                                        }).then(function() {
                                                            $scope.$apply();
                                                        });
                                                    });
                                                });
                                            });
                                        } else {
                                            vm.toast('error', 'Add Buyer action cancelled');
                                        }
                                    });
                                }
                            }).catch(function(err) {
                                console.log(err);
                            });
                        } else {
                            vm.toast('error', 'Add Buyer action cancelled');
                        }
                    });
                }
            }).catch(function(err) {
                console.log(err);
            });
        }

        vm.updateBuyer = function(buyer) {
            buyer.editInfo = false;
            vm.editInfo = false;
            delete buyer.editInfo;
            vm.buyers.$save(buyer).then(function() {
                vm.showDetail = true;
                $scope.$apply();
            });
        }

        vm.deleteBuyer = function(buyer, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This Buyer will no longer have access to any Policies you have sent them. This action cannot be un-done. You will have to resend any Policies to this Buyer if you add them again.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        // Remove current Org from Buyer's invitedTo or linkedTo org list
                        root.child('orgs/' + buyer.orgType + '/' + buyer.orgKey + '/invitedTo/' + vm.currentUser.org.$id).remove().then(function() {
                            root.child('orgs/' + buyer.orgType + '/' + buyer.orgKey + 'linkedTo/' + vm.currentUser.org.$id).remove().then(function() {
                                vm.buyers.$remove(buyer).then(function() {
                                    sweetAlert.swal({
                                        title: "Deleted!",
                                        text: "",
                                        type: "success",
                                        confirmButtonColor: "#00B200",
                                        confirmButtonText: "OK",
                                        timer: 1000
                                    }).then(function() {
                                        $scope.$apply();
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }).catch(function(err) {
                                console.log(err);
                            });
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                }
            )
        }

        vm.clickBuyer = function($event, buyer) {
            if ($event.target.tagName === 'BUTTON' || $event.target.tagName === 'INPUT' || $event.target.tagName === 'A') {
                $event.stopPropagation();
            } else if (!vm.editInfo) {
                if (vm.showDetail === buyer.name) {
                    vm.showDetail = undefined;
                } else {
                    vm.showDetail = buyer.name;
                }
            }
        }

        vm.clickSupplier = function($event, supplier) {
            if ($event.target.tagName === 'BUTTON' || $event.target.tagName === 'INPUT') {
                $event.stopPropagation();
            } else if (!vm.editInfo) {
                if (vm.showDetail === supplier.orgName) {
                    vm.showDetail = undefined;
                } else {
                    vm.showDetail = supplier.orgName;
                }
            }
        }

        vm.updateSupplier = function(supplier) {
            supplier.editInfo = false;
            vm.editInfo = false;
            delete supplier.editInfo;
            vm.suppliers.$save(supplier).then(function() {
                vm.showDetail = true;
                $scope.$apply();
            });
        }
    }

    angular.module('yapp')
        .controller('BuyersCtrl', BuyersCtrl);
})();