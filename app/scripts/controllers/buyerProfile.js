(function() {
    'use strict';

    /* @ngInject */
    function BuyerProfileCtrl(stdData, $fancyModal, zohoService, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain, apiService, $window, sharedService) {
        var vm = this;
        vm.pageIsDirty = false;
        vm.form = {};
        vm.tab = 1;
        var root = firebase.database().ref();

        vm.showData = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        vm.yesNo = stdData.yesNo;
        vm.fileCategories = stdData.fileCategories;
        vm.roles = stdData.isubmitRoles;
        vm.bdnRoles = stdData.imanagerRoles;
        vm.states = stdData.states;

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            jQuery('body').off('ifUnchecked', 'input');
            jQuery('body').off('ifChecked', 'input');
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                    vm.isAdmin = true;
                }
                vm.userOrgType = authService.determineUserOrgType(vm.currentUser.profile.role);
                activate();
            }
        });

        function activate() {
            jQuery('body').on('ifChecked', 'input', function(event) {
                if (event.target.name === 'buyerFileCategory') {
                    vm.showSaveFileAccess = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerFileCategoryAll') {
                    vm.buyer.fileCategoryAccess = vm.fileCategories;
                    vm.showSaveFileAccess = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerStateLicense') {
                    vm.showSaveLicensing = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerStateLicenseAll') {
                    vm.buyer.licensedStates = vm.states;
                    vm.showSaveLicensing = true;
                    $scope.$apply();
                }
            });
            jQuery('body').on('ifUnchecked', 'input', function(event) {
                if (event.target.name === 'buyerFileCategory') {
                    vm.showSaveFileAccess = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerFileCategoryAll') {
                    vm.buyer.fileCategoryAccess = [];
                    vm.showSaveFileAccess = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerStateLicense') {
                    vm.showSaveLicensing = true;
                    $scope.$apply();
                } else if (event.target.name === 'buyerStateLicenseAll') {
                    vm.buyer.licensedStates = [];
                    vm.showSaveLicensing = true;
                    $scope.$apply();
                }
            });

            var buyer = $firebaseObject(firebase.database().ref('orgs/' + vm.userOrgType + '/' + vm.currentUser.org.$id + '/buyers/' + $state.params.ok));
            buyer.$loaded().then(function(buyer) {
                vm.buyer = buyer;
                vm.buyerPolicies = [];
                firebase.database().ref('orgs/' + buyer.orgType + '/' + buyer.orgKey + '/policies').orderByChild('fromOrg').equalTo(vm.currentUser.org.$id).once('value', function(snap) {
                    if (snap.exists()) {
                        var policyArr = snap.val();
                        $window.async.forEachOfSeries(Object.keys(policyArr), function(policyIdx, key, callback) {
                            var policy = policyArr[policyIdx];
                            firebase.database().ref('privatePolicies/' + policyIdx).once('value', function(pdata) {
                                if (pdata.exists()) {
                                    vm.buyerPolicies.push({
                                        pid: policyIdx,
                                        bid: vm.buyer.orgGuid,
                                        data: pdata.val()
                                    });
                                }
                                callback();
                            });
                        }, function(err) {
                            if (err) {
                                console.log(err);
                            }
                            $scope.$apply();
                        });
                    } else {
                        $scope.$apply();
                    }
                });
            });
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.copyInfo = function(what) {
            if (what === 'buyerInfo') {
                vm.updatedInfo = {};
                vm.updatedInfo.email = (vm.buyer.address) ? sharedService.cloneObject(vm.buyer.email) : '';
                vm.updatedInfo.phone = (vm.buyer.address) ? sharedService.cloneObject(vm.buyer.phone) : '';
                vm.updatedInfo.fax = (vm.buyer.address) ? sharedService.cloneObject(vm.buyer.fax) : '';
                vm.updatedInfo.primaryContact = (vm.buyer.address) ? sharedService.cloneObject(vm.buyer.primaryContact) : '';
                vm.updatedInfo.address = (vm.buyer.address) ? sharedService.cloneObject(vm.buyer.address) : null;
            }
        }

        vm.saveUpdatedInfo = function(what) {
            if (what === 'buyerInfo') {
                vm.buyer.email = (vm.updatedInfo.email) ? sharedService.cloneObject(vm.updatedInfo.email) : '';
                vm.buyer.phone = (vm.updatedInfo.phone) ? sharedService.cloneObject(vm.updatedInfo.phone) : '';
                vm.buyer.fax = (vm.updatedInfo.fax) ? sharedService.cloneObject(vm.updatedInfo.fax) : '';
                vm.buyer.primaryContact = (vm.updatedInfo.primaryContact) ? sharedService.cloneObject(vm.updatedInfo.primaryContact) : '';
                vm.buyer.address = (vm.updatedInfo.address) ? sharedService.cloneObject(vm.updatedInfo.address) : null;
                vm.updatedInfo = {};
                vm.buyer.$save().then(function() {
                    vm.editBuyerInfo = false;
                    vm.toast('success', 'Buyer information updated');
                    $scope.$apply();
                });
            }
        }

        vm.saveFileCategories = function() {
            vm.buyer.$save(vm.buyer.fileCategoryAccess).then(function() {
                vm.toast('success', 'Default File Access updated');
                vm.showSaveFileAccess = false;
            });
        }

        vm.saveLicensing = function() {
            vm.buyer.$save(vm.buyer.licensedStates).then(function() {
                vm.toast('success', 'State Licensing updated');
                vm.showSaveLicensing = false;
            });
        }

        vm.saveNote = function() {
            vm.buyer.$save(vm.buyer.note).then(function() {
                vm.toast('success', 'Buyer Note updated');
                vm.showSaveNotes = false;
            });
        }

        vm.updateBuyer = function(buyer) {
            buyer.editInfo = false;
            vm.editInfo = false;
            delete buyer.editInfo;
            vm.buyers.$save(buyer).then(function() {
                vm.showDetail = true;
                $scope.$apply();
            });
        }

        vm.deleteBuyer = function(buyer, index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This action cannot be un-done",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        // Remove current Org from Buyer's invitedTo or linkedTo org list
                        root.child('orgs/' + buyer.orgType + '/' + buyer.orgKey + '/invitedTo/' + vm.currentUser.org.$id).remove().then(function() {
                            root.child('orgs/' + buyer.orgType + '/' + buyer.orgKey + 'linkedTo/' + vm.currentUser.org.$id).remove().then(function() {
                                vm.buyers.$remove(buyer).then(function() {
                                    sweetAlert.swal({
                                        title: "Deleted!",
                                        text: "",
                                        type: "success",
                                        confirmButtonColor: "#00B200",
                                        confirmButtonText: "OK",
                                        timer: 1000
                                    }).then(function() {
                                        $scope.$apply();
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }).catch(function(err) {
                                console.log(err);
                            });
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                }
            )
        }

        vm.showMessage = function(type, helpTitle, helpText) {
            if (type === 'File Access') {
                sweetAlert.swal({
                    title: "What's This?",
                    html: "Whenever a Buyer is sent a Policy, you are required to set up their access to any uploaded documents. This is done broadly by document category, then can be narrowed down for each specific document.<br><br>The categories selected here will appear as the initial set of category filters when adding this Buyer in the future.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'State Licensing') {
                sweetAlert.swal({
                    title: "What's This?",
                    html: "When creating a Policy, you will need to choose a Policy State. Only Buyers who are licensed in that state will appear in the list of choices when sending the Policy out for offers.",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        if (route === 'policy') {
                            $state.go('policy', { pid: vm.policyKey });
                        } else {
                            $state.go(route);
                        }
                    } else {
                        //vm.savePolicy('close');
                    }
                });
            } else {
                if (route === 'policy') {
                    $state.go('policy', { pid: vm.policyKey });
                } else {
                    $state.go(route);
                }
            }
        }
    }
    angular.module('yapp')
        .controller('BuyerProfileCtrl', BuyerProfileCtrl);
})();