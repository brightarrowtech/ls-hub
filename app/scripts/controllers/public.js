(function() {
    'use strict';

    /* @ngInject */
    function PublicCtrl(VER, ENV, $scope, $state, $stateParams, $firebaseArray, $timeout, sweetAlert, brandingDomain) {
        var vm = this;
        vm.showContent = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.version = VER;

        vm.environment = '';
        if (ENV === 'demo') {
            vm.environment = ' - Demo';
        } else if (ENV === 'dev') {
            vm.environment = ' - Dev';
        }
    }
    angular.module('yapp')
        .controller('PublicCtrl', PublicCtrl);
})();