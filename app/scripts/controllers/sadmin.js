(function() {
    'use strict';

    /* @ngInject */
    function SAdminCtrl($scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert) {
        var vm = this;
        vm.isSAdmin = false;
        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.role === 'SAdmin') {
                vm.isSAdmin = true;
                loadPage();
            } else {
                $state.go('inventory');
            }
        });

        function loadPage() {
            var root = firebase.database().ref();
            vm.funderGroups = $firebaseArray(root.child('funderGroup'));

            vm.addNewFunderGroup = function() {
                vm.funderGroups.$add({
                    funderGroupName: 'n/a',
                    funderGroupContact: 'n/a'
                }).then(function(ref) {
                    sweetAlert.swal({
                        title: "Success",
                        text: "New Funder Added",
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                });
            }
        }
    }
    angular.module('yapp')
        .controller('SAdminCtrl', SAdminCtrl);
})();