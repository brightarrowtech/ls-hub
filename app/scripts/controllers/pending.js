(function() {
    'use strict';

    /* @ngInject */
    function PendingCtrl($scope, $state, authService, sweetAlert, brandingDomain, currentAuth) {
        var vm = this;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        // authService.getCurrentUser(currentAuth).then(function(user) {
        //     vm.currentUser = user;
        // });

        vm.goTo = function(route) {
            if (route === 'profile') {
                $state.go('profile', { 'init': 'true' });
            } else {
                $state.go(route);
            }
        }

        vm.verifyEmail = function() {
            vm.verifying = true;
            authService.verifyEmail().then(function(response) {
                vm.verifying = false;
                if (response.status === 'success') {
                    sweetAlert.swal({
                        title: "Success",
                        text: "Check your email to verify",
                        type: "success",
                        timer: 2000
                    }).then(function() {});
                } else {
                    sweetAlert.swal({
                        title: "Uh Oh!",
                        text: "Unable to send verification email - " + response.message,
                        type: "error",
                        timer: 2000
                    }).then(function() {});
                }
            });
        }
    }
    angular.module('yapp')
        .controller('PendingCtrl', PendingCtrl);
})();