(function() {
    'use strict';

    /* @ngInject */
    function PublicApplicationCtrl(stdData, ENV, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, sweetAlert, pandadocService, policyService, brandingDomain, toastr) {
        var vm = this;
        vm.showPage = 2;
        vm.isCreator = false;
        vm.pageIsDirty = false;
        vm.isLoading = true;
        vm.isError = false;
        vm.isPublic = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        vm.states = stdData.states;
        vm.genders = stdData.genders;
        vm.maritalStatus = stdData.maritalStatus;
        vm.types = stdData.policyTypes;
        vm.yesNo = stdData.yesNo;
        vm.yesNoUnsure = stdData.yesNoUnsure;
        vm.beneTypes = stdData.beneTypes;

        loadPage();

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function loadPage() {
            if ($state.params.pubId) {
                /**
                 * LOADING AN EXISTING POLICY
                 */
                var root = firebase.database().ref();
                vm.formPublicGuid = $state.params.pubId;
                vm.policy = $firebaseObject(root.child('publicForms').child(vm.formPublicGuid));
                vm.policy.$loaded().then(function(form) {
                    if (vm.policy.hasOwnProperty("$value") && vm.policy.$value === null) {
                        vm.isLoading = false;
                        vm.isError = true;
                    } else {
                        vm.storage = firebase.storage();
                        vm.storageRef = vm.storage.ref('publicForms/' + vm.formPublicGuid);
                        vm.isLoading = false;
                        
                        // TODO: Get form contact info if it exists
                        vm.policyHasContactOptions = false;

                        if (vm.policy.forms.application.status === 'Pending Signatures') {
                            vm.goToPage(100);
                        } else if (vm.policy.forms.application.status === 'Complete') {
                            vm.goToPage(101);
                        } else {
                            updateLabels();
                        }
                    }
                }).catch(function(err) {
                    vm.isLoading = false;
                    vm.isError = true;
                });
            } else {
                vm.isError = true;
                vm.isLoading = false;
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel.panel-active .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.page-panel.panel-active .placeHolder').filter(function() {
                    console.log(jQuery(this.nextElementSibling).val());
                    if (jQuery(this.nextElementSibling).val() !== '') {
                        return this;
                    }
                }).addClass('active');
                jQuery('.page-panel.panel-active .pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.page-panel.panel-active .flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.policy[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = vm.policy[parts[0]][parts[1]];
                        } else if (parts.length === 3) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]];
                        } else if (parts.length === 4) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]][parts[3]];
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('input').on('ifChecked', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = true;
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = true;
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        if (!vm.policy.secondaryInsured) {
                            vm.policy.secondaryInsured = {};
                        }
                        vm.policy.secondaryInsured.isDeceased = true;
                    }
                    $scope.$apply();
                });
                jQuery('input').on('ifUnchecked', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = false;
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = false;
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        vm.policy.secondaryInsured.isDeceased = false;
                    }
                    $scope.$apply();
                });
                document.getElementById('scrollContainer').scrollTo(0, 0);
                $scope.$apply();
            }, 10);
        }

        vm.submitApplication = function() {
            vm.submittingApplication = true;

            removeUndefined(vm.policy);

            vm.policy.forms.application.status = 'Needs Review';

            vm.policy.$save().then(function(ref) {
                // Push notification to alerts node
                var root = firebase.database().ref();
                root.child('alerts').child(vm.policy.alertId).push({
                    policyNumber: vm.policy.policyNumber ? vm.policy.policyNumber : vm.policy.LSHID,
                    createdAt: firebase.database.ServerValue.TIMESTAMP,
                    message: 'Policy ' + vm.policy.LSHID + ' - the application form has been completed by a third party user',
                    publicGuid: vm.formPublicGuid,
                    type: 'thirdPartyComplete'
                }).then(function(ref) {
                    $state.go('thank-you');
                }).catch(function(err) {
                    sweetAlert.swal({
                        title: 'Error',
                        text: err.message,
                        type: 'error'
                    }).then(function(result) {
                        vm.submittingApplication = false;
                    });
                });
            }).catch(function(err) {
                sweetAlert.swal({
                    title: 'Error',
                    text: err.message,
                    type: 'error'
                }).then(function(result) {
                    vm.submittingApplication = false;
                });
            });
        }

        vm.signPublicApplication = function() {
            vm.signingApplication = true;
            console.log('Submitting application for signature');
            
            var recipients = [];  
            var templateIdx = 0;          

            if (vm.policy.insured) {
                if (vm.policy.forms.application.recipients.insured.emailAddress) {
                    recipients.push({
                        email: vm.policy.forms.application.recipients.insured.emailAddress ? vm.policy.forms.application.recipients.insured.emailAddress : '',
                        first_name: vm.policy.insured.firstName,
                        last_name: vm.policy.insured.lastName,
                        role: 'Primary Insured'
                    });
                    vm.policy.insured.fullName = vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName;
                    if (!vm.policy.forms.medicalPrimary) {
                        vm.policy.forms.medicalPrimary = {
                            formName: 'Medical Questionnaire (Primary)',
                            className: 'medicalPrimary',
                            status: 'Start Now',
                            recipient: {
                                firstName: vm.policy.insured.firstName,
                                lastName: vm.policy.insured.lastName,
                                fullName: vm.policy.insured.fullName,
                                emailAddress: vm.policy.forms.application.recipients.insured.emailAddress ? vm.policy.forms.application.recipients.insured.emailAddress : ''
                            }
                        };
                    }
                } else {
                    vm.toast('error', 'All signers must have email addresses');
                    vm.signingApplication = false;
                    return false;
                }
            } else {
                delete vm.policy.forms.application.recipients.insured;
            }
            if (vm.policy.secondaryInsured && vm.policy.insured.secondaryInsured[0] === 'Yes') {
                if (vm.policy.forms.application.recipients.secondaryInsured.emailAddress) {
                    recipients.push({
                        email: vm.policy.forms.application.recipients.secondaryInsured.emailAddress ? vm.policy.forms.application.recipients.secondaryInsured.emailAddress : '',
                        first_name: vm.policy.secondaryInsured.firstName,
                        last_name: vm.policy.secondaryInsured.lastName,
                        role: 'Secondary Insured'
                    });               
                    vm.policy.secondaryInsured.fullName = vm.policy.secondaryInsured.firstName + ' ' + vm.policy.secondaryInsured.lastName;
                    templateIdx += 2;
                    if (!vm.policy.forms.medicalSecondary) {
                        vm.policy.forms.medicalSecondary = {
                            formName: 'Medical Questionnaire (Secondary)',
                            className: 'medicalSecondary',
                            status: 'Start Now',
                            recipient: {
                                firstName: vm.policy.secondaryInsured.firstName,
                                lastName: vm.policy.secondaryInsured.lastName,
                                fullName: vm.policy.secondaryInsured.fullName,
                                emailAddress: vm.policy.forms.application.recipients.secondaryInsured.emailAddress ? vm.policy.forms.application.recipients.secondaryInsured.emailAddress : ''
                            }
                        };
                    }
                } else {
                    vm.toast('error', 'All signers must have email addresses');
                    vm.signingApplication = false;
                    return false;
                }
            } else {
                delete vm.policy.secondaryInsured;
                delete vm.policy.forms.application.recipients.secondaryInsured;
            }
            if (vm.policy.owner && vm.policy.isInsuredAlsoOwner[0] === 'No') {
                if (vm.policy.forms.application.recipients.owner.emailAddress) {
                    recipients.push({
                        email: vm.policy.forms.application.recipients.owner.emailAddress ? vm.policy.forms.application.recipients.owner.emailAddress : '',
                        first_name: vm.policy.owner.firstName,
                        last_name: vm.policy.owner.lastName,
                        role: 'Policy Owner'
                    });
                    vm.policy.owner.fullName = vm.policy.forms.application.recipients.owner.firstName + ' ' + vm.policy.forms.application.recipients.owner.lastName;
                    templateIdx += 4;
                } else {
                    vm.toast('error', 'All signers must have email addresses');
                    vm.signingApplication = false;
                    return false;
                }
            } else {
                delete vm.policy.owner;
                delete vm.policy.forms.application.recipients.owner;
            }
            if (vm.policy.forms.application.recipients.agent) {
                recipients.push({
                    email: vm.policy.forms.application.recipients.agent.emailAddress,
                    first_name: vm.policy.forms.application.recipients.agent.firstName,
                    last_name: vm.policy.forms.application.recipients.agent.lastName,
                    role: 'Agent'
                });
                vm.policy.agent = {
                    fullName: vm.policy.forms.application.recipients.agent.fullName
                };
                templateIdx += 5;
            }

            // Save application form (to persist emails if edited)
            vm.policy.$save().then(function(res) {
                console.log('Form recipients updated');
                continueESign(recipients, templateIdx);
            });
        }

        function continueESign(recipients, templateIdx) {
            var formVersion = 'PSOA';
            //var templateId = 'sASt8ng6NNNLZqUid5kmTR'; // PSOA - Atlas
            var templateId = 'cQKZU8Et4AaDb7pSTiA2Ai'; // PSOA - Rapid
            if (templateIdx === 7) {
                formVersion = 'PSA';
                //templateId = '6agzrJ5q6LH83piZw2yp7C'; // PSA - Atlas
                templateId = 'Gy38i59nr2ewyEVeQKpe2j'; // PSA - Rapid
            } else if (templateIdx === 9) {
                formVersion = 'POA';
                //templateId = 'rpdSERfYoxj5d2uKYRhYMB'; // POA - Atlas
                templateId = 'XtbHK7rMUySEGncfXHoMbU'; // POA - Rapid
            } else if (templateIdx === 5) {
                formVersion = 'PA';
                if (ENV === 'prod') {
                    //templateId = '9wXty4podVGuzarvXNt23n'; // PA - Production - Atlas
                    templateId = 'oTVagwTEXdef4Emcp8LGLa'; // PA - Production - Rapid
                } else if (ENV === 'demo') {
                    templateId = 'QxkYWq3sbV2JPUwNSegPxi'; // PA - Demo
                } else if (ENV === 'dev') {
                    templateId = ' ATCcb4BvG6EXaxrcek2w8S'; // PA - Dev
                }
            }

            // Convert all check boxes to strings
            if (vm.policy.secondaryInsured) {
                vm.policy.secondaryInsured.isDeceased = (vm.policy.secondaryInsured.isDeceased) ? 'Yes' : 'No';
            }

            vm.policy.forms.application.signMethod = 'E-Signature';

            vm.policy.$save().then(function() {
                var data = {
                    templateId: templateId,
                    documentName: "Application Form - " + vm.policy.insured.lastName,
                    recipients: recipients,
                    fieldData: {
                        "agent.fullName": { "value": vm.policy.agent.fullName ? vm.policy.agent.fullName : '' },
                        "annualPremiumAmount": { "value": vm.policy.annualPremiumAmount ? vm.policy.annualPremiumAmount : '' },
                        "assignmentDetails": { "value": vm.policy.assignmentDetails ? vm.policy.assignmentDetails : '' },
                        "carrier": { "value": vm.policy.carrier ? vm.policy.carrier : '' },
                        "currentBene.fullName": { "value": (vm.policy.currentBene) ? vm.policy.currentBene.fullName : '' },
                        "currentBene.gender.name": { "value": (vm.policy.currentBene && vm.policy.currentBene.gender) ? vm.policy.currentBene.gender[0].name : '' },
                        "currentBene.dateOfBirth": { "value": (vm.policy.currentBene && vm.policy.currentBene.dateOfBirth) ? vm.policy.currentBene.dateOfBirth : '' },
                        "currentBene.ssn": { "value": (vm.policy.currentBene && vm.policy.currentBene.fullName) ? vm.policy.currentBene.fullName : '' },
                        "currentBene.address.address": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.address : '' },
                        "currentBene.address.city": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.city : '' },
                        "currentBene.address.state": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.state : '' },
                        "currentBene.address.zip": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.zip : '' },
                        "currentBene.phoneNumber": { "value": (vm.policy.currentBene && vm.policy.currentBene.phoneNumber) ? vm.policy.currentBene.phoneNumber : '' },
                        "currentBene.emailAddress": { "value": (vm.policy.currentBene && vm.policy.currentBene.emailAddress) ? vm.policy.currentBene.emailAddress : '' },
                        "currentBene.maritalStatus": { "value": (vm.policy.currentBene && vm.policy.currentBene.maritalStatus) ? vm.policy.currentBene.maritalStatus[0].name : '' },
                        "currentBene.beneType": { "value": (vm.policy.currentBene && vm.policy.currentBene.beneType) ? vm.policy.currentBene.beneType[0] : '' },
                        "currentBene.relationshipToInsured": { "value": (vm.policy.currentBene && vm.policy.currentBene.relationshipToInsured) ? vm.policy.currentBene.relationshipToInsured : '' },
                        "currentSecondaryBene.fullName": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                        "currentSecondaryBene.gender.name": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.gender) ? vm.policy.currentSecondaryBene.gender[0].name : '' },
                        "currentSecondaryBene.dateOfBirth": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                        "currentSecondaryBene.ssn": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                        "currentSecondaryBene.address.address": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.address : '' },
                        "currentSecondaryBene.address.city": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.city : '' },
                        "currentSecondaryBene.address.state": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.state : '' },
                        "currentSecondaryBene.address.zip": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.zip : '' },
                        "currentSecondaryBene.phoneNumber": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.phoneNumber) ? vm.policy.currentSecondaryBene.phoneNumber : '' },
                        "currentSecondaryBene.emailAddress": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.emailAddress) ? vm.policy.currentSecondaryBene.emailAddress : '' },
                        "currentSecondaryBene.maritalStatus": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.maritalStatus) ? vm.policy.currentSecondaryBene.maritalStatus[0].name : '' },
                        "currentSecondaryBene.beneType": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.beneType) ? vm.policy.currentSecondaryBene.beneType[0] : '' },
                        "dateOfIssue": { "value": vm.policy.dateOfIssue ? vm.policy.dateOfIssue : '' },
                        "faceAmount": { "value": vm.policy.faceAmount ? vm.policy.faceAmount : '' },
                        "hasPolicyBeenAssigned": { "value": (vm.policy.hasPolicyBeenAssigned) ? vm.policy.hasPolicyBeenAssigned[0] : '' },
                        "hasPolicyChangedBene": { "value": (vm.policy.hasPolicyChangedBene) ? vm.policy.hasPolicyChangedBene[0] : '' },
                        "hasPolicyChangedOwner": { "value": (vm.policy.hasPolicyChangedOwner) ? vm.policy.hasPolicyChangedOwner[0] : '' },
                        "hasOwnerDeclaredBankruptcy": { "value": (vm.policy.hasOwnerDeclaredBankruptcy) ? vm.policy.hasOwnerDeclaredBankruptcy[0] : '' },
                        "insured.address.oneLiner": { "value": (vm.policy.insured.address && vm.policy.insured.address.oneLiner) ? vm.policy.insured.address.oneLiner : '' },
                        "insured.address.address": { "value": (vm.policy.insured.address && vm.policy.insured.address.address) ? vm.policy.insured.address.address : '' },
                        "insured.address.city": { "value": (vm.policy.insured.address && vm.policy.insured.address.city) ? vm.policy.insured.address.city : '' },
                        "insured.address.state": { "value": (vm.policy.insured.address && vm.policy.insured.address.state) ? vm.policy.insured.address.state : '' },
                        "insured.address.zip": { "value": (vm.policy.insured.address && vm.policy.insured.address.zip) ? vm.policy.insured.address.zip : '' },
                        "insured.dateOfBirth": { "value": vm.policy.insured.dateOfBirth ? vm.policy.insured.dateOfBirth : '' },
                        "insured.dependentChildren": { "value": (vm.policy.insured.dependentChildren) ? vm.policy.insured.dependentChildren[0] : '' },
                        "insured.emailAddress": { "value": vm.policy.insured.emailAddress ? vm.policy.insured.emailAddress : '' },
                        "insured.fullName": { "value": vm.policy.insured.fullName ? vm.policy.insured.fullName : '' },
                        "insured.gender.name": { "value": (vm.policy.insured.gender) ? vm.policy.insured.gender[0].name : '' },
                        "insured.maritalStatus": { "value": (vm.policy.insured.maritalStatus) ? vm.policy.insured.maritalStatus[0].name : '' },
                        "insured.medicalHistory": { "value": vm.policy.insured.medicalHistory ? vm.policy.insured.medicalHistory : '' },
                        "insured.nameOfSpouse": { "value": (vm.policy.insured.nameOfSpouse) ? vm.policy.insured.nameOfSpouse : '' },
                        "insured.phoneNumber": { "value": vm.policy.insured.phoneNumber ? vm.policy.insured.phoneNumber : '' },
                        "insured.primaryPhysicianName": { "value": vm.policy.insured.primaryPhysicianName ? vm.policy.insured.primaryPhysicianName : '' },
                        "insured.primaryPhysicianPhone": { "value": vm.policy.insured.primaryPhysicianPhone ? vm.policy.insured.primaryPhysicianPhone : '' },
                        "insured.specialistOneName": { "value": vm.policy.insured.specialistOneName ? vm.policy.insured.specialistOneName : '' },
                        "insured.specialistOnePhone": { "value": vm.policy.insured.specialistOnePhone ? vm.policy.insured.specialistOnePhone : '' },
                        "insured.specialistTwoName": { "value": vm.policy.insured.specialistTwoName ? vm.policy.insured.specialistTwoName : '' },
                        "insured.specialistTwoPhone": { "value": vm.policy.insured.specialistTwoPhone ? vm.policy.insured.specialistTwoPhone : '' },
                        "insured.ssn": { "value": vm.policy.insured.ssn ? vm.policy.insured.ssn : '' },
                        "intentToSellExplanation": { "value": vm.policy.intentToSellExplanation ? vm.policy.intentToSellExplanation : '' },
                        "interestedPartyOne.dateObtained": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.dateObtained : '' },
                        "interestedPartyOne.mannerObtained": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.mannerObtained : '' },
                        "interestedPartyOne.name": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.name : '' },
                        "interestedPartyOne.natureOfInterest": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.natureOfInterest : '' },
                        "interestedPartyOne.relationship": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.relationship : '' },
                        "interestedPartyThree.dateObtained": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.dateObtained : '' },
                        "interestedPartyThree.mannerObtained": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.mannerObtained : '' },
                        "interestedPartyThree.name": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.name : '' },
                        "interestedPartyThree.natureOfInterest": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.natureOfInterest : '' },
                        "interestedPartyThree.relationship": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.relationship : '' },
                        "interestedPartyTwo.dateObtained": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.dateObtained : '' },
                        "interestedPartyTwo.mannerObtained": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.mannerObtained : '' },
                        "interestedPartyTwo.name": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.name : '' },
                        "interestedPartyTwo.natureOfInterest": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.natureOfInterest : '' },
                        "interestedPartyTwo.relationship": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.relationship : '' },
                        "isInsuredAlsoOwner": { "value": (vm.policy.isInsuredAlsoOwner) ? vm.policy.isInsuredAlsoOwner[0] : '' },
                        "isOwnerInLegalSuit": { "value": (vm.policy.isOwnerInLegalSuit) ? vm.policy.isOwnerInLegalSuit[0] : '' },
                        "lastPremiumPaid": { "value": vm.policy.lastPremiumPaid ? vm.policy.lastPremiumPaid : '' },
                        "lastPremiumPaidAmount": { "value": vm.policy.lastPremiumPaidAmount ? vm.policy.lastPremiumPaidAmount : '' },
                        "nextPremiumDue": { "value": vm.policy.nextPremiumDue ? vm.policy.nextPremiumDue : '' },
                        "originalPolicyBene": { "value": vm.policy.originalPolicyBene ? vm.policy.originalPolicyBene : '' },
                        "originalPolicyBeneRelationship": { "value": vm.policy.originalPolicyBeneRelationship ? vm.policy.originalPolicyBeneRelationship : '' },
                        "originalPolicyOwner": { "value": vm.policy.originalPolicyOwner ? vm.policy.originalPolicyOwner : '' },
                        "originalPurposeForInsurance": { "value": vm.policy.originalPurposeForInsurance ? vm.policy.originalPurposeForInsurance : '' },
                        "policyFinanced.explanation": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.explanation : '' },
                        "policyFinanced.lender": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.lender : '' },
                        "policyFinanced.maturityDate": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.maturityDate : '' },
                        "policyFinanced.payoffAmount": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.payoffAmount : '' },
                        "policyFinanced.principalAmount": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.principalAmount : '' },
                        "policyNumber": { "value": vm.policy.policyNumber ? vm.policy.policyNumber : '' },
                        "policyType": { "value": vm.policy.policyType ? vm.policy.policyType : '' },
                        "wasPolicyFinanced": { "value": (vm.policy.wasPolicyFinanced) ? vm.policy.wasPolicyFinanced[0] : '' },
                        "wasThereIntentToSell": { "value": (vm.policy.wasThereIntentToSell) ? vm.policy.wasThereIntentToSell[0] : '' }
                    },
                    metaData: {
                        policyNumber: vm.policy.policyNumber,
                        policyKey: 'LOOKUP',
                        LSHID: vm.policy.LSHID,
                        formName: "Application Form",
                        className: "application",
                        formVersion: formVersion,
                        noteToRecipients: (vm.policy.forms.application) ? vm.policy.forms.application.noteToRecipients : null,
                        env: ENV
                    }
                };

                if (vm.policy.secondaryInsured && vm.policy.insured.secondaryInsured[0] === 'Yes') {
                    data.fieldData["secondaryInsured.address.oneLiner"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.oneLiner : '' };
                    data.fieldData["secondaryInsured.address.address"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.address : '' };
                    data.fieldData["secondaryInsured.address.city"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.city : '' };
                    data.fieldData["secondaryInsured.address.state"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.state : '' };
                    data.fieldData["secondaryInsured.address.zip"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.zip : '' };
                    data.fieldData["secondaryInsured.dateOfBirth"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dateOfBirth) ? vm.policy.secondaryInsured.dateOfBirth : '' };
                    data.fieldData["secondaryInsured.dependentChildren"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dependentChildren) ? vm.policy.secondaryInsured.dependentChildren[0] : '' };
                    data.fieldData["secondaryInsured.emailAddress"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.emailAddress) ? vm.policy.secondaryInsured.emailAddress : '' };
                    data.fieldData["secondaryInsured.fullName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.fullName) ? vm.policy.secondaryInsured.fullName : '' };
                    data.fieldData["secondaryInsured.gender.name"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.gender) ? vm.policy.secondaryInsured.gender[0].name : '' };
                    data.fieldData["secondaryInsured.isDeceased"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.isDeceased) ? vm.policy.secondaryInsured.isDeceased : '' };
                    data.fieldData["secondaryInsured.maritalStatus"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.maritalStatus) ? vm.policy.secondaryInsured.maritalStatus[0].name : '' };
                    data.fieldData["secondaryInsured.medicalHistory"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.medicalHistory) ? vm.policy.secondaryInsured.medicalHistory : '' };
                    data.fieldData["secondaryInsured.nameOfSpouse"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.nameOfSpouse) ? vm.policy.secondaryInsured.nameOfSpouse : '' };
                    data.fieldData["secondaryInsured.phoneNumber"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.phoneNumber) ? vm.policy.secondaryInsured.phoneNumber : '' };
                    data.fieldData["secondaryInsured.primaryPhysicianName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.primaryPhysicianName) ? vm.policy.secondaryInsured.primaryPhysicianName : '' };
                    data.fieldData["secondaryInsured.primaryPhysicianPhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.primaryPhysicianPhone) ? vm.policy.secondaryInsured.primaryPhysicianPhone : '' };
                    data.fieldData["secondaryInsured.specialistOneName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistOneName) ? vm.policy.secondaryInsured.specialistOneName : '' };
                    data.fieldData["secondaryInsured.specialistOnePhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistOnePhone) ? vm.policy.secondaryInsured.specialistOnePhone : '' };
                    data.fieldData["secondaryInsured.specialistTwoName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistTwoName) ? vm.policy.secondaryInsured.specialistTwoName : '' };
                    data.fieldData["secondaryInsured.specialistTwoPhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistTwoPhone) ? vm.policy.secondaryInsured.specialistTwoPhone : '' };
                    data.fieldData["secondaryInsured.ssn"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.ssn) ? vm.policy.secondaryInsured.ssn : '' };
                }

                if (vm.policy.owner && vm.policy.isInsuredAlsoOwner[0] === 'No') {
                    data.fieldData["owner.address.oneLiner"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.oneLiner : '' };
                    data.fieldData["owner.address.address"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.address : '' };
                    data.fieldData["owner.address.city"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.city : '' };
                    data.fieldData["owner.address.state"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.state : '' };
                    data.fieldData["owner.address.zip"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.zip : '' };
                    data.fieldData["owner.authorizedRep"] = { "value": (vm.policy.owner && vm.policy.owner.authorizedRep) ? vm.policy.owner.authorizedRep : '' };
                    data.fieldData["owner.dateOfBirth"] = { "value": (vm.policy.owner && vm.policy.owner.dateOfBirth) ? vm.policy.owner.dateOfBirth : '' };
                    data.fieldData["owner.dlNumber"] = { "value": (vm.policy.owner && vm.policy.owner.dlNumber) ? vm.policy.owner.dlNumber : '' };
                    data.fieldData["owner.emailAddress"] = { "value": (vm.policy.owner && vm.policy.owner.emailAddress) ? vm.policy.owner.emailAddress : '' };
                    data.fieldData["owner.fullName"] = { "value": (vm.policy.owner && vm.policy.owner.fullName) ? vm.policy.owner.fullName : '' };
                    data.fieldData["owner.maritalStatus"] = { "value": (vm.policy.owner && vm.policy.owner.maritalStatus) ? vm.policy.owner.maritalStatus[0].name : '' };
                    data.fieldData["owner.nameOfSpouse"] = { "value": (vm.policy.owner && vm.policy.owner.nameOfSpouse) ? vm.policy.owner.nameOfSpouse : '' };
                    data.fieldData["owner.ownerRelationship"] = { "value": (vm.policy.owner && vm.policy.owner.ownerRelationship) ? vm.policy.owner.ownerRelationship : '' };
                    data.fieldData["owner.phoneNumber"] = { "value": (vm.policy.owner && vm.policy.owner.phoneNumber) ? vm.policy.owner.phoneNumber : '' };
                    data.fieldData["owner.ssntin"] = { "value": (vm.policy.owner && vm.policy.owner.ssntin) ? vm.policy.owner.ssntin : '' };
                    data.fieldData["owner.stateOfFormation"] = { "value": (vm.policy.owner && vm.policy.owner.stateOfFormation) ? vm.policy.owner.stateOfFormation : '' };
                    data.fieldData["owner.stateOfIssue.id"] = { "value": (vm.policy.owner && vm.policy.owner.stateOfIssue) ? vm.policy.owner.stateOfIssue.id : '' };
                }
                pandadocService.createDocumentFromTemplate(data).then(function(response) {
                    if (response.status === 'success') {
                        if (response.data && response.data.id) {
                            vm.policy.status.applicationFinished = firebase.database.ServerValue.TIMESTAMP;
                            vm.policy.forms.application.createdDtTm = firebase.database.ServerValue.TIMESTAMP;
                            vm.policy.forms.application.documentId = response.data.id;
                            vm.policy.forms.application.status = 'Pending Signatures';
                            vm.policy.forms.application.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;
                            vm.policy.status.display = {
                                step: 'Application Signatures',
                                status: 'Pending',
                                text: 'Pending Signatures'
                            };
                            sweetAlert.swal({
                                title: 'Success',
                                text: 'Your Application has been successfully submitted for e-signature',
                                type: 'success'
                            }).then(function(result) {
                                // Save and go to final page
                                vm.signingApplication = false;
                                vm.savePolicy(100);
                            });
                        } else {
                            sweetAlert.swal({
                                title: 'Error',
                                text: 'No e-signature Document ID returned...',
                                type: 'error'
                            }).then(function(result) {
                                $scope.$apply(function() {
                                    vm.signingApplication = false;
                                });
                            });
                        }
                    } else {
                        sweetAlert.swal({
                            title: 'Error',
                            text: response,
                            type: 'error'
                        }).then(function(result) {
                            $scope.$apply(function() {
                                vm.signingApplication = false;
                            });
                        });
                    }
                });
            });
        }

        vm.onSelectCallback = function(item, convertToValue) {
            vm.pageIsDirty = true;
            if (convertToValue) {
                item = item[0];
            }
            updateLabels();
        }

        vm.goToPage = function(page) {
            if (vm.pageIsDirty) {
                vm.savePolicy(page);
            } else if ((page === 'next' && vm.showPage + 1 === 5) || page === 5) {
                checkRequiredFields();
            } else if ((page === 'next' && vm.showPage + 1 === 6) || page === 6) {
                // Set up form recipients
                if (vm.policy.insured && !vm.policy.forms.application.recipients.insured) {
                    vm.policy.forms.application.recipients.insured = {
                        firstName: vm.policy.insured.firstName ? vm.policy.insured.firstName : undefined,
                        lastName: vm.policy.insured.lastName ? vm.policy.insured.lastName : undefined,
                        fullName: vm.policy.insured.firstName ? vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName : undefined,
                        emailAddress: vm.policy.insured.emailAddress ? vm.policy.insured.emailAddress : undefined
                    };
                }
                if (vm.policy.secondaryInsured && !vm.policy.forms.application.recipients.secondaryInsured) {
                    vm.policy.forms.application.recipients.secondaryInsured = {
                        firstName: vm.policy.secondaryInsured.firstName ? vm.policy.secondaryInsured.firstName : undefined,
                        lastName: vm.policy.secondaryInsured.lastName ? vm.policy.secondaryInsured.lastName : undefined,
                        fullName: vm.policy.secondaryInsured.firstName ? vm.policy.secondaryInsured.firstName + ' ' + vm.policy.secondaryInsured.lastName : undefined,
                        emailAddress: vm.policy.secondaryInsured.emailAddress ? vm.policy.secondaryInsured.emailAddress : undefined
                    };
                }
                if (vm.policy.owner && !vm.policy.forms.application.recipients.owner) {
                    vm.policy.forms.application.recipients.owner = {
                        firstName: vm.policy.owner.firstName ? vm.policy.owner.firstName : undefined,
                        lastName: vm.policy.owner.lastName ? vm.policy.owner.lastName : undefined,
                        fullName: vm.policy.owner.firstName ? vm.policy.owner.firstName + ' ' + vm.policy.owner.lastName : undefined,
                        emailAddress: vm.policy.owner.emailAddress ? vm.policy.owner.emailAddress : undefined
                    };
                }
                vm.showPage++;
                updateLabels();
            } else if (page === 'next') {
                vm.showPage++;
                updateLabels();
            } else if (page === 'previous') {
                vm.showPage--;
                updateLabels();
            } else {
                vm.showPage = page;
                updateLabels();
            }
        }

        function checkRequiredFields() {
            /**
             * Validate all required fields
             */
            var requiredFields = [{
                field: 'insured.firstName',
                page: 2
            }, {
                field: 'insured.lastName',
                page: 2
            }, {
                field: 'insured.gender',
                page: 2
            }, {
                field: 'insured.dateOfBirth',
                page: 2
            }, {
                field: 'insured.ssn',
                page: 2
            }, {
                field: 'insured.address',
                page: 2
            }, {
                field: 'insured.phoneNumber',
                page: 2
            }, {
                field: 'insured.primaryPhysicianName',
                page: 2
            }, {
                field: 'insured.primaryPhysicianPhone',
                page: 2
            }, {
                field: 'state',
                page: 2
            }, {
                field: 'policyNumber',
                page: 2
            }, {
                field: 'carrier',
                page: 3
            }, {
                field: 'faceAmount',
                page: 3
            }, {
                field: 'policyType',
                page: 3
            }, {
                field: 'isInsuredAlsoOwner',
                page: 4
            }];
            var missingReqOnPage = 0;
            for (let index = 0; index < requiredFields.length; index++) {
                const req = requiredFields[index];
                var reqVal = resolvePath(req.field, vm.policy);
                if (!reqVal) {
                    console.log('Missing ' + req.field + ' on page ' + req.page);
                    if (missingReqOnPage === 0) {
                        missingReqOnPage = req.page;
                    }
                    jQuery('#' + req.field.replace(/\./g, '_')).addClass('missing-req');
                } else {
                    jQuery('#' + req.field.replace(/\./g, '_')).removeClass('missing-req');
                }
            }
            if (missingReqOnPage > 0) {
                console.log('Some Required Fields are Missing');
                vm.showPage = 5;
                vm.requiredFieldsMissing = true;
            } else {
                console.log('All required fields are complete');
                vm.showPage = 5;
                vm.requiredFieldsMissing = false;
            }
        }

        function resolvePath(path, obj) {
            return path.split('.').reduce(function(prev, curr) {
                return prev ? prev[curr] : ''
            }, obj || self)
        }

        vm.savePolicy = function(nextPage) {
            var root = firebase.database().ref();

            if (vm.formPublicGuid !== 'NEW') {
                vm.policy.forms.application.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;

                // Convert number text to numbers for sorting
                if (vm.policy.faceAmount) {
                    vm.policy.faceAmount = parseInt(vm.policy.faceAmount);
                }
                if (vm.policy.premium) {
                    vm.policy.premium = parseInt(vm.policy.premium);
                }
                if (vm.policy.estimatedCoiPremium) {
                    vm.policy.estimatedCoiPremium = parseInt(vm.policy.estimatedCoiPremium);
                }

                // Calculate Insured Age
                if (vm.policy.insured.dateOfBirth) {
                    vm.policy.insured.age = vm.calculateAge(vm.policy.insured.dateOfBirth);
                }

                removeUndefined(vm.policy);

                vm.policy.$save().then(function(ref) {
                    vm.toast('success', 'Application Saved');
                    vm.pageIsDirty = false;
                    if ((nextPage === 'next' && vm.showPage + 1 === 5) || nextPage === 5) {
                        checkRequiredFields();
                    } else if (nextPage === 'next') {
                        vm.showPage++;
                    } else if (nextPage === 'previous') {
                        vm.showPage--;
                    } else if (nextPage > 0) {
                        vm.showPage = nextPage;
                    }
                    updateLabels();
                });
            }
        }

        function removeUndefined(object) {
            for (let [key,value] of Object.entries(object)) {
                if (key.substr(0,1) !== '$') {
                    if (typeof(value) === 'object') {
                        removeUndefined(value);
                    } else if (typeof(value) === 'undefined') {
                        object[key] = null;
                    } else {
                        JSON.parse( JSON.stringify( object[key] ) );
                    }
                }
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }
    }
    angular.module('yapp')
        .controller('PublicApplicationCtrl', PublicApplicationCtrl);
})();