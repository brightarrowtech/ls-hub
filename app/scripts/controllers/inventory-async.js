function checkForUserPolicies(createdOnPlatform) {
    return new Promise(function(resolve, reject) {
        root.child('policies').orderByChild('createdById').equalTo(vm.currentUser.auth.uid).once('value', function(snap) {
            if (snap.exists()) {
                snap.forEach(function(policy) {
                    var thisPolicy = policy.val();
                    var addIt = true;
                    // Don't double-add an org created policy...
                    vm.policies.forEach(function(addedPolicy) {
                        if (addedPolicy.policyKey === policy.key) {
                            addIt = false;
                        }
                    });
                    if (addIt) {
                        if (thisPolicy.policyIsActive && !thisPolicy.isArchived && createdOnPlatform === thisPolicy.createdOnPlatform) {
                            if (thisPolicy.insured && thisPolicy.insured.gender) {
                                thisPolicy.insured.safeName = thisPolicy.insured.lastName + ', ' + thisPolicy.insured.firstName.charAt(0);
                                delete thisPolicy.insured.firstName;
                                delete thisPolicy.insured.state;
                                delete thisPolicy.insured.dateOfBirth;
                                delete thisPolicy.insured.gender.name;
                            }
                            delete thisPolicy.adminFiles;
                            delete thisPolicy.caseFiles;
                            delete thisPolicy.lastUpdatedBy;
                            delete thisPolicy.lastUpdatedById;
                            delete thisPolicy.updateLog;
                            delete thisPolicy.private;
                            thisPolicy.policyKey = policy.key;
                            vm.activePolicyCount++;
                            vm.policies.push(thisPolicy);
                        }
                    }
                });
                resolve();
            } else {
                resolve();
            }
        });
    });
}

function checkForOrgPolicies(createdOnPlatform) {
    return new Promise(function(resolve, reject) {
        $window.async.forEachOf(Object.keys(vm.currentUser.org.policies), function(pid, key, callback) {
            // If policy has a "fromOrg" field, first check if the current user is "linkedTo" or "invitedTo" the "fromOrg"
            let policyData = vm.currentUser.org.policies[pid];
            if (policyData.fromOrg) {
                if (vm.currentUser.org.linkedTo && vm.currentUser.org.linkedTo[policyData.fromOrg]) {
                    // My Org has accepted this Supplier...carry on
                    root.child('privatePolicies/' + pid).once('value', (policy) => {
                        if (policy.exists()) {
                            var thisPolicy = policy.val();
                            thisPolicy.publicGuid = policy.key;
                            if (thisPolicy.policyIsActive && !thisPolicy.isArchived) {
                                thisPolicy.insured.safeName = thisPolicy.insured.firstName + ' ' + (thisPolicy.insured.lastName ? thisPolicy.insured.lastName : thisPolicy.insured.lastNameInitial + '.');
                                vm.activeAvailableCount++;
                                vm.available.push(thisPolicy);
                            }
                        }
                        callback();
                    }).catch(function(error) {
                        console.log(error);
                        callback();
                    });
                } else {
                    callback();
                }
            } else {
                // No fromOrg present....carry on
                root.child('policies/' + pid).once('value', (policy) => {
                    if (policy.exists()) {
                        var thisPolicy = policy.val();
                        var addIt = true;
                        // For the Back Office, don't add Policies that don't have "sentToBackOffice" status set to true, or that have been sent out for offers
                        if (vm.userRole === 'Back Office') {
                            if (!thisPolicy.status.sentToBackOffice) {
                                addIt = false;
                            }
                            if (thisPolicy.status.outForOffers) {
                                addIt = false;
                            }
                        }
                        // Don't double-add a personally created policy...
                        vm.policies.forEach(function(addedPolicy) {
                            if (addedPolicy.policyKey === policy.key) {
                                addIt = false;
                            }
                        });
                        if (addIt) {
                            if (thisPolicy.policyIsActive && !thisPolicy.isArchived && createdOnPlatform === thisPolicy.createdOnPlatform) {
                                vm.activePolicyCount++;
                                if (thisPolicy.insured && thisPolicy.insured.gender) {
                                    thisPolicy.insured.safeName = thisPolicy.insured.lastName + ', ' + thisPolicy.insured.firstName.charAt(0);
                                    delete thisPolicy.insured.firstName;
                                    delete thisPolicy.insured.state;
                                    delete thisPolicy.insured.dateOfBirth;
                                    delete thisPolicy.insured.gender.name;
                                }
                                delete thisPolicy.adminFiles;
                                delete thisPolicy.caseFiles;
                                delete thisPolicy.lastUpdatedBy;
                                delete thisPolicy.lastUpdatedById;
                                delete thisPolicy.updateLog;
                                delete thisPolicy.private;
                                thisPolicy.policyKey = policy.key;
                                vm.policies.push(thisPolicy);
                            }
                        }
                    }
                    callback();
                }).catch(function(error) {
                    callback(error);
                });
            }
        }, function(err) {
            if (err) {
                console.log(err);
            }
            resolve();
        });
    });
}