(function() {
    'use strict';

    /* @ngInject */
    function AuditLogCtrl($window, ENV, stdData, toastr, $scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, policyService, currentAuth, sweetAlert, notifyService, apiService, brandingDomain, sharedService) {
        var vm = this;

        // URL parsing to determine platform
        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        // Import standard data to data model
        vm.types = stdData.policyTypes;
        vm.yesNo = stdData.yesNo;

        // Establish global Firebase RTDB root reference
        var root = firebase.database().ref();

        /**

██████╗  █████╗  ██████╗ ███████╗    ██╗      ██████╗  █████╗ ██████╗ 
██╔══██╗██╔══██╗██╔════╝ ██╔════╝    ██║     ██╔═══██╗██╔══██╗██╔══██╗
██████╔╝███████║██║  ███╗█████╗      ██║     ██║   ██║███████║██║  ██║
██╔═══╝ ██╔══██║██║   ██║██╔══╝      ██║     ██║   ██║██╔══██║██║  ██║
██║     ██║  ██║╚██████╔╝███████╗    ███████╗╚██████╔╝██║  ██║██████╔╝
╚═╝     ╚═╝  ╚═╝ ╚═════╝ ╚══════╝    ╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═════╝ 
                                                                      
         * Description: Initial page load functions
         * 
         * authService.getCurrentUser() - Get the logged in user and determine if their account is in good standing
         * loadPage() - Get the Policy based on ID's in querystring params
         * 
         */

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }
            vm.userRole = vm.currentUser.profile.role;
            // Get and check permissions here...
            vm.user = $firebaseObject(root.child('users/' + vm.currentUser.auth.uid));
            vm.user.$loaded().then(function(user) {
                loadPage();
            });
        });

        function loadPage() {
            if ($state.params.pid) {
                vm.policyKey = $state.params.pid;
                vm.policyRef = root.child('auditTrail').child(vm.policyKey).orderByChild('updatedAt').limitToLast(15);
                vm.policyRef.once('value', function(snap) {
                    if (snap.exists()) {
                        vm.history = snap.val();

                        snap.forEach(function(item) {
                            var idx = 0;
                            item.val().changes.forEach(function(change) {
                                vm.history[item.key].changes[idx].pathStr = '';
                                vm.history[item.key].changes[idx].newType = typeof(change.rhs);
                                vm.history[item.key].changes[idx].oldType = typeof(change.lhs);
                                var pathIdx = 0;
                                change.path.forEach(function(pathPart) {
                                    vm.history[item.key].changes[idx].pathStr = vm.history[item.key].changes[idx].pathStr + pathPart;
                                    if (pathIdx < change.path.length - 1) {
                                        vm.history[item.key].changes[idx].pathStr = vm.history[item.key].changes[idx].pathStr + ' <i class="fa fa-angle-right"></i> ';
                                    }
                                    pathIdx++;
                                });
                                idx++;
                            });
                        });

                        // Validate access
                        if ('a' === 'b') {
                            // on iManager, everyone who will be viewing a Policy will have an Org
                            // and everyone's Org will be listed on the policy as having access (created by their Org)
                            vm.history = undefined;
                            sweetAlert.swal({
                                title: "Error",
                                text: "The link you provided is not valid, or you do not have access to view this Policy.",
                                type: "error"
                            }).then(function() {
                                $state.go('inventory');
                            });
                        } else {
                            vm.hidePage = false;
                        }
                    } else {
                        sweetAlert.swal({
                            title: "Policy not found",
                            text: "Sorry, we couldn't find a Policy using that link. Sending you to your Inventory.",
                            type: "error",
                            showCancelButton: false
                        }).then(function(result) {
                            $state.go('inventory');
                        });
                    }
                }).catch(function(e) {
                    if (e.code === 'PERMISSION_DENIED') {
                        sweetAlert.swal({
                            title: "Permission Denied",
                            text: "You do not have access to view this Policy.",
                            type: "error",
                            showCancelButton: false
                        }).then(function(result) {
                            $state.go('inventory');
                        });
                    }
                });
            } else {
                $state.go('application');
            }
        }


        /**

██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗     ███████╗██╗   ██╗███╗   ██╗ ██████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗    ██╔════╝██║   ██║████╗  ██║██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝    █████╗  ██║   ██║██╔██╗ ██║██║        ██║   ██║██║   ██║██╔██╗ ██║███████╗
██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗    ██╔══╝  ██║   ██║██║╚██╗██║██║        ██║   ██║██║   ██║██║╚██╗██║╚════██║
██║  ██║███████╗███████╗██║     ███████╗██║  ██║    ██║     ╚██████╔╝██║ ╚████║╚██████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝    ╚═╝      ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
                                                                                                                              
         * Description: Miscellaneous functions to support other features
         * 
         * vm.toast() - Call the toastr service to display popup notification
         * updateLabels() - Handle the jvFloat labels, iCheck inputs and Flatpickr date pickers
         * vm.showMessage() - SWAL popup response to clicking "?" icons in UI
         * vm.calculateAge() - Determines age in years giving a date of birth
         * uuidv4() - Returns an adequately random GUID for Buyer Policy copies
         * vm.onSelectCallback() - Function called after updating a <select> in the UI
         * adjustStatusImage() - Updates the Policy status image in the top right of the UI
         * updateStatus() - Updates the Policy "status" node, including the display text
         * vm.goTo() - Navigate to a new route
         * 
         */

        function adjustStatusImage() {
            vm.statusImage = 'Timeline';
            if (vm.policy.status.applicationSigned) {
                vm.statusImage += '_1';
            }
            if (vm.policy.status.illustrationsReceived) {
                if (vm.policy.status.illustrationsOrdered) {
                    vm.statusImage += '_2B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.illustrationsReceived = false;
                }
            } else if (vm.policy.status.illustrationsOrdered) {
                vm.statusImage += '_2A';
            }
            if (vm.policy.status.medicalsReceived) {
                if (vm.policy.status.medicalsOrdered) {
                    vm.statusImage += '_3B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.medicalsReceived = false;
                }
            } else if (vm.policy.status.medicalsOrdered) {
                vm.statusImage += '_3A';
            }
            if (vm.policy.status.medicalSummaryReceived) {
                if (!vm.policy.status.medicalSummaryRequested && vm.platform === 'isubmit') {
                    vm.policy.status.medicalSummaryReceived = false;
                }
            }
            if (vm.policy.status.outForOffers) {
                if (vm.policy.status.illustrationsReceived && vm.policy.status.medicalsReceived) {
                    if (vm.policy.status.offerInterest) {
                        vm.statusImage = 'Timeline_4Int';
                    } else if (vm.policy.status.offerNoInterest) {
                        vm.statusImage = 'Timeline_4NoInt';
                    } else {
                        vm.statusImage = 'Timeline_4A';
                    }
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.outForOffers = false;
                }
            }
            if (vm.policy.status.offerReceived) {
                if (vm.policy.status.outForOffers) {
                    vm.statusImage = 'Timeline_4B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.offerReceived = false;
                }
            }
        }

        function updateStatus() {
            vm.policy.status.display.text = 'Started';
            if (vm.policy.status.applicationSigned) {
                vm.policy.status.display.text = 'In Progress';
            }
            if (vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Out for Offers';
            }
            if (vm.policy.status.offerNoInterest) {
                vm.policy.status.display.text = 'Case Rejected';
            }
            if (vm.policy.status.offerReceived) {
                vm.policy.status.display.text = 'Offer Received';
            }
            Object.keys(vm.policy.forms).forEach(function(form) {
                if (vm.policy.forms[form].status === 'Pending Signatures') {
                    vm.policy.status.display.text = 'Pending Signatures';
                } else if (vm.policy.forms[form].status === 'Pending 3rd Party') {
                    vm.policy.status.display.text = 'Pending 3rd Party';
                } else if (vm.policy.forms[form].status === 'Needs Review') {
                    vm.policy.status.display.text = 'Pending Agent Review';
                }
            });
            var updates = {};
            if (vm.platform === 'isubmit') {
                if (vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Agent Action Required';
                    updates['actionRequired'] = true;
                } else {
                    updates['actionRequired'] = null;
                }
                if (vm.policy.boActionRequired) {
                    vm.policy.status.display.text = 'Back Office Action Required';
                    delete vm.policy.status.submittedToBroker;
                    updates['boActionRequired'] = true;
                } else {
                    updates['boActionRequired'] = null;
                }
                if (vm.policy.brokerActionRequired) {
                    vm.policy.status.display.text = 'Broker Action Required';
                    updates['brokerActionRequired'] = true;
                } else {
                    updates['brokerActionRequired'] = null;
                }
                if (vm.policy.status.submittedToBroker && !vm.policy.status.outForOffers && !vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Pending Broker Review';
                }
                if (!vm.policy.status.sentToBackOffice && !vm.policy.actionRequired) {
                    vm.policy.status.display.text = 'Pending Broker Review';
                }
            }
            updates['status'] = vm.policy.status;
            vm.policyRef.update(updates).then(function(ref) {
                vm.toast('success', 'Status updated');
            });
        }

        vm.goTo = function(route) {
            $state.go(route);
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    defaultDate: "today",
                    maxDate: new Date(),
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.policy[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = vm.policy[parts[0]][parts[1]];
                        } else if (parts.length === 3) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]];
                        } else if (parts.length === 4) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]][parts[3]];
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('body').on('ifChecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'statusSelection') {
                        delete vm.policy.statusSelection[0].$$hashKey;
                        vm.policy.latestStatusAt = moment().unix() * 1000;
                        if (!vm.policy.updateLog) {
                            vm.policy.updateLog = [];
                        }
                        vm.policy.updateLog.push({
                            action: 'Status Updated',
                            updatedAt: firebase.database.ServerValue.TIMESTAMP,
                            updatedBy: vm.currentUser.profile.displayName,
                            updatedById: vm.currentUser.auth.uid
                        });
                        var updates = {};
                        updates['latestStatusAt'] = firebase.database.ServerValue.TIMESTAMP;
                        updates['statusSelection'] = vm.policy.statusSelection;
                        updates['updateLog'] = vm.policy.updateLog;
                        var data = {
                            targets: [],
                            policyKey: vm.policyKey,
                            createdBy: vm.currentUser.profile.displayName,
                            createdByRole: vm.currentUser.profile.role,
                            note: vm.policy.statusSelection[0].status + ' - ' + vm.policy.latestStatus,
                            LSHID: vm.policy.LSHID,
                            action: 'Status Updated'
                        };
                        vm.policyRef.update(updates).then(function() {
                            vm.toast('success', 'Policy Status successfully set');
                            sendNotification(data);
                        }).catch(function(err) {
                            vm.toast('error', err.message);
                        });
                    } else if (event.target.name === 'visibleToSupply') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        sweetAlert.swal({
                            title: "Confirm Action",
                            html: "This will remove access to this file from any Buyers who would otherwise be able to see files of this category. Are you sure?",
                            type: "question",
                            showCancelButton: true,
                            cancelButtonText: "No",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }).then(function(result) {
                            if (!result.dismiss) {
                                vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function() {
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        // TODO: Remove existing access to this file from any existing Buyers
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.caseFiles[fileIdx].$id] = null;
                                        });
                                        root.update(updates).then(function() {
                                            sweetAlert.swal({
                                                title: "Success",
                                                text: "File access has been removed from all Buyers",
                                                type: "success",
                                                timer: 1000
                                            }).then(function() {});
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    }
                                    vm.toast('success', 'Policy Updated');
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        });
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = true;
                        vm.policyRef.child('policyIsActive').set(true).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        if (event.target.name === 'policyActionRequired') {
                            vm.setActionRequired('agent', 'on');
                        } else if (event.target.name === 'policyBOActionRequired') {
                            vm.setActionRequired('back-office', 'on');
                        } else if (event.target.name === 'policyBrokerActionRequired') {
                            vm.setActionRequired('broker', 'on');
                        }
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = vm.buyers;
                    } else if (event.target.className.indexOf('status-check') >= 0) {
                        vm.policy.status[event.target.name] = true;
                        vm.policy.status[event.target.name + 'At'] = moment().unix() * 1000;
                        adjustStatusImage();
                        updateStatus();
                        var data = {
                            targets: [],
                            policyKey: vm.policyKey,
                            createdBy: vm.currentUser.profile.displayName,
                            createdByRole: vm.currentUser.profile.role,
                            note: event.target.getAttribute('notificationText') + ' for this Policy',
                            LSHID: vm.policy.LSHID,
                            action: event.target.name === 'offerReceived' ? 'Offer Received' : 'Status Updated'
                        };
                        sendNotification(data);
                    }
                    $scope.$apply();
                });
                jQuery('body').on('ifUnchecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'visibleToSupply') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        if (!vm.caseFiles[fileIdx].category) {
                            sweetAlert.swal({
                                title: "No File Category",
                                text: "Please categorize this file first so we know how to distribute it to Buyers",
                                type: "warning"
                            }).then(function() {
                                vm.caseFiles[fileIdx].hiddenFromBuyers = true;
                                $scope.$apply();
                            });
                        } else {
                            // Check all Buyers to see if any of them have access to this file's category
                            var buyersWithAccess = [];
                            var buyerAccessMessage = 'The following Buyers will be able to view this file:<br><br>';
                            if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                vm.policy.buyers.forEach(buyer => {
                                    delete buyer.$$hashKey;
                                    if (buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(vm.caseFiles[fileIdx].category) > -1) {
                                        buyersWithAccess.push(buyer);
                                        buyerAccessMessage += buyer.buyerName + '<br>';
                                    }
                                });
                            }
                            buyerAccessMessage += '<br>Do you want to continue?';
                            if (buyersWithAccess.length > 0) {
                                sweetAlert.swal({
                                    title: "Confirm Action",
                                    html: buyerAccessMessage,
                                    type: "question",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonColor: "#00B200",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false
                                }).then(function(result) {
                                    if (!result.dismiss) {
                                        // Update /sharedCaseFiles/[each buyer's PolicyGuid] in a single transaction
                                        var updates = {};
                                        var caseFileCopy = sharedService.cloneObject(vm.caseFiles[fileIdx]);
                                        delete caseFileCopy.$id;
                                        delete caseFileCopy.$priority;
                                        delete caseFileCopy.$$hashKey;
                                        buyersWithAccess.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.caseFiles[fileIdx].$id] = caseFileCopy;
                                        });

                                        vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                                            root.update(updates).then(function() {
                                                sweetAlert.swal({
                                                    title: "Success",
                                                    text: "File access has been granted to all selected Buyers",
                                                    type: "success",
                                                    timer: 1000
                                                }).then(function() {});
                                            }).catch(function(err) {
                                                console.log(err);
                                            });
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    } else {
                                        sweetAlert.swal({
                                            title: "Success",
                                            text: "No changes have been made",
                                            type: "success",
                                            timer: 1000
                                        }).then(function() {
                                            vm.caseFiles[fileIdx].hiddenFromBuyers = true;
                                        });
                                    }
                                });
                            } else {
                                vm.caseFiles.$save(vm.caseFiles[fileIdx]).then(function(ref) {
                                    vm.toast('success', 'Policy Updated');
                                });
                            }
                        }
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = false;
                        vm.policyRef.child('policyIsActive').set(false).then(function(ref) {
                            vm.toast('success', 'Policy Updated');
                        });
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        if (event.target.name === 'policyActionRequired') {
                            vm.setActionRequired('agent', 'off');
                        } else if (event.target.name === 'policyBOActionRequired') {
                            vm.setActionRequired('back-office', 'off');
                        } else if (event.target.name === 'policyBrokerActionRequired') {
                            vm.setActionRequired('broker', 'off');
                        }
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        vm.policy.secondaryInsured.isDeceased = false;
                    } else if (event.target.name.indexOf('ActionRequired') > -1) {
                        updateStatus();
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = [];
                    } else if (event.target.className.indexOf('status-check') >= 0) {
                        vm.policy.status[event.target.name] = false;
                        delete vm.policy.status[event.target.name + 'At'];
                        adjustStatusImage();
                        updateStatus();
                    }
                    $scope.$apply();
                });
                jQuery('.tabs').tabslet();
                $scope.$apply();
            }, 0);
        }

        vm.showMessage = function(type) {
            if (type === 'Out for Offers') {
                sweetAlert.swal({
                    title: "Out for Offers",
                    text: "If you have already sent this Policy out to Buyers by another means, you can check this box to update the Policy Status to reflect this. Either way, you can always use the \"Send to Buyers\" button to distribute Policies through the iManager platform",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Confirm Document Access') {
                sweetAlert.swal({
                    title: "Confirm Document Access",
                    text: "Buyers can be set up with Default Document Category access, but you may want to adjust that for any given Policy. Document access is granted by File Category, and can optionally be overridden per Document to block it from all Buyers.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Latest Update') {
                sweetAlert.swal({
                    title: "Latest Policy Update",
                    text: "This section shows the most recent status update from the Broker. This information is also included in the Digest Email you can subscribe to in your Profile",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.onSelectCallback = function(input) {
            jQuery('.placeHolder-' + input).addClass('active');
        }
    }
    angular.module('yapp')
        .controller('AuditLogCtrl', AuditLogCtrl);
})();