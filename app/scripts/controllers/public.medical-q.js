(function() {
    'use strict';

    /* @ngInject */
    function PublicMedicalQCtrl(stdData, ENV, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, sweetAlert, pandadocService) {
        var vm = this;
        vm.showPage = 1;
        vm.pageIsDirty = false;
        vm.isLoading = true;
        vm.isError = false;
        vm.isPublic = true;

        vm.yesNo = stdData.yesNo;
        vm.genders = stdData.genders;

        vm.doYouLiveInOptions = [
            'Assisted Living Facility',
            'Skilled Nursing Facility',
            'Nursing Home',
            'Other'
        ];
        vm.activitiesNeedingAssistance = [
            'Meal Planning',
            'Taking Medications',
            'Shopping',
            'Walking',
            'Bathing',
            'Dressing'
        ];
        vm.heartDisorders = [
            'High blood pressure',
            'Atrial fibrillation',
            'Irregular Pulse or Arrhythmia Other than AFIB',
            'Coronary Artery Disease',
            'Angina (chest pain from heart disease)',
            'Heart Attack(s)',
            'Heart Valve Disease',
            'Heart Failure',
            'Other'
        ];
        vm.circulatoryDisorders = [
            'Stroke',
            'TIA or Mini-Stroke',
            'Aneurysm of an Artery',
            'Arterial Blockage in the Neck, Abdomen or Legs',
            'Venous Disease (blood clots, deep vein thrombosis or embolism)',
            'Other'
        ];
        vm.cancers = [
            'Tumor or Malignancy',
            'Leukemia',
            'Lymphoma',
            'Multiple Myeloma',
            'Blood Cancers (MPNs)',
            'Myelodyplastic Syndrome',
            'Other Cancerous Disorder'
        ];
        vm.neurologicalDisorders = [
            'Parkinson\'s Disease',
            'Multiple Sclerosis',
            'ALS (Lou Gehrig’s Disease)',
            'Loss of Consciousness',
            'Convulsions or Epilepsy',
            'Poor Vision',
            'Chronic Pain',
            'Sleep Apnea',
            'Other'
        ];
        vm.mentalNervousDisorders = [
            'Memory or Cognitive Impairment Without Dementia',
            'Alzheimer\'s or Other Type of Dementia',
            'Depression',
            'Schizophrenia',
            'Other'
        ];
        vm.digestiveDisorders = [
            'Diabetes',
            'Liver (not due to infection)',
            'Colon or Rectum',
            'Small Intestine',
            'Esophagus or Stomach',
            'GI Bleeding (upper or lower)',
            'Other'
        ];
        vm.infectionsDiseases = [
            'Hepatitis',
            'Pneumonia',
            'Sepsis (blood infection)',
            'Shingles',
            'Urinary Tract Infection',
            'MRSA',
            'Other'
        ];
        vm.respiratoryDisorders = [
            'Asthma',
            'COPD, Emphysema or Chronic Bronchitis',
            'Shortness of Breath at Rest or with Minimal Exertion',
            'Chronic Lung Infection',
            'Other'
        ];
        vm.genitourinaryDisorders = [
            'Prostate',
            'Bladder',
            'Kidney Disease, Impaired Function or Failure',
            'Urine Abnormalities',
            'Other'
        ];
        vm.bloodAbnormalities = [
            'Anemia',
            'High Cholesterol or Triglycerides',
            'Abnormalities of Platelets, White or Red Blood Cells',
            'Abnormal Bruising, Bleeding or Clotting',
            'Disorder of the Spleen, Bone Marrow or Lymph Nodes',
            'Other'
        ];
        vm.boneJointNerveAbnormalities = [
            'Paralysis or Significant Physical Impairment',
            'Gout',
            'Numbness in Extremities',
            'Problems with Balance or Walking Injury or Accidental Fall',
            'Degenerative Arthritis',
            'Rheumatoid Arthritis',
            'Osteoporosis',
            'Fracture of Hip, Vertebra or Other Bone',
            'Other'
        ];
        vm.immuneDisorders = [
            'HIV',
            'Autoimmune Disease',
            'Systemic Lupus',
            'Connective Tissue Disease',
            'Other'
        ];
        vm.alcoholUse = [
            'Alcoholism or Alcohol Abuse',
            'Illegal Drug Use',
            'Marijuana',
            'Prescription Drug Abuse',
            'Ever been advised by a medical professional to reduce or eliminate alcohol or drug use, including prescription drugs'
        ];

        loadPage();

        function loadPage() {
            if ($state.params.pubId) {
                /**
                 * LOADING AN EXISTING POLICY
                 */
                var root = firebase.database().ref();
                vm.formPublicGuid = $state.params.pubId;
                vm.form = $firebaseObject(root.child('publicForms').child(vm.formPublicGuid));
                vm.form.$loaded().then(function(form) {
                    if (vm.form.hasOwnProperty("$value") && vm.form.$value === null) {
                        vm.isLoading = false;
                        vm.isError = true;
                    } else {
                        vm.storage = firebase.storage();
                        vm.storageRef = vm.storage.ref('publicForms/' + vm.formPublicGuid);
                        vm.isLoading = false;
                        updateLabels();
                    }
                }).catch(function(err) {
                    vm.isLoading = false;
                    vm.isError = true;
                });
            } else {
                vm.isError = true;
                vm.isLoading = false;
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.form[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = (vm.form[parts[0]]) ? vm.form[parts[0]][parts[1]] : '';
                        } else if (parts.length === 3) {
                            compStr = (vm.form[parts[0]][parts[1]]) ? vm.form[parts[0]][parts[1]][parts[2]] : '';
                        } else if (parts.length === 4) {
                            compStr = (vm.form[parts[0]][parts[1]][parts[2]]) ? vm.form[parts[0]][parts[1]][parts[2]][parts[3]] : '';
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('input').on('ifChecked', function(event) {
                    vm.pageIsDirty = true;
                    $scope.$apply();
                });
                jQuery('input').on('ifUnchecked', function(event) {
                    vm.pageIsDirty = true;
                    $scope.$apply();
                });
                document.getElementById('scrollContainer').scrollTo(0, 0);
                $scope.$apply();
            }, 10);
        }

        vm.goToPage = function(page) {
            if (page > vm.showPage && vm.pageIsDirty) {
                vm.saveForm('nextPage');
                // vm.showPage = page;
                // updateLabels();
            } else {
                vm.showPage = page;
                updateLabels();
            }
        }

        vm.saveForm = function(actionAfterSave) {
            var root = firebase.database().ref();

            vm.actionAfterSave = actionAfterSave;

            if (!vm.form.data) {
                vm.form.data = {};
            }

            if (vm.form.data.recipient.siblingOne && vm.form.data.recipient.siblingOne.gender) {
                delete vm.form.data.recipient.siblingOne.gender[0].$$hashKey;
            }
            if (vm.form.data.recipient.siblingTwo && vm.form.data.recipient.siblingTwo.gender) {
                delete vm.form.data.recipient.siblingTwo.gender[0].$$hashKey;
            }
            if (vm.form.data.recipient.siblingThree && vm.form.data.recipient.siblingThree.gender) {
                delete vm.form.data.recipient.siblingThree.gender[0].$$hashKey;
            }
            if (vm.form.data.recipient.siblingFour && vm.form.data.recipient.siblingFour.gender) {
                delete vm.form.data.recipient.siblingFour.gender[0].$$hashKey;
            }
            if (vm.form.data.recipient.spouse && vm.form.data.recipient.spouse.gender) {
                delete vm.form.data.recipient.spouse.gender[0].$$hashKey;
            }

            vm.form.$save().then(function(ref) {
                sweetAlert.swal({
                    title: "Saved!",
                    text: "",
                    type: "success",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "OK",
                    timer: 1000
                }).then(function(result) {
                    vm.pageIsDirty = false;
                    if (vm.actionAfterSave === 'nextPage') {
                        vm.showPage++;
                    }
                    updateLabels();
                });
            }).catch(function(err) {
                if (err) {
                    console.log(err.message);
                }
            });
        }

        vm.submitForm = function() {
            // TODO: Validate any required fields

            vm.submittingForm = true;

            vm.form.$save().then(function(ref) {
                // Push notification to alerts node
                var root = firebase.database().ref();
                root.child('alerts').child(vm.form.alertId).push({
                    policyNumber: vm.form.data.policyNumber,
                    createdAt: firebase.database.ServerValue.TIMESTAMP,
                    message: 'Policy ' + vm.form.data.policyNumber + ' medical questionnaire has been completed by a third party user',
                    publicGuid: vm.formPublicGuid,
                    type: 'thirdPartyComplete'
                }).then(function(ref) {
                    $state.go('thank-you');
                }).catch(function(err) {
                    sweetAlert.swal({
                        title: 'Error',
                        text: err.message,
                        type: 'error'
                    }).then(function(result) {
                        vm.submittingForm = false;
                    });
                });
            }).catch(function(err) {
                sweetAlert.swal({
                    title: 'Error',
                    text: err.message,
                    type: 'error'
                }).then(function(result) {
                    vm.submittingForm = false;
                });
            });
        }
    }
    angular.module('yapp')
        .controller('PublicMedicalQCtrl', PublicMedicalQCtrl);
})();