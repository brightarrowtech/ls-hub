(function() {
    'use strict';

    /* @ngInject */
    function SwitchCtrl(VER, ENV, $scope, $state, $stateParams, sweetAlert, brandingDomain, authService, toastr) {
        var vm = this;
        vm.showContent = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.version = VER;

        vm.environment = '';
        if (ENV === 'demo') {
            vm.environment = ' - Demo';
        } else if (ENV === 'dev') {
            vm.environment = ' - Dev';
        }

        if ($stateParams.tk) {
            setTimeout(function() {
                authService.loginWithToken($stateParams.tk).then(function(authUser) {
                    $state.go('inventory');
                }).catch(function(err) {
                    console.log(err);
                });
            }, 1000);
        } else {
            setTimeout(function() {
                vm.authError = true;
                toastr['error']('Missing auth token');
                $scope.$apply();
            }, 2000);
        }
    }
    angular.module('yapp')
        .controller('SwitchCtrl', SwitchCtrl);
})();