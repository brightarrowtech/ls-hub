(function() {
    'use strict';

    /* @ngInject */
    function CalculatorCtrl(ENV, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, pandadocService, currentAuth, sweetAlert, apiService) {
        var vm = this;
        vm.showPage = 1;
        vm.estimateLow = 0;
        vm.estimateHigh = 0;

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.healthStatusOptions = ['Good', 'Slightly Bad', 'Bad', 'Very Bad', 'Terminal'];

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }
            loadPage();
        });

        function loadPage() {
            updateLabels();
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                // Possibly prompt here to create a policy based on provided information?
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        $state.go(route);
                    } else {
                        $state.go(route);
                    }
                });
            } else {
                $state.go(route);
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.calculator .page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.calculator .placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.calculator .pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                $scope.$apply();
            }, 10);
        }

        var EstimatePayout = {

            resultArr: [
                //Better/Same - Little Worse - Worse - Much Worse - Terminal
                [0, 0, 10, 25, 50], // 70 or Less
                [0, 5, 15, 25, 50], // 71 - 75
                [0, 5, 15, 30, 50], // 76 - 79
                [0, 5, 20, 30, 50], // 80
                [0, 5, 20, 35, 50], // 81 - 84
                [5, 15, 25, 40, 50], // 85 - 89
                [10, 20, 25, 35, 35], // 90
                [10, 20, 30, 35, 35], // 91 - 94
                [10, 20, 25, 30, 30], // 95 oe more                    
            ],

            yearBirth: '',
            healthChange: '',
            ageStage: 0,
            offerStage: 0,

            getAge: function(yearBirth) {
                var currentTime = new Date();
                var yearCurrent = currentTime.getFullYear();
                return yearCurrent - yearBirth;
            },

            changeAgeStage: function(yearBirth) {

                var age = this.getAge(yearBirth);

                if (age <= 70) { // 70 or Less
                    this.ageStage = 0;
                } else if (age >= 71 && age <= 75) { // 71 - 75
                    this.ageStage = 1;
                } else if (age >= 76 && age <= 79) { // 76 - 79
                    this.ageStage = 2;
                } else if (age == 80) { // 80
                    this.ageStage = 3;
                } else if (age >= 81 && age <= 84) { // 81 - 84
                    this.ageStage = 4;
                } else if (age >= 85 && age <= 89) { // 85 - 89
                    this.ageStage = 5;
                } else if (age == 90) { // 90
                    this.ageStage = 6;
                } else if (age >= 91 && age <= 94) { // 91 - 94
                    this.ageStage = 7;
                } else if (age >= 95) { // 95 or more
                    this.ageStage = 8;
                } else {
                    this.ageStage = 0;
                }
            },

            changeOfferStage: function(healthChange) {
                switch (healthChange) {
                    case 'Good':
                        this.offerStage = 0;
                        break;
                    case 'Slightly Bad':
                        this.offerStage = 1;
                        break;
                    case 'Bad':
                        this.offerStage = 2;
                        break;
                    case 'Very Bad':
                        this.offerStage = 3;
                        break;
                    case 'Terminal':
                        this.offerStage = 4;
                        break;
                    default:
                        this.offerStage = 0;
                }
            },

            updateEstimate: function() {
                this.yearBirth = vm.form.yearBorn;
                this.healthChange = vm.form.healthStatus;
                this.changeAgeStage(this.yearBirth);
                this.changeOfferStage(this.healthChange);
                this.calcOffer();
            },

            calcOffer: function() {
                var res = this.resultArr[this.ageStage][this.offerStage];
                var benefit = vm.form.faceValue;
                var offerResult = 0;

                var estimate = parseInt(benefit * res) / 100;
                vm.estimateLow = estimate * .5;
                vm.estimateHigh = estimate * .85;
            }
        }

        vm.calculateEstimate = function() {
            if (!vm.form || !vm.form.faceValue || !vm.form.yearBorn || !vm.form.healthStatus) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "All fields are required to run an estimate",
                    type: "error"
                }).then(function() {});
            } else {
                vm.calculatingEstimate = true;
                setTimeout(function() {
                    EstimatePayout.updateEstimate();
                    vm.calculatingEstimate = false;
                    vm.showPage = 2;
                    $scope.$apply();
                }, 2000);
            }
        }
    }

    angular.module('yapp')
        .controller('CalculatorCtrl', CalculatorCtrl);
})();