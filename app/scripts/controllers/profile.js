(function() {
    'use strict';

    /* @ngInject */
    function ProfileCtrl($window, notifyService, stdData, $fancyModal, zohoService, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain, sharedService) {
        var vm = this;
        vm.pageIsDirty = false;
        vm.form = {};
        var root = firebase.database().ref();
        vm.checkingOrg = true;
        vm.myOrgUplineName = 'n/a';
        vm.isBdn = false;
        vm.seatOrgLevels = [];

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        vm.invitationLinkRoot = 'https://' + vm.subdomain + '.' + vm.domain + '.' + vm.tld;
        // if (ENV === 'demo') {
        //     vm.invitationLinkRoot += ':3000';
        // }

        vm.yesNo = stdData.yesNo;
        vm.states = stdData.states;
        if (vm.platform === 'isubmit') {
            vm.roles = stdData.isubmitRoles;
        } else {
            vm.roles = stdData.imanagerRoles;
        }
        vm.daySelect = ['Sun', 'Mon', 'Tue', 'Wed', 'Thurs', 'Fri', 'Sat'];

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else if (!vm.currentUser.profile.isAuthorized) {
                $state.go('not-authorized');
            } else {
                if (!vm.currentUser.profile.viewedProfile) {
                    root.child('users/' + vm.currentUser.auth.uid + '/viewedProfile').set(true);
                }
                if (!vm.currentUser.profile.notificationEmails) {
                    root.child('users/' + vm.currentUser.auth.uid).update({
                        sendAllNotificationEmails: false,
                        notificationEmails: {
                            allEnabled: false,
                            emailAddress: vm.currentUser.auth.email
                        }
                    });
                }
                if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                    vm.isAdmin = true;
                }
                vm.userOrgType = authService.determineUserOrgType(vm.currentUser.profile.role);
                if (vm.currentUser.profile.myUpline) {
                    vm.myUplineOptions = [];
                    vm.myUplineDisplay = [];
                    $window.async.forEachOfSeries(vm.currentUser.profile.myUpline, function(upline, key, callback) {
                        // Get details for each upline Org
                        root.child('orgs/' + upline.orgType).child(upline.orgKey).once('value', function(snap) {
                            if (snap.exists()) {
                                var org = snap.val();
                                upline.email = org.email;
                                upline.primaryContact = org.primaryContact;
                                upline.phone = org.phone;
                                upline.fax = org.fax;
                                vm.myUplineDisplay.push({
                                    isDefault: upline.isDefault,
                                    name: org.name,
                                    orgKey: upline.orgKey,
                                    email: org.email,
                                    primaryContact: org.primaryContact,
                                    phone: org.phone,
                                    fax: org.fax
                                });
                                if (org.name !== 'Atlas Life Settlements') {
                                    vm.myUplineOptions.push({
                                        isDefault: upline.isDefault,
                                        name: org.name,
                                        orgKey: upline.orgKey,
                                        email: org.email,
                                        primaryContact: org.primaryContact,
                                        phone: org.phone,
                                        fax: org.fax
                                    });
                                }
                            }
                            callback();
                        }).catch(function(err) {
                            callback(err);
                        });
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            loadPage();
                        }
                    });
                } else {
                    loadPage();
                }
            }
        });

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function loadPage() {
            if (vm.currentUser.profile.myOrg) {
                vm.company = vm.currentUser.profile.company;
                vm.seatOrgLevels.push({
                    orgKey: vm.currentUser.profile.myOrg.orgKey,
                    company: vm.currentUser.profile.company
                });
                // If "hostedPage" is in querystring, this is a redirect from Zoho.
                if ($state.params.hostedpage_id) {
                    // Step 4: Get the Hosted Page details to retrieve the Subscription ID
                    var hostedPageId = $state.params.hostedpage_id;
                    zohoService.getHostedPageDetails(hostedPageId).then(function(response) {
                        if (response.status === 'success') {
                            // store response.data in Firebase for this Org
                            root.child('orgs').child(vm.currentUser.profile.myOrg.orgKey).child('subscription').update(response.data).then(function(ref) {
                                // store subscription status in orgs/public so downline can validate
                                root.child('orgs/public').child(vm.currentUser.profile.myOrg.orgKey).child('status').set(response.data.status).then(function(ref) {
                                    continuePageLoad();
                                });
                            });
                        } else {
                            console.log(response);
                            continuePageLoad();
                        }
                    });
                } else {
                    continuePageLoad();
                }
            } else {
                continuePageLoad();
            }
        }

        function continuePageLoad() {
            if (vm.currentUser.org) {
                vm.myOrg = vm.currentUser.org;
                // Get seat info for My Org
                root.child('orgs/seats').child(vm.currentUser.profile.myOrg.orgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        vm.myOrgSeats = snap.val();
                        vm.orgSeatsArr = [];
                        Object.keys(vm.myOrgSeats).forEach(function(key) {
                            vm.orgSeatsArr.push(vm.myOrgSeats[key]);
                        });
                    }
                    if (vm.userOrgType !== 'buyer' && vm.userOrgType !== 'broker') {
                        getPublicOrg();
                    } else {
                        allDone();
                    }
                });
            } else {
                getPublicOrg();
            }
        }

        function getPublicOrg() {
            if (vm.currentUser.profile.myOrg) {
                root.child('orgs/public').child(vm.currentUser.profile.myOrg.orgKey).once('value', function(snap) {
                    if (snap.exists()) {
                        vm.myOrgPublicData = snap.val();
                        if (vm.myOrgPublicData.uplineOrgKey) {
                            root.child('orgs/public').child(vm.myOrgPublicData.uplineOrgKey).once('value', function(snap) {
                                if (snap.exists()) {
                                    vm.myOrgUplineName = snap.val().name;
                                }
                                allDone()
                            });
                        } else {
                            allDone()
                        }
                    } else {
                        allDone()
                    }
                });
            } else {
                allDone()
            }
        }

        function properCase (str) {
            if ((str===null) || (str===''))
                 return false;
            else
             str = str.toString();
          
           return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        }

        function allDone() {
            jQuery('body').off('ifChecked', 'input');
            jQuery('body').off('ifUnchecked', 'input');
            updateLabels();
            if (!vm.currentUser.profile.myOrg && vm.userOrgType === 'buyer') {
                vm.createBuyerOrg();
            } else if (vm.currentUser.profile.myOrg && vm.currentUser.profile.myOrg.orgStatus === 'pending' && (vm.userOrgType === 'buyer')) {
                // Need to set seat and public org info
                var updates = {};
                updates['orgs/seats/' + vm.currentUser.profile.myOrg.orgKey + '/' + vm.currentUser.auth.uid] = {
                    userRef: vm.currentUser.auth.uid,
                    displayName: vm.currentUser.profile.displayName,
                    joinedOn: firebase.database.ServerValue.TIMESTAMP,
                    email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                    company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                    role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                    phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                    isOwner: true
                };
                updates['orgs/public/' + vm.currentUser.profile.myOrg.orgKey] = {
                    name: vm.currentUser.org.name,
                    seats: 99999,
                    seatsRemaining: 99999,
                    uplineOrgKey: null,
                    status: 'active',
                    orgType: vm.currentUser.org.orgType
                };
                updates['users/' + vm.currentUser.auth.uid + '/myOrg/orgStatus'] = null;
                root.update(updates).then(() => {
                    vm.addNewOrg = false;
                    vm.addingNewOrg = false;
                    vm.toast('success', 'Your new Organization is set up and ready to go. Please take a moment to finish filling our your Profile\'s information. Enjoy!');
                    if (!vm.myOrgSeats) {
                        vm.myOrgSeats = {};
                        vm.orgSeatsArr = [];
                    }
                    vm.myOrgSeats[vm.currentUser.auth.uid] = {
                        userRef: vm.currentUser.auth.uid,
                        displayName: vm.currentUser.profile.displayName,
                        joinedOn: moment().unix() * 1000,
                        email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                        company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                        role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                        phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                        isOwner: true
                    };
                    vm.orgSeatsArr.push({
                        userRef: vm.currentUser.auth.uid,
                        displayName: vm.currentUser.profile.displayName,
                        joinedOn: moment().unix() * 1000,
                        email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                        company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                        role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                        phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                        isOwner: true
                    });
                    vm.checkingOrg = false;
                    //vm.myOrg = ;
                    vm.copyInfo('myInfo');
                    vm.editMyInfo = true;
                    $scope.$apply();
                });
            } else {
                vm.checkingOrg = false;
                if ($state.params.init && vm.platform === 'isubmit') {
                    vm.copyInfo('myInfo');
                    vm.showTutorial();
                    vm.editMyInfo = true;
                }
                $scope.$apply();
            }
        }

        vm.onSelectCallback = function(whichSelect) {
            vm.currentUser.profile[whichSelect] = properCase(document.querySelector('#' + whichSelect + '-select .ui-select-match-text').innerText);
        }

        vm.refreshDownline = function() {
            var data = {};
            authService.refreshDownlineUsers(data).then(function(response) {
                vm.toast('success', response);
            });
        }

        vm.setSort = function(col) {
            vm.sortCol = col; 
            vm.sortReverse = !vm.sortReverse;
        }

        vm.showTutorial = function() {
            $fancyModal.open({
                templateUrl: '../../views/modals/welcome.html',
                themeClass: 'welcomeModal',
                controller: ['$scope', function($scope) {
                    $scope.step = 1;
                    $scope.substep = 'a';
                    $scope.close = function() {
                        $fancyModal.close();
                    }
                    $scope.goToPrev = function() {
                        $scope.step = $scope.step - 1;
                    };
                    $scope.goToNext = function() {
                        $scope.step = $scope.step + 1;
                    };
                }]
            });
        }

        vm.confirmReferral = function(ref) {
            sweetAlert.swal({
                title: "Confirm",
                text: "Are you sure you want to make this Member an Admin in your Organization? The Member will have access to all policies within your Organization, and be able to edit your Organization's details.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    var referralKey = ref.key;
                    var updates = {};
                    updates['orgs/' + vm.myOrg.orgType + '/' + vm.myOrg.$id + '/' + referralKey] = 'admin';
                    updates['orgs/seats/' + vm.myOrg.$id + '/' + referralKey] = {
                        joinedOn: firebase.database.ServerValue.TIMESTAMP,
                        userRef: referralKey,
                        displayName: ref.name,
                        email: ref.email,
                        isAdmin: true
                    };
                    updates['orgs/' + vm.myOrg.orgType + '/' + vm.myOrg.$id + '/pendingReferrals/' + referralKey] = null;
                    root.update(updates).then(function() {
                        vm.myOrgSeats[referralKey] = {
                            joinedOn: moment().unix() * 1000,
                            userRef: referralKey,
                            displayName: ref.name,
                            email: ref.email,
                            isAdmin: true
                        };
                        sweetAlert.swal({
                            title: "Success",
                            text: ref.name + " has been added to your Organization as an Admin",
                            type: "success",
                            timer: 1000
                        }).then(function() {
                            $scope.$apply();
                        });
                    });
                }
            });
        }

        vm.toggleUserNotificationEmails = function() {
            vm.showUserNotificationEmailDetails = (vm.currentUser.profile.notificationEmails && vm.currentUser.profile.notificationEmails.allEnabled) ? true : false;
            root.child('users/' + vm.currentUser.auth.uid).update({
                notificationEmails: {
                    allEnabled: vm.showUserNotificationEmailDetails,
                    emailAddress: (vm.currentUser.profile.notificationEmails && vm.currentUser.profile.notificationEmails.emailAddress) ? vm.currentUser.profile.notificationEmails.emailAddress : vm.currentUser.auth.email
                }
            }).then(() => {
                vm.currentUser.profile.notificationEmailAddress = (vm.currentUser.profile.notificationEmails && vm.currentUser.profile.notificationEmails.emailAddress) ? vm.currentUser.profile.notificationEmails.emailAddress : vm.currentUser.auth.email;
                var message = vm.showUserNotificationEmailDetails ? ' Enabled' : ' Disabled';
                vm.toast('success', 'All Personal Notification Emails have been ' + message);
                $scope.$apply();
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.saveUserNotificationEmailAddress = function() {
            root.child('users/' + vm.currentUser.auth.uid + '/notificationEmails').update({
                emailAddress: vm.currentUser.profile.notificationEmails.emailAddress
            }).then(() => {
                vm.toast('success', 'Personal Notification Email Address Updated');
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.toggleOrgNotificationEmails = function() {
            vm.showOrgNotificationEmailDetails = (vm.myOrg.notificationEmails && vm.myOrg.notificationEmails.allEnabled) ? true : false;
            root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey).update({
                notificationEmails: {
                    allEnabled: vm.showOrgNotificationEmailDetails,
                    emailAddress: (vm.myOrg.notificationEmails && vm.myOrg.notificationEmails.emailAddress) ? vm.myOrg.notificationEmails.emailAddress : vm.currentUser.auth.email
                }
            }).then(() => {
                vm.myOrg.notificationEmailAddress = (vm.myOrg.notificationEmails && vm.myOrg.notificationEmails.emailAddress) ? vm.myOrg.notificationEmails.emailAddress : vm.currentUser.auth.email;
                var message = vm.showOrgNotificationEmailDetails ? ' Enabled' : ' Disabled';
                vm.toast('success', 'All Org-Level Notification Emails have been ' + message);
                $scope.$apply();
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.saveOrgNotificationEmailAddress = function() {
            root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey + '/notificationEmails').update({
                emailAddress: vm.myOrg.notificationEmails.emailAddress
            }).then(() => {
                vm.toast('success', 'Org-Level Notification Email Address Updated');
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.toggleBackOfficeNotificationEmails = function() {
            vm.showBackOfficeNotificationEmailDetails = (vm.myOrg.backOfficeNotificationEmails && vm.myOrg.backOfficeNotificationEmails.allEnabled) ? true : false;
            root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey).update({
                backOfficeNotificationEmails: {
                    allEnabled: vm.showBackOfficeNotificationEmailDetails,
                    emailAddress: (vm.myOrg.backOfficeNotificationEmails && vm.myOrg.backOfficeNotificationEmails.emailAddress) ? vm.myOrg.backOfficeNotificationEmails.emailAddress : vm.currentUser.auth.email
                }
            }).then(() => {
                vm.myOrg.backOfficeNotificationEmailAddress = (vm.myOrg.backOfficeNotificationEmails && vm.myOrg.backOfficeNotificationEmails.emailAddress) ? vm.myOrg.backOfficeNotificationEmails.emailAddress : vm.currentUser.auth.email;
                var message = vm.showBackOfficeNotificationEmailDetails ? ' Enabled' : ' Disabled';
                vm.toast('success', 'All Back Office Notification Emails have been ' + message);
                $scope.$apply();
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.saveBackOfficeNotificationEmailAddress = function() {
            root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey + '/backOfficeNotificationEmails').update({
                emailAddress: vm.myOrg.backOfficeNotificationEmails.emailAddress
            }).then(() => {
                vm.toast('success', 'Back Office Notification Email Address Updated');
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.toggleDigestEmails = function() {
            vm.showDigestEmailDetails = (vm.currentUser.profile.digestEmail && vm.currentUser.profile.digestEmail.subscribed) ? true : false;
            if (vm.currentUser.profile.digestEmail && !vm.currentUser.profile.digestEmail.dayOfWeek) {
                vm.currentUser.profile.digestEmail.dayOfWeek = ['Mon'];
                var today = new Date();
                vm.currentUser.profile.digestEmail.nextRunDate = new Date();
                vm.currentUser.profile.digestEmail.nextRunDate.setDate(today.getDate() + (7 + vm.daySelect.indexOf(vm.currentUser.profile.digestEmail.dayOfWeek[0]) - today.getDay() - 1) % 7 + 1);
            }
            root.child('users/' + vm.currentUser.auth.uid).update({
                hasDigestEmail: vm.showDigestEmailDetails,
                digestEmail: {
                    subscribed: vm.showDigestEmailDetails,
                    emailAddress: (vm.currentUser.profile.digestEmail && vm.currentUser.profile.digestEmail.emailAddress) ? vm.currentUser.profile.digestEmail.emailAddress : vm.currentUser.auth.email,
                    nextRunDate: vm.currentUser.profile.digestEmail.nextRunDate,
                    dayOfWeek: vm.currentUser.profile.digestEmail.dayOfWeek
                }
            }).then(() => {
                vm.currentUser.profile.digestEmailAddress = (vm.currentUser.profile.digestEmail && vm.currentUser.profile.digestEmail.emailAddress) ? vm.currentUser.profile.digestEmail.emailAddress : vm.currentUser.auth.email;
                var message = vm.showDigestEmailDetails ? ' Enabled' : ' Disabled';
                vm.toast('success', 'Digest Emails have been ' + message);
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.saveDigestEmailAddress = function() {
            root.child('users/' + vm.currentUser.auth.uid + '/digestEmail').update({
                emailAddress: vm.currentUser.profile.digestEmail.emailAddress
            }).then(() => {
                vm.toast('success', 'Digest Email Address Updated');
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        vm.markViewedAgreement = function() {
            vm.myOrg.viewedTermsAt = moment().unix() * 1000;
            root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey).update({
                viewedTermsAt: firebase.database.ServerValue.TIMESTAMP
            }).then(() => {
                vm.toast('success', 'Broker Authorization Agreement has been viewed');
            }).catch(function(err) {
                console.log(err);
                vm.toast('error', err.message);
            });
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('body').on('ifChecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'uplineTermsExisting') {
                        vm.myOrg.termsAgreedAt = moment().unix() * 1000;
                        vm.myOrg.agreedToTerms = true;
                        // Update user's org to reflect agreement to the Broker Authorization Agreement
                        root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey).update({
                            termsAgreedAt: firebase.database.ServerValue.TIMESTAMP,
                            agreedToTerms: true
                        }).then(() => {
                            vm.toast('success', 'Thank you! You\'re all set.');
                        }).catch(function(err) {
                            console.log(err);
                            vm.toast('error', err.message);
                        });
                    } else if (event.target.name === 'dayOfWeek') {
                        // Calculate next run date...
                        var today = new Date();
                        var resultDate = new Date();
                        resultDate.setDate(today.getDate() + (7 + vm.daySelect.indexOf(vm.currentUser.profile.digestEmail.dayOfWeek[0]) - today.getDay() - 1) % 7 + 1);
                        root.child('users/' + vm.currentUser.auth.uid + '/digestEmail').update({
                            dayOfWeek: vm.currentUser.profile.digestEmail.dayOfWeek,
                            nextRunDate: resultDate
                        }).then(() => {
                            vm.toast('success', 'Digest Email day of week updated');
                        }).catch(function(err) {
                            console.log(err);
                            vm.toast('error', err.message);
                        });
                    }
                    $scope.$apply();
                });
                jQuery('body').on('ifUnchecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    $scope.$apply();
                });
                $scope.$apply();
            }, 10);
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        if (route === 'policy') {
                            $state.go('policy', { pid: vm.policyKey });
                        } else {
                            $state.go(route);
                        }
                    } else {
                        //vm.savePolicy('close');
                    }
                });
            } else {
                if (route === 'policy') {
                    $state.go('policy', { pid: vm.policyKey });
                } else {
                    $state.go(route);
                }
            }
        }

        vm.copyInfo = function(what) {
            if (what === 'myInfo') {
                vm.updatedInfo = {};
                vm.updatedInfo.company = sharedService.cloneObject(vm.currentUser.profile.company);
                vm.updatedInfo.email = sharedService.cloneObject(vm.currentUser.profile.email);
                vm.updatedInfo.phone = sharedService.cloneObject(vm.currentUser.profile.phone);
                vm.updatedInfo.fax = sharedService.cloneObject(vm.currentUser.profile.fax);
                vm.updatedInfo.role = sharedService.cloneObject(vm.currentUser.profile.role);
                vm.updatedInfo.address = sharedService.cloneObject(vm.currentUser.profile.address);
            } else if (what === 'myOrg') {
                vm.updatedInfo = {};
                vm.updatedInfo.primaryContact = sharedService.cloneObject(vm.myOrg.primaryContact);
                vm.updatedInfo.phone = sharedService.cloneObject(vm.myOrg.phone);
                vm.updatedInfo.fax = sharedService.cloneObject(vm.myOrg.fax);
                vm.updatedInfo.email = sharedService.cloneObject(vm.myOrg.email);
            }
        }

        vm.saveUpdatedInfo = function(what) {
            var atomicUpdate = {};
            if (what === 'myInfo') {
                if (vm.updatedInfo.email !== vm.currentUser.profile.email) {
                    authService.changeEmail(vm.updatedInfo.email).then(function() {
                        continueSavePersonalInfo();
                    }).catch(function(err) {
                        vm.toast('error', err.message);
                    });
                } else {
                    continueSavePersonalInfo();
                }
            } else if (what === 'myOrg') {
                vm.myOrg.primaryContact = sharedService.cloneObject(vm.updatedInfo.primaryContact);
                vm.myOrg.phone = sharedService.cloneObject(vm.updatedInfo.phone);
                vm.myOrg.fax = sharedService.cloneObject(vm.updatedInfo.fax);
                vm.myOrg.email = sharedService.cloneObject(vm.updatedInfo.email);
                vm.updatedInfo = {};
                root.child('orgs/' + vm.currentUser.profile.myOrg.orgType + '/' + vm.currentUser.profile.myOrg.orgKey).update({
                    primaryContact: (vm.myOrg.primaryContact) ? vm.myOrg.primaryContact : '',
                    phone: (vm.myOrg.phone) ? vm.myOrg.phone : '',
                    fax: (vm.myOrg.fax) ? vm.myOrg.fax : '',
                    email: (vm.myOrg.email) ? vm.myOrg.email : ''
                }).then(function(ref) {
                    vm.editMyInfo = false;
                    vm.editMyOrg = false;
                    sweetAlert.swal({
                        title: "Success",
                        text: "Your info was successfully updated!",
                        type: "success",
                        timer: 1000
                    }).then(function() {
                        $state.go('profile', { init: null });
                    });
                });
            }
        }

        function continueSavePersonalInfo() {
            var atomicUpdate = {};
            vm.currentUser.profile.company = sharedService.cloneObject(vm.updatedInfo.company);
            vm.currentUser.profile.email = sharedService.cloneObject(vm.updatedInfo.email);
            vm.currentUser.profile.phone = sharedService.cloneObject(vm.updatedInfo.phone);
            vm.currentUser.profile.fax = sharedService.cloneObject(vm.updatedInfo.fax);
            vm.currentUser.profile.role = sharedService.cloneObject(vm.updatedInfo.role);
            vm.currentUser.profile.address = sharedService.cloneObject(vm.updatedInfo.address);
            vm.updatedInfo = {};
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/company'] = (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/email'] = (vm.currentUser.profile.email) ? vm.currentUser.profile.email : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/phone'] = (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/fax'] = (vm.currentUser.profile.fax) ? vm.currentUser.profile.fax : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/role'] = (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/address'] = (vm.currentUser.profile.address) ? vm.currentUser.profile.address : null;
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/streetAddress'] = (vm.currentUser.profile.address && vm.currentUser.profile.address.address) ? vm.currentUser.profile.address.address : '';
            atomicUpdate['users/' + vm.currentUser.auth.uid + '/cityStateZip'] = (vm.currentUser.profile.address && vm.currentUser.profile.address.address) ? vm.currentUser.profile.address.city + ', ' + vm.currentUser.profile.address.state + ' ' + vm.currentUser.profile.address.zip : '';
            // Update personal info for each upline seat
            if (vm.currentUser.profile.myUpline) {
                vm.currentUser.profile.myUpline.forEach(function(upline) {
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/company'] = (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/email'] = (vm.currentUser.profile.email) ? vm.currentUser.profile.email : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/phone'] = (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/fax'] = (vm.currentUser.profile.fax) ? vm.currentUser.profile.fax : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/role'] = (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/streetAddress'] = (vm.currentUser.profile.address && vm.currentUser.profile.address.address) ? vm.currentUser.profile.address.address : '';
                    atomicUpdate['orgs/seats/' + upline.orgKey + '/' + vm.currentUser.auth.uid + '/cityStateZip'] = (vm.currentUser.profile.address && vm.currentUser.profile.address.address) ? vm.currentUser.profile.address.city + ', ' + vm.currentUser.profile.address.state + ' ' + vm.currentUser.profile.address.zip : '';
                });
            }
            root.update(atomicUpdate).then(function(ref) {
                vm.editMyInfo = false;
                vm.editMyOrg = false;
                sweetAlert.swal({
                    title: "Success",
                    text: "Your info was successfully updated!",
                    type: "success",
                    timer: 1000
                }).then(function() {
                    $state.go('profile', { init: null });
                });
            });
        }

        vm.updateSeatNote = function(seat) {
            vm.myOrgSeats[seat.userRef].notes = sharedService.cloneObject(seat.newNote);
            root.child('orgs/seats').child(vm.currentUser.profile.myOrg.orgKey).child(seat.userRef).update({
                notes: seat.newNote
            }).then(function(ref) {
                $scope.$apply();
            });
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.startAddNewOrg = function() {
            // Copy "My Info" fields over to new Org
            vm.addNewOrg = true;
            vm.newOrg = {};
            vm.newOrg.orgType = 'supplier';
            vm.newOrg.name = sharedService.cloneObject(vm.currentUser.profile.company);
            vm.newOrg.primaryContact = sharedService.cloneObject(vm.currentUser.profile.displayName);
            vm.newOrg.phone = sharedService.cloneObject(vm.currentUser.profile.phone);
            vm.newOrg.email = sharedService.cloneObject(vm.currentUser.profile.email);
            vm.newOrg.alertId = sharedService.cloneObject(vm.currentUser.profile.alertId);
            vm.newOrg.fax = sharedService.cloneObject(vm.currentUser.profile.fax);
            vm.newOrg.plan = 'free';
            if (vm.myUplineOptions.length === 0) {
                vm.newOrg.uplineOrgKey = '-L8crXXb_c8NRSJUgRpn'; // Rapid Life Settlement
            } else if (vm.myUplineOptions.length === 1) {
                vm.newOrg.uplineOrgKey = vm.myUplineOptions[0].orgKey; // Only non-Atlas Upline
            }
            vm.addNewUpline = false;
            updateLabels();
        }

        vm.createBuyerOrg = function() {
            // Only proceed if the current user doesn't already have a "myOrg" node on their profile
            if (!vm.currentUser.profile.myOrg) {
                vm.newOrg = {
                    name: vm.currentUser.profile.company ? vm.currentUser.profile.company : vm.currentUser.profile.displayName,
                    email: vm.currentUser.profile.email,
                    primaryContact: vm.currentUser.profile.displayName,
                    phone: vm.currentUser.profile.phone,
                    orgGuid: uuidv4(),
                    status: 'active',
                    isApproved: true,
                    plan: 'free',
                    orgType: vm.userOrgType
                };
                vm.saveOrg();
            } else {
                vm.checkingOrg = false;
                $scope.$apply();
            }
        }

        vm.saveOrg = function() {
            // Validate form
            if (!vm.newOrg.name) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "New Organization name cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!vm.newOrg.email) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "New Organization email cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!vm.newOrg.primaryContact) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "New Organization contact cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!vm.newOrg.phone) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "New Organization phone cannot be blank",
                    type: "error"
                }).then(function() {});
            } else if (!vm.newOrg.uplineOrgKey && vm.userOrgType !== 'buyer') {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "You must select an Upline Organization",
                    type: "error"
                }).then(function() {});
            } else {
                if (!vm.newOrg.fax) {
                    vm.newOrg.fax = '';
                }
                vm.addingNewOrg = true;
                vm.newOrg.createdAt = firebase.database.ServerValue.TIMESTAMP;
                vm.newOrg.createdBy = vm.currentUser.profile.displayName;
                vm.newOrg.createdById = vm.currentUser.auth.uid;
                vm.newOrg[vm.currentUser.auth.uid] = 'admin';
                if (!vm.newOrg.status) {
                    vm.newOrg.status = 'active';
                }

                if (vm.newOrg.orgType === 'supplier') {
                    // Broker Authorization Agreement terms must have been accepted to get here...
                    vm.newOrg.termsAgreedAt = moment().unix() * 1000;
                    vm.newOrg.agreedToTerms = true;

                    // This is a Downline Org...
                    vm.newOrg.isApproved = true;

                    // Set newOrg uplineOrgKey 
                    if (vm.myUplineOptions.length === 1) {
                        vm.newOrg.uplineOrgKey = vm.myUplineOptions[0].orgKey;
                    } else {
                        vm.newOrg.uplineOrgKey = (vm.newOrg.uplineOrgKey.orgKey) ? vm.newOrg.uplineOrgKey.orgKey : vm.newOrg.uplineOrgKey;
                    }
                }

                var planCode = '';
                if (vm.newOrg.plan === 'basic' || vm.newOrg.plan === 'bronze') {
                    vm.newOrg.seats = 6;
                    vm.newOrg.seatsRemaining = 6;
                    planCode = 'LSH-B';
                } else if (vm.newOrg.plan === 'premium' || vm.newOrg.plan === 'silver') {
                    vm.newOrg.seats = 21;
                    vm.newOrg.seatsRemaining = 21;
                    planCode = 'LSH-P';
                } else if (vm.newOrg.plan === 'ultimate' || vm.newOrg.plan === 'gold' || vm.newOrg.plan === 'exclusive') {
                    vm.newOrg.seats = 99999;
                    vm.newOrg.seatsRemaining = 99999;
                    planCode = 'LSH-U';
                } else if (vm.newOrg.plan === 'imo') {
                    vm.newOrg.seats = 99999;
                    vm.newOrg.seatsRemaining = 99999;
                    planCode = 'IMO';
                } else if (vm.newOrg.plan === 'free') {
                    vm.newOrg.seats = 25;
                    vm.newOrg.seatsRemaining = 25;
                    planCode = 'FREE';
                }

                root.child('orgs/' + vm.newOrg.orgType).push(vm.newOrg).then((snap) => {
                    vm.newOrg.orgKey = snap.key;

                    var updates = {};
                    updates['orgs/seats/' + vm.newOrg.orgKey + '/' + vm.currentUser.auth.uid] = {
                        userRef: vm.currentUser.auth.uid,
                        displayName: vm.currentUser.profile.displayName,
                        joinedOn: firebase.database.ServerValue.TIMESTAMP,
                        email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                        company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                        role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                        phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                        isOwner: true
                    };
                    if (vm.newOrg.orgType === 'supplier') {
                        updates['orgs/seats/' + vm.newOrg.uplineOrgKey + '/' + vm.currentUser.auth.uid + '/hasOrg'] = true;
                        updates['orgs/seats/' + vm.newOrg.uplineOrgKey + '/' + vm.currentUser.auth.uid + '/orgName'] = vm.newOrg.name;
                        updates['orgs/seats/' + vm.newOrg.uplineOrgKey + '/' + vm.currentUser.auth.uid + '/orgType'] = 'supplier';
                        updates['orgs/seats/' + vm.newOrg.uplineOrgKey + '/' + vm.currentUser.auth.uid + '/orgKey'] = vm.newOrg.orgKey;
                    }
                    updates['orgs/public/' + vm.newOrg.orgKey] = {
                        name: vm.newOrg.name,
                        seats: vm.newOrg.seats,
                        seatsRemaining: vm.newOrg.seatsRemaining,
                        uplineOrgKey: (vm.newOrg.orgType === 'supplier') ? vm.newOrg.uplineOrgKey : null,
                        status: 'active',
                        orgType: vm.newOrg.orgType
                    };
                    updates['users/' + vm.currentUser.auth.uid + '/myOrg'] = {
                        orgKey: vm.newOrg.orgKey,
                        orgType: vm.newOrg.orgType,
                        uplineOrgKey: (vm.newOrg.orgType === 'supplier') ? vm.newOrg.uplineOrgKey : null
                    };
                    updates['orgs/' + vm.newOrg.orgType + '/' + vm.newOrg.orgKey + '/orgKey'] = vm.newOrg.orgKey;
                    root.update(updates).then(() => {
                        vm.addNewOrg = false;
                        vm.addingNewOrg = false;

                        var message = "Thank you! Your new Organization is set up and ready to go. Please take a moment to finish filling our your Profile's information. Enjoy!";
                        if (vm.newOrg.orgType === 'supplier' && ENV === 'prod') {
                            // SEND EMAIL TO ADMIN TO INITIATE ORG CONTRACT
                            var linkRoot = vm.subdomain + '.' + vm.domain + '.' + vm.tld;
                            var recipients = [];
                            recipients.push({
                                email: 'support@lshub.net',
                                template: 'new-isubmit-org-request',
                                subData: {
                                    name: vm.currentUser.profile.displayName,
                                    companyName: vm.newOrg.name,
                                    companyOrgKey: vm.newOrg.orgKey,
                                    companyPhone: vm.newOrg.phone,
                                    companyEmail: vm.newOrg.email
                                }
                            });
                            notifyService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                                if (response.status === 'success') {
                                    // Do nothing...
                                } else {
                                    vm.toast('error', 'Something went wrong...Please contact support@lshub.net directly');
                                }
                            });
                        }
                        vm.toast('success', message);
                        if (!vm.myOrgSeats) {
                            vm.myOrgSeats = {};
                            vm.orgSeatsArr = [];
                        }
                        vm.myOrgSeats[vm.currentUser.auth.uid] = {
                            userRef: vm.currentUser.auth.uid,
                            displayName: vm.currentUser.profile.displayName,
                            joinedOn: moment().unix() * 1000,
                            email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                            company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                            role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                            phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                            isOwner: true
                        };
                        vm.orgSeatsArr.push({
                            userRef: vm.currentUser.auth.uid,
                            displayName: vm.currentUser.profile.displayName,
                            joinedOn: moment().unix() * 1000,
                            email: (vm.currentUser.auth.email) ? vm.currentUser.auth.email : '',
                            company: (vm.currentUser.profile.company) ? vm.currentUser.profile.company : '',
                            role: (vm.currentUser.profile.role) ? vm.currentUser.profile.role : '',
                            phone: (vm.currentUser.profile.phone) ? vm.currentUser.profile.phone : '',
                            isOwner: true
                        });
                        vm.checkingOrg = false;
                        vm.myOrg = vm.newOrg;
                        if (vm.newOrg.orgType !== 'supplier') {
                            vm.copyInfo('myInfo');
                            vm.editMyInfo = true;
                            $scope.$apply();
                        } else {
                            getPublicOrg();
                        }
                    });
                });
            }
        }

        vm.showMessage = function(type, helpTitle, helpText) {
            if (type === 'Invite Members') {
                sweetAlert.swal({
                    title: "What are Organization Members?",
                    text: "With a paid subscription, you can add additional Members to your Organization. Depending on their access level, they can add or view inventory, offers, files, etc.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Enable Pack View') {
                sweetAlert.swal({
                    title: "What is Pack View?",
                    text: "With a paid subscription, you can add additional Members to your Organization. Depending on their access level, they can add or view inventory, offers, files, etc.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Enable User Notification Emails') {
                sweetAlert.swal({
                    title: "What are Personal Notification Emails?",
                    text: "Personal Notifications are sent to you directly. These would be for Policies that you created or were assigned to you and will include things like new file uploads, offers, or completed actions. Certain types of Notifications, including urgent action requests, will always generate an email, regardless of this setting.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Enable Org Notification Emails') {
                sweetAlert.swal({
                    title: "What are Org-Level Notification Emails?",
                    text: "Org-Level Notifications are sent to your Organization as part of the distribution hierarchy. These would be for Policies that you created by your downline and will include things like new file uploads, offers, or completed actions. Certain types of Notifications, including urgent action requests, will always generate an email, regardless of this setting.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Enable Back Office Notification Emails') {
                sweetAlert.swal({
                    title: "What are Back Office Notification Emails?",
                    text: "Back Office Notifications are sent to the Back Office provider for certain policy actions, specifically messages and policy status updates.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Enable Digest Email') {
                sweetAlert.swal({
                    title: "What are Digest Emails?",
                    text: "With a paid subscription, you can add additional Members to your Organization. Depending on their access level, they can add or view inventory, offers, files, etc.",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.removeUpline = function(index) {
            sweetAlert.swal({
                title: "Confirm",
                text: "Are you sure you want to unlink this Upline Organization?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes"
            }).then(
                function(result) {
                    if (!result.dismiss) {
                        var uplineOrgKey = vm.currentUser.profile.myUpline[index].orgKey;
                        // Give seat back to Org
                        root.child('orgs/public').child(uplineOrgKey).once('value', function(snap) {
                            if (snap.exists()) {
                                var seatsRemaining = snap.val().seatsRemaining;
                                snap.ref.update({
                                    seatsRemaining: seatsRemaining + 1
                                }).then(function(ref) {
                                    root.child('orgs/seats').child(uplineOrgKey).orderByChild('userRef').equalTo(vm.currentUser.auth.uid).once('value', function(snap) {
                                        if (snap.exists()) {
                                            var nodeKey = Object.keys(snap.val())[0];
                                            snap.ref.child(nodeKey).remove().then(function(ref) {
                                                vm.currentUser.profile.myUpline.splice(index, 1);
                                                vm.currentUser.profile.myUpline.forEach(function(upline) {
                                                    delete upline.$$hashKey;
                                                });
                                                root.child('users').child(vm.currentUser.auth.uid).child('myUpline').set(vm.currentUser.profile.myUpline).then((done) => {
                                                    sweetAlert.swal({
                                                        title: "Success",
                                                        text: "Organization was successfully unlinked",
                                                        type: "success",
                                                        timer: 1000
                                                    }).then(function() {
                                                        $scope.$apply();
                                                    });
                                                });
                                            });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
        }

        vm.drillToOrg = function(seat, direction) {
            if (direction === 'up') {
                vm.seatOrgLevels.pop();
            } else {
                vm.seatOrgLevels.push({
                    orgKey: seat.orgKey,
                    company: seat.company
                });
            }
            vm.company = seat.company;
            root.child('orgs/seats').child(seat.orgKey).once('value', function(snap) {
                if (snap.exists()) {
                    vm.myOrgSeats = snap.val();
                    vm.orgSeatsArr = [];
                    Object.keys(vm.myOrgSeats).forEach(function(key) {
                        vm.orgSeatsArr.push(vm.myOrgSeats[key]);
                    });
                    $scope.$apply();
                } else {
                    vm.myOrgSeats = {};
                    vm.orgSeatsArr = [];
                }
            });
        }

        vm.toggleAdmin = function(seat, index) {
            if (!seat.isAdmin) {
                sweetAlert.swal({
                    title: "Confirm",
                    text: "Are you sure you want to make this Member an Admin in your Organization? The Member will have access to all policies within your Organization, and be able to edit your Organization's details.",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonText: "Yes"
                }).then(
                    function(result) {
                        if (!result.dismiss) {
                            vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]].isAdmin = true;
                            root.child('orgs/seats').child(vm.currentUser.profile.myOrg.orgKey).update(vm.myOrgSeats).then(function(ref) {
                                root.child('orgs/' + vm.currentUser.profile.myOrg.orgType).child(vm.currentUser.profile.myOrg.orgKey).child(vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]].userRef).set('admin').then(function(ref) {
                                    vm.toast('success', 'Admin successfully added');
                                });
                            });
                        }
                    });
            } else if (vm.platform === 'isubmit') {
                sweetAlert.swal({
                    title: "Confirm",
                    text: "Are you sure you want to remove Admin rights for this Member?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#d9534f",
                    confirmButtonText: "Yes"
                }).then(
                    function(result) {
                        if (!result.dismiss) {
                            vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]].isAdmin = false;
                            root.child('orgs/seats').child(vm.currentUser.profile.myOrg.orgKey).update(vm.myOrgSeats).then(function(ref) {
                                root.child('orgs/' + vm.currentUser.profile.myOrg.orgType).child(vm.currentUser.profile.myOrg.orgKey).child(vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]].userRef).remove().then(function(ref) {
                                    vm.toast('success', 'Admin successfully removed');
                                });
                            });
                        }
                    });
            } else if (vm.platform === 'imanager') {
                sweetAlert.swal({
                    title: "Sorry",
                    text: "iManager currently only supports the admin role. If you wish to remove this member, click 'Remove this Member' below",
                    type: "info"
                }).then(
                    function(result) {});
            }
        }

        vm.removeSeatUser = function(seat, index) {
            sweetAlert.swal({
                title: "Confirm",
                text: "Are you sure you want to remove this Member?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#d9534f",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    var seatToRemove = vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]].userRef;
                    delete vm.myOrgSeats[Object.keys(vm.myOrgSeats)[index]];
                    root.child('orgs/seats').child(vm.currentUser.profile.myOrg.orgKey).child(seatToRemove).remove().then(function(ref) {
                        root.child('orgs/public').child(vm.currentUser.profile.myOrg.orgKey).once('value', function(snap) {
                            if (snap.exists()) {
                                var seatsRemaining = snap.val().seatsRemaining;
                                snap.ref.update({
                                    seatsRemaining: seatsRemaining + 1
                                }).then(function(ref) {
                                    vm.toast('success', 'Member successfully removed');
                                });
                            }
                        });
                    });
                }
            });
        }
    }
    angular.module('yapp')
        .controller('ProfileCtrl', ProfileCtrl);
})();