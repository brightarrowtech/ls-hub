(function() {
    'use strict';

    /* @ngInject */
    function InventoryCtrl(stdData, brandingDomain, ENV, $scope, $state, $stateParams, sweetAlert, sharedService, $firebaseArray, $firebaseObject, authService, policyService, currentAuth, $timeout, $window) {
        var vm = this;
        vm.policies = [];
        vm.available = [];
        vm.callCount = 0;
        vm.sortCol = 'displayStatus';
        vm.sortDir = 'asc';
        vm.searchResults = 0;
        vm.searchTerm = '';
        vm.activeAvailableCount = 0;
        vm.activePolicyCount = 0;
        vm.isBuyer = false;
        vm.isBackOffice = false;
        vm.showNotInterested = false;
        vm.showClosed = false;
        vm.fetchingPolicies = true;
        //vm.addToPack = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }
        vm.imanagerRoles = stdData.imanagerRoles;

        var root = firebase.database().ref();

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            vm.userRole = vm.currentUser.profile.role;
            if (vm.imanagerRoles.indexOf(vm.userRole) > -1) {
                vm.isBuyer = true;
            }
            if (vm.userRole === 'Back Office') {
                vm.isBackOffice = true;
            }

            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                if (vm.currentUser.profile.sortCol) {
                    vm.sortCol = vm.currentUser.profile.sortCol;
                }
                vm.sortReverse = (vm.currentUser.profile.sortReverse !== undefined) ? vm.currentUser.profile.sortReverse : true;
                vm.filtersEnabled = (vm.currentUser.profile.filtersEnabled !== undefined) ? vm.currentUser.profile.filtersEnabled : false;
                vm.numPerPageOpt = [10, 20, 50, 100];
                vm.numPerPage = vm.currentUser.profile.numPerPage ? vm.currentUser.profile.numPerPage : vm.numPerPageOpt[0];
                vm.currentPage = 1;
                var createdOnPlatform = vm.subdomain.indexOf('imanager') > -1 ? 'imanager' : 'isubmit';

                if (location.href.indexOf('#new') > -1) {
                    vm.activeTab = 1;
                } else if (location.href.indexOf('#active') > -1) {
                    vm.activeTab = 2;
                } else {
                    vm.activeTab = vm.isBuyer ? 1 : 3;
                }

                setFilters();

                vm.doSearch();

                if (vm.currentUser.profile.role === 'Admin') {
                    vm.isAdmin = true;
                }
                addNotificationIcons();
                wrapItUp();
            }
        });

        function addNotificationIcons() {
            vm.fetchingPolicies = false;
            // Query for user notifications
            root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.auth.uid).once('value', function(userNotifications) {
                if (userNotifications.exists()) {
                    $scope.$applyAsync(function() {
                        userNotifications.forEach(function(n) {
                            let notification = n.val();
                            var policyIdx = vm.policies.findIndex(x => x.policyKey === notification.policyKey);
                            if (policyIdx > -1) {
                                if (notification.isUnread) {
                                    if (vm.policies[policyIdx].hasNotification) {
                                        vm.policies[policyIdx].notificationCount++;
                                        vm.policies[policyIdx].notificationKeys.push({
                                            type: 'user',
                                            key: notification.notifyKey
                                        });
                                    } else {
                                        vm.policies[policyIdx].hasNotification = true;
                                        vm.policies[policyIdx].notificationCount = 1;
                                        vm.policies[policyIdx].notificationKeys = [];
                                        vm.policies[policyIdx].notificationKeys.push({
                                            type: 'user',
                                            key: notification.notifyKey
                                        });
                                    }
                                }
                            }
                        });
                    });
                }
            }).catch(function(err) {
                console.log('Error retrieving user-level notifications');
                console.log(err);
            });
        }

        function checkForBuyerNotifications() {
            // Query for Buyer notifications
            if (vm.currentUser.org && vm.currentUser.org.orgGuid) {
                // Get Buyer Notifications
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.orgGuid).once('value', function(orgNotifications) {
                    if (orgNotifications.exists()) {
                        orgNotifications.forEach(function(n) {
                            let notification = n.val();
                            var policyIdx = vm.available.findIndex(x => x.publicGuid === notification.pid);
                            if (policyIdx > -1) {
                                if (notification.isUnread) {
                                    var processNotification = true;
                                    if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                        processNotification = false;
                                    } else if (vm.currentUser.profile.role === 'Back Office' && notification.createdByRole !== 'iBroker') {
                                        processNotification = false;
                                    }
                                    if (processNotification) {
                                        if (vm.available[policyIdx].hasNotification) {
                                            vm.available[policyIdx].notificationCount++;
                                            vm.available[policyIdx].notificationKeys.push({
                                                type: notification.targetType,
                                                key: notification.notifyKey
                                            });
                                        } else {
                                            vm.available[policyIdx].hasNotification = true;
                                            vm.available[policyIdx].notificationCount = 1;
                                            vm.available[policyIdx].notificationKeys = [];
                                            vm.available[policyIdx].notificationKeys.push({
                                                type: notification.targetType,
                                                key: notification.notifyKey
                                            });
                                        }
                                    }
                                }
                            }
                        });
                        $scope.$apply();
                    } else {
                        $scope.$apply();
                    }
                }).catch(function(err) {
                    console.log('Error retrieving buyer-level notifications');
                    console.log(err);
                    $scope.$apply();
                });
            } else {
                $scope.$apply();
            }
        }

        function getOrgNotifications() {
            // Query for Org notifications
            if (vm.currentUser.org) {
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.$id).once('value', function(orgNotifications) {
                    if (orgNotifications.exists()) {
                        orgNotifications.forEach(function(n) {
                            let notification = n.val();
                            var policyIdx = vm.policies.findIndex(x => x.policyKey === notification.policyKey);
                            if (policyIdx > -1) {
                                if (notification.isUnread) {
                                    var processNotification = true;
                                    if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                        processNotification = false;
                                    } else if (vm.isBackOffice && notification.createdByRole !== 'iBroker') {
                                        processNotification = false;
                                    }
                                    if (processNotification) {
                                        if (vm.policies[policyIdx].hasNotification) {
                                            vm.policies[policyIdx].notificationCount++;
                                            vm.policies[policyIdx].notificationKeys.push({
                                                type: notification.targetType,
                                                key: notification.notifyKey
                                            });
                                        } else {
                                            vm.policies[policyIdx].hasNotification = true;
                                            vm.policies[policyIdx].notificationCount = 1;
                                            vm.policies[policyIdx].notificationKeys = [];
                                            vm.policies[policyIdx].notificationKeys.push({
                                                type: notification.targetType,
                                                key: notification.notifyKey
                                            });
                                        }
                                    }
                                }
                            }
                        });
                        $scope.$apply();
                    } else {
                        $scope.$apply();
                    }
                }).catch(function(err) {
                    console.log('Error retrieving org-level notifications');
                    console.log(err);
                    $scope.$apply();
                });
            } else {
                $scope.$apply();
            }
        }

        vm.switchTab = function(tab) {
            vm.fetchingPolicies = true;
            vm.activeTab = tab;
            vm.doSearch();
        }

        function wrapItUp() {
            $scope.$applyAsync(function() {
                setTimeout(function() {
                    addNotificationIcons();
                    checkForBuyerNotifications();
                    getOrgNotifications();
                }, 0);
            });
        }

        // Set initial Filter Values
        vm.ageOptions = {
            start: [0,120],
            connect: true,
            tooltips: [wNumb({ decimals: 0 }), wNumb({ decimals: 0 })],
            step: 1,
            behaviour: 'snap',
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': 0,
                'max': 120
            }
        };
        
        vm.premiumOptions = {
            start: [0,250],
            connect: true,
            tooltips: [wNumb({ decimals: 0, thousand: ',', postfix: 'k' }), wNumb({ decimals: 0, thousand: ',', postfix: 'k' })],
            format: wNumb({
                decimals: 0,
                thousand: ',',
                prefix: '$',
                encoder: function(value) {
                    return value * 1000;
                },
                decoder: function(value) {
                    return value / 1000;
                }
            }),
            behaviour: 'snap',
            range: {
                'min': 0,
                'max': 250
            }
        };
        
        vm.faceAmountOptions = {
            start: [0,10],
            connect: true,
            tooltips: [wNumb({ decimals: 1, thousand: ',', postfix: 'm' }), wNumb({ decimals: 1, thousand: ',', postfix: 'm' })],
            format: wNumb({
                decimals: 0,
                thousand: ',',
                prefix: '$',
                encoder: function(value) {
                    return value * 1000000;
                },
                decoder: function(value) {
                    return value / 1000000;
                }
            }),
            behaviour: 'snap',
            range: {
                'min': 0,
                'max': 10
            }
        };

        vm.ageEventHandlers = {
            end: function(values, handle, unencoded) {
                if (handle) {
                    vm.ageMax = values[handle];
                } else {
                    vm.ageMin = values[handle];
                }
                vm.doSearch();
            }
        }
        vm.premiumEventHandlers = {
            end: function(values, handle, unencoded) {
                if (handle) {
                    vm.premiumMax = values[handle];
                } else {
                    vm.premiumMin = values[handle];
                }
                vm.doSearch();
            }
        }
        vm.faceAmountEventHandlers = {
            end: function(values, handle, unencoded) {
                if (handle) {
                    vm.faceAmountMax = values[handle];
                } else {
                    vm.faceAmountMin = values[handle];
                }
                vm.doSearch();
            }
        }

        vm.setAge = function(which, skipSearch) {
            $scope.$applyAsync(function() {
                if (which === 'min' || which === 'both') {
                    vm.ageOptions.start[0] = parseInt(vm.ageMin);
                } 
                if (which === 'max' || which === 'both') {
                    vm.ageOptions.start[1] = parseInt(vm.ageMax);
                }
            });
            if (!skipSearch) {
                vm.doSearch();
            }
        }

        vm.setPremium = function(which, skipSearch) {
            $scope.$applyAsync(function() {
                var premiumNumFormat = wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: '$'
                });
                if ((which === 'min' || which === 'both')  && vm.premiumMin !== '' && vm.premiumMin !== '$') {
                    vm.premiumOptions.start[0] = parseInt(vm.premiumMin.replace(/[\$\,]/g,''));
                    vm.premiumMin = premiumNumFormat.to(parseInt(vm.premiumMin.replace(/[\$\,]/g,'')));
                } 
                if ((which === 'max' || which === 'both') && vm.premiumMax !== '' && vm.premiumMax !== '$') {
                    vm.premiumOptions.start[1] = parseInt(vm.premiumMax.replace(/[\$\,]/g,''));
                    vm.premiumMax = premiumNumFormat.to(parseInt(vm.premiumMax.replace(/[\$\,]/g,'')));
                }
            });
            if (!skipSearch) {
                vm.doSearch();
            }
        }

        vm.setFaceAmount = function(which, skipSearch) {
            $scope.$applyAsync(function() {
                var faceNumFormat = wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: '$'
                });
                if ((which === 'min' || which === 'both') && vm.faceAmountMin !== '' && vm.faceAmountMin !== '$') {
                    vm.faceAmountOptions.start[0] = parseInt(vm.faceAmountMin.replace(/[\$\,]/g,''));
                    vm.faceAmountMin = faceNumFormat.to(parseInt(vm.faceAmountMin.replace(/[\$\,]/g,'')));
                } 
                if ((which === 'max' || which === 'both') && vm.faceAmountMax !== '' && vm.faceAmountMax !== '$') {
                    vm.faceAmountOptions.start[1] = parseInt(vm.faceAmountMax.replace(/[\$\,]/g,''));
                    vm.faceAmountMax = faceNumFormat.to(parseInt(vm.faceAmountMax.replace(/[\$\,]/g,'')));
                }
            });
            if (!skipSearch) {
                vm.doSearch();
            }
        }

        function setFilters() {
            vm.resetFilters(true);
            // vm.ageOptions.start = [0, 120];
            // vm.premiumOptions.start = [0, 250000];
            // vm.faceAmountOptions.start = [0, 10000000];
            
            // Set saved Filter Values
            vm.ageMin = (vm.currentUser.profile.ageMin !== undefined) ? parseInt(vm.currentUser.profile.ageMin) : 0;
            vm.ageMax = (vm.currentUser.profile.ageMax !== undefined) ? parseInt(vm.currentUser.profile.ageMax) : 120;
            if (vm.currentUser.profile.ageOptions && vm.currentUser.profile.ageOptions.start !== undefined) {
                vm.setAge('both', true);
            } 

            vm.premiumMin = (vm.currentUser.profile.premiumMin !== undefined) ? vm.currentUser.profile.premiumMin : "$0";
            vm.premiuMax = (vm.currentUser.profile.premiumMax !== undefined) ? vm.currentUser.profile.premiumMax : "$250,000";
            if (vm.currentUser.profile.premiumOptions && vm.currentUser.profile.premiumOptions.start !== undefined) {
                vm.setPremium('both', true);
            }

            vm.faceAmountMin = (vm.currentUser.profile.faceAmountMin !== undefined) ? vm.currentUser.profile.faceAmountMin : "$0";
            vm.faceAmountMax = (vm.currentUser.profile.faceAmountMax !== undefined) ? vm.currentUser.profile.faceAmountMax : "$10,000,000";
            if (vm.currentUser.profile.faceAmountOptions && vm.currentUser.profile.faceAmountOptions.start !== undefined) {
                vm.setFaceAmount('both', true);
            }

            vm.showNotInterested = (vm.currentUser.profile.showNotInterested !== undefined) ? vm.currentUser.profile.showNotInterested : false;
            vm.showClosed = (vm.currentUser.profile.showClosed !== undefined) ? vm.currentUser.profile.showClosed : false;

            if ($stateParams.ageMin) {
                vm.ageMin = parseInt($stateParams.ageMin);
                vm.ageOptions.start[0] = parseInt($stateParams.ageMin);
            }
            if ($stateParams.ageMax) {
                vm.ageMax = parseInt($stateParams.ageMax);
                vm.ageOptions.start[1] = parseInt($stateParams.ageMax);
            }
            if ($stateParams.premiumMin) {
                vm.premiumMin = parseInt($stateParams.premiumMin) / 1000;
                vm.premiumOptions.start[0] = parseInt($stateParams.premiumMin) / 1000;
            }
            if ($stateParams.premiumMax) {
                vm.premiumMax = parseInt($stateParams.premiumMax) / 1000;
                vm.premiumOptions.start[1] = parseInt($stateParams.premiumMax) / 1000;
            }
            if ($stateParams.faceMin) {
                vm.faceAmountMin = parseInt($stateParams.faceMin) / 1000000;
                vm.faceAmountOptions.start[0] = parseInt($stateParams.faceMin) / 1000000;
            }
            if ($stateParams.faceMax) {
                vm.faceAmountMax = parseInt($stateParams.faceMax) / 1000000;
                vm.faceAmountOptions.start[1] = parseInt($stateParams.faceMax) / 1000000;
            }
        }

        vm.showMessage = function(type, helpTitle, helpText) {
            if (type === 'Add New Policy') {
                sweetAlert.swal({
                    title: "Add New Policy",
                    text: "Start here if you have a Policy that you want to: Track & Manage, Price/Re-price, or Sell on the Secondary/Tertiary Market",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.enableFilters = function(type) {
            if (type === 'cases') {
                vm.showFilters = vm.filtersEnabled;
            } else {
                vm.showAvailableFilters = vm.filtersEnabled;
            }
            root.child('users/' + vm.currentUser.auth.uid).update({
                filtersEnabled: !vm.filtersEnabled
            }).then(function() {
                vm.doSearch();
            });
        }

        vm.setSort = function(col) {
            vm.sortCol = col; 
            vm.sortReverse = !vm.sortReverse;
            vm.searchObj.sort = {
                field: vm.sortCol,
                direction: vm.sortReverse ? 'desc' : 'asc'
            };
            vm.doSearch(true);
            root.child('users/' + vm.currentUser.auth.uid).update({
                sortCol: col,
                sortReverse: vm.sortReverse
            });
        }

        vm.viewPolicy = function(policy) {
            if (vm.activeTab < 3) {
                $state.go('buyerPolicy', { 'pid': policy.policyKey, 'bid': vm.currentUser.org.orgGuid });
            } else if (vm.activeTab === 3) {
                $state.go('policy', { 'pid': policy.policyKey });
            }
        }

        vm.flagPolicy = function(pid) {
            var addFlag = true;
            var policy = $firebaseObject(root.child('policies/' + pid));
            policy.$loaded().then(function(policy) {
                if (!policy.flaggedBy) {
                    policy.flaggedBy = [];
                }
                if (policy.flaggedBy.indexOf(vm.currentUser.auth.uid) > -1) {
                    addFlag = false;
                    policy.flaggedBy.splice(vm.currentUser.auth.uid, 1);
                } else {
                    policy.flaggedBy.push(vm.currentUser.auth.uid);
                }
                vm.policies.find(x => x.policyKey === pid).flaggedBy = policy.flaggedBy;
                policy.$save(policy).then(function() {
                    root.child('users/' + vm.currentUser.auth.uid + '/flaggedPolicies/' + pid).update({
                        'flagged': addFlag ? 'true' : null,
                        'key': addFlag ? pid : null,
                        'lastName': addFlag ? (policy.insured.lastName ? policy.insured.lastName : 'n/a') : null,
                        'policyNumber': addFlag ? (policy.policyNumber ? policy.policyNumber : 'n/a') : null
                    });
                });
            });
        }

        vm.logout = function() {
            authService.logout();
        }

        vm.addNewPolicy = function() {
            if (vm.platform === 'isubmit') {
                $state.go('application');
            } else if (vm.platform === 'imanager') {
                $state.go('caseIntake');
            }
        }

        vm.resetFilters = function(noSearch) {
            vm.ageMin = 0;
            vm.ageMax = 120;
            vm.ageOptions.start = [0,120];
            vm.premiumMin = '$0';
            vm.premiumMax = '$250,000';
            vm.premiumOptions.start = [0,250000];
            vm.faceAmountMin = '$0';
            vm.faceAmountMax = '$10,000,000';
            vm.faceAmountOptions.start = [0,10000000];
            vm.showNotInterested = false;
            vm.showClosed = false;

            if (!noSearch) {
                vm.doSearch();
            }
        }

        vm.saveFilters = function() {
            root.child('users/' + vm.currentUser.auth.uid).update({
                ageMin: vm.ageMin,
                ageOptions: {
                    start: [vm.ageOptions.start[0],vm.ageOptions.start[1]]
                },
                ageMax: vm.ageMax,
                premiumMin: vm.premiumMin,
                premiumOptions: {
                    start: [vm.premiumOptions.start[0],vm.premiumOptions.start[1]]
                },
                premiumMax: vm.premiumMax,
                faceAmountMin: vm.faceAmountMin,
                faceAmountOptions: {
                    start: [vm.faceAmountOptions.start[0],vm.faceAmountOptions.start[1]]
                },
                faceAmountMax: vm.faceAmountMax,
                showNotInterested: vm.showNotInterested,
                showClosed: vm.showClosed
            }).then(function() {
                sweetAlert.swal('Saved','Your default filter preferences have been saved','success');
            });
        }

        vm.doSearch = function(paging) {
            vm.fetchingPolicies = true;
            var numFormat = wNumb({
                decimals: 0,
                thousand: ',',
                prefix: '$'
            });
            
            if (!paging) {
                // mapping outdated sort columns to new index names
                if (vm.sortCol === 'insured.safeName') {
                    vm.sortCol = 'fullName';
                } else if (vm.sortCol === 'insured.gender[0].id') {
                    vm.sortCol = 'gender';
                } else if (vm.sortCol === 'insured.age') {
                    vm.sortCol = 'age';
                } else if (vm.sortCol === 'insured.healthStatus') {
                    vm.sortCol = 'healthStatus';
                } else if (vm.sortCol === 'status.display.text') {
                    vm.sortCol = 'displayStatus';
                }
                vm.searchObj = {
                    sort: {
                        field: vm.sortCol,
                        direction: vm.sortReverse ? 'desc' : 'asc'
                    },
                    paging: {
                        from: 0,
                        size: vm.numPerPage
                    }
                };

                if (vm.searchTerm !== '') {
                    let searchFields = ['fullName','LSHID','carrier'];
                    if (vm.activeTab === 3) {
                        searchFields.push('createdBy.createdByName');
                    }
                    vm.searchObj.searchFilter = {
                        text: vm.searchTerm,
                        fields: searchFields
                    };
                }

                if (!vm.searchObj.boolFilters) {
                    vm.searchObj.boolFilters = [];
                }
                if (vm.activeTab === 3) {
                    vm.searchObj.boolFilters.push({
                        key: 'isPrivate',
                        value: false
                    });
                } else {
                    vm.searchObj.boolFilters.push({
                        key: 'isPrivate',
                        value: true
                    });
                }
                vm.searchObj.boolFilters.push({
                    key: 'isArchived',
                    value: false
                });
                if (vm.activeTab !== 3 && !vm.showNotInterested) {
                    vm.searchObj.boolFilters.push({
                        key: 'imInterested',
                        value: true
                    });
                }
                if (vm.filtersEnabled) {
                    let searchAgeMin = parseInt(vm.ageMin);
                    if (vm.ageMin === 0) {
                        searchAgeMin = -2000;
                    }
                    if (!vm.searchObj.numberFilters) {
                        vm.searchObj.numberFilters = [];
                    }
                    vm.searchObj.numberFilters.push({
                        field: 'age',
                        comparator: 'between',
                        from: searchAgeMin,
                        to: parseInt(vm.ageMax)
                    },{
                        field: 'annualPremiumAmount',
                        comparator: 'between',
                        from: numFormat.from(vm.premiumMin) ? numFormat.from(vm.premiumMin) : 0,
                        to: numFormat.from(vm.premiumMax) ? numFormat.from(vm.premiumMax) : 250000
                    },{
                        field: 'faceAmount',
                        comparator: 'between',
                        from: numFormat.from(vm.faceAmountMin) ? numFormat.from(vm.faceAmountMin) : 0,
                        to: numFormat.from(vm.faceAmountMax) ? numFormat.from(vm.faceAmountMax) : 10000000
                    });
                    if (!vm.showClosed) {
                        vm.searchObj.boolFilters.push({
                            key: 'isClosed',
                            value: false
                        });
                    }
                } else {
                    vm.searchObj.boolFilters.push({
                        key: 'isClosed',
                        value: false
                    });
                }
            }

            let orgKey = (vm.currentUser.org && vm.currentUser.org.orgKey) ? vm.currentUser.org.orgKey : null;
            if (!orgKey) {
                orgKey = (vm.currentUser.org && vm.currentUser.org.$id) ? vm.currentUser.org.$id : null;
            }

            let searchData = {
                uid: vm.currentUser.auth.uid,
                orgKey: orgKey,
                orgGuid: (vm.currentUser.org && vm.currentUser.org.orgGuid) ? vm.currentUser.org.orgGuid : null,
                searchTab: vm.activeTab,
                searchObj: vm.searchObj
            }
            policyService.esSearch(searchData).then(function(results) {
                results.hits.forEach(function(hit) {
                    hit.statusIcons = {};
                    if (hit.status) {
                        hit.status.forEach(function(status) {
                            if (status.progress) {
                                hit.statusIcons[status.progress.activity] = sharedService.cloneObject(status.progress.activityAt);
                            }
                        });
                    }
                });
                $scope.$applyAsync(function() {
                    vm.policies = results.hits;
                    vm.activePolicyCount = results.hitCount;
                    vm.fetchingPolicies = false;
                });
            });
        }

        vm.select = function(page, scrollUp) {
            console.log('Loading page ' + page + '. Sorted by ' + vm.sortCol + ' in ' + vm.sortDir + ' order.');
            vm.searchObj.sort = {
                field: vm.sortCol,
                direction: vm.sortReverse ? 'desc' : 'asc'
            };
            vm.searchObj.paging = {
                from: (page - 1) * vm.numPerPage,
                size: vm.numPerPage
            };
            vm.doSearch(true);
        };

        vm.onNumPerPageChange = function(selectAll) {
            vm.select(1, false);
            vm.currentUser.profile.numPerPage = vm.numPerPage;
            root.child('users/' + vm.currentUser.auth.uid).update({
                numPerPage: vm.numPerPage
            }).then(function() {
                console.log('User contacts per page setting updated');
            });
            return vm.currentPage = 1;
        };
    }

    angular.module('yapp')
        .controller('InventoryCtrl', InventoryCtrl);

})();