(function() {
    'use strict';

    /* @ngInject */
    function BuyerPolicyCtrl(stdData, ENV, imgrData, toastr, $scope, $state, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, notifyService, apiService, policyService, brandingDomain, $window, sharedService) {
        var vm = this;
        vm.newAdminFiles = [];
        vm.newCaseFiles = [];
        vm.statusImage = 'Timeline';
        vm.showPage = 1;
        vm.isCreator = false;
        vm.pageIsDirty = false;
        vm.selectedBuyers = [];
        vm.hideView = true;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        vm.leCompanies = imgrData.leCompanies;
        vm.pricingCompanies = imgrData.pricingCompanies;
        vm.medicalSummaryCompanies = imgrData.medicalSummaryCompanies;
        vm.fileCategories = stdData.fileCategories;
        vm.states = stdData.states;
        vm.genders = stdData.genders;
        vm.maritalStatus = stdData.maritalStatus;
        vm.types = stdData.policyTypes;
        vm.yesNo = stdData.yesNo;
        vm.imanagerRoles = stdData.imanagerRoles;

        // Establish global Firebase RTDB root reference
        var root = firebase.database().ref();

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            vm.userRole = vm.currentUser.profile.role;
            if (vm.currentUser && (vm.currentUser.profile.isSAdmin || vm.userRole === 'Admin')) {
                vm.isAdmin = true;
            }
            if (vm.subdomain.indexOf('isubmit') > -1 && !vm.isAdmin) {
                sweetAlert.swal({
                    title: "Error",
                    text: "The link you provided is not valid, or you do not have access to view this Policy.",
                    type: "error"
                }).then(function() {
                    $state.go('inventory');
                });
            } else {
                loadPage();
            }
        });

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        vm.setOrderObject = function(company) {
            if (company.active) {
                vm.newOrder = sharedService.cloneObject(company);
                vm.newOrder.selectedFileCategories = [];
                vm.newOrder.validOrderOptions = [];
            }
        }

        vm.startOrder = function(type) {
            if ((vm.addNewLE || vm.addNewPR || vm.addNewMS) && vm.newOrder) {
                sweetAlert.swal({
                    title: "Abandon Order?",
                    text: "It looks like you're in the middle of setting up an Order. Are you sure you want to cancel?",
                    type: "question",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes"
                }).then(function(result) {
                    if (!result.dismiss) {
                        continueStartOrder(type);
                    }
                });
            } else {
                continueStartOrder(type);
            }
        }

        function continueStartOrder(type) {
            if (type === 'LE') {
                vm.addNewLE = true;
                vm.addNewPR = false;
                vm.addNewMS = false;
                delete vm.newOrder;
                togglePanelHighlight('lifeExpectancies', true);
            } else if (type === 'PR') {
                vm.addNewLE = false;
                vm.addNewPR = true;
                vm.addNewMS = false;
                delete vm.newOrder;
                togglePanelHighlight('pricingReports', true);
            } else if (type === 'MS') {
                vm.addNewLE = false;
                vm.addNewPR = false;
                vm.addNewMS = true;
                delete vm.newOrder;
                togglePanelHighlight('medicalSummaries', true);
            }
            $scope.$apply();
        }

        function markViewed() {
            if (vm.isBuyer && !vm.viewAsBuyer && !vm.managePage && vm.currentUser.org.orgGuid) {
                if (vm.policy[vm.currentUser.org.orgGuid].indexOf('viewed') < 0) {
                    vm.policy[vm.currentUser.org.orgGuid] = vm.policy[vm.currentUser.org.orgGuid] + '|viewed:' + (moment().unix() * 1000).toString();
                    console.log(vm.policy[vm.currentUser.org.orgGuid]);
                    var updates = {};
                    updates['privatePolicies/' + vm.policyGuid + '/' + vm.currentUser.org.orgGuid] = vm.policy[vm.currentUser.org.orgGuid];
                    updates['privatePolicies/' + vm.policyGuid + '/hasViewedPolicy'] = true;
                    updates['privatePolicies/' + vm.policyGuid + '/viewedPolicyAt'] = moment().unix() * 1000;
                    root.update(updates).then(function() {
                        var data = {
                            pk: vm.policy.parentKey,
                            pg: vm.policyGuid,
                            pt: (vm.policy.parentKey === vm.policy.rootKey) ? 'policies' : 'privatePolicies'
                        };
                        policyService.buyerMarkViewed(data).then(function(response) {
                            if (response.status === 'success') {
                                console.log(response.message);
                            } else {
                                console.log(response.message);
                            }
                        });
                    });
                }
            }
        }

        vm.cancelOrder = function(type) {
            if ((vm.addNewLE || vm.addNewPR || vm.addNewMS) && vm.newOrder) {
                sweetAlert.swal({
                    title: "Abandon Order?",
                    text: "It looks like you're in the middle of setting up an Order. Are you sure you want to cancel?",
                    type: "question",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes"
                }).then(function(result) {
                    if (!result.dismiss) {
                        continueCancelOrder(type);
                    }
                });
            } else {
                continueCancelOrder(type);
            }
        }

        function continueCancelOrder(type) {
            vm.addNewLE = false;
            vm.addNewMS = false;
            vm.addNewPR = false;
            delete vm.newOrder;
            if (type === 'LE') {
                togglePanelHighlight('lifeExpectancies', false);
            } else if (type === 'PR') {
                togglePanelHighlight('pricingReports', false);
            } else if (type === 'MS') {
                togglePanelHighlight('medicalSummaries', false);
            }
            $scope.$apply();
        }

        vm.submitOrder = function(type) {
            vm.dataToSend = {
                orderType: type,
                companyName: vm.newOrder.name,
                selectedFiles: [],
                selectedOptions: {},
                publicGuid: vm.policyGuid ? vm.policyGuid : null,
                policyKey: vm.policyKey ? vm.policyKey : null,
                orderCreatedBy: vm.currentUser.profile.displayName,
                orderCreatedByRole: vm.currentUser.profile.role,
                orderCreatedById: vm.currentUser.auth.uid
            };
            Object.keys(vm.newOrder.selectedOptions).forEach(function(optionName) {
                vm.dataToSend.selectedOptions[optionName] = vm.newOrder.selectedOptions[optionName][0];
            });
            if (vm.newOrder.selectedFiles) {
                vm.newOrder.selectedFiles.forEach(function(file) {
                    vm.dataToSend.selectedFiles.push({
                        category: file.category,
                        link: file.link,
                        name: file.name,
                        type: file.type
                    });
                });
            }
            if (!vm.managePage && vm.buyerGuid && vm.policyGuid) {
                vm.dataToSend.orgGuid = vm.buyerGuid;
            }

            // Save this order to the database for processing
            root.child('orders').push(vm.dataToSend).then(function(ref) {
                vm.orderKey = ref.key;
                vm.privateCaseFiles.$add({
                    category: 'Order',
                    orderStatus: 'Pending',
                    name: type + ' Order',
                    type: 'order',
                    orderKey: vm.orderKey,
                    company: vm.newOrder.name,
                    hiddenFromBuyers: true
                }).then(function() {
                    sweetAlert.swal({
                        title: "Success",
                        text: "Your Order has been submitted for processing!",
                        type: "success",
                        timer: 1500
                    }).then(function() {
                        vm.addNewLE = false;
                        vm.addNewMS = false;
                        vm.addNewPR = false;
                        delete vm.newOrder;
                        processOrderFiles();
                    });
                }).catch(function(err) {
                    console.log(err);
                });
            }).catch(function(err) {
                console.log(err);
            });
        }

        function finishOrder(type) {
            vm.privateCaseFiles.$add({
                category: 'Order',
                orderStatus: 'Pending',
                name: type + ' Order',
                type: 'order',
                orderKey: vm.orderKey,
                company: vm.newOrder.name
            }).then(function() {
                sweetAlert.swal({
                    title: "Success",
                    text: "Your Order has been submitted for processing!",
                    type: "success"
                }).then(function() {
                    vm.addNewLE = false;
                    vm.addNewMS = false;
                    vm.addNewPR = false;
                    delete vm.newOrder;
                    processOrderFiles();
                });
            });
        }

        function loadPage() {
            if ($state.params.pid || $state.params.pk) {
                if (vm.imanagerRoles.indexOf(vm.currentUser.profile.role) > -1) {
                    vm.isBuyer = true;
                }
                /**
                 * LOADING AN EXISTING POLICY
                 */
                vm.policyGuid = $state.params.pid;
                if ($state.params.pk) {
                    vm.policyKey = $state.params.pk;
                    vm.managePage = true;
                }
                if ($state.params.bid) {
                    vm.buyerGuid = $state.params.bid;
                    if (vm.managePage) {
                        vm.managePage = false;
                        vm.viewAsBuyer = true;
                        continuePageLoad();
                    } else if (vm.buyerGuid !== vm.currentUser.org.orgGuid) {
                        sweetAlert.swal({
                            title: "Error",
                            text: "The link you provided is not valid, or you do not have access to view this Policy.",
                            type: "error"
                        }).then(function() {
                            $state.go('inventory');
                        });
                    } else if (!vm.currentUser.org.policies || !vm.currentUser.org.policies[vm.policyGuid] || !vm.currentUser.org.linkedTo || !vm.currentUser.org.linkedTo[vm.currentUser.org.policies[vm.policyGuid].fromOrg]) {
                        sweetAlert.swal({
                            title: "Error",
                            text: "You do not have access to view this Policy. Please check for any pending Supplier Invitations!",
                            type: "error"
                        }).then(function() {
                            $state.go('buyers', { tab: 3 });
                        });
                    } else if (vm.buyerGuid === vm.currentUser.org.orgGuid) {
                        vm.isBuyer = true;
                        continuePageLoad();
                    } else {
                        continuePageLoad();
                    }
                } else if ($state.params.pid) {
                    sweetAlert.swal({
                        title: "Error",
                        text: "The link you provided is not valid, or you do not have access to view this Policy.",
                        type: "error"
                    }).then(function() {
                        $state.go('inventory');
                    });
                } else {
                    continuePageLoad();
                }
            } else {
                sweetAlert.swal({
                    title: "Error",
                    text: "The link you provided is not valid, or you do not have access to view this Policy.",
                    type: "error"
                }).then(function() {
                    $state.go('inventory');
                });
            }
        }

        function continuePageLoad() {
            vm.hideView = false;
            vm.lvl = $state.params.lvl;
            if (vm.viewAsBuyer) {
                // get the Buyer Name for reference
                if ($state.params.lvl && $state.params.lvl.toString() === '2') {
                    vm.policyBuyers = $firebaseObject(root.child('privatePolicies').child(vm.policyKey).child('buyers'));
                } else {
                    vm.policyBuyers = $firebaseObject(root.child('policies').child(vm.policyKey).child('buyers'));
                }
                vm.policyBuyers.$loaded().then(function() {
                    vm.policyBuyers.forEach(buyer => {
                        if (buyer.orgGuid === vm.buyerGuid) {
                            vm.currentBuyer = buyer;
                        }
                    });
                    $scope.$apply();
                });
            }

            if (vm.managePage) {
                vm.policy = $firebaseObject(root.child('policies').child(vm.policyKey));
                vm.policyRef = root.child('policies').child(vm.policyKey);
                vm.offers = $firebaseArray(root.child('policies').child(vm.policyKey).child('offers'));
            } else {
                vm.policy = $firebaseObject(root.child('privatePolicies').child(vm.policyGuid));
                vm.policyRef = root.child('privatePolicies').child(vm.policyGuid);
                vm.offers = $firebaseArray(root.child('privatePolicies').child(vm.policyGuid).child('offers'));
            }

            if (vm.currentUser.org) {
                vm.buyers = $firebaseArray(root.child('orgs/' + vm.currentUser.org.orgType + '/' + vm.currentUser.org.$id + '/buyers'));
            }

            vm.policy.$loaded().then(function() {
                if (vm.currentUser.org && vm.currentUser.org.linkedTo) {
                    vm.supplier = vm.currentUser.org.linkedTo[vm.policy.createdByOrgKey];
                }
                vm.storage = firebase.storage();
                var storageKey = vm.policyKey ? vm.policyKey : vm.policyGuid;
                vm.storageRef = vm.storage.ref('policies/' + storageKey);
                // Determine if user or user's org is the Policy Creator
                if (vm.policy.createdById === vm.currentUser.auth.uid) {
                    vm.isCreator = true;
                } else if (vm.currentUser.profile.myOrg && vm.policy.createdByOrgKey === vm.currentUser.profile.myOrg.orgKey) {
                    vm.isCreator = true;
                }
                // Determine if user is an Admin
                if (vm.currentUser.profile.myOrg && vm.policy[vm.currentUser.profile.myOrg.orgKey] === 'admin') {
                    vm.isAdmin = true;
                    if (!vm.policy.private) {
                        vm.policy.private = {};
                    }
                    if (!vm.policy.adminFiles) {
                        vm.policy.adminFiles = [];
                    }
                }
                // Get authorized case files
                if (vm.buyerGuid && !vm.managePage) {
                    vm.sharedCaseFiles = $firebaseArray(root.child('sharedCaseFiles/' + vm.policyGuid));
                    vm.sharedCaseFiles.$loaded().then(function() {
                        vm.medicalSummaryFiles = [];
                        vm.sharedCaseFiles.forEach(function(file) {
                            if (file.category === 'Medical Summary') {
                                vm.medicalSummaryFiles.push(file);
                            }
                        });
                        if (!vm.viewAsBuyer && vm.currentUser.org) {
                            vm.privateCaseFiles = $firebaseArray(root.child('caseFiles/' + vm.currentUser.org.$id + '/' + vm.policyGuid));
                            vm.privateCaseFiles.$loaded().then(function() {
                                processOrderFiles();
                            });
                        } else {
                            processOrderFiles();
                        }
                    });
                    
                } else if (vm.managePage) {
                    //vm.privateCaseFiles = $firebaseArray(root.child('caseFiles/' + vm.currentUser.org.$id + '/' + vm.policyKey));
                    vm.privateCaseFiles = $firebaseArray(root.child('caseFiles/' + vm.policyKey));
                    vm.privateCaseFiles.$loaded().then(function() {
                        processOrderFiles();
                    });
                }
                if (vm.policy.insured.dateOfBirth) {
                    vm.insuredAge = moment(vm.policy.insured.dateOfBirth).diff(moment(), 'years') * -1;
                }
                if (vm.policy.insured.bmi) {
                    vm.bmi = vm.policy.insured.bmi;
                }
                if (vm.policy.secondaryInsured) {
                    if (vm.policy.secondaryInsured.dateOfBirth) {
                        vm.secondaryInsuredAge = moment(vm.policy.secondaryInsured.dateOfBirth).diff(moment(), 'years') * -1;
                    }
                    if (vm.policy.secondaryInsured.bmi) {
                        vm.bmi = vm.policy.secondaryInsured.bmi;
                    }
                }

                markViewed();

                // TODO: Update this to be more efficient. Offers should be written to a write-only node that is accessible by the Supplier only so they can pull all Buyer offers in a single request
                vm.downlineOffers = [];
                if (vm.policy.buyers) {
                    $window.async.forEachOfSeries(vm.policy.buyers, function(buyer, key, callback) {
                        root.child('privatePolicies/' + buyer.publicGuid + '/offers').once('value', function(snap) {
                            if (snap.exists()) {
                                snap.forEach(function(offer) {
                                    var offerData = offer.val();
                                    vm.downlineOffers.push({
                                        offerAmount: offerData.offerAmount ? offerData.offerAmount : 'n/a',
                                        rdb: offerData.rdb ? offerData.rdb : null,
                                        rdbAmount: offerData.rdbAmount ? offerData.rdbAmount : null,
                                        offerSentAt: offerData.offerSentAt,
                                        offerSentBy: offerData.offerSentBy,
                                        orgGuid: buyer.orgGuid,
                                        publicGuid: buyer.publicGuid,
                                        isTrackingOffer: offerData.isTrackingOffer,
                                        offerSentOnBehalfOf: offerData.offerSentOnBehalfOf
                                    });
                                });
                            }
                            callback();
                        });
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            jQuery('body').off('ifUnchecked', 'input');
                            jQuery('body').off('ifChecked', 'input');
                            updateLabels();
                            processNotifications();
                        }
                    });
                } else {
                    jQuery('body').off('ifUnchecked', 'input');
                    jQuery('body').off('ifChecked', 'input');
                    updateLabels();
                    processNotifications();
                }
            }).catch(function(err) {
                console.log(err);
            });
        }

        function processNotifications() {
            // Convert any existing Primary Ailments to Policy Notes
            if (vm.policy.primaryAilments && !vm.policy.policyNotes) {
                vm.policy.policyNotes = vm.policy.primaryAilments;
                vm.savePolicyNotes();
            }

            // Query for policy notifications for the current user
            root.child('notifications').orderByChild('pid').equalTo(vm.policyGuid).once('value', function(policyNotifications) {
                if (policyNotifications.exists()) {
                    policyNotifications.forEach(function(p) {
                        let notification = p.val();
                        if (notification.isUnread) {
                            if ((notification.targetType === 'user' && notification.targetId === vm.currentUser.auth.uid) || (notification.targetType === 'org' && vm.currentUser.profile.myOrg && notification.targetId === vm.currentUser.profile.myOrg.orgKey) || (notification.targetType === 'buyer' && vm.currentUser.org && notification.targetId == vm.currentUser.org.orgGuid)) {
                                var processNotification = true;
                                if (vm.currentUser.profile.role === 'iBroker' && notification.createdByRole === 'iBroker') {
                                    processNotification = false;
                                } else if (vm.currentUser.profile.role === 'Back Office' && notification.createdByRole !== 'iBroker') {
                                    processNotification = false;
                                }
                                if (processNotification) {
                                    // Clear this notification
                                    root.child('notifications/' + notification.notifyKey).update({
                                        isUnread: false,
                                        readBy: vm.currentUser.auth.uid,
                                        readAt: firebase.database.ServerValue.TIMESTAMP
                                    }).then(function() {
                                        vm.toast('success', 'Notifications cleared');
                                    });
                                }
                            }
                        }
                    })
                }
            });
        }

        vm.requestIllustration = function() {
            sweetAlert.swal({
                title: "Confirm Action",
                html: "This will send a message to the Supplier requesting new or updated Illustrations for this Policy. Do you wish to continue?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    sweetAlert.swal({
                        title: "Add Instructions",
                        html: "Provide any details you want the Supplier to know about this request",
                        type: "question",
                        input: "textarea",
                        showCancelButton: true,
                        cancelButtonText: "Cancel",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Send",
                        closeOnConfirm: false
                    }).then(function(result) {
                        if (result.value) {
                            vm.newPublicNote = "The Buyer has requested new or updated Illustrations for this Policy with the following note: " + result.value;
                            vm.saveNewPublicNote();
                            sweetAlert.swal({
                                title: "Illustrations Requested",
                                text: "You will receive a notification if the Supplier provides new Illustrations",
                                type: "success",
                                timer: 1500
                            }).then(function() {});
                        }
                    });
                }
            });
        }

        vm.confirmPolicy = function() {
            if (vm.policy.policyConfirmed) {
                sweetAlert.swal({
                    title: "Already Confirmed",
                    text: "Confirmed by " + vm.policy.policyConfirmedBy + " on " + moment(vm.policy.policyConfirmedAt).format('MM-DD-YYYY'),
                    type: "success"
                }).then(function() {});
            } else {
                sweetAlert.swal({
                    title: "Confirm Action",
                    html: "Are you sure this Policy is ready for your Buyers to see? No one will have access until you select them using the \"Send to Buyers\" option on the Policy View.",
                    type: "question",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes"
                }).then(function(result) {
                    if (!result.dismiss) {
                        vm.policy.policyConfirmed = true;
                        vm.policy.policyConfirmedAt = firebase.database.ServerValue.TIMESTAMP;
                        vm.policy.policyConfirmedById = vm.currentUser.auth.uid;
                        vm.policy.policyConfirmedBy = vm.currentUser.profile.displayName;
                        vm.policy.$save().then(function() {
                            vm.toast('success', 'This Policy\'s Buyer View has been confirmed');
                            $state.go('policy', { pid: vm.policyKey });
                        }).catch(function(err) {
                            console.log(err);
                        });
                    }
                });
            }
        }

        function processOrderFiles() {
            vm.leOrders = [];
            vm.prOrders = [];
            vm.msOrders = [];
            if (vm.privateCaseFiles && vm.privateCaseFiles.length > 0) {
                vm.privateCaseFiles.forEach(file => {
                    if (file.type === 'order') {
                        if (file.name === 'Life Expectancy Order') {
                            vm.leOrders.push(file);
                        } else if (file.name === 'Pricing Report Order') {
                            vm.prOrders.push(file);
                        } else if (file.name === 'Medical Summary Order') {
                            vm.msOrders.push(file);
                        }
                    }
                });
            }
            if (vm.sharedCaseFiles && vm.sharedCaseFiles.length > 0) {
                vm.sharedCaseFiles.forEach(file => {
                    if (file.type === 'order') {
                        if (file.name === 'Life Expectancy Order') {
                            vm.leOrders.push(file);
                        } else if (file.name === 'Pricing Report Order') {
                            vm.prOrders.push(file);
                        } else if (file.name === 'Medical Summary Order') {
                            vm.msOrders.push(file);
                        }
                    }
                });
            }
            $scope.$apply();
        }

        vm.scrollToAddOffer = function() {
            vm.addOffer = true;
            var elmnt = document.getElementById("offers");
            elmnt.scrollIntoView();
            togglePanelHighlight('offers', true);
            $scope.$apply();
        }

        vm.addNewOffer = function() {
            vm.newOffer = {};
            vm.addOffer = true;
            togglePanelHighlight('offers', true);
            $scope.$apply();
        }

        vm.addOfferOnBehalfOfBuyer = function() {
            sweetAlert.swal({
                title: "Confirm Action",
                text: "This will add a 'Tracking Offer' and will be non-binding on the Buyer, unless the Buyer specifically approves the details of this Offer. Both you and the Buyer will be able to edit this Offer until it is approved by the Buyer.",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.newOffer = {
                        isTrackingOffer: true
                    };
                    vm.addOffer = true;
                    togglePanelHighlight('offers', true);
                    $scope.$apply();
                }
            });
        }

        vm.retractOffer = function(offer) {
            var offerType = offer.isTrackingOffer ? 'Tracking ' : '';
            sweetAlert.swal({
                title: "Confirm Action",
                text: "Are you sure you want to remove this " + offerType + "Offer? It will no longer be visible to the Suppier.",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.offers.$remove(offer).then(function(ref) {
                        sweetAlert.swal({
                            title: offerType + "Offer Removed",
                            text: "Your " + offerType + "Offer has been successfully removed",
                            type: "success",
                            timer: 1500
                        }).then(function(result) {});
                    });
                }
            });
        }

        vm.editOffer = function(offer) {
            vm.offerBackup = sharedService.cloneObject(offer);
            vm.newOffer = offer;
            vm.editingOffer = true;
            togglePanelHighlight('offers', true);
            updateLabels();
        }

        vm.updateOffer = function() {
            if (vm.newOffer.isTrackingOffer && (vm.isBuyer && !vm.viewAsBuyer) || (vm.isBuyer && vm.lvl)) {
                // This is a Tracking Offer approval by the Buyer
                sweetAlert.swal({
                    title: "Confirm Action",
                    html: "Clicking \"Yes\" below will approve the Tracking Offer that the Policy Supplier added on your behalf. They will receive a notification. This Offer is non-binding.<br><br>Do you agree?",
                    type: "question",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes"
                }).then(function(result) {
                    if (!result.dismiss) {
                        vm.newOffer.trackingOfferDetails = vm.offerBackup;
                        vm.newOffer.trackingOfferDetails.offerApprovedAt = firebase.database.ServerValue.TIMESTAMP;
                        vm.newOffer.offerSentAt = firebase.database.ServerValue.TIMESTAMP;
                        vm.newOffer.offerSentBy = vm.currentUser.profile.displayName;
                        vm.newOffer.offerSentOnBehalfOf = 'n/a';
                        vm.newOffer.offerSentById = vm.currentUser.auth.uid;
                        vm.newOffer.offerSentByBID = vm.buyerGuid;
                        vm.newOffer.isTrackingOffer = null;

                        vm.offers.$save(vm.newOffer).then(function() {
                            sweetAlert.swal({
                                title: "Tracking Offer Approved",
                                text: "This Tracking Offer has been successfully Approved",
                                type: "success",
                                timer: 1500
                            }).then(function(result) {
                                vm.editingOffer = false;
                                vm.newOffer = {};
                                togglePanelHighlight('offers', false);
                                $scope.$apply();
                            });
                        });
                    }
                });
            } else {
                vm.offers.$save(vm.newOffer).then(function() {
                    sweetAlert.swal({
                        title: "Offer Updated",
                        text: "Your offer has been successfully updated",
                        type: "success",
                        timer: 1500
                    }).then(function(result) {
                        vm.editingOffer = false;
                        vm.newOffer = {};
                        togglePanelHighlight('offers', false);
                        $scope.$apply();
                    });
                });
            }
        }

        vm.cancelOffer = function() {
            if (vm.editingOffer) {
                vm.newOffer.note = vm.offerBackup.note ? vm.offerBackup.note : null;
                vm.newOffer.offerAmount = vm.offerBackup.offerAmount;
                vm.newOffer.rdb = vm.offerBackup.rdb ? vm.offerBackup.rdb : null;
            }
            vm.newOffer = {};
            vm.editingOffer = false;
            vm.addOffer = false;
            jQuery('#newOffer .placeHolder.active').each(function(ele) { jQuery(this).removeClass('active'); });
            togglePanelHighlight('offers', false);
        }

        vm.submitOffer = function() {
            var offerType = vm.newOffer.isTrackingOffer ? 'Tracking ' : '';
            sweetAlert.swal({
                title: "Confirm Action",
                html: "Clicking \"Yes\" below will submit a new " + offerType + "Offer to the Supplier that sent you this case. They will receive a notification. This Offer is non-binding.<br><br>Do you agree?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.newOffer.offerSentAt = firebase.database.ServerValue.TIMESTAMP;
                    vm.newOffer.offerSentBy = vm.currentUser.profile.displayName;
                    vm.newOffer.offerSentOnBehalfOf = vm.newOffer.isTrackingOffer ? vm.currentBuyer.buyerName : 'n/a';
                    vm.newOffer.offerSentById = vm.currentUser.auth.uid;
                    vm.newOffer.offerSentByBID = vm.buyerGuid;
                    vm.offers.$add(vm.newOffer).then(function() {
                        var offerType = vm.newOffer.isTrackingOffer ? 'Tracking ' : '';
                        var data = {
                            targets: [],
                            policyKey: vm.policyKey ? vm.policyKey : vm.policy.parentKey,
                            pid: vm.policyGuid ? vm.policyGuid : null,
                            bid: vm.buyerGuid ? vm.buyerGuid : null,
                            lvl: vm.policy.lvl ? vm.policy.lvl : null,
                            createdBy: vm.currentUser.profile.displayName,
                            createdByRole: vm.currentUser.profile.role,
                            note: 'A new ' + offerType + 'Offer has been submitted for this Policy',
                            LSHID: vm.policy.LSHID,
                            action: 'New Policy ' + offerType + 'Offer'
                        };
                        sendNotification(data);
                        sweetAlert.swal({
                            title: offerType + "Offer Sent",
                            text: "Your " + offerType + "Offer has been successfully sent",
                            type: "success",
                            timer: 1500
                        }).then(function(result) {
                            vm.addOffer = false;
                            togglePanelHighlight('offers', false);
                            $scope.$apply();
                        });
                    });
                }
            });
        }

        vm.cancelIllustration = function() {
            vm.addIllustration = false;
            vm.newIllustrationStrategy = '';
            vm.newIllustrationPremium = '';
            vm.editingIllustrationIdx = undefined;
            jQuery('#newIllustration .placeHolder.active').each(function(ele) { jQuery(this).removeClass('active'); });
        }

        vm.clickBuyer = function($event, buyer) {
            if ($event.target.tagName === 'INPUT') {
                $event.stopPropagation();
            } else if (!vm.editInfo) {
                if (vm.showDetail === buyer.buyerName) {
                    vm.showDetail = undefined;
                } else {
                    vm.showDetail = buyer.buyerName;
                }
            }
        }

        vm.updateBuyer = function(buyer, index) {
            delete buyer.editInfo;
            delete buyer.$$hashKey;
            vm.policyRef.child('buyers/' + index).update(buyer).then(function() {
                // For each fileCategory, check if there are matching files in vm.sharedCaseFiles. If so, add them to buyer's sharedCaseFiles node.
                var updates = {};
                if (vm.sharedCaseFiles) {
                    vm.sharedCaseFiles.forEach(function(caseFile) {
                        if (!caseFile.hiddenFromBuyers && buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(caseFile.category) > -1) {
                            var caseFileCopy = sharedService.cloneObject(caseFile);
                            delete caseFileCopy.$id;
                            delete caseFileCopy.$priority;
                            delete caseFileCopy.$$hashKey;
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = caseFileCopy;
                        } else {
                            // File is hidden or they don't have access to this category, set to null in case it exists
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = null;
                        }
                    });
                }
                if (vm.privateCaseFiles) {
                    vm.privateCaseFiles.forEach(function(caseFile) {
                        if (!caseFile.hiddenFromBuyers && buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(caseFile.category) > -1) {
                            var caseFileCopy = sharedService.cloneObject(caseFile);
                            delete caseFileCopy.$id;
                            delete caseFileCopy.$priority;
                            delete caseFileCopy.$$hashKey;
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = caseFileCopy;
                        } else {
                            // File is hidden or they don't have access to this category, set to null in case it exists
                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + caseFile.$id] = null;
                        }
                    });
                }
                root.update(updates).then(function() {
                    vm.toast('success', 'Buyer Updated');
                    vm.showSave = false;
                    $scope.$apply();
                });
            });
        }

        vm.saveIllustration = function() {
            if (vm.newIllustrationStrategy && vm.newIllustrationPremium) {
                if (!vm.policy.illustrations) {
                    vm.policy.illustrations = [];
                }
                var illustration = {
                    strategy: vm.newIllustrationStrategy,
                    premium: vm.newIllustrationPremium
                };

                if (vm.editingIllustrationIdx > -1) {
                    // update the existing illustration
                    vm.policy.illustrations[vm.editingIllustrationIdx] = illustration;
                } else {
                    // push the new illustration
                    vm.policy.illustrations.push(illustration);
                }

                vm.policy.$save(vm.policy.illustrations).then(function() {
                    // If the page is being managed, save this illustration to each Buyer-specific version of the Policy as well
                    if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                        if (vm.policy.illustrations) {
                            vm.policy.illustrations.forEach(illustration => {
                                delete illustration.$$hashKey;
                            });
                        } else {
                            vm.policy.illustrations = null;
                        }
                        var updates = {};
                        vm.policy.buyers.forEach(buyer => {
                            updates['privatePolicies/' + buyer.publicGuid + '/illustrations'] = vm.policy.illustrations;
                        });
                        root.update(updates).then(function() {
                            vm.cancelIllustration();
                            vm.toast('success', 'Policy Updated');
                        });
                    } else {
                        vm.cancelIllustration();
                        vm.toast('success', 'Policy Updated');
                    }
                });
            } else {
                sweetAlert.swal({
                    title: "Error",
                    text: "You must provide an Age and a Premium value to save an Illustration",
                    type: "warning"
                }).then(function() {});
            }
        }

        vm.removeIllustration = function(illustration) {
            sweetAlert.swal({
                title: "Confirm Action",
                text: "Are you sure you want to remove this Illustration?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    var idx = vm.policy.illustrations.indexOf(illustration);
                    if (idx > -1) {
                        vm.policy.illustrations.splice(idx, 1);
                        vm.policy.$save(vm.policy.illustrations).then(function(ref) {
                            // If the page is being managed, save this illustration to each Buyer-specific version of the Policy as well
                            if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                                if (vm.policy.illustrations) {
                                    vm.policy.illustrations.forEach(illustration => {
                                        delete illustration.$$hashKey;
                                    });
                                } else {
                                    vm.policy.illustrations = null;
                                }
                                var updates = {};
                                vm.policy.buyers.forEach(buyer => {
                                    updates['privatePolicies/' + buyer.publicGuid + '/illustrations'] = vm.policy.illustrations;
                                });
                                root.update(updates).then(function() {
                                    vm.toast('success', 'Policy Updated');
                                });
                            } else {
                                vm.toast('success', 'Policy Updated');
                            }
                        });
                    }
                }
            });
        }

        vm.editIllustration = function(illustration) {
            var idx = vm.policy.illustrations.indexOf(illustration);
            if (idx > -1) {
                vm.editingIllustrationIdx = idx;
            }
            vm.newIllustrationPremium = illustration.premium;
            vm.newIllustrationStrategy = illustration.strategy;
            vm.addIllustration = true;
        }

        vm.editBMI = function(bmi, which) {
            var idx = vm.policy[which].bmiHistory.indexOf(bmi);
            if (idx > -1) {
                vm.editingBMIIdx = idx;
            }
            vm.bmiUpdatedAt = bmi.dateTaken;
            vm.policy[which].height = bmi.height;
            vm.policy[which].heightFt = bmi.heightFt;
            vm.policy[which].heightIn = bmi.heightIn;
            vm.policy[which].weight = bmi.weight;
            vm.editHumanInfo = true;
        }

        vm.removeBMI = function(bmi, which) {
            sweetAlert.swal({
                title: "Confirm Action",
                text: "Are you sure you want to remove this entry?",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes"
            }).then(function(result) {
                if (!result.dismiss) {
                    var idx = vm.policy[which].bmiHistory.indexOf(bmi);
                    if (idx > -1) {
                        vm.policy[which].bmiHistory.splice(idx, 1);
                        vm.policy.$save(vm.policy[which].bmiHistory).then(function() {
                            // If the page is being managed, save this illustration to each Buyer-specific version of the Policy as well
                            if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                                if (vm.policy[which].bmiHistory) {
                                    vm.policy[which].bmiHistory.forEach(bmi => {
                                        delete bmi.$$hashKey;
                                    });
                                } else {
                                    vm.policy[which].bmiHistory = null;
                                }
                                // Save this update to all connected Policies

                                root.child('privatePolicies').orderByChild('rootKey').equalTo(vm.policyKey).once('value', function(snap) {
                                    if (snap.exists()) {
                                        var updates = {};
                                        snap.forEach(function(policy) {
                                            updates[policy.key + '/' + which + '/bmiHistory'] = vm.policy[which].bmiHistory;
                                        });
                                        root.child('privatePolicies').update(updates).then(function() {
                                            vm.toast('success', 'Policy Updated');
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    } else {
                                        vm.toast('success', 'Policy Updated');
                                    }
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            } else {
                                vm.toast('success', 'Policy Updated');
                            }
                        });
                    }
                }
            });
        }

        vm.saveBMIUpdate = function(which) {
            if (vm.policy[which].heightFt || vm.policy[which].heightIn || vm.policy[which].weight) {
                //var ftinArr = vm.policy[which].height.replace('"', '').split('\'');
                vm.policy[which].heightFt = vm.policy[which].heightFt ? vm.policy[which].heightFt : 0;
                vm.policy[which].heightIn = vm.policy[which].heightIn ? vm.policy[which].heightIn : 0;
                vm.policy[which].weight = vm.policy[which].weight ? vm.policy[which].weight : 0;
                vm.policy[which].height = vm.policy[which].heightFt + '\'' + vm.policy[which].heightIn + '"';
                var height = convertToMeters(vm.policy[which].heightFt, vm.policy[which].heightIn);
                var weight = convertToKg(vm.policy[which].weight);
                if (height && weight) {
                    calculateBMI(height, weight, which);
                }
            }
            if (!vm.policy[which].bmiHistory) {
                vm.policy[which].bmiHistory = [];
            }
            var bmi = {
                dateTaken: vm.bmiUpdatedAt ? vm.bmiUpdatedAt : moment().unix() * 1000,
                height: vm.policy[which].height,
                heightFt: vm.policy[which].heightFt,
                heightIn: vm.policy[which].heightIn,
                weight: vm.policy[which].weight,
                bmi: vm.bmi,
                bmiClass: vm.bmiClass
            };

            if (vm.editingBMIIdx > -1) {
                vm.policy[which].bmiHistory[vm.editingBMIIdx] = bmi;
            } else {
                vm.policy[which].bmiHistory.push(bmi);
            }
            vm.policy.$save(vm.policy[which].bmiHistory).then(function() {
                // If the page is being managed, save this illustration to each Buyer-specific version of the Policy as well
                if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                    if (vm.policy[which].bmiHistory) {
                        vm.policy[which].bmiHistory.forEach(bmi => {
                            delete bmi.$$hashKey;
                        });
                    } else {
                        vm.policy[which].bmiHistory = null;
                    }
                    var updates = {};
                    vm.policy.buyers.forEach(buyer => {
                        updates['privatePolicies/' + buyer.publicGuid + '/' + which + '/bmiHistory'] = vm.policy[which].bmiHistory;
                    });
                    root.update(updates).then(function() {
                        vm.editHumanInfo = false;
                        vm.bmiUpdatedAt = undefined;
                        vm.toast('success', 'Policy Updated');
                        $scope.$apply();
                    });
                } else {
                    vm.editHumanInfo = false;
                    vm.bmiUpdatedAt = undefined;
                    vm.toast('success', 'Policy Updated');
                    $scope.$apply();
                }
            });
        }

        function calculateBMI(height, weight, which) {
            var bmi = weight / Math.pow(height, 2);
            vm.bmi = Math.round(bmi * 100) / 100;
            vm.policy[which].bmi = vm.bmi;
            if (vm.bmi < 18.5) {
                vm.bmiClass = 'Underweight';
            } else if (vm.bmi >= 18.5 && vm.bmi < 25) {
                vm.bmiClass = 'Normal';
            } else if (vm.bmi >= 25 && vm.bmi <= 30) {
                vm.bmiClass = 'Overweight';
            } else if (vm.bmi > 30) {
                vm.bmiClass = 'Obese';
            }
        }

        vm.savePolicyNotes = function() {
            vm.policy.$save(vm.policy.policyNotes).then(function() {
                // If the page is being managed, save this illustration to each Buyer-specific version of the Policy as well
                if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                    var updates = {};
                    vm.policy.buyers.forEach(buyer => {
                        updates['privatePolicies/' + buyer.publicGuid + '/policyNotes'] = vm.policy.policyNotes;
                    });
                    root.update(updates).then(function() {
                        vm.showPolicyNotesSave = false;
                        vm.toast('success', 'Policy Updated');
                        $scope.$apply();
                    });
                } else {
                    vm.showPolicyNotesSave = false;
                    vm.toast('success', 'Policy Updated');
                    $scope.$apply();
                }
            });
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.placeHolder').filter(function() {
                    return jQuery('#' + jQuery(this).attr('for')).val() && jQuery('#' + jQuery(this).attr('for')).val().length !== 0;
                }).addClass('active');
                jQuery('.pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    maxDate: new Date(),
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        var compStr = '';
                        if (parts.length === 1) {
                            compStr = vm.policy[parts[0]];
                        } else if (parts.length === 2) {
                            compStr = vm.policy[parts[0]][parts[1]];
                        } else if (parts.length === 3) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]];
                        } else if (parts.length === 4) {
                            compStr = vm.policy[parts[0]][parts[1]][parts[2]][parts[3]];
                        }
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('body').on('ifChecked', 'input', function(event) {
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'illustrationsUploaded' ||
                        event.target.name === 'medicalSummaryUploaded' ||
                        event.target.name === 'pricingReportUploaded' ||
                        event.target.name === 'lifeExpectancyUploaded') {
                        vm.policy[event.target.name] = true;
                        vm.policy.$save(vm.policy[event.target.name]).then(function() {
                            if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                                var updates = {};
                                vm.policy.buyers.forEach(buyer => {
                                    updates['privatePolicies/' + buyer.publicGuid + '/' + event.target.name] = vm.policy[event.target.name];
                                });
                                root.update(updates).then(function() {
                                    vm.toast('success', 'Policy Updated');
                                });
                            } else {
                                vm.toast('success', 'Policy Updated');
                            }
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        sweetAlert.swal({
                            title: "Confirm Action",
                            html: "This will remove access to this file from any Buyers who would otherwise be able to see files of this category. Are you sure?",
                            type: "question",
                            showCancelButton: true,
                            cancelButtonText: "No",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }).then(function(result) {
                            if (!result.dismiss) {
                                vm.privateCaseFiles.$save(vm.privateCaseFiles[fileIdx]).then(function() {
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        // Remove existing access to this file from any existing Buyers
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.privateCaseFiles[fileIdx].$id] = null;
                                        });
                                        root.update(updates).then(function() {
                                            sweetAlert.swal({
                                                title: "Success",
                                                text: "File access has been removed from all Buyers",
                                                type: "success",
                                                timer: 1000
                                            }).then(function() {});
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    }
                                    vm.toast('success', 'Policy Updated');
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            } else {
                                vm.privateCaseFiles[fileIdx].hiddenFromBuyers = false;
                                $scope.$apply();
                            }
                        });
                    } else if (event.target.name === 'hiddenFromSupplier') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        sweetAlert.swal({
                            title: "Confirm Action",
                            html: "This will remove access to this file from your Supplier. Are you sure?",
                            type: "question",
                            showCancelButton: true,
                            cancelButtonText: "No",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }).then(function(result) {
                            if (!result.dismiss) {
                                vm.privateCaseFiles.$save(vm.privateCaseFiles[fileIdx]).then(function() {
                                    // Remove existing access to this file from the Supplier
                                    root.child('sharedCaseFiles/' + vm.policyGuid + '/' + vm.privateCaseFiles[fileIdx].$id).remove(function() {
                                        sweetAlert.swal({
                                            title: "Success",
                                            text: "File access has been removed from the Supplier",
                                            type: "success",
                                            timer: 1000
                                        }).then(function() {});
                                    }).catch(function(err) {
                                        console.log(err);
                                    });
                                    vm.toast('success', 'Policy Updated');
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            } else {
                                vm.privateCaseFiles[fileIdx].hiddenFromSupplier = false;
                                $scope.$apply();
                            }
                        });
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        sweetAlert.swal({
                            title: "Confirm Action",
                            html: "This will remove access to this file from any Buyers who would otherwise be able to see files of this category. Are you sure?",
                            type: "question",
                            showCancelButton: true,
                            cancelButtonText: "No",
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "Yes",
                            closeOnConfirm: false
                        }).then(function(result) {
                            if (!result.dismiss) {
                                vm.sharedCaseFiles.$save(vm.sharedCaseFiles[fileIdx]).then(function() {
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        // Remove existing access to this file from any existing Buyers
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.sharedCaseFiles[fileIdx].$id] = null;
                                        });
                                        root.update(updates).then(function() {
                                            sweetAlert.swal({
                                                title: "Success",
                                                text: "File access has been removed from all Buyers",
                                                type: "success",
                                                timer: 1000
                                            }).then(function() {});
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    }
                                    vm.toast('success', 'Policy Updated');
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            } else {
                                vm.privateCaseFiles[fileIdx].hiddenFromBuyers = true;
                                $scope.$apply();
                            }
                        });
                    } else if (event.target.name.indexOf('orderOption-') > -1) {
                        var optionName = event.target.name.replace('orderOption-', '');

                        var optionValue = vm.newOrder.selectedOptions[optionName][0];
                        var isRequired = event.target.getAttribute('data-isRequired');
                        if (isRequired) {
                            var requirementMet = false;
                            var requiredValue = event.target.getAttribute('data-requiredValue');
                            if (requiredValue && requiredValue === optionValue) {
                                requirementMet = true;
                            } else if (!requiredValue) {
                                requirementMet = true;
                            }
                            var optIdx = vm.newOrder.validOrderOptions.indexOf(optionName);
                            if (requirementMet) {
                                if (optIdx < 0) {
                                    vm.newOrder.validOrderOptions.push(optionName);
                                }
                            } else {
                                if (optIdx > -1) {
                                    vm.newOrder.validOrderOptions.splice(optIdx, 1);
                                }
                            }
                        }
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = vm.buyers;
                    }
                    if (event.target.name.indexOf('orderFileReq-') > -1) {
                        var fileCategory = event.target.getAttribute('data-category');
                        var catIdx = vm.newOrder.selectedFileCategories.indexOf(fileCategory);
                        if (catIdx === -1) {
                            vm.newOrder.selectedFileCategories.push(fileCategory);
                        }
                    }
                    $scope.$apply();
                });
                jQuery('body').on('ifUnchecked', 'input', function(event) {
                    if (event.target.name === 'buyerFileCategory') {
                        vm.showSave = event.target.getAttribute('data-buyerName');
                    } else if (event.target.name === 'illustrationsUploaded' ||
                        event.target.name === 'medicalSummaryUploaded' ||
                        event.target.name === 'pricingReportUploaded' ||
                        event.target.name === 'lifeExpectancyUploaded') {
                        vm.policy[event.target.name] = false;
                        vm.policy.$save(vm.policy[event.target.name]).then(function() {
                            if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                                var updates = {};
                                vm.policy.buyers.forEach(buyer => {
                                    updates['privatePolicies/' + buyer.publicGuid + '/' + event.target.name] = vm.policy[event.target.name];
                                });
                                root.update(updates).then(function() {
                                    vm.toast('success', 'Policy Updated');
                                });
                            } else {
                                vm.toast('success', 'Policy Updated');
                            }
                        });
                    } else if (event.target.name === 'hiddenFromSupplier') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        if (!vm.privateCaseFiles[fileIdx].category) {
                            sweetAlert.swal({
                                title: "No File Category",
                                text: "Please categorize this file first so we know how to display it to the Supplier",
                                type: "warning"
                            }).then(function() {
                                vm.privateCaseFiles[fileIdx].hiddenFromSupplier = true;
                                $scope.$apply();
                            });
                        } else {
                            sweetAlert.swal({
                                title: "Confirm Action",
                                text: "The Supplier for this Policy will have access to view this file. Do you wish to continue?",
                                type: "question",
                                showCancelButton: true,
                                cancelButtonText: "No",
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "Yes",
                                closeOnConfirm: false
                            }).then(function(result) {
                                if (!result.dismiss) {
                                    // Update /sharedCaseFiles/[current PolicyGuid]
                                    var updates = {};
                                    var caseFileCopy = sharedService.cloneObject(vm.privateCaseFiles[fileIdx]);
                                    delete caseFileCopy.$id;
                                    delete caseFileCopy.$priority;
                                    delete caseFileCopy.$$hashKey;
                                    updates['sharedCaseFiles/' + vm.policyGuid + '/' + vm.privateCaseFiles[fileIdx].$id] = caseFileCopy;

                                    vm.privateCaseFiles.$save(vm.privateCaseFiles[fileIdx]).then(function(ref) {
                                        root.update(updates).then(function() {
                                            sweetAlert.swal({
                                                title: "Success",
                                                text: "File access has been granted to the Supplier",
                                                type: "success",
                                                timer: 1000
                                            }).then(function() {});
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    }).catch(function(err) {
                                        console.log(err);
                                    });
                                } else {
                                    sweetAlert.swal({
                                        title: "Success",
                                        text: "No changes have been made",
                                        type: "success",
                                        timer: 1000
                                    }).then(function() {
                                        vm.privateCaseFiles[fileIdx].hiddenFromSupplier = true;
                                        $scope.$apply();
                                    });
                                }
                            });
                        }
                    } else if (event.target.name === 'hiddenFromBuyers') {
                        var fileIdx = event.target.getAttribute('data-idx');
                        if (!vm.privateCaseFiles[fileIdx].category) {
                            sweetAlert.swal({
                                title: "No File Category",
                                text: "Please categorize this file first so we know how to distribute it to Buyers",
                                type: "warning"
                            }).then(function() {
                                vm.privateCaseFiles[fileIdx].hiddenFromBuyers = true;
                                $scope.$apply();
                            });
                        } else {
                            // Check all Buyers to see if any of them have access to this file's category
                            var buyersWithAccess = [];
                            var buyerAccessMessage = 'The following Buyers will be able to view this file:<br><br>';
                            if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                vm.policy.buyers.forEach(buyer => {
                                    delete buyer.$$hashKey;
                                    if (buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(vm.privateCaseFiles[fileIdx].category) > -1) {
                                        buyersWithAccess.push(buyer);
                                        buyerAccessMessage += buyer.buyerName + '<br>';
                                    }
                                });
                            }
                            buyerAccessMessage += '<br>Do you want to continue?';
                            if (buyersWithAccess.length > 0) {
                                sweetAlert.swal({
                                    title: "Confirm Action",
                                    html: buyerAccessMessage,
                                    type: "question",
                                    showCancelButton: true,
                                    cancelButtonText: "No",
                                    confirmButtonColor: "#00B200",
                                    confirmButtonText: "Yes",
                                    closeOnConfirm: false
                                }).then(function(result) {
                                    if (!result.dismiss) {
                                        // Update /sharedCaseFiles/[each buyer's PolicyGuid] in a single transaction
                                        var updates = {};
                                        var caseFileCopy = sharedService.cloneObject(vm.privateCaseFiles[fileIdx]);
                                        delete caseFileCopy.$id;
                                        delete caseFileCopy.$priority;
                                        delete caseFileCopy.$$hashKey;
                                        buyersWithAccess.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + vm.privateCaseFiles[fileIdx].$id] = caseFileCopy;
                                        });

                                        vm.privateCaseFiles.$save(vm.privateCaseFiles[fileIdx]).then(function(ref) {
                                            root.update(updates).then(function() {
                                                sweetAlert.swal({
                                                    title: "Success",
                                                    text: "File access has been granted to all selected Buyers",
                                                    type: "success",
                                                    timer: 1000
                                                }).then(function() {});
                                            }).catch(function(err) {
                                                console.log(err);
                                            });
                                        }).catch(function(err) {
                                            console.log(err);
                                        });
                                    } else {
                                        sweetAlert.swal({
                                            title: "Success",
                                            text: "No changes have been made",
                                            type: "success",
                                            timer: 1000
                                        }).then(function() {
                                            vm.privateCaseFiles[fileIdx].hiddenFromBuyers = true;
                                        });
                                    }
                                });
                            } else {
                                vm.privateCaseFiles.$save(vm.privateCaseFiles[fileIdx]).then(function(ref) {
                                    vm.toast('success', 'Policy Updated');
                                });
                            }
                        }
                    } else if (event.target.name.indexOf('orderFileReq-') > -1) {
                        var fileCategory = event.target.getAttribute('data-category');
                        var catIdx = vm.newOrder.selectedFileCategories.indexOf(fileCategory);
                        if (catIdx > -1) {
                            vm.newOrder.selectedFileCategories.splice(catIdx, 1);
                        }
                    } else if (event.target.className.indexOf('buyer-check') >= 0) {
                        vm.selectedBuyers = [];
                    }
                    $scope.$apply();
                });
                jQuery('.tabs').tabslet();
                $scope.$apply();
            }, 0);
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function sendNotification(data) {
            var policyLink = vm.subdomain + '.' + vm.domain + '.' + vm.tld;
            // Add targets to notify
            if (vm.platform === 'isubmit' && !vm.buyerGuid) {
                policyLink = policyLink + '/policy?pid=' + vm.policyKey;
                // Add policy creator if not self and only if there is no creating org (creator will be included with the org notification)
                if (vm.policy.createdById !== vm.currentUser.auth.uid && !vm.policy.createdByOrgKey) {
                    data.targets.push({
                        type: 'user',
                        id: vm.policy.createdById
                    });
                }
                // Add org upline, not including own org, unless you are SAdmin or Back Office
                if (vm.policy.upline) {
                    vm.policy.upline.forEach(function(uplineOrg) {
                        if (uplineOrg.orgKey !== vm.currentUser.profile.myOrg.orgKey || vm.currentUser.profile.role === 'SAdmin' || vm.currentUser.profile.role === 'Back Office') {
                            data.targets.push({
                                type: uplineOrg.orgType ? uplineOrg.orgType : 'org',
                                id: uplineOrg.orgKey
                            });
                        }
                    });
                }
            } else if (vm.platform === 'imanager' || vm.buyerGuid) {
                // Adjust policyLink for cross-platform communication
                if (vm.platform === 'isubmit' && vm.buyerGuid) {
                    // This is someone on the iSubmit side notifying someone on the iManager side...
                    policyLink = policyLink.replace('isubmit','imanager');
                } else if (vm.platform === 'imanager' && vm.createdOnPlatform === 'isubmit') {
                    // This is a buyer on iManager notifying the supplier on iSubmit...
                    policyLink = policyLink.replace('imanager','isubmit');
                }

                // If user's own Org created the Policy, then they are the supplier and should be notifying the buyer
                if (vm.currentUser.org && (vm.policy.createdByOrgKey === vm.currentUser.org.orgKey || vm.policy.createdByOrgKey === vm.currentUser.org.$id)) {
                    policyLink = policyLink + '/bdn/policy?pid=' + vm.policyGuid + '&bid=' + vm.buyerGuid;
                    data.targets.push({
                        type: 'buyer',
                        id: vm.buyerGuid
                    });
                    //id: vm.buyers.find(x => x.orgGuid === vm.buyerGuid).orgKey
                } else {
                    // Otherwise, this is a Buyer doing Buyer things...Send notification to the Supplier
                    policyLink = policyLink + '/bdn/policy?pid=' + vm.policyGuid + '&bid=' + vm.buyerGuid + '&pk=' + vm.policy.parentKey;
                    data.targets.push({
                        type: vm.policy.createdByOrgType ? vm.policy.createdByOrgType : 'org',
                        id: vm.policy.createdByOrgKey
                    });
                }
            }
            if (data.targets && data.targets.length > 0) {
                data.notificationSubData = {
                    link: policyLink,
                    linkRoot: vm.subdomain + '.' + vm.domain + '.' + vm.tld,
                    platform: vm.subdomain.indexOf('isubmit') > -1 ? 'iSubmit' : 'iManager',
                    headerImg: vm.subdomain.indexOf('isubmit') > -1 ? 'isubmit' : 'imanager'
                };
                notifyService.addNotification(data).then(function() {
                    vm.toast('success', 'Notifications sent');
                });
            }
        }

        vm.setActionRequired = function(type) {
            if (type === 'agent') {
                sweetAlert.swal({
                    title: "Add a Public Note",
                    text: "Please add a Note so the Agent knows what they need to do",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.actionRequired = true;
                        // Save Public Note
                        vm.newPublicNote = response.value;
                        vm.saveNewPublicNote();
                        updateStatus();
                    } else {
                        vm.policy.actionRequired = false;
                        $scope.$apply();
                    }
                });
            } else if (type === 'back-office') {
                sweetAlert.swal({
                    title: "Add an Internal Note",
                    text: "Please add a Note so the Back Office knows what they need to do",
                    type: "question",
                    input: "textarea",
                    showCancelButton: true
                }).then(function(response) {
                    if (response.value) {
                        vm.policy.boActionRequired = true;
                        // Save Private Note
                        vm.newAdminNote = response.value;
                        vm.saveNewAdminNote();
                        updateStatus();
                    } else {
                        vm.policy.boActionRequired = false;
                        $scope.$apply();
                    }
                });
            }
        }

        vm.showDetails = function(form, index, event) {
            if (vm.showFormDetails === index) {
                // Close the detail view
                vm.showFormDetails = -1;
                angular.element(document.querySelector('#' + form.className + '-container')).removeClass('show-detail');
            } else {
                vm.showFormDetails = index;
                angular.element(document.querySelectorAll('.show-detail')).removeClass('show-detail');
                angular.element(document.querySelector('#' + form.className + '-container')).addClass('show-detail');
            }
            event.stopPropagation();
        }

        vm.cancelByReload = function() {
            $state.reload();
            updateLabels();
        }

        vm.savePrivate = function() {
            vm.policy.$save(vm.policy.private).then(function(ref) {
                vm.toast('success', 'Policy Updated');
                vm.privateIsDirty = false;
            });
        }

        vm.editPolicy = function() {
            if (vm.policy.status.applicationFinished) {
                vm.showEditInstructions = true;
            } else {
                vm.loadForm(vm.policy.forms['application']);
            }
        }

        vm.submitToBuyers = function() {
            var selectedBuyersPlural = (vm.selectedBuyers.length > 1) ? 's' : '';
            sweetAlert.swal({
                title: "Confirm Action",
                text: "The " + vm.selectedBuyers.length + " selected Buyer" + selectedBuyersPlural + " will be notified about this Policy by email, and it will show up in their Available Policies when they log into their iManager account. Click OK to proceed, or Cancel to adjust Buyer list",
                type: "question",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonColor: "#00B200",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    continueSendToBuyers();
                }
            });
        }

        function continueSendToBuyers() {
            vm.sendingToBuyers = true;

            vm.policy.status.outForOffers = true;
            vm.policy.status.outForOffersAt = firebase.database.ServerValue.TIMESTAMP;

            if (!vm.policy.buyers) {
                vm.policy.buyers = [];
            }

            vm.policyCopy = sharedService.cloneObject(vm.policy);

            // Prep the Policy Copy for distribution
            if (!vm.policyCopy.rootKey) {
                vm.policyCopy.rootKey = vm.policyKey;
            }
            vm.policyCopy.parentKey = vm.policyGuid;
            vm.policyCopy.lvl = 2;

            delete vm.policyCopy.$$conf;
            delete vm.policyCopy.$id;
            delete vm.policyCopy.$priority;
            delete vm.policyCopy.$resolved;
            delete vm.policyCopy.createdBy;
            delete vm.policyCopy.createdAt;
            delete vm.policyCopy.createdById;
            delete vm.policyCopy.createdByOrgKey;
            delete vm.policyCopy.caseFiles;
            delete vm.policyCopy.updateLog;
            delete vm.policyCopy.lastUpdatedAt;
            delete vm.policyCopy.lastUpdatedBy;
            delete vm.policyCopy.lastUpdatedById;
            delete vm.policyCopy.status;
            delete vm.policyCopy.offers;
            delete vm.policyCopy.buyers;
            delete vm.policyCopy.publicNotes;
            //delete vm.policyCopy.illustrations;

            // Set initial status for new Policy copy
            vm.policyCopy.status = {
                display: {
                    status: "Received",
                    step: "Received",
                    text: "Received"
                }
            };
            vm.policyCopy.status.receivedFromSupplier = true;
            vm.policyCopy.status.receivedFromSupplierAt = firebase.database.ServerValue.TIMESTAMP;

            // Set new created by/at info that next level will see
            vm.policyCopy.createdBy = vm.currentUser.profile.displayName;
            vm.policyCopy.createdById = vm.currentUser.auth.uid;
            vm.policyCopy.createdAt = firebase.database.ServerValue.TIMESTAMP;
            vm.policyCopy.buyerInterested = true;
            if (vm.currentUser.org) {
                vm.policyCopy.createdByOrgKey = vm.currentUser.org.$id;
                vm.policyCopy.createdByOrgName = vm.currentUser.org.name;
                vm.policyCopy.createdByOrgType = vm.currentUser.org.orgType;
            }

            // Change my own Buyer GUID from "buyer" to "chain" for this copy
            if (vm.currentUser.org && vm.currentUser.org.orgGuid && vm.policyCopy[vm.currentUser.org.orgGuid]) {
                vm.policyCopy[vm.currentUser.org.orgGuid] = vm.policyCopy[vm.currentUser.org.orgGuid].replace('buyer', 'chain');
            }

            if (vm.policyCopy.insured.secondaryInsured && vm.policyCopy.insured.secondaryInsured[0] === 'No') {
                delete vm.policyCopy.secondaryInsured;
            }

            // Prep object for email notifications
            var recipients = [];

            $window.async.forEachOfSeries(vm.selectedBuyers, function(buyer, key, callback) {
                // Skip if Buyer already exists
                if (!vm.policyCopy[buyer.orgGuid]) {
                    // Add reference to new policy to determine buyer access
                    vm.policyCopy[buyer.orgGuid] = "buyer|" + (moment().unix() * 1000).toString();

                    // Generate new GUID for this Buyer/Policy copy
                    let policyGuid = uuidv4();

                    // Add private buyer specific data to the Policy
                    vm.policy.buyers.push({
                        orgGuid: buyer.orgGuid,
                        buyerName: buyer.name ? buyer.name : '',
                        publicGuid: policyGuid,
                        buyerPhone: buyer.phone ? buyer.phone : '',
                        buyerFax: buyer.fax ? buyer.fax : '',
                        buyerEmail: buyer.email ? buyer.email : '',
                        buyerAddress: buyer.address ? buyer.address : null,
                        fileCategoryAccess: buyer.fileCategoryAccess ? buyer.fileCategoryAccess : null,
                        buyerInterested: true,
                        buyerNotes: buyer.notes ? buyer.notes : '',
                        addedAt: moment().unix() * 1000,
                        addedBy: vm.currentUser.profile.displayName,
                        addedById: vm.currentUser.auth.uid
                    });

                    // Add selected Buyer to email notifications object
                    if (buyer.email) {
                        recipients.push({
                            email: buyer.email,
                            template: 'buyer-new-policy-notification',
                            validate: {
                                checkPath: 'orgs/public/' + buyer.orgKey,
                                checkKey: 'status',
                                checkVal: 'active',
                                resendLink: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + buyer.orgKey + '&ic='
                            },
                            subData: {
                                link: 'imanager.' + vm.domain + '.' + vm.tld + '/bdn/policy?pid=' + policyGuid + '&bid=' + buyer.orgGuid,
                                name: buyer.name,
                                email: buyer.email,
                                faceAmount: vm.policyCopy.faceAmount,
                                carrier: vm.policyCopy.carrier,
                                supplierName: vm.currentUser.org.name,
                                supplierEmail: vm.currentUser.org.email,
                                supplierPhone: vm.currentUser.org.phone
                            }
                        });
                    } else {
                        vm.toast('error', 'No email available for Buyer ' + buyer.name);
                    }

                    // Add default access to each case file
                    let buyerCaseFiles = [];
                    if (vm.policy.caseFiles) {
                        vm.policy.caseFiles.forEach(function(caseFile) {
                            delete caseFile.$$hashKey;
                            if (buyer.fileCategoryAccess && buyer.fileCategoryAccess.indexOf(caseFile.category) > -1 && !caseFile.hiddenFromBuyers) {
                                buyerCaseFiles.push(caseFile);
                            }
                        });
                    }

                    let accessIndicator = {
                        'hasAccess': true,
                        'fromOrg': vm.policyCopy.createdByOrgKey
                    };

                    var updates = {};
                    updates['privatePolicies/' + policyGuid] = vm.policyCopy;
                    updates['orgs/' + buyer.orgType + '/' + buyer.orgKey + '/policies/' + policyGuid] = accessIndicator;
                    updates['sharedCaseFiles/' + policyGuid + '/' + buyer.orgGuid] = buyerCaseFiles;
                    root.update(updates).then(function() {
                        delete vm.policyCopy[buyer.orgGuid];
                        callback();
                    }).catch(function(err) {
                        callback(err);
                    });
                }
            }, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Done adding all buyers');
                    vm.policyCopy = null;

                    // Save the new buyer list to the server
                    if (vm.policy.buyers) {
                        vm.policy.buyers.forEach(buyer => {
                            delete buyer.$$hashKey;
                        });
                    } else {
                        vm.policy.buyers = null;
                    }
                    vm.policy.$save(vm.policy.buyers).then(function(ref) {

                        // Send email notifications to all Buyers
                        apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                            if (response.status === 'success') {
                                vm.toast('success', 'All Buyers have been emailed');
                            } else {
                                vm.toast('error', 'Some emails failed, but the Policy is available to all Buyers');
                            }
                            // Update status & buyer list                        
                            vm.policy.status.display = {
                                status: 'Out for Offers',
                                step: 'Out for Offers',
                                text: 'Out for Offers'
                            };
                            vm.policy.status.outForOffers = true;
                            vm.policy.status.outForOffersAt = firebase.database.ServerValue.TIMESTAMP;
                            vm.policy.$save(vm.policy.status).then(function(response) {
                                vm.pickBuyers = false;
                                vm.pickMoreBuyers = false;
                                vm.selectedBuyers = [];
                                vm.sendToAllBuyers = false;
                            }).catch(function(err) {
                                console.log(err);
                                vm.sendingToBuyers = false;
                            });
                        });
                    }).catch(function(err) {
                        console.log(err);
                        vm.sendingToBuyers = false;
                    });
                }
            });
        }

        vm.flagPolicy = function() {
            var data = {
                publicGuid: vm.policyGuid,
                buyerOrgKey: vm.currentUser.org ? vm.currentUser.org.$id : '',
                buyerOrgType: vm.currentUser.org ? vm.currentUser.org.orgType : '',
                supplierOrgKey: vm.policy.createdByOrgKey,
                supplierOrgType: vm.policy.createdByOrgType,
                insuredLastName: 'n/a',
                policyNumber: vm.policy.policyNumber
            };
            policyService.buyerFlagPolicy(data).then(function(response) {
                if (response.status === 'success') {
                    sweetAlert.swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        timer: 1500
                    }).then(function() {});
                } else {
                    console.log(response.message);
                    sweetAlert.swal({
                        title: "Error",
                        text: "There was a problem flagging this policy. Please contact support.",
                        type: "warning"
                    }).then(function() {});
                }
            });
        }

        vm.resendInvite = function() {
            var recipients = [];
            // Add selected Buyer to email notifications object
            var currentBuyerOrgKey = vm.buyers.find(x => x.orgGuid === vm.currentBuyer.orgGuid).orgKey;
            if (vm.currentBuyer.buyerEmail) {
                recipients.push({
                    email: vm.currentBuyer.buyerEmail,
                    template: 'buyer-new-policy-notification',
                    validate: {
                        checkPath: 'orgs/public/' + currentBuyerOrgKey,
                        checkKey: 'status',
                        checkVal: 'active',
                        resendLink: 'imanager.' + vm.domain + '.' + vm.tld + '/register?bok=' + currentBuyerOrgKey + '&ic='
                    },
                    subData: {
                        link: 'imanager.' + vm.domain + '.' + vm.tld + '/bdn/policy?pid=' + vm.policyGuid + '&bid=' + vm.currentBuyer.orgGuid,
                        name: vm.currentBuyer.buyerName,
                        email: vm.currentBuyer.buyerEmail,
                        faceAmount: vm.policy.faceAmount,
                        carrier: vm.policy.carrier,
                        supplierName: vm.currentUser.org.name,
                        supplierEmail: vm.currentUser.org.email,
                        supplierPhone: vm.currentUser.org.phone
                    }
                });
            }
            apiService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                if (response.status === 'success') {
                    vm.toast('success', 'Buyer has been emailed');
                } else {
                    vm.toast('error', 'Some emails failed, but the Policy is available to all Buyers');
                }
            });
        }

        vm.loadForm = function(form) {
            if (form.className === 'medicalPrimary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyGuid, recipient: 'Primary Insured' });
            } else if (form.className === 'medicalSecondary' && (form.status === 'Start Now' || form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('medical', { pid: vm.policyGuid, recipient: 'Secondary Insured' });
            } else if (form.className === 'application' && (form.status === 'In Progress' || form.status === 'Needs Review' || form.status === 'Pending 3rd Party')) {
                $state.go('application', { pid: vm.policyGuid });
            } else if (form.status === 'Complete') {
                window.open(form.downloadURL, '_blank');
            } else if (form.status === 'Pending Upload') {
                window.open(form.downloadURL, '_blank');
            } else {
                window.open(
                    'https://us-central1-lis-pipeline.cloudfunctions.net/viewDocument?documentId=' + form.documentId + '&email=' + encodeURIComponent(vm.currentUser.profile.email),
                    '_blank'
                );
            }
        }

        function adjustStatusImage() {
            vm.statusImage = 'Timeline';
            if (vm.policy.status.applicationSigned) {
                vm.statusImage += '_1';
            }
            if (vm.policy.status.illustrationsReceived) {
                if (vm.policy.status.illustrationsOrdered) {
                    vm.statusImage += '_2B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.illustrationsReceived = false;
                }
            } else if (vm.policy.status.illustrationsOrdered) {
                vm.statusImage += '_2A';
            }
            if (vm.policy.status.medicalsReceived) {
                if (vm.policy.status.medicalsOrdered) {
                    vm.statusImage += '_3B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.medicalsReceived = false;
                }
            } else if (vm.policy.status.medicalsOrdered) {
                vm.statusImage += '_3A';
            }
            if (vm.policy.status.medicalSummaryReceived) {
                if (!vm.policy.status.medicalSummaryRequested && vm.platform === 'isubmit') {
                    vm.policy.status.medicalSummaryReceived = false;
                }
            }
            if (vm.policy.status.outForOffers) {
                if (vm.policy.status.illustrationsReceived && vm.policy.status.medicalsReceived) {
                    if (vm.policy.status.offerInterest) {
                        vm.statusImage = 'Timeline_4Int';
                    } else if (vm.policy.status.offerNoInterest) {
                        vm.statusImage = 'Timeline_4NoInt';
                    } else {
                        vm.statusImage = 'Timeline_4A';
                    }
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.outForOffers = false;
                }
            }
            if (vm.policy.status.offerReceived) {
                if (vm.policy.status.outForOffers) {
                    vm.statusImage = 'Timeline_4B';
                } else if (vm.platform === 'isubmit') {
                    vm.policy.status.offerReceived = false;
                }
            }
        }

        function updateStatus() {
            vm.policy.status.display.text = 'Started';
            if (vm.policy.status.applicationSigned) {
                vm.policy.status.display.text = 'In Progress';
            }
            if (vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Out for Offers';
            }
            if (vm.policy.status.offerNoInterest) {
                vm.policy.status.display.text = 'Case Rejected';
            }
            if (vm.policy.status.offerReceived) {
                vm.policy.status.display.text = 'Offer Received';
            }
            Object.keys(vm.policy.forms).forEach(function(form) {
                if (vm.policy.forms[form].status === 'Pending Signatures') {
                    vm.policy.status.display.text = 'Pending Signatures';
                } else if (vm.policy.forms[form].status === 'Pending 3rd Party') {
                    vm.policy.status.display.text = 'Pending 3rd Party';
                } else if (vm.policy.forms[form].status === 'Needs Review') {
                    vm.policy.status.display.text = 'Pending Agent Review';
                }
            });
            if (vm.policy.actionRequired) {
                vm.policy.status.display.text = 'Agent Action Required';
            }
            if (vm.policy.boActionRequired) {
                vm.policy.status.display.text = 'Back Office Action Required';
                delete vm.policy.status.submittedToBroker;
            }
            if (vm.policy.status.submittedToBroker && !vm.policy.status.outForOffers) {
                vm.policy.status.display.text = 'Pending Broker Review';
            }
            vm.policy.$save(vm.policy.status).then(function(ref) {
                if (vm.managePage && vm.policy.policyConfirmed && vm.policy.buyers) {
                    var updates = {};
                    vm.policy.buyers.forEach(buyer => {
                        updates['privatePolicies/' + buyer.publicGuid + '/status'] = vm.policy.status;
                    });
                    root.update(updates).then(function() {
                        vm.toast('success', 'Policy Updated');
                    });
                } else {
                    vm.toast('success', 'Policy Updated');
                }
            });
        }

        vm.goToRoute = function(route, routeParams) {
            if (routeParams) {
                $state.go(route, routeParams);
            } else {
                $state.go(route);
            }
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        $state.go(route);
                    } else {
                        vm.savePolicy('close');
                    }
                });
            } else {
                $state.go(route);
            }
        }

        vm.formUpload = function(type, className, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                vm.newFiles = [];
                vm.newFiles.push({
                    name: $event.target.files[i].name,
                    type: $event.target.files[i].type,
                    date: $event.target.files[i].lastModified,
                    file: $event.target.files[i]
                });
            }
            $scope.$apply();
            vm.doUpload(type, vm.newFiles, className);
        }

        vm.fileChanged = function(type, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                if (type === 'case') {
                    vm.newCaseFiles.push({
                        name: $event.target.files[i].name,
                        type: $event.target.files[i].type,
                        date: $event.target.files[i].lastModified,
                        file: $event.target.files[i]
                    });
                }
            }
            $scope.$apply();
            if (type === 'case') {
                vm.doUpload(type, vm.newCaseFiles, 'caseFile');
            }
        }

        vm.doUpload = function(type, files, className) {
            vm.filesToLoad = [];
            files.forEach(function(file) {
                vm.filesToLoad.push(file);
            });
            async.forEachOfSeries(vm.filesToLoad, function(fileObj, key, callback) {
                var metadata = {
                    contentType: fileObj.file.type,
                    customMetadata: {
                        'uploadedAt': moment(),
                        'uploadedBy': vm.currentUser.profile.displayName,
                        'uploadedById': vm.currentUser.auth.uid,
                        'uploadedByOrgKey': vm.currentUser.org ? vm.currentUser.org.$id : 'n/a',
                        'policyKey': vm.managePage ? vm.policyKey : null,
                        'policyGuid': vm.managePage ? null : vm.policyGuid,
                        'policyNumber': vm.policy.policyNumber,
                        'formName': null,
                        'className': className
                    }
                };

                var storage = $firebaseStorage(vm.storageRef.child(type).child(fileObj.file.name));
                var uploadTask = storage.$put(fileObj.file, metadata);

                uploadTask.$progress(function(snapshot) {
                    if (snapshot.totalBytes > 0) {
                        fileObj.percentUploaded = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    } else {
                        fileObj.percentUploaded = 100;
                    }
                });

                uploadTask.$complete(function(snapshot) {
                    fileObj.link = snapshot.downloadURL;
                    fileObj.uploadedAt = firebase.database.ServerValue.TIMESTAMP;
                    fileObj.uploadedBy = vm.currentUser.profile.displayName;
                    fileObj.uploadedById = vm.currentUser.auth.uid;
                    fileObj.uploadedByOrgKey = vm.currentUser.org ? vm.currentUser.org.$id : null
                    fileObj.hiddenFromBuyers = true;
                    fileObj.hiddenFromSupplier = true;
                    fileObj.fromBuyer = true;

                    if (!vm.policy.updateLog) {
                        vm.policy.updateLog = [];
                    }

                    vm.policy.updateLog.push({
                        action: 'File uploaded to ' + type + ' tab: ' + fileObj.file.name,
                        updatedAt: firebase.database.ServerValue.TIMESTAMP,
                        updatedBy: vm.currentUser.profile.displayName,
                        updatedById: vm.currentUser.auth.uid
                    });

                    // TODO: Add notifications for new files here...

                    if (type === 'case') {
                        delete fileObj.$$hashKey;
                        vm.privateCaseFiles.$add(fileObj).then(function(ref) {
                            vm.newCaseFiles.splice(fileObj, 1);
                            // STAMP THIS FILE
                            var stampData = {
                                orgKey: vm.currentUser.org ? vm.currentUser.org.$id : null,
                                policyKey: null,
                                policyGuid: vm.policyGuid,
                                policyNumber: vm.policy.policyNumber,
                                className: className,
                                formName: null,
                                fileName: fileObj.file.name,
                                fileKey: ref.key,
                                fileType: type,
                                downloadURL: snapshot.downloadURL
                            };
                            policyService.stampFile(stampData).then(function(response) {
                                if (response.status === 'success') {
                                    var updates = {};
                                    updates['updateLog'] = vm.policy.updateLog;
                                    vm.policyRef.update(updates).then(function(ref) {
                                        callback();
                                    }).catch(function(err) {
                                        console.log(err);
                                        callback();
                                    });
                                } else {
                                    console.log(response);
                                    callback();
                                }
                            });
                        });
                    }
                }, fileObj);
            }, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    vm.toast('success', 'All files uploaded to the server');
                }
            });
        }

        vm.updateFileMetadata = function(type, file, index) {
            var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
            var meta = {
                category: file.category
            };
            vm.privateCaseFiles.$save(file).then(function(ref) {
                storage.$updateMetadata(meta).then(function(updatedMeta) {
                    vm.toast('success', 'File category updated');
                });
            });
        }

        vm.stopClick = function(event) {
            vm.policy.buyerInterested = vm.policy.buyerInterested;
            event.stopPropagation();
        }

        vm.toggleInterest = function(event) {
            vm.policy.buyerInterested = !vm.policy.buyerInterested;
            event.stopPropagation();
            var updates = {};
            updates['privatePolicies/' + vm.policyGuid + '/buyerInterested'] = vm.policy.buyerInterested;
            root.update(updates).then(function() {
                var data = {
                    pk: vm.policy.parentKey,
                    pg: vm.policyGuid,
                    pt: (vm.policy.parentKey === vm.policy.rootKey) ? 'policies' : 'privatePolicies',
                    bi: vm.policy.buyerInterested
                };
                policyService.toggleBuyerInterested(data).then(function(response) {
                    if (response.status === 'success') {
                        vm.toast('success', response.message);
                    } else {
                        console.log(response.message);
                        sweetAlert.swal({
                            title: "Error",
                            text: "There was a problem flagging this policy. Please contact support.",
                            type: "warning"
                        }).then(function() {});
                    }
                });
                //vm.toast('success', 'Interest Updated');
            });
        }

        vm.downloadAll = function(type) {
            //var files = vm.policy.caseFiles;
            var files = vm.sharedCaseFiles;

            if (!files || files.length === 0) {
                vm.toast('error', 'No files to download');
            } else {
                var zip = new JSZip();
                var count = 0;
                var zipFilename = vm.policy.policyNumber + ".zip";

                files.forEach(function(file) {
                    var filename = file.name;
                    // loading a file and add it in a zip file
                    JSZipUtils.getBinaryContent(file.link, function(err, data) {
                        if (err) {
                            throw err;
                        }
                        zip.file(filename, data, { binary: true });
                        count++;
                        if (count == files.length) {
                            zip.generateAsync({ type: "blob" })
                                .then(function(zipFile) {
                                    saveAs(zipFile, zipFilename);
                                });
                        }
                    });
                });
            }
        }

        vm.removeFile = function(type, file, index) {
            if (vm.isIMO || vm.isAdmin || file.uploadedById === vm.currentUser.auth.uid) {
                sweetAlert.swal({
                    title: "Are you sure?",
                    text: "Do you really want to remove this file?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (!result.dismiss) {
                        var storage = $firebaseStorage(vm.storageRef.child(type).child(file.name));
                        var uploadTask = storage.$delete().then(function() {
                            if (!vm.policy.updateLog) {
                                vm.policy.updateLog = [];
                            }

                            vm.policy.updateLog.push({
                                action: 'File removed from ' + type + ' tab: ' + file.name,
                                updatedAt: firebase.database.ServerValue.TIMESTAMP,
                                updatedBy: vm.currentUser.profile.displayName,
                                updatedById: vm.currentUser.auth.uid
                            });

                            if (type === 'case') {
                                var fileId = file.$id;
                                vm.privateCaseFiles.$remove(file).then(function() {
                                    // Also remove this file from any buyer who had access to it
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + fileId] = null;
                                        });
                                    }
                                    // Also remove this file from the Supplier access list
                                    updates['sharedCaseFiles/' + vm.policyGuid + '/' + fileId] = null;
                                    root.update(updates).then(function() {
                                        vm.policyRef.child('updateLog').update(vm.policy.updateLog).then(function() {
                                            vm.toast('success', 'File removed');
                                        });
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        }).catch(function(err) {
                            if (type === 'case') {
                                var fileId = file.$id;
                                vm.privateCaseFiles.$remove(file).then(function() {
                                    // Also remove this file from any buyer who had access to it
                                    if (vm.policy.buyers && vm.policy.buyers.length > 0) {
                                        var updates = {};
                                        vm.policy.buyers.forEach(function(buyer) {
                                            updates['sharedCaseFiles/' + buyer.publicGuid + '/' + fileId] = null;
                                        });
                                    }
                                    // Also remove this file from the Supplier access list
                                    updates['sharedCaseFiles/' + vm.policyGuid + '/' + fileId] = null;
                                    root.update(updates).then(function() {
                                        vm.policyRef.child('updateLog').update(vm.policy.updateLog).then(function() {
                                            vm.toast('success', 'File removed');
                                        });
                                    });
                                }).catch(function(err) {
                                    console.log(err);
                                });
                            }
                        });
                    }
                });
            }
        }

        vm.archivePolicy = function() {
            if (vm.isIMO || vm.isAdmin || file.uploadedById === vm.currentUser.auth.uid) {
                sweetAlert.swal({
                    title: "Are you sure?",
                    text: "Deleted Policies will be archived for some time, but will be purged when resources are needed. See Administrator for Policy Restoration details.",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#B20000",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (!result.dismiss) {
                        vm.policy.isArchived = true;
                        vm.policy.$save(vm.policy).then(function(ref) {
                            vm.toast('success', 'Policy has been Archived');
                            setTimeout(function() {
                                $state.go('inventory');
                            }, 1000);
                        });
                    }
                });
            }
        }

        vm.onSelectCallback = function(input) {
            vm.pageIsDirty = true;
            jQuery('.placeHolder-' + input).addClass('active');
            if (input === 'type' && vm.policy.type !== 'Survivorship Universal Life') {
                vm.policy.secondaryInsured = {};
            } else if (input === 'category') {

            }
        }

        vm.removePublicNote = function(index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this note?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.policy.publicNotes[index].isDeleted = true;
                    vm.policy.$save(vm.policy.publicNotes).then(function() {
                        console.log('Note removed');
                        vm.toast('success', 'Public Note removed');
                    });
                }
            });
        }

        vm.saveNewPublicNote = function() {
            if (!vm.policy.publicNotes) {
                vm.policy.publicNotes = [];
            }
            vm.policy.publicNotes.push({
                createdBy: vm.currentUser.profile.displayName,
                createdById: vm.currentUser.auth.uid,
                createdByRole: vm.currentUser.profile.role,
                createdAt: firebase.database.ServerValue.TIMESTAMP,
                note: vm.newPublicNote
            });
            vm.policy.$save(vm.policy.publicNotes).then(function() {
                console.log('Note saved');
                vm.toast('success', 'Message saved');
                var data = {
                    targets: [],
                    policyKey: vm.policyKey ? null : vm.policy.parentKey,
                    pid: vm.policyGuid ? vm.policyGuid : null,
                    bid: vm.buyerGuid ? vm.buyerGuid : null,
                    lvl: vm.policy.lvl ? vm.policy.lvl : null,
                    createdBy: vm.currentUser.profile.displayName,
                    createdByRole: vm.currentUser.profile.role,
                    note: vm.newPublicNote,
                    LSHID: vm.policy.LSHID,
                    action: 'New Public Note',
                    isUrgent: true
                };
                sendNotification(data);
                vm.newPublicNote = '';
            }).catch(function(err) {
                console.log('Error saving public note on buyer policy');
                console.log(err);
            });
        }

        vm.removeAdminNote = function(index) {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this note?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.policy.private.adminNotes.splice(index, 1);
                    vm.policy.$save(vm.policy.private.adminNotes).then(function() {
                        console.log('Note removed');
                        vm.toast('success', 'Internal Note removed');
                    });
                }
            });
        }

        vm.saveNewAdminNote = function() {
            if (!vm.policy.private) {
                vm.policy.private = {};
            }
            if (!vm.policy.private.adminNotes) {
                vm.policy.private.adminNotes = [];
            }
            vm.policy.private.adminNotes.push({
                createdBy: vm.currentUser.profile.displayName,
                createdById: vm.currentUser.auth.uid,
                createdByRole: vm.currentUser.profile.role,
                createdAt: firebase.database.ServerValue.TIMESTAMP,
                note: vm.newAdminNote
            });
            if (vm.policyGuid === 'NEW') {
                vm.savePolicy('');
            } else {
                vm.policy.$save(vm.policy.private.adminNotes).then(function() {
                    console.log('Note saved');
                    var data = {
                        targets: [],
                        policyKey: vm.policyGuid,
                        createdBy: vm.currentUser.profile.displayName,
                        createdByRole: vm.currentUser.profile.role,
                        note: vm.newAdminNote,
                        LSHID: vm.policy.LSHID,
                        action: 'New Internal Note'
                    };
                    sendNotification(data);
                    vm.newAdminNote = '';
                });
            }
        }

        vm.savePolicy = function(actionAfterSave) {
            vm.actionAfterSave = actionAfterSave;

            if (vm.policyGuid !== 'NEW') {
                vm.policy.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;
                vm.policy.lastUpdatedBy = vm.currentUser.profile.displayName;
                vm.policy.lastUpdatedById = vm.currentUser.auth.uid;

                if (!vm.policy.updateLog) {
                    vm.policy.updateLog = [];
                }

                vm.policy.updateLog.push({
                    action: 'Policy information updated',
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    updatedBy: vm.currentUser.profile.displayName,
                    updatedById: vm.currentUser.auth.uid
                });

                // Convert number text to numbers for sorting
                if (vm.policy.faceAmount) {
                    vm.policy.faceAmount = parseInt(vm.policy.faceAmount);
                }
                if (vm.policy.premium) {
                    vm.policy.premium = parseInt(vm.policy.premium);
                }
                if (vm.policy.estimatedCoiPremium) {
                    vm.policy.estimatedCoiPremium = parseInt(vm.policy.estimatedCoiPremium);
                }

                // Calculate Insured Age
                if (vm.policy.insured.dateOfBirth) {
                    vm.policy.insured.age = vm.calculateAge(vm.policy.insured.dateOfBirth);
                }
                if (vm.policy.secondaryInsured) {
                    if (vm.policy.secondaryInsured.dateOfBirth) {
                        vm.policy.secondaryInsured.age = vm.calculateAge(vm.policy.secondaryInsured.dateOfBirth);
                    }
                }

                vm.policy.$save().then(function(ref) {
                    vm.toast('success', 'Form updated');
                    vm.pageIsDirty = false;
                    if (vm.actionAfterSave) {
                        $state.go('policy', { pid: vm.actionAfterSave });
                    }
                });
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        vm.requestBackOfficeProcessing = function() {
            sweetAlert.swal({
                title: "Process this Policy?",
                text: "This Policy will be sent to the Back Office for processing and will likely result in charges. Are you sure it's ready to send?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.dismiss) {
                    // do nothing
                } else {
                    vm.policy.boActionRequired = true;
                    var data = {
                        targets: [],
                        policyKey: vm.policyGuid,
                        createdBy: vm.currentUser.profile.displayName,
                        createdByRole: vm.currentUser.profile.role,
                        note: 'The Broker has submitted a new Policy for Back Office Processing',
                        action: 'Action Required'
                    };
                    data.targets.push({
                        type: 'ibroker',
                        id: '-L8crXXb_c8NRSJUgRpn'
                    });
                    notifyService.addNotification(data).then(function() {
                        vm.policy.status.sentToBackOffice = true;
                        vm.policy.status.display.text = 'In Progress';
                        updateStatus();
                    });
                }
            });
        }

        vm.requestBrokerReview = function() {
            var data = {
                targets: [],
                policyKey: vm.policyGuid,
                createdBy: vm.currentUser.profile.displayName,
                createdByRole: vm.currentUser.profile.role,
                note: 'The Back Office has submitted the Policy for Broker Review',
                action: 'Action Required'
            };
            data.targets.push({
                type: 'ibroker',
                id: '-L8crXXb_c8NRSJUgRpn'
            });
            notifyService.addNotification(data).then(function() {
                vm.policy.status.submittedToBroker = true;
                vm.policy.status.display.text = 'Pending Broker Review';
                updateStatus();
            });
        }

        // vm.requestMedicalSummary = function() {
        //     firebase.database().ref().child('policies/' + vm.policyGuid + '/adminFiles').orderByChild('category').equalTo('Medical Records').once('value', function(snap) {
        //         if (!snap.exists()) {
        //             sweetAlert.swal({
        //                 title: "Missing Medical Records",
        //                 text: "Please make sure that you have uploaded at least one set of Medical Records to the 'Back-end Files' tab and categorized that file as 'Medical Records'",
        //                 type: "error"
        //             }).then(function() {});
        //         } else {
        //             vm.policy.status.medicalSummaryRequested = true;
        //             updateStatus();
        //         }
        //     });
        // }

        function convertToKg(lbs) {
            return lbs / 2.20462;
        }

        function convertToMeters(ft, inch) {
            var ftin = (ft * 12) + parseInt(inch);
            var m = ftin / 39.3700787;
            return m;
        }

        vm.showMessage = function(type, helpTitle, helpText) {
            if (type === 'Face Amount') {
                sweetAlert.swal({
                    title: "Policy Face Amount",
                    text: "This is the sum paid on the policy's maturity date or on the death of the insured.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Policy Number') {
                sweetAlert.swal({
                    title: "Policy Number",
                    text: "This is the actual Policy reference number that the Carrier uses to identify this Policy.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Date Issued') {
                sweetAlert.swal({
                    title: "Date Issued",
                    text: "The date the Policy was originally written for the insured.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Carrier') {
                sweetAlert.swal({
                    title: "Policy Carrier",
                    text: "This is the company that holds the insurance policy, to which insurance payments are sent and from which death benefits or other payments are received if you file a covered claim.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Illustrations') {
                sweetAlert.swal({
                    title: "Policy Illustrations",
                    text: "Illustrations are a set of projections, prepared by the insurance company. They show how a policy will perform in a given scenario.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'MedicalQ') {
                sweetAlert.swal({
                    title: "Medical Questionnaire",
                    text: "Some Policies include a health questionnaire that the insured fills out and signs. This is not a required form. In this case, the form either will not be provided, or has not been completed yet and will appear here later.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Custom') {
                sweetAlert.swal({
                    title: helpTitle,
                    type: "info",
                    html: helpText
                }).then(function() {});
            } else if (type === 'Confirm Buyer View') {
                sweetAlert.swal({
                    title: "Confirm Buyer View",
                    type: "info",
                    html: "All Buyers will see the same \"General Information\", \"Primary Ailments\" and gender/age/BMI information when they view this Policy.<br><br>\"Medical Summaries\", \"Pricing Reports\" and \"Life Expectancies\" can be ordered from this page, but won't be available to Buyers unless specified in the \"Case Files\" tab on the previous (Policy) page."
                }).then(function() {});
            } else if (type === 'LSHID') {
                sweetAlert.swal({
                    title: "Policy ID",
                    type: "info",
                    html: "This is the internal Policy Identifier on the LS Hub platform. Use this as a reference when contacting Support or a Buyer/Supplier about a case."
                }).then(function() {});
            }
        }

        function togglePanelHighlight(panelId, active) {
            if (active) {
                jQuery('#' + panelId).addClass('highlight-panel');
                jQuery('.highlight-cover').each(function(e) {
                    jQuery(this).show();
                });
            } else {
                jQuery('#' + panelId).removeClass('highlight-panel');
                jQuery('.highlight-cover').each(function(e) {
                    jQuery(this).hide();
                });
            }
        }
    }

    angular.module('yapp')
        .controller('BuyerPolicyCtrl', BuyerPolicyCtrl);
})();