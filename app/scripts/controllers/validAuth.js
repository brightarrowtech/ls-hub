(function() {
    'use strict';

    /* @ngInject */
    function ValidAuthCtrl(VER, ENV, toastr, $window, $scope, $state, $stateParams, sweetAlert, $firebaseArray, Auth, authService, currentAuth, $timeout, brandingDomain) {
        var vm = this;
        vm.showContent = false;
        vm.csb = false;

        vm.version = VER;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.environment = '';
        if (ENV === 'demo') {
            vm.environment = ' - Demo';
        } else if (ENV === 'dev') {
            vm.environment = ' - Dev';
        }

        if (vm.subdomain.indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        } else if (vm.subdomain.indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        }

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;

            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                vm.showContent = true;
            }

            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }

            vm.orgNotificationCount = 0;
            vm.buyerNotificationCount = 0;
            vm.userNotificationCount = 0;
            vm.notificationCount = 0;

            if (vm.currentUser && vm.currentUser.profile.role === 'IMO') {
                vm.isIMO = true;
            } else {
                vm.isInvestor = true;
            }

            var root = firebase.database().ref();

            // Check for new Supplier Invitations
            if (vm.currentUser.org) {
                vm.supplierInvites = $firebaseArray(firebase.database().ref('orgs/' + vm.currentUser.org.orgType + '/' + vm.currentUser.org.$id + '/invitedTo'));
                vm.supplierInvites.$loaded().then(function(org) {
                    if (org.length > 0) {
                        var plural = (org.length === 1) ? 'Invitation' : 'Invitations';
                        sweetAlert.swal({
                            title: "New Suppliers",
                            text: "You have " + org.length + " new Supplier " + plural + " waiting for you to review. Click OK to go there now!",
                            type: "info",
                            showCancelButton: true,
                            confirmButtonText: "OK",
                            cancelButtonText: "Not Now"
                        }).then(function(result) {
                            if (!result.dismiss) {
                                $state.go('buyers', { tab: 3 });
                            }
                        });
                    }
                }).catch(function(err) {
                    console.log(err);
                });
            }

            // Check for any new user notifications
            root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.auth.uid).on('value', function(userNotifications) {
                vm.notificationCount -= vm.userNotificationCount;
                vm.userNotificationCount = 0;
                if (userNotifications.exists()) {
                    userNotifications.forEach(function(n) {
                        let notification = n.val();
                        if (notification.isUnread) {
                            vm.userNotificationCount++;
                        }
                    });
                }
                vm.notificationCount += vm.userNotificationCount;
                $scope.$apply();
            });

            // Check for any new org notifications
            if (vm.currentUser.org) {
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.$id).on('value', function(orgNotifications) {
                    vm.notificationCount -= vm.orgNotificationCount;
                    vm.orgNotificationCount = 0;
                    if (orgNotifications.exists()) {
                        orgNotifications.forEach(function(n) {
                            let notification = n.val();
                            if (notification.isUnread) {
                                var processNotification = true;
                                if (vm.currentUser.profile.role === 'Back Office' || vm.currentUser.profile.role === 'iBroker') {
                                    if (notification.createdByRole === vm.currentUser.profile.role) {
                                        processNotification = false;
                                    }
                                }
                                if (processNotification) {
                                    vm.orgNotificationCount++;
                                }
                            }
                        });
                    }
                    vm.notificationCount += vm.orgNotificationCount;
                    $scope.$apply();
                });
            }

            // Check for any new buyer notifications
            if (vm.currentUser.org && vm.currentUser.org.orgGuid) {
                root.child('notifications').orderByChild('targetId').equalTo(vm.currentUser.org.orgGuid).on('value', function(orgNotifications) {
                    vm.notificationCount -= vm.buyerNotificationCount;
                    vm.buyerNotificationCount = 0;
                    if (orgNotifications.exists()) {
                        orgNotifications.forEach(function(n) {
                            let notification = n.val();
                            if (notification.isUnread) {
                                var processNotification = true;
                                if (vm.currentUser.profile.role === 'Back Office' || vm.currentUser.profile.role === 'iBroker') {
                                    if (notification.createdByRole === vm.currentUser.profile.role) {
                                        processNotification = false;
                                    }
                                }
                                if (processNotification) {
                                    vm.buyerNotificationCount++;
                                }
                            }
                        });
                    }
                    vm.notificationCount += vm.buyerNotificationCount;
                    $scope.$apply();
                });
            }

            // Check for any new public alerts
            root.child('alerts').child(vm.currentUser.profile.alertId).on('value', function(snap) {
                if (snap.exists()) {
                    snap.forEach(function(notice) {
                        toastr['info']('New alert: ' + notice.val().message);
                        if (notice.val().type === 'thirdPartyComplete') {
                            var publicGuid = notice.val().publicGuid;
                            // Get Policy Key
                            root.child('policies').orderByChild('policyNumber').equalTo(notice.val().policyNumber).once('value', function(policy) {
                                if (policy.exists()) {
                                    policy.forEach(function(p) {
                                        var policyKey = p.key;
                                        root.child('policies').child(policyKey).child('status/display').update({
                                            text: 'Pending Agent Review'
                                        });
                                        root.child('policies').child(policyKey).child('forms').orderByChild('publicGuid').equalTo(publicGuid).once('value', function(snap) {
                                            if (snap.exists()) {
                                                snap.forEach(function(form) {
                                                    form.ref.update({
                                                        status: 'Needs Review'
                                                    }).then(function(response) {
                                                        // Remove notice
                                                        root.child('alerts').child(vm.currentUser.profile.alertId).child(notice.key).remove();
                                                    });
                                                });
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });

        vm.switchDomains = function() {
            var otherDomain = (vm.subdomain.indexOf('isubmit') > -1) ? 'iManager' : 'iSubmit';
            sweetAlert.swal({
                title: "Are you sure?",
                text: "You will be redirected to the " + otherDomain + " platform. Do you wish to continue?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#00B200",
                confirmButtonText: "Yes, go to " + otherDomain,
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.mintingToken = true;
                    authService.mintToken().then(function(response) {
                        if (response.data && response.data.status === 'success') {
                            var subTag = '-local';
                            var port = ':3000';
                            // var subTag = (ENV === 'demo') ? '-demo' : '';
                            // var port = '';
                            var switchProxy = '';
                            if (vm.subdomain.indexOf('imanager') > -1) {
                                switchProxy = 'http://isubmit' + subTag + '.' + vm.domain + '.' + vm.tld + port + '/switch?tk=' + response.data.data;
                            } else if (vm.subdomain.indexOf('isubmit') > -1) {
                                switchProxy = 'http://imanager' + subTag + '.' + vm.domain + '.' + vm.tld + port + '/switch?tk=' + response.data.data;
                            }
                            $timeout(function() {
                                window.location.href = switchProxy;
                                return false;
                            }, 750);
                        } else {
                            toastr['error']('Error generating auth token');
                            vm.mintingToken = false;
                            $scope.$apply();
                        }
                    });
                }
            });
        }

        vm.removeImpersonation = function() {
            var data = {
                admin: currentAuth.uid,
                uid: '',
                active: false
            };
            authService.impersonateUser(data).then(function(response) {
                if (response.status === 'success') {
                    sweetAlert.swal({
                        title: "Success",
                        text: response.message,
                        type: "success",
                        timer: 1000
                    }).then(function() {});
                    $scope.$apply();
                } else {
                    console.log(response);
                }
            });
        }

        vm.toggleSidebar = function() {
            vm.csb = !vm.csb;
            jQuery('body').toggleClass('csb');
        }

        vm.logout = function() {
            $state.go('logout');
        }

        vm.printAgreement = function() {
            var mywindow = window.open('', 'PRINT');

            mywindow.document.write('<html><head><title>' + document.title + '</title>');
            mywindow.document.write('<style type="text/css">h1 { font-size: 20px; } ol.numbered > li { margin-top:15px; } ol.lettered > li { list-style-type: lower-roman; }</style>')
            mywindow.document.write('</head><body >');
            mywindow.document.write(document.getElementById('brokerAgreement').innerHTML);
            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
        }

        vm.closeTab = function() {
            window.close();
        }

    }
    angular.module('yapp')
        .controller('ValidAuthCtrl', ValidAuthCtrl);
})();