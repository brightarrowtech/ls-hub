(function() {
    'use strict';

    /* @ngInject */
    function ApplicationCtrl(policyService, $window, stdData, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, pandadocService, currentAuth, sweetAlert, apiService, notifyService, brandingDomain, sharedService) {
        var vm = this;
        vm.statusImage = 'Timeline';
        vm.showPage = 1;
        vm.isCreator = false;
        vm.pageIsDirty = false;

        var setPathObj = {};

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        var root = firebase.database().ref();

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (vm.currentUser && vm.currentUser.profile.isAdmin) {
                vm.isAdmin = true;
            }
            vm.user = $firebaseObject(root.child('users/' + vm.currentUser.auth.uid));
            vm.user.$loaded().then(function(user) {
                if (vm.currentUser.profile.myUpline && vm.currentUser.profile.myUpline.length === 1) {
                    vm.uplineOrg = vm.currentUser.profile.myUpline[0];
                } else if (!vm.currentUser.profile.myUpline) {
                    vm.uplineOrg = 'n/a';
                }
                loadPage();
            });
        });

        vm.states = stdData.states;
        vm.genders = stdData.genders;
        vm.maritalStatus = stdData.maritalStatus;
        vm.types = stdData.policyTypes;
        vm.yesNo = stdData.yesNo;
        vm.yesNoUnsure = stdData.yesNoUnsure;
        vm.beneTypes = stdData.beneTypes;
        vm.activitiesNeedingAssistance = [
            'Meal Planning',
            'Taking Medications',
            'Shopping',
            'Walking',
            'Bathing',
            'Dressing'
        ];
        vm.heightRegex = /^(3-7)'(?:\s*(?:1[01]|0-9)(''|"))?$/

        function loadPage() {
            jQuery('*[required]').each(function(idx) {
                jQuery(this).on('change', function() {
                    //var dataPath = jQuery(this).attr('ng-model');
                    var dataField = jQuery(this).attr('ng-model').replace('vm.policy.', '');
                    var dataVal = resolvePath(dataField, vm.policy);
                    if (dataVal === undefined) {
                        setPath(vm.policy, dataField.split('.'), '');
                    }
                    jQuery(this).parents('.form-group').removeClass('missing-req');
                });
            });

            // vm.suppliers = $firebaseArray(root.child('funderGroup/' + '-L1T77aG2BKJ5p6UH6PR' + '/suppliers'));
            // vm.suppliers.$loaded().then(function(suppliers) {});

            if ($state.params.pid) {
                /**
                 * LOADING AN EXISTING POLICY
                 */
                vm.policyKey = $state.params.pid;
                vm.policy = $firebaseObject(root.child('policies').child(vm.policyKey));
                vm.policy.$loaded().then(function(policy) {
                    vm.storage = firebase.storage();
                    vm.storageRef = vm.storage.ref('policies/' + vm.policyKey);
                    if (!vm.policy.secondaryInsured) {
                        vm.policy.secondaryInsured = {};
                    }
                    if (!vm.policy.medicalDetails) {
                        vm.policy.medicalDetails = {};
                    }
                    if((vm.policy.medicalDetails 
                        && vm.policy.medicalDetails.diagnoses == null) || vm.policy.medicalDetails == null){
                        Object.assign(vm.policy.medicalDetails, {
                            diagnoses: [{
                                diagnosis: "",
                                dateOfDiagnosis: "",
                                treatmentReceived: "",
                                dateOfTreatment: "",
                                results: "",
                            }]
                        })
                    }
                    if(vm.policy.medicalDetails
                        && vm.policy.medicalDetails.familyHistory == null){
                            Object.assign(vm.policy.medicalDetails,
                                {
                                    familyHistory: [{
                                        relationship: "",
                                        healthConditions: "",
                                        ageDeceased: ""
                                    }]
                                })
                    }
                    if(vm.policy.medicalDetails
                        && vm.policy.medicalDetails.medications == null){
                            Object.assign(vm.policy.medicalDetails,
                                {
                                    medications: []
                                }
                            )
                    }
                    if (!vm.policy.status) {
                        vm.policy.status = {};
                    } else if (vm.policy.status.applicationInProgress) {
                        vm.showPage = 2;
                        if ($state.params.step) {
                            vm.showPage = parseInt($state.params.step);
                        }
                    }
                    if (vm.policy.createdById === vm.currentUser.auth.uid) {
                        vm.isCreator = true;
                        if (!vm.policy.private) {
                            vm.policy.private = {};
                        }
                        if (!vm.policy.adminFiles) {
                            vm.policy.adminFiles = [];
                        }
                    }
                    if (!vm.policy.caseFiles) {
                        vm.policy.caseFiles = [];
                    }
                    if (!vm.policy.forms) {
                        vm.policy.forms = {};
                    }
                    if (!vm.policy.forms.application) {
                        vm.policy.forms.application = {
                            formName: 'Application Form',
                            className: 'application',
                            status: 'In Progress',
                            publicGuid: uuidv4(),
                            recipients: {
                                insured: {
                                    fullName: vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName,
                                    firstName: vm.policy.insured.firstName,
                                    lastName: vm.policy.insured.lastName
                                },
                                secondaryInsured: {},
                                owner: {},
                                agent: {
                                    fullName: vm.currentUser.profile.displayName,
                                    emailAddress: vm.currentUser.profile.email
                                }
                            }
                        };
                    } 
                    if (!vm.policy.forms.application.publicGuid) {
                        vm.policy.forms.application.publicGuid = uuidv4();
                    }
                    if (!vm.policy.forms.application.recipients) {
                        vm.policy.forms.application.recipients = {
                            insured: {
                                fullName: vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName,
                                firstName: vm.policy.insured.firstName,
                                lastName: vm.policy.insured.lastName
                            },
                            secondaryInsured: {},
                            owner: {}
                        }
                    }
                    if (!vm.policy.forms.application.recipients.agent) {
                        // Agent should be "createdBy" person for this Policy
                        vm.policy.forms.application.recipients.agent = {
                            fullName: vm.policy.createdBy
                        };
                    }
                    if (!vm.policy.forms.medicalPrimary && vm.platform === 'isubmit') {
                        vm.policy.forms.medicalPrimary = {
                            formName: 'Medical Questionnaire (Primary)',
                            className: 'medicalPrimary',
                            status: 'Start Now'
                        };
                    }
                    if (vm.policy.forms.application.status === 'Pending 3rd Party' || vm.policy.forms.application.status === 'Needs Review') {
                        // Go get whatever data the 3rd party has provided so far and merge it into the Policy
                        root.child('publicForms').child(vm.policy.forms.application.publicGuid).once('value', function(snap) {
                            if (snap.exists()) {
                                if (!vm.policy.formBackup) {
                                    var backupData = sharedService.cloneObject(vm.policy);
                                    delete backupData.$$conf;
                                    delete backupData.$$hashKey;
                                    delete backupData.$id;
                                    delete backupData.$priority;
                                    delete backupData.$resolved;
                                    var thirdPartyFormData = snap.val();
                                    thirdPartyFormData.formBackup = true;
                                    thirdPartyFormData.forms.application.recipients = sharedService.cloneObject(vm.policy.forms.application.recipients);

                                    root.child('formBackups').child(vm.policyKey).set(backupData).then(function() {
                                        root.child('policies').child(vm.policyKey).update(thirdPartyFormData).then(function() {
                                            vm.toast('success', '3rd Party Data Merged');
                                            vm.pageIsDirty = false;
                                            jQuery('body').off('ifChecked', 'input');
                                            jQuery('body').off('ifUnchecked', 'input');
                                            updateLabels();
                                        }).catch(function(err) {
                                            console.log(err.message);
                                            jQuery('body').off('ifChecked', 'input');
                                            jQuery('body').off('ifUnchecked', 'input');
                                            updateLabels();
                                        });
                                    });
                                } else {
                                    var thirdPartyFormData = snap.val();
                                    thirdPartyFormData.forms.application.recipients = sharedService.cloneObject(vm.policy.forms.application.recipients);
                                    root.child('policies').child(vm.policyKey).update(thirdPartyFormData).then(function() {
                                        vm.toast('success', '3rd Party Data Merged');
                                        vm.pageIsDirty = false;
                                        jQuery('body').off('ifChecked', 'input');
                                        jQuery('body').off('ifUnchecked', 'input');
                                        updateLabels();
                                    }).catch(function(err) {
                                        console.log(err.message);
                                        jQuery('body').off('ifChecked', 'input');
                                        jQuery('body').off('ifUnchecked', 'input');
                                        updateLabels();
                                    });
                                }
                            } else {
                                jQuery('body').off('ifChecked', 'input');
                                jQuery('body').off('ifUnchecked', 'input');
                                updateLabels();
                            }
                        });
                    } else {
                        jQuery('body').off('ifChecked', 'input');
                        jQuery('body').off('ifUnchecked', 'input');
                        updateLabels();
                    }
                });
            } else {
                /**
                 * CREATING A NEW POLICY
                 */
                vm.policy = {
                    policyIsActive: true
                };
                vm.policy.adminFiles = [];
                vm.policy.caseFiles = [];
                vm.policy.insured = {};
                
                vm.policy.medicalDetails = {
                    diagnoses: [{
                        diagnosis: "",
                        dateOfDiagnosis: "",
                        treatmentReceived: "",
                        dateOfTreatment: "",
                        results: "",
                    }],
                    familyHistory: [{
                        relationship: "",
                        healthConditions: "",
                        ageDeceased: ""
                    }],
                    medications: []
                }
                vm.policy.secondaryInsured = {};
                vm.policy.private = {};
                vm.policy.status = {};
                vm.policy.owner = {};
                vm.policy.forms = {
                    application: {
                        formName: 'Application Form',
                        className: 'application',
                        status: 'In Progress',
                        publicGuid: uuidv4(),
                        recipients: {
                            insured: {},
                            secondaryInsured: {},
                            owner: {},
                            agent: {
                                fullName: vm.currentUser.profile.displayName,
                                emailAddress: vm.currentUser.profile.email
                            }
                        }
                    }
                };
                if (vm.platform === 'isubmit') {
                    vm.policy.forms.medicalPrimary = {
                        formName: 'Medical Questionnaire (Primary)',
                        className: 'medicalPrimary',
                        status: 'Start Now',
                        publicGuid: uuidv4()
                    };
                }
                vm.policyKey = 'NEW';
                jQuery('body').off('ifChecked', 'input');
                jQuery('body').off('ifUnchecked', 'input');
                updateLabels();
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('.application .page-panel .jvFloat').length === 0) {
                    jQuery('.float-me').jvFloat();
                }
                jQuery('.application .placeHolder').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('.application .pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('.flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    allowInput: true,
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        instance.element.value = dateStr;
                        instance.element.dispatchEvent(new Event('change', { 'bubbles': true }));
                        console.log(dateStr, instance, parts)
                        var compStr = '';
                        if (dateStr !== compStr) {
                            vm.pageIsDirty = true;
                        }
                        jQuery('.placeHolder[for="' + instance.element.id + '"]').addClass('active');
                        instance.close();
                    }
                });
                jQuery('body').on('ifChecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = true;
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = true;
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        if (!vm.policy.secondaryInsured) {
                            vm.policy.secondaryInsured = {};
                        }
                        vm.policy.secondaryInsured.isDeceased = true;
                    }
                    if (event.target.required) {
                        jQuery(event.target).parents('.form-group').removeClass('missing-req');
                    }
                    $scope.$apply();
                });
                jQuery('body').on('ifUnchecked', 'input', function(event) {
                    vm.pageIsDirty = true;
                    if (event.target.name === 'hasCertifiedLE') {
                        vm.policy.private.hasCertifiedLE = false;
                    } else if (event.target.name === 'policyIsActive') {
                        vm.policy.policyIsActive = false;
                    } else if (event.target.name === 'secondaryInsuredDeceased') {
                        vm.policy.secondaryInsured.isDeceased = false;
                    }
                    $scope.$apply();
                });
                // document.getElementById('scrollContainer').scrollTo(0, 0);
                $scope.$apply();
            }, 10);
        }

        function getNextUplineOrg(currentOrgKey) {
            root.child('orgs/public/' + currentOrgKey).once('value', function(snap) {
                if (snap.exists()) {
                    var org = snap.val();
                    if (org.uplineOrgKey) {
                        // This is an upline chain org. Add it and continue looking up
                        vm.uplineOrgArr.push({
                            key: currentOrgKey,
                            type: org.orgType
                        });
                        getNextUplineOrg(org.uplineOrgKey);
                    } else {
                        // This is the last upline in the chain...make it the Broker
                        vm.uplineOrgArr.push({
                            key: currentOrgKey,
                            type: org.orgType
                        });
                        gotAllOrgs();
                    }
                } else {
                    gotAllOrgs();
                }
            });
        }

        vm.beginApplication = function() {
            vm.beginningApp = true;
            if (!vm.policy.policyNumber || !vm.policy.insured.firstName || !vm.policy.insured.lastName || !vm.policy.state || !vm.uplineOrg) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "All fields are required to start an application",
                    type: "error"
                }).then(function() {
                    vm.beginningApp = false;
                    $scope.$apply();
                });
            } else if (vm.policyKey === 'NEW') {
                vm.policy.insured.fullName = vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName;

                // Link this policy with the Upline Orgs all the way to the top
                vm.uplineOrgArr = [];
                // If the user owns an Org, add it to the access list so other Org Admins can see it regardless of their linked Upline(s)
                if (vm.currentUser.profile.myOrg) {
                    vm.uplineOrgArr.push({
                        key: vm.currentUser.profile.myOrg.orgKey,
                        type: vm.currentUser.profile.myOrg.orgType
                    });
                }

                if (vm.uplineOrg !== 'n/a') {
                    // Get the Upline hierarchy recursively
                    getNextUplineOrg(vm.uplineOrg.orgKey);
                } else {
                    gotAllOrgs();
                }
            }
        }

        function gotAllOrgs() {
            vm.policy.upline = [];

            vm.uplineOrgArr.forEach(function(org) {
                vm.policy[org.key] = org.type;
                vm.policy.upline.push({
                    orgKey: org.key,
                    orgType: org.type
                });
            });

            if (vm.uplineOrg === 'n/a') {
                vm.policy.submittedTo = {
                    orgKey: vm.currentUser.profile.myOrg.orgKey,
                    name: vm.currentUser.org.name
                };
                setSubmittedTo();
            } else {
                vm.policy.submittedTo = {
                    orgKey: vm.uplineOrg.orgKey,
                    name: vm.uplineOrg.name
                };
                setSubmittedTo();
            }
        }

        function setSubmittedTo() {
            console.warn('SET SUBMITTED TO')
            vm.policy.createdAt = firebase.database.ServerValue.TIMESTAMP;
            vm.policy.createdBy = vm.currentUser.profile.displayName;
            vm.policy.createdById = vm.currentUser.auth.uid;
            if (vm.currentUser.profile.myOrg) {
                vm.policy.createdByOrgKey = vm.currentUser.profile.myOrg.orgKey;
                vm.policy.createdByOrgType = vm.currentUser.profile.myOrg.orgType;
            }
            vm.policy.agent = {};
            vm.policy.forms.application.recipients.insured = {
                firstName: vm.policy.insured.firstName,
                lastName: vm.policy.insured.lastName,
                fullName: vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName
            };
            vm.policy.status.caseReceived = true;
            vm.policy.status.caseReceivedAt = firebase.database.ServerValue.TIMESTAMP;
            vm.policy.status.applicationInProgress = true;
            vm.policy.status.display = {
                step: 'Application',
                status: 'In Progress',
                text: vm.platform === 'isubmit' ? 'Started' : 'In Progress'
            };
            vm.policy.createdOnPlatform = vm.subdomain.indexOf('imanager') > -1 ? 'imanager' : 'isubmit';
            vm.statusImage = 'Timeline_1';
            delete vm.policy.state.$$hashKey;

            vm.policy.latestStatus = "No updates have been posted here yet";
            vm.policy.statusSelection = [{
                class: 'success',
                status: 'On Track'
            }];

            vm.policy.LSHID = 'L' + moment().format('X').toString().substr(2, 8);

            vm.policy = JSON.parse(angular.toJson(vm.policy))

            root.child('policies').push(vm.policy).then(function(ref) {
                vm.policyKey = ref.key;

                $window.async.forEachOfSeries(vm.uplineOrgArr, function(org, key, callback) {
                    // Associate this Policy with all Upline Orgs
                    var data = {
                        uid: vm.currentUser.auth.uid,
                        org: org,
                        policyKey: vm.policyKey
                    };
                    policyService.setPolicyPermissions(data).then(function(response) {
                        if (response.status === 'success') {
                            callback();
                        } else {
                            callback(response.message);
                        }
                    });
                }, function(err) {
                    if (err) {
                        console.log(err);
                        vm.beginningApp = false;
                        $scope.$apply();
                    } else {
                        // Save new policy reference to user's profile
                        if (!vm.user.myPolicies) {
                            vm.user.myPolicies = {};
                        }
                        vm.user.myPolicies[vm.policyKey] = {
                            policyKey: vm.policyKey,
                            policyNumber: vm.policy.policyNumber,
                            insured: {
                                firstName: vm.policy.insured.firstName,
                                lastName: vm.policy.insured.lastName
                            },
                            state: vm.policy.state
                        };
                        vm.user.$save().then(function(response) {
                            vm.policy = $firebaseObject(root.child('policies').child(ref.key));
                            vm.policy.$loaded().then(function(policy) {
                                vm.storage = firebase.storage();
                                vm.storageRef = vm.storage.ref('policies/' + vm.policyKey);
                                if (!vm.policy.secondaryInsured) {
                                    vm.policy.secondaryInsured = {};
                                }
                                if (vm.currentUser.auth.uid === vm.policy.createdById) {
                                    if (!vm.policy.private) {
                                        vm.policy.private = {};
                                    }
                                    if (!vm.policy.adminFiles) {
                                        vm.policy.adminFiles = [];
                                    }
                                }
                                if (!vm.policy.caseFiles) {
                                    vm.policy.caseFiles = [];
                                }
                                vm.toast('success', 'Application Saved');
                                vm.pageIsDirty = false;
                                vm.showPage = 2;
                            });
                        });
                    }
                });
            }).catch(function(err) {
                console.log(err);
                vm.beginningApp = false;
                $scope.$apply();
            });
        }

        vm.submitApplication = function() {
            if (vm.platform === 'imanager' && vm.policy.forms.application.status === 'Pending Upload') {
                $state.go('policy', { 'pid': vm.policyKey });
            } else {
                /**
                 * Validate all required fields
                 */
                var requiredFields = [{
                    field: 'insured.firstName',
                    page: 2
                }, {
                    field: 'insured.lastName',
                    page: 2
                }, {
                    field: 'insured.gender',
                    page: 2
                }, {
                    field: 'insured.dateOfBirth',
                    page: 2
                }, {
                    field: 'insured.ssn',
                    page: 2
                }, {
                    field: 'insured.address.address',
                    page: 2
                }, {
                    field: 'insured.phoneNumber',
                    page: 2
                }, {
                    field: 'insured.primaryPhysicianName',
                    page: 2
                }, {
                    field: 'insured.primaryPhysicianPhone',
                    page: 2
                }, {
                    field: 'carrier',
                    page: 3
                }, {
                    field: 'policyNumber',
                    page: 2
                }, {
                    field: 'faceAmount',
                    page: 3
                }, {
                    field: 'policyType',
                    page: 3
                }, {
                    field: 'isInsuredAlsoOwner',
                    page: 4
                }, {
                    field: 'state',
                    page: 2
                }];

                if (vm.platform === 'imanager') {
                    requiredFields = [];
                }

                var missingReqOnPage = 0;
                for (let index = 0; index < requiredFields.length; index++) {
                    const req = requiredFields[index];
                    var reqVal = resolvePath(req.field, vm.policy);
                    if (!reqVal) {
                        console.log('Missing ' + req.field + ' on page ' + req.page);
                        if (missingReqOnPage === 0) {
                            missingReqOnPage = req.page;
                        }
                        jQuery('#' + req.field.replace(/\./g, '_')).addClass('missing-req');
                    } else {
                        jQuery('#' + req.field.replace(/\./g, '_')).removeClass('missing-req');
                    }
                }
                if (missingReqOnPage > 0) {
                    vm.toast('error', 'Some Required Fields are Missing');
                    vm.goToPage(missingReqOnPage);
                } else {
                    // Update policy status to "Application Finished"
                    vm.submittingApplication = true;

                    if (vm.platform === 'imanager') {
                        vm.signMethod = 'n/a';
                        vm.policy.forms.application.status = 'Pending Upload';
                    }

                    if (vm.signMethod === 'esign') {
                        vm.policy.forms.application.signMethod = 'E-Signature';
                        if (vm.policy.forms.application.recipients.insured && !vm.policy.forms.application.recipients.insured.emailAddress) {
                            sweetAlert.swal({
                                title: "Uh Oh!",
                                text: "Insured Email cannot be blank when using e-signature",
                                type: "error"
                            }).then(function() {});
                        } else if (vm.policy.forms.application.recipients.secondaryInsured && !vm.policy.forms.application.recipients.secondaryInsured.emailAddress) {
                            sweetAlert.swal({
                                title: "Uh Oh!",
                                text: "Secondary Insured Email cannot be blank when using e-signature",
                                type: "error"
                            }).then(function() {});
                        } else if (vm.policy.forms.application.recipients.owner && !vm.policy.forms.application.recipients.owner.emailAddress) {
                            sweetAlert.swal({
                                title: "Uh Oh!",
                                text: "Owner Email cannot be blank when using e-signature",
                                type: "error"
                            }).then(function() {});
                        } else if (vm.policy.forms.application.recipients.agent && !vm.policy.forms.application.recipients.agent.emailAddress) {
                            sweetAlert.swal({
                                title: "Uh Oh!",
                                text: "Agent Email cannot be blank when using e-signature",
                                type: "error"
                            }).then(function() {});
                        }
                    } else if (vm.signMethod = 'printAndSign') {
                        vm.policy.forms.application.signMethod = 'Print and Sign';
                        vm.policy.forms.application.createdDtTm = firebase.database.ServerValue.TIMESTAMP;
                    }

                    var recipients = [];
                    var templateIdx = 0;

                    if (vm.platform === 'isubmit') {
                        if (vm.policy.forms.application.recipients.insured) {
                            recipients.push({
                                email: vm.policy.forms.application.recipients.insured.emailAddress ? vm.policy.forms.application.recipients.insured.emailAddress : '',
                                first_name: vm.policy.forms.application.recipients.insured.firstName,
                                last_name: vm.policy.forms.application.recipients.insured.lastName,
                                role: 'Primary Insured'
                            });
                            if (!vm.policy.forms.medicalPrimary) {
                                vm.policy.forms.medicalPrimary = {
                                    formName: 'Medical Questionnaire (Primary)',
                                    className: 'medicalPrimary',
                                    status: 'Start Now',
                                    recipient: {
                                        firstName: vm.policy.forms.application.recipients.insured.firstName,
                                        lastName: vm.policy.forms.application.recipients.insured.lastName,
                                        fullName: vm.policy.forms.application.recipients.insured.firstName + ' ' + vm.policy.forms.application.recipients.insured.lastName,
                                        emailAddress: vm.policy.forms.application.recipients.insured.emailAddress ? vm.policy.forms.application.recipients.insured.emailAddress : ''
                                    }
                                };
                            }
                        }
                        if (vm.policy.forms.application.recipients.secondaryInsured) {
                            recipients.push({
                                email: vm.policy.forms.application.recipients.secondaryInsured.emailAddress ? vm.policy.forms.application.recipients.secondaryInsured.emailAddress : '',
                                first_name: vm.policy.forms.application.recipients.secondaryInsured.firstName,
                                last_name: vm.policy.forms.application.recipients.secondaryInsured.lastName,
                                role: 'Secondary Insured'
                            });
                            vm.policy.secondaryInsured.fullName = vm.policy.forms.application.recipients.secondaryInsured.firstName + ' ' + vm.policy.forms.application.recipients.secondaryInsured.lastName;
                            templateIdx += 2;
                            if (!vm.policy.forms.medicalSecondary) {
                                vm.policy.forms.medicalSecondary = {
                                    formName: 'Medical Questionnaire (Secondary)',
                                    className: 'medicalSecondary',
                                    status: 'Start Now',
                                    recipient: {
                                        firstName: vm.policy.forms.application.recipients.secondaryInsured.firstName,
                                        lastName: vm.policy.forms.application.recipients.secondaryInsured.lastName,
                                        fullName: vm.policy.secondaryInsured.fullName,
                                        emailAddress: vm.policy.forms.application.recipients.secondaryInsured.emailAddress ? vm.policy.forms.application.recipients.secondaryInsured.emailAddress : ''
                                    }
                                };
                            }
                        }
                        if (vm.policy.forms.application.recipients.owner) {
                            recipients.push({
                                email: vm.policy.forms.application.recipients.owner.emailAddress ? vm.policy.forms.application.recipients.owner.emailAddress : '',
                                first_name: vm.policy.forms.application.recipients.owner.firstName,
                                last_name: vm.policy.forms.application.recipients.owner.lastName,
                                role: 'Policy Owner'
                            });
                            vm.policy.owner.fullName = vm.policy.forms.application.recipients.owner.firstName + ' ' + vm.policy.forms.application.recipients.owner.lastName;
                            templateIdx += 4;
                        }
                        if (vm.policy.forms.application.recipients.agent) {
                            recipients.push({
                                email: vm.policy.forms.application.recipients.agent.emailAddress,
                                first_name: vm.policy.forms.application.recipients.agent.firstName,
                                last_name: vm.policy.forms.application.recipients.agent.lastName,
                                role: 'Agent'
                            });
                            vm.policy.agent = {
                                fullName: vm.policy.forms.application.recipients.agent.fullName
                            };
                            templateIdx += 5;
                        }

                        var formVersion = 'PSOA';
                        //var templateId = 'sASt8ng6NNNLZqUid5kmTR'; // PSOA - Atlas
                        var templateId = 'cQKZU8Et4AaDb7pSTiA2Ai'; // PSOA - Rapid
                        if (templateIdx === 7) {
                            formVersion = 'PSA';
                            //templateId = '6agzrJ5q6LH83piZw2yp7C'; // PSA - Atlas
                            templateId = 'Gy38i59nr2ewyEVeQKpe2j'; // PSA - Rapid
                        } else if (templateIdx === 9) {
                            formVersion = 'POA';
                            //templateId = 'rpdSERfYoxj5d2uKYRhYMB'; // POA - Atlas
                            templateId = 'XtbHK7rMUySEGncfXHoMbU'; // POA - Rapid
                        } else if (templateIdx === 5) {
                            formVersion = 'PA';
                            if (ENV === 'prod') {
                                //templateId = '9wXty4podVGuzarvXNt23n'; // PA - Production - Atlas
                                templateId = 'oTVagwTEXdef4Emcp8LGLa'; // PA - Production - Rapid
                            } else if (ENV === 'demo') {
                                templateId = 'QxkYWq3sbV2JPUwNSegPxi'; // PA - Demo
                            } else if (ENV === 'dev') {
                                templateId = ' ATCcb4BvG6EXaxrcek2w8S'; // PA - Dev
                            }
                        }
                    }

                    // Convert all check boxes to strings
                    if (vm.policy.secondaryInsured) {
                        vm.policy.secondaryInsured.isDeceased = (vm.policy.secondaryInsured.isDeceased) ? 'Yes' : 'No';
                    }

                    if (vm.platform === 'isubmit') {
                        var data = {
                            templateId: templateId,
                            documentName: "Application Form - " + vm.policy.insured.lastName,
                            recipients: recipients,
                            fieldData: {
                                "agent.fullName": { "value": vm.policy.agent.fullName ? vm.policy.agent.fullName : '' },
                                "annualPremiumAmount": { "value": vm.policy.annualPremiumAmount ? vm.policy.annualPremiumAmount : '' },
                                "assignmentDetails": { "value": vm.policy.assignmentDetails ? vm.policy.assignmentDetails : '' },
                                "carrier": { "value": vm.policy.carrier ? vm.policy.carrier : '' },
                                "currentBene.fullName": { "value": (vm.policy.currentBene) ? vm.policy.currentBene.fullName : '' },
                                "currentBene.gender.name": { "value": (vm.policy.currentBene && vm.policy.currentBene.gender) ? vm.policy.currentBene.gender[0].name : '' },
                                "currentBene.dateOfBirth": { "value": (vm.policy.currentBene && vm.policy.currentBene.dateOfBirth) ? vm.policy.currentBene.dateOfBirth : '' },
                                "currentBene.ssn": { "value": (vm.policy.currentBene && vm.policy.currentBene.fullName) ? vm.policy.currentBene.fullName : '' },
                                "currentBene.address.address": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.address : '' },
                                "currentBene.address.city": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.city : '' },
                                "currentBene.address.state": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.state : '' },
                                "currentBene.address.zip": { "value": (vm.policy.currentBene && vm.policy.currentBene.address) ? vm.policy.currentBene.address.zip : '' },
                                "currentBene.phoneNumber": { "value": (vm.policy.currentBene && vm.policy.currentBene.phoneNumber) ? vm.policy.currentBene.phoneNumber : '' },
                                "currentBene.emailAddress": { "value": (vm.policy.currentBene && vm.policy.currentBene.emailAddress) ? vm.policy.currentBene.emailAddress : '' },
                                "currentBene.maritalStatus": { "value": (vm.policy.currentBene && vm.policy.currentBene.maritalStatus) ? vm.policy.currentBene.maritalStatus[0].name : '' },
                                "currentBene.beneType": { "value": (vm.policy.currentBene && vm.policy.currentBene.beneType) ? vm.policy.currentBene.beneType[0] : '' },
                                "currentBene.relationshipToInsured": { "value": (vm.policy.currentBene && vm.policy.currentBene.relationshipToInsured) ? vm.policy.currentBene.relationshipToInsured : '' },
                                "currentSecondaryBene.fullName": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                                "currentSecondaryBene.gender.name": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.gender) ? vm.policy.currentSecondaryBene.gender[0].name : '' },
                                "currentSecondaryBene.dateOfBirth": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                                "currentSecondaryBene.ssn": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.fullName) ? vm.policy.currentSecondaryBene.fullName : '' },
                                "currentSecondaryBene.address.address": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.address : '' },
                                "currentSecondaryBene.address.city": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.city : '' },
                                "currentSecondaryBene.address.state": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.state : '' },
                                "currentSecondaryBene.address.zip": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.address) ? vm.policy.currentSecondaryBene.address.zip : '' },
                                "currentSecondaryBene.phoneNumber": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.phoneNumber) ? vm.policy.currentSecondaryBene.phoneNumber : '' },
                                "currentSecondaryBene.emailAddress": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.emailAddress) ? vm.policy.currentSecondaryBene.emailAddress : '' },
                                "currentSecondaryBene.maritalStatus": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.maritalStatus) ? vm.policy.currentSecondaryBene.maritalStatus[0].name : '' },
                                "currentSecondaryBene.beneType": { "value": (vm.policy.currentSecondaryBene && vm.policy.currentSecondaryBene.beneType) ? vm.policy.currentSecondaryBene.beneType[0] : '' },
                                "dateOfIssue": { "value": vm.policy.dateOfIssue ? vm.policy.dateOfIssue : '' },
                                "faceAmount": { "value": vm.policy.faceAmount ? vm.policy.faceAmount : '' },
                                "hasPolicyBeenAssigned": { "value": (vm.policy.hasPolicyBeenAssigned) ? vm.policy.hasPolicyBeenAssigned[0] : '' },
                                "hasPolicyChangedBene": { "value": (vm.policy.hasPolicyChangedBene) ? vm.policy.hasPolicyChangedBene[0] : '' },
                                "hasPolicyChangedOwner": { "value": (vm.policy.hasPolicyChangedOwner) ? vm.policy.hasPolicyChangedOwner[0] : '' },
                                "hasOwnerDeclaredBankruptcy": { "value": (vm.policy.hasOwnerDeclaredBankruptcy) ? vm.policy.hasOwnerDeclaredBankruptcy[0] : '' },
                                "insured.address.oneLiner": { "value": (vm.policy.insured.address && vm.policy.insured.address.oneLiner) ? vm.policy.insured.address.oneLiner : '' },
                                "insured.address.address": { "value": (vm.policy.insured.address && vm.policy.insured.address.address) ? vm.policy.insured.address.address : '' },
                                "insured.address.city": { "value": (vm.policy.insured.address && vm.policy.insured.address.city) ? vm.policy.insured.address.city : '' },
                                "insured.address.state": { "value": (vm.policy.insured.address && vm.policy.insured.address.state) ? vm.policy.insured.address.state : '' },
                                "insured.address.zip": { "value": (vm.policy.insured.address && vm.policy.insured.address.zip) ? vm.policy.insured.address.zip : '' },
                                "insured.dateOfBirth": { "value": vm.policy.insured.dateOfBirth ? vm.policy.insured.dateOfBirth : '' },
                                "insured.dependentChildren": { "value": (vm.policy.insured.dependentChildren) ? vm.policy.insured.dependentChildren[0] : '' },
                                "insured.emailAddress": { "value": vm.policy.insured.emailAddress ? vm.policy.insured.emailAddress : '' },
                                "insured.fullName": { "value": vm.policy.insured.fullName ? vm.policy.insured.fullName : '' },
                                "insured.gender.name": { "value": (vm.policy.insured.gender) ? vm.policy.insured.gender[0].name : '' },
                                "insured.maritalStatus": { "value": (vm.policy.insured.maritalStatus) ? vm.policy.insured.maritalStatus[0].name : '' },
                                "insured.medicalHistory": { "value": vm.policy.insured.medicalHistory ? vm.policy.insured.medicalHistory : '' },
                                "insured.nameOfSpouse": { "value": (vm.policy.insured.nameOfSpouse) ? vm.policy.insured.nameOfSpouse : '' },
                                "insured.phoneNumber": { "value": vm.policy.insured.phoneNumber ? vm.policy.insured.phoneNumber : '' },
                                "insured.primaryPhysicianName": { "value": vm.policy.insured.primaryPhysicianName ? vm.policy.insured.primaryPhysicianName : '' },
                                "insured.primaryPhysicianPhone": { "value": vm.policy.insured.primaryPhysicianPhone ? vm.policy.insured.primaryPhysicianPhone : '' },
                                "insured.specialistOneName": { "value": vm.policy.insured.specialistOneName ? vm.policy.insured.specialistOneName : '' },
                                "insured.specialistOnePhone": { "value": vm.policy.insured.specialistOnePhone ? vm.policy.insured.specialistOnePhone : '' },
                                "insured.specialistTwoName": { "value": vm.policy.insured.specialistTwoName ? vm.policy.insured.specialistTwoName : '' },
                                "insured.specialistTwoPhone": { "value": vm.policy.insured.specialistTwoPhone ? vm.policy.insured.specialistTwoPhone : '' },
                                "insured.ssn": { "value": vm.policy.insured.ssn ? vm.policy.insured.ssn : '' },
                                "intentToSellExplanation": { "value": vm.policy.intentToSellExplanation ? vm.policy.intentToSellExplanation : '' },
                                "interestedPartyOne.dateObtained": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.dateObtained : '' },
                                "interestedPartyOne.mannerObtained": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.mannerObtained : '' },
                                "interestedPartyOne.name": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.name : '' },
                                "interestedPartyOne.natureOfInterest": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.natureOfInterest : '' },
                                "interestedPartyOne.relationship": { "value": (vm.policy.interestedPartyOne) ? vm.policy.interestedPartyOne.relationship : '' },
                                "interestedPartyThree.dateObtained": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.dateObtained : '' },
                                "interestedPartyThree.mannerObtained": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.mannerObtained : '' },
                                "interestedPartyThree.name": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.name : '' },
                                "interestedPartyThree.natureOfInterest": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.natureOfInterest : '' },
                                "interestedPartyThree.relationship": { "value": (vm.policy.interestedPartyThree) ? vm.policy.interestedPartyThree.relationship : '' },
                                "interestedPartyTwo.dateObtained": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.dateObtained : '' },
                                "interestedPartyTwo.mannerObtained": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.mannerObtained : '' },
                                "interestedPartyTwo.name": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.name : '' },
                                "interestedPartyTwo.natureOfInterest": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.natureOfInterest : '' },
                                "interestedPartyTwo.relationship": { "value": (vm.policy.interestedPartyTwo) ? vm.policy.interestedPartyTwo.relationship : '' },
                                "isInsuredAlsoOwner": { "value": (vm.policy.isInsuredAlsoOwner) ? vm.policy.isInsuredAlsoOwner[0] : '' },
                                "isOwnerInLegalSuit": { "value": (vm.policy.isOwnerInLegalSuit) ? vm.policy.isOwnerInLegalSuit[0] : '' },
                                "lastPremiumPaid": { "value": vm.policy.lastPremiumPaid ? vm.policy.lastPremiumPaid : '' },
                                "lastPremiumPaidAmount": { "value": vm.policy.lastPremiumPaidAmount ? vm.policy.lastPremiumPaidAmount : '' },
                                "nextPremiumDue": { "value": vm.policy.nextPremiumDue ? vm.policy.nextPremiumDue : '' },
                                "originalPolicyBene": { "value": vm.policy.originalPolicyBene ? vm.policy.originalPolicyBene : '' },
                                "originalPolicyBeneRelationship": { "value": vm.policy.originalPolicyBeneRelationship ? vm.policy.originalPolicyBeneRelationship : '' },
                                "originalPolicyOwner": { "value": vm.policy.originalPolicyOwner ? vm.policy.originalPolicyOwner : '' },
                                "originalPurposeForInsurance": { "value": vm.policy.originalPurposeForInsurance ? vm.policy.originalPurposeForInsurance : '' },
                                "policyFinanced.explanation": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.explanation : '' },
                                "policyFinanced.lender": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.lender : '' },
                                "policyFinanced.maturityDate": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.maturityDate : '' },
                                "policyFinanced.payoffAmount": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.payoffAmount : '' },
                                "policyFinanced.principalAmount": { "value": (vm.policy.policyFinanced) ? vm.policy.policyFinanced.principalAmount : '' },
                                "policyNumber": { "value": vm.policy.policyNumber ? vm.policy.policyNumber : '' },
                                "policyType": { "value": vm.policy.policyType ? vm.policy.policyType : '' },
                                "wasPolicyFinanced": { "value": (vm.policy.wasPolicyFinanced) ? vm.policy.wasPolicyFinanced[0] : '' },
                                "wasThereIntentToSell": { "value": (vm.policy.wasThereIntentToSell) ? vm.policy.wasThereIntentToSell[0] : '' }
                            },
                            metaData: {
                                policyNumber: vm.policy.policyNumber,
                                policyKey: vm.policyKey,
                                LSHID: vm.policy.LSHID,
                                formName: "Application Form",
                                className: "application",
                                formVersion: formVersion,
                                noteToRecipients: (vm.policy.forms.application) ? vm.policy.forms.application.noteToRecipients : null,
                                env: ENV
                            }
                        };

                        if (vm.policy.insured.secondaryInsured && vm.policy.insured.secondaryInsured[0] === 'Yes' && vm.policy.secondaryInsured) {
                            data.fieldData["secondaryInsured.address.oneLiner"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.oneLiner : '' };
                            data.fieldData["secondaryInsured.address.address"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.address : '' };
                            data.fieldData["secondaryInsured.address.city"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.city : '' };
                            data.fieldData["secondaryInsured.address.state"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.state : '' };
                            data.fieldData["secondaryInsured.address.zip"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.address) ? vm.policy.secondaryInsured.address.zip : '' };
                            data.fieldData["secondaryInsured.dateOfBirth"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dateOfBirth) ? vm.policy.secondaryInsured.dateOfBirth : '' };
                            data.fieldData["secondaryInsured.dependentChildren"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dependentChildren) ? vm.policy.secondaryInsured.dependentChildren[0] : '' };
                            data.fieldData["secondaryInsured.emailAddress"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.emailAddress) ? vm.policy.secondaryInsured.emailAddress : '' };
                            data.fieldData["secondaryInsured.fullName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.fullName) ? vm.policy.secondaryInsured.fullName : '' };
                            data.fieldData["secondaryInsured.gender.name"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.gender) ? vm.policy.secondaryInsured.gender[0].name : '' };
                            data.fieldData["secondaryInsured.isDeceased"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.isDeceased) ? vm.policy.secondaryInsured.isDeceased : '' };
                            data.fieldData["secondaryInsured.maritalStatus"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.maritalStatus) ? vm.policy.secondaryInsured.maritalStatus[0].name : '' };
                            data.fieldData["secondaryInsured.medicalHistory"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.medicalHistory) ? vm.policy.secondaryInsured.medicalHistory : '' };
                            data.fieldData["secondaryInsured.nameOfSpouse"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.nameOfSpouse) ? vm.policy.secondaryInsured.nameOfSpouse : '' };
                            data.fieldData["secondaryInsured.phoneNumber"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.phoneNumber) ? vm.policy.secondaryInsured.phoneNumber : '' };
                            data.fieldData["secondaryInsured.primaryPhysicianName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.primaryPhysicianName) ? vm.policy.secondaryInsured.primaryPhysicianName : '' };
                            data.fieldData["secondaryInsured.primaryPhysicianPhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.primaryPhysicianPhone) ? vm.policy.secondaryInsured.primaryPhysicianPhone : '' };
                            data.fieldData["secondaryInsured.specialistOneName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistOneName) ? vm.policy.secondaryInsured.specialistOneName : '' };
                            data.fieldData["secondaryInsured.specialistOnePhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistOnePhone) ? vm.policy.secondaryInsured.specialistOnePhone : '' };
                            data.fieldData["secondaryInsured.specialistTwoName"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistTwoName) ? vm.policy.secondaryInsured.specialistTwoName : '' };
                            data.fieldData["secondaryInsured.specialistTwoPhone"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.specialistTwoPhone) ? vm.policy.secondaryInsured.specialistTwoPhone : '' };
                            data.fieldData["secondaryInsured.ssn"] = { "value": (vm.policy.secondaryInsured && vm.policy.secondaryInsured.ssn) ? vm.policy.secondaryInsured.ssn : '' };
                        }

                        if (vm.policy.owner) {
                            data.fieldData["owner.address.address"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.address : '' };
                            data.fieldData["owner.address.city"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.city : '' };
                            data.fieldData["owner.address.state"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.state : '' };
                            data.fieldData["owner.address.zip"] = { "value": (vm.policy.owner && vm.policy.owner.address) ? vm.policy.owner.address.zip : '' };
                            data.fieldData["owner.authorizedRep"] = { "value": (vm.policy.owner && vm.policy.owner.authorizedRep) ? vm.policy.owner.authorizedRep : '' };
                            data.fieldData["owner.dateOfBirth"] = { "value": (vm.policy.owner && vm.policy.owner.dateOfBirth) ? vm.policy.owner.dateOfBirth : '' };
                            data.fieldData["owner.dlNumber"] = { "value": (vm.policy.owner && vm.policy.owner.dlNumber) ? vm.policy.owner.dlNumber : '' };
                            data.fieldData["owner.emailAddress"] = { "value": (vm.policy.owner && vm.policy.owner.emailAddress) ? vm.policy.owner.emailAddress : '' };
                            data.fieldData["owner.fullName"] = { "value": (vm.policy.owner && vm.policy.owner.fullName) ? vm.policy.owner.fullName : '' };
                            data.fieldData["owner.maritalStatus"] = { "value": (vm.policy.owner && vm.policy.owner.maritalStatus) ? vm.policy.owner.maritalStatus[0].name : '' };
                            data.fieldData["owner.nameOfSpouse"] = { "value": (vm.policy.owner && vm.policy.owner.nameOfSpouse) ? vm.policy.owner.nameOfSpouse : '' };
                            data.fieldData["owner.ownerRelationship"] = { "value": (vm.policy.owner && vm.policy.owner.ownerRelationship) ? vm.policy.owner.ownerRelationship : '' };
                            data.fieldData["owner.phoneNumber"] = { "value": (vm.policy.owner && vm.policy.owner.phoneNumber) ? vm.policy.owner.phoneNumber : '' };
                            data.fieldData["owner.ssntin"] = { "value": (vm.policy.owner && vm.policy.owner.ssntin) ? vm.policy.owner.ssntin : '' };
                            data.fieldData["owner.stateOfFormation"] = { "value": (vm.policy.owner && vm.policy.owner.stateOfFormation) ? vm.policy.owner.stateOfFormation : '' };
                            data.fieldData["owner.stateOfIssue.id"] = { "value": (vm.policy.owner && vm.policy.owner.stateOfIssue) ? vm.policy.owner.stateOfIssue.id : '' };
                        }
                    }

                    if (vm.platform === 'imanager') {
                        vm.policy.forms.application.status = 'Pending Upload';
                        vm.policy.status.display = {
                            step: 'Case Intake',
                            status: 'Ongoing',
                            text: 'In Progress'
                        }
                        vm.policy.$save().then(function(ref) {
                            sweetAlert.swal({
                                title: 'Case Intake Complete',
                                text: 'If you want to keep track of any Case Files, including a copy of the actual Policy Application, be sure to upload them on the next page.',
                                type: 'success'
                            }).then(function(result) {
                                // Save and go to policy page
                                vm.submittingApplication = false;
                                vm.savePolicy('goToPolicy');
                            });
                        }).catch(function(err) {
                            sweetAlert.swal({
                                title: 'Error',
                                text: err.message,
                                type: 'error'
                            }).then(function(result) {
                                vm.submittingApplication = false;
                            });
                        });
                    } else {
                        if (vm.signMethod === 'printAndSign') {
                            apiService.fillPDF(data).then(function(response) {
                                if (response.status === 'success') {
                                    vm.policy.forms.application.downloadURL = response.data;
                                    vm.policy.forms.application.status = 'Pending Upload';
                                    vm.policy.status.applicationFinished = firebase.database.ServerValue.TIMESTAMP;
                                    vm.policy.status.display = {
                                        step: 'Application Signatures',
                                        status: 'Pending',
                                        text: 'Pending Signatures'
                                    }
                                    vm.policy.$save().then(function(ref) {
                                        // Notify upline about new Policy!
                                        var notifyData = {
                                            targets: [],
                                            policyKey: vm.policyKey,
                                            createdBy: vm.currentUser.profile.displayName,
                                            createdByRole: vm.currentUser.profile.role,
                                            note: 'Policy has been submitted and is Pending Application Signature. Face Amount: $' + vm.policy.faceAmount + ' Insured: ' + vm.policy.insured.age + '/' + vm.policy.insured.gender[0].id + '.',
                                            LSHID: vm.policy.LSHID,
                                            action: 'New Policy Created'
                                        };
                                        // Policy upline hierarchy is added in order, so loop and send to all above yourself
                                        for (let i = 0; i < (vm.policy.upline.length); i++) {
                                            if (vm.policy.upline[i].orgKey) {
                                                notifyData.targets.push({
                                                    type: vm.policy[vm.policy.upline[i].orgKey],
                                                    id: vm.policy.upline[i].orgKey
                                                });
                                            }
                                        }
                                        notifyService.addNotification(notifyData).then(function() {
                                            vm.toast('success', 'Notifications sent');
                                        });
                                        sweetAlert.swal({
                                            title: 'Success',
                                            text: 'Your Application has been successfully created. The filled-in form will be opened in a new tab after you click "OK".',
                                            type: 'success'
                                        }).then(function(result) {
                                            // Save and go to policy page
                                            vm.submittingApplication = false;
                                            window.open(vm.policy.forms.application.downloadURL, '_blank');
                                            vm.savePolicy('goToPolicy');
                                        });
                                    }).catch(function(err) {
                                        sweetAlert.swal({
                                            title: 'Error',
                                            text: err.message,
                                            type: 'error'
                                        }).then(function(result) {
                                            vm.submittingApplication = false;
                                            $scope.$apply();
                                        });
                                    });
                                } else {
                                    sweetAlert.swal({
                                        title: 'Error',
                                        text: response.message,
                                        type: 'error'
                                    }).then(function(result) {
                                        vm.submittingApplication = false;
                                        $scope.$apply();
                                    });
                                }
                            });
                        } else if (vm.signMethod === 'esign') {
                            pandadocService.createDocumentFromTemplate(data).then(function(response) {
                                console.log("PANDADOC RESPONSE: ", response)
                                if (response.status === 'success') {
                                    // Notify upline about new Policy!
                                    var notifyData = {
                                        targets: [],
                                        policyKey: vm.policyKey,
                                        createdBy: vm.currentUser.profile.displayName,
                                        createdByRole: vm.currentUser.profile.role,
                                        note: 'Policy has been submitted and is Pending Application Signature. Face Amount: $' + vm.policy.faceAmount + ' Insured: ' + vm.policy.insured.age + '/' + vm.policy.insured.gender[0].id + '.',
                                        LSHID: vm.policy.LSHID,
                                        action: 'New Policy Created'
                                    };
                                    // Policy upline hierarchy is added in order, so loop and send to all above yourself
                                    for (let i = 0; i < (vm.policy.upline.length); i++) {
                                        if (vm.policy.upline[i].orgKey) {
                                            notifyData.targets.push({
                                                type: vm.policy[vm.policy.upline[i].orgKey],
                                                id: vm.policy.upline[i].orgKey
                                            });
                                        }
                                    }
                                    notifyService.addNotification(notifyData).then(function() {
                                        vm.toast('success', 'Notifications sent');
                                    });
                                    console.log("PANDADOC RESPONSE: ", response)
                                    if (response.data && response.data.id) {
                                        // Store the new PandaDoc document ID in Firebase NOW (not after the first webhook arrives!!!)
                                        vm.policy.forms.application.documentId = response.data.id;
                                        vm.policy.forms.application.className = 'application';
                                        vm.policy.forms.application.formName = 'Application Form';
                                        vm.policy.status.applicationFinished = firebase.database.ServerValue.TIMESTAMP;
                                        vm.policy.forms.application.status = 'Pending Signatures';
                                        vm.policy.status.display = {
                                            step: 'Application Signatures',
                                            status: 'Pending',
                                            text: 'Pending Signatures'
                                        }
                                        vm.policy.$save().then(function(ref) {
                                            sweetAlert.swal({
                                                title: 'Success',
                                                text: 'Your Application has been successfully submitted for e-signature',
                                                type: 'success'
                                            }).then(function(result) {
                                                // Save and go to policy page
                                                vm.submittingApplication = false;
                                                vm.savePolicy('goToPolicy');
                                            });
                                        }).catch(function(err) {
                                            sweetAlert.swal({
                                                title: 'Error',
                                                text: err.message,
                                                type: 'error'
                                            }).then(function(result) {
                                                vm.submittingApplication = false;
                                            });
                                        });
                                    }
                                } else {
                                    vm.submittingApplication = false;
                                    console.warn(JSON.stringify(response.detail))
                                    sweetAlert.swal({
                                        title: 'Error',
                                        text: JSON.stringify(response.detail),
                                        type: 'error'
                                    }).then(function(result) {});
                                }
                            });
                        }
                    }
                }
            }
        }
        vm.addHealthCondition = function(){
            vm.policy.medicalDetails.diagnoses.push(
                {
                    diagnosis: "",
                    dateOfDiagnosis: "",
                    treatmentReceived: "",
                    dateOfTreatment: "",
                    results: "",
                }
            )
            updateLabels();
            document.querySelector("#addHealthConditionButton").scrollIntoView({behavior:"smooth"})
        }

        vm.addFamilyHistory = function(){
            vm.policy.medicalDetails.familyHistory.push(
                {
                    relationship: "",
                    healthConditions: "",
                    ageDeceased: ""
                }
            )
        }

        vm.addMedication = function(){
            if(vm.policy.medicalDetails.medications == null){
                vm.policy.medicalDetails
                .medications = [];
            }
            vm.policy.medicalDetails.medications.push(
                {
                    medicationDetails: ""
                }
            )
        }

        vm.goToPage = function(page) {
            if (vm.pageIsDirty) {
                vm.savePolicy(page);
            } else if (page === 'next') {
                vm.showPage++;
                updateLabels();
            } else if (page === 'previous') {
                vm.showPage--;
                updateLabels();
            } else {
                vm.showPage = page;
                updateLabels();
            }
            // if (page > vm.showPage && vm.pageIsDirty) {
            //     vm.nextPage = page;
            //     vm.savePolicy('goToPage');
            // } else {
            //     vm.showPage = page;
            // }
        }

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        if (route === 'policy') {
                            $state.go(route, { pid: vm.policyKey });
                        } else {
                            $state.go(route);
                        }
                    } else {
                        if (route === 'policy') {
                            vm.savePolicy('goToPolicy');
                        } else {
                            vm.savePolicy('close');
                        }
                    }
                });
            } else {
                if (route === 'policy') {
                    $state.go(route, { pid: vm.policyKey });
                } else {
                    $state.go(route);
                }
            }
        }

        vm.removeThirdParty = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "This will remove any data the 3rd party provided and prevent the 3rd party from accessing this form further. You can request 3rd party completion again, but the current link will no longer work.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, remove access!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    root.child('publicForms').child(vm.policy.forms.application.publicGuid).remove().then(function(ref) {
                        root.child('policies').child(vm.policyKey).child('forms/application').update({
                            publicGuid: uuidv4(),
                            status: 'In Progress',
                            thirdParty: null
                        }).then(function(ref) {
                            if (vm.policy.formBackup) {
                                root.child('formBackups').child(vm.policyKey).once('value', function(snap) {
                                    if (snap.exists()) {
                                        var restoredData = snap.val();
                                        restoredData.forms.application.thirdParty = null;
                                        restoredData.forms.application.publicGuid = uuidv4();
                                        restoredData.forms.application.status = 'In Progress';
                                        restoredData.formBackup = null;
                                        root.child('policies').child(vm.policyKey).update(restoredData).then(function(ref) {
                                            root.child('formBackups').child(vm.policyKey).remove().then(function() {
                                                sweetAlert.swal({
                                                    title: "Success",
                                                    text: "",
                                                    type: "success",
                                                    timer: 2500
                                                }).then(function(result) {});
                                            });
                                        });
                                    }
                                });
                            } else {
                                sweetAlert.swal({
                                    title: "Success",
                                    text: "",
                                    type: "success",
                                    timer: 2500
                                }).then(function(result) {});
                            }
                        });
                    });
                }
            });
        }

        vm.approveThirdParty = function() {
            vm.justApprovedThirdParty = true;
            root.child('formBackups').child(vm.policyKey).remove().then(function() {
                root.child('publicForms').child(vm.policy.forms.application.publicGuid).remove().then(function(ref) {
                    root.child('policies').child(vm.policyKey).child('forms/application').update({
                        publicGuid: uuidv4(),
                        status: 'In Progress'
                    }).then(function(ref) {
                        root.child('policies').child(vm.policyKey).child('status/display').update({
                            text: vm.platform === 'isubmit' ? 'Started' : 'In Progress'
                        }).then(function() {
                            vm.toast('success', 'Application Saved');
                        }).catch(function(err) {
                            console.log(err.message);
                        });
                    });
                });
            });
        }

        vm.sendToThirdParty = function() {
            if (!vm.thirdParty || !vm.thirdParty.firstName || !vm.thirdParty.emailAddress) {
                sweetAlert.swal({
                    title: "Uh Oh!",
                    text: "Name and Email are required to send this form out for 3rd party completion.",
                    type: "error"
                }).then(function() {});
            } else {
                if (vm.pageIsDirty) {
                    sweetAlert.swal({
                        title: "Save Changes?",
                        text: "You have unsaved changes. Do you want to save first?",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "No",
                        confirmButtonColor: "#00B200",
                        confirmButtonText: "Yes, Save!",
                        closeOnConfirm: false
                    }).then(function(result) {
                        if (result.dismiss) {
                            continueSendToThirdParty();
                        } else {
                            vm.savePolicy('requestThirdParty');
                        }
                    });
                } else {
                    continueSendToThirdParty();
                }
            }
        }

        function continueSendToThirdParty() {
            // Copy any current form data to publicForms/[publicGuid] in Firebase
            vm.sendingToThirdParty = true;
            vm.policyCopy = sharedService.cloneObject(vm.policy);

            var publicGuid = vm.policyCopy.forms.application.publicGuid;

            delete vm.policyCopy.$$conf;
            delete vm.policyCopy.$id;
            delete vm.policyCopy.$priority;
            delete vm.policyCopy.$resolved;
            delete vm.policyCopy.adminFiles;
            delete vm.policyCopy.caseFiles;
            delete vm.policyCopy.createdBy;
            delete vm.policyCopy.createdById;
            delete vm.policyCopy.createdByOrgKey;
            delete vm.policyCopy.createdByOrgType;
            delete vm.policyCopy.actualCreatedBy;
            delete vm.policyCopy.actualCreatedById;
            delete vm.policyCopy.actualCreatedByOrgKey;
            delete vm.policyCopy.actualCreatedByOrgType;
            delete vm.policyCopy.policyIsActive;
            delete vm.policyCopy.private;
            delete vm.policyCopy.publicGuid;
            delete vm.policyCopy.submittedTo;
            delete vm.policyCopy.updateLog;
            delete vm.policyCopy.lastUpdateAt;
            delete vm.policyCopy.lastUpdatedBy;
            delete vm.policyCopy.lastUpdatedById;
            delete vm.policyCopy.latestStatus;
            delete vm.policyCopy.latestStatusAt;

            vm.policyCopy.alertId = vm.currentUser.profile.alertId;

            vm.policyCopy.forms.application.status = 'Pending 3rd Party';
            vm.policyCopy.forms.application.thirdParty = {
                fullName: vm.thirdParty.firstName + ' ' + vm.thirdParty.lastName,
                emailAddress: vm.thirdParty.emailAddress
            };
            if (vm.policyCopy.status.display.text) {
                vm.policyCopy.status.display.text = 'Pending 3rd Party';
            }

            root.child('publicForms').child(publicGuid).update(vm.policyCopy).then(function(ref) {
                vm.policyCopy = null;
                root.child('policies').child(vm.policyKey).child('forms/application').update({
                    status: 'Pending 3rd Party',
                    thirdParty: {
                        fullName: vm.thirdParty.firstName + ' ' + vm.thirdParty.lastName,
                        emailAddress: vm.thirdParty.emailAddress,
                        requestedAt: firebase.database.ServerValue.TIMESTAMP
                    }
                }).then(function(response) {
                    root.child('policies').child(vm.policyKey).child('status/display').update({
                        text: 'Pending 3rd Party'
                    }).then(function(response) {
                        // Create an email message to the recipient with link to [url]/view/application?pubId=[publicGuid]
                        var linkRoot = (ENV === 'dev') ? 'localhost:3000' : vm.subdomain + '.' + vm.domain + '.' + vm.tld;
                        var recipients = [];
                        recipients.push({
                            email: vm.thirdParty.emailAddress,
                            template: 'third-party-form-request',
                            subData: {
                                name: vm.thirdParty.firstName,
                                link: linkRoot + '/view/application?pubId=' + publicGuid
                            }
                        });
                        notifyService.sendEmail({ env: ENV, recipients: recipients }).then(function(response) {
                            if (response.status === 'success') {
                                vm.sendingToThirdParty = false;
                                sweetAlert.swal({
                                    title: "Success",
                                    text: "The requested 3rd party has been emailed a link to this Policy.",
                                    type: "success",
                                    timer: 2500
                                }).then(function(result) {
                                    // Go back to the policy
                                    $state.go('policy', { pid: vm.policyKey });
                                });
                            } else {
                                vm.sendingToThirdParty = false;
                                console.log(response.message);
                                sweetAlert.swal({
                                    title: "Uh Oh!",
                                    text: "Error sending email to 3rd party recipient!",
                                    type: "error"
                                }).then(function() {});
                            }
                        });
                    }).catch(function(err) {
                        console.log(err);
                        vm.sendingToThirdParty = false;
                    });
                }).catch(function(err) {
                    console.log(err);
                    vm.sendingToThirdParty = false;
                });
            }).catch(function(err) {
                console.log(err);
                vm.sendingToThirdParty = false;
            });
        }

        vm.onSelectCallback = function(item, convertToValue) {
            vm.pageIsDirty = true;
            if (convertToValue) {
                item = item[0];
            }
            if (item === 'isPolicyConvertible' && vm.policy.isPolicyConvertible === 'No') {
                vm.policy.conversionDeadline = '';
            } else if (item === 'policyType' && vm.policy.policyType !== 'Term') {
                delete vm.policy.isPolicyConvertible;
                delete vm.policy.conversionDeadline;
            }
        }

        vm.savePolicy = function(nextPage) {
            console.log("Saving policy")
            if (vm.policyKey !== 'NEW') {
                vm.policy.lastUpdatedAt = firebase.database.ServerValue.TIMESTAMP;
                vm.policy.lastUpdatedBy = vm.currentUser.profile.displayName;
                vm.policy.lastUpdatedById = vm.currentUser.auth.uid;

                if (!vm.policy.updateLog) {
                    vm.policy.updateLog = [];
                }

                vm.policy.updateLog.push({
                    updatedAt: firebase.database.ServerValue.TIMESTAMP,
                    updatedBy: vm.currentUser.profile.displayName,
                    updatedById: vm.currentUser.auth.uid
                });

                // Convert number text to numbers for sorting
                if (vm.policy.faceAmount) {
                    vm.policy.faceAmount = parseInt(vm.policy.faceAmount);
                }
                if (vm.policy.annualPremiumAmount) {
                    vm.policy.annualPremiumAmount = parseInt(vm.policy.annualPremiumAmount);
                }
                if (vm.policy.estimatedCoiPremium) {
                    vm.policy.estimatedCoiPremium = parseInt(vm.policy.estimatedCoiPremium);
                }

                // Calculate Insured Age
                if (vm.policy.insured.dateOfBirth) {
                    vm.policy.insured.age = vm.calculateAge(vm.policy.insured.dateOfBirth);
                }

                if (!vm.policy.forms.application.recipients) {
                    vm.policy.forms.application.recipients = {};
                }

                vm.policy.insured.fullName = vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName;

                // if (vm.policy.insured) {
                //     if (!vm.policy.forms.application.recipients.insured) {
                //         vm.policy.forms.application.recipients.insured = {};
                //     }
                //     if (!vm.policy.forms.application.recipients.insured.firstName) {
                //         vm.policy.forms.application.recipients.insured.firstName = vm.policy.insured.firstName ? vm.policy.insured.firstName : null;
                //     }
                //     if (!vm.policy.forms.application.recipients.insured.lastName) {
                //         vm.policy.forms.application.recipients.insured.lastName = vm.policy.insured.lastName ? vm.policy.insured.lastName : null;
                //     }
                // }
                // if (vm.policy.secondaryInsured) {
                //     if (!vm.policy.forms.application.recipients.secondaryInsured) {
                //         vm.policy.forms.application.recipients.secondaryInsured = {};
                //     }
                //     if (!vm.policy.forms.application.recipients.secondaryInsured.firstName) {
                //         vm.policy.forms.application.recipients.secondaryInsured.firstName = vm.policy.secondaryInsured.firstName ? vm.policy.secondaryInsured.firstName : null;
                //     }
                //     if (!vm.policy.forms.application.recipients.secondaryInsured.lastName) {
                //         vm.policy.forms.application.recipients.secondaryInsured.lastName = vm.policy.secondaryInsured.lastName ? vm.policy.secondaryInsured.lastName : null;
                //     }
                // }

                // Update application form fields
                vm.policy.forms.application.recipients.insured = {
                    emailAddress: (vm.policy.insured.emailAddress) ? vm.policy.insured.emailAddress : '',
                    firstName: (vm.policy.insured.firstName) ? vm.policy.insured.firstName : '',
                    lastName: (vm.policy.insured.lastName) ? vm.policy.insured.lastName : ''
                }
                vm.policy.forms.application.recipients.insured.fullName = vm.policy.forms.application.recipients.insured.firstName + ' ' + vm.policy.forms.application.recipients.insured.lastName;
                if (vm.policy.insured.secondaryInsured && vm.policy.insured.secondaryInsured[0] === 'Yes') {
                    vm.policy.forms.application.recipients.secondaryInsured = {
                        emailAddress: (vm.policy.secondaryInsured.emailAddress) ? vm.policy.secondaryInsured.emailAddress : '',
                        firstName: (vm.policy.secondaryInsured.firstName) ? vm.policy.secondaryInsured.firstName : '',
                        lastName: (vm.policy.secondaryInsured.lastName) ? vm.policy.secondaryInsured.lastName : ''
                    }
                    vm.policy.forms.application.recipients.secondaryInsured.fullName = vm.policy.forms.application.recipients.secondaryInsured.firstName + ' ' + vm.policy.forms.application.recipients.secondaryInsured.lastName;
                }
                if (vm.policy.isInsuredAlsoOwner && vm.policy.isInsuredAlsoOwner[0] === 'No' && vm.policy.owner) {
                    vm.policy.forms.application.recipients.owner = {
                        firstName: (vm.policy.owner.firstName) ? vm.policy.owner.firstName : '',
                        lastName: (vm.policy.owner.lastName) ? vm.policy.owner.lastName : '',
                        emailAddress: (vm.policy.owner.emailAddress) ? vm.policy.owner.emailAddress : ''
                    }
                    vm.policy.forms.application.recipients.owner.fullName = vm.policy.forms.application.recipients.owner.firstName + ' ' + vm.policy.forms.application.recipients.owner.lastName;
                }

                vm.policy.$save().then(function(ref) {
                    if (vm.policy.forms.application.status === 'Pending 3rd Party' || vm.policy.buyers) {
                        // let tmpCopy = [];
                        // tmpCopy.push(vm.policy);
                        // vm.policyCopy = tmpCopy.slice(0)[0];
                        // tmpCopy = null;
                        vm.policyCopy = sharedService.cloneObject(vm.policy);

                        delete vm.policyCopy.$$conf;
                        delete vm.policyCopy.$id;
                        delete vm.policyCopy.$priority;
                        delete vm.policyCopy.$resolved;
                        delete vm.policyCopy.private;
                        delete vm.policyCopy.caseFiles;
                        delete vm.policyCopy.createdById;
                        delete vm.policyCopy.upline;
                        delete vm.policyCopy.submittedTo;
                        delete vm.policyCopy.updateLog;
                        delete vm.policyCopy.lastUpdatedAt;
                        delete vm.policyCopy.lastUpdatedBy;
                        delete vm.policyCopy.lastUpdatedById;
                        delete vm.policyCopy.archivedForms;
                        delete vm.policyCopy.publicNotes;
                        delete vm.policyCopy.forms.medicalPrimary;
                        delete vm.policyCopy.forms.medicalSecondary;
                        delete vm.policyCopy.buyers;

                        if (vm.policy.buyers) {
                            delete vm.policyCopy.forms.application;
                            delete vm.policyCopy.policyKey;
                            delete vm.policyCopy.currentBene;

                            if (vm.policyCopy.insured.secondaryInsured && vm.policyCopy.insured.secondaryInsured[0] === 'Yes') {
                                var secondaryInsuredDataToKeep = {
                                    dateOfBirth: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.dateOfBirth) ? vm.policy.secondaryInsured.dateOfBirth : '',
                                    gender: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.gender) ? vm.policy.secondaryInsured.gender : '',
                                    firstName: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.firstName) ? vm.policy.secondaryInsured.firstName : '',
                                    lastNameInitial: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.lastName) ? vm.policy.secondaryInsured.lastName.charAt(0) : '',
                                    lastName: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.lastName) ? vm.policy.secondaryInsured.lastName : '',
                                    isDeceased: (vm.policy.secondaryInsured && vm.policy.secondaryInsured.isDeceased) ? vm.policy.secondaryInsured.isDeceased : null
                                };
                                delete vm.policyCopy.secondaryInsured;
                                vm.policyCopy.secondaryInsured = secondaryInsuredDataToKeep;
                            } else {
                                delete vm.policyCopy.secondaryInsured;
                            }

                            // Keep insured.dateOfBirth, insured.gender, insured.firstName first initial of insured.lastName
                            var primaryInsuredDataToKeep = {
                                dateOfBirth: (vm.policy.insured && vm.policy.insured.dateOfBirth) ? vm.policy.insured.dateOfBirth : '',
                                gender: (vm.policy.insured && vm.policy.insured.gender) ? vm.policy.insured.gender : '',
                                firstName: (vm.policy.insured && vm.policy.insured.firstName) ? vm.policy.insured.firstName : '',
                                lastNameInitial: (vm.policy.insured && vm.policy.insured.lastName) ? vm.policy.insured.lastName.charAt(0) : '',
                                lastName: (vm.policy.insured && vm.policy.insured.lastName) ? vm.policy.insured.lastName : ''
                            };
                            delete vm.policyCopy.insured;
                            vm.policyCopy.insured = primaryInsuredDataToKeep;


                            root.child('privatePolicies').orderByChild('rootKey').equalTo(vm.policyKey).once('value', function(snap) {
                                if (snap.exists()) {
                                    var updates = {};
                                    snap.forEach(function(policy) {
                                        updates[policy.key + '/insured'] = vm.policyCopy.insured ? vm.policyCopy.insured : null;
                                        updates[policy.key + '/secondaryInsured'] = vm.policyCopy.secondaryInsured ? vm.policyCopy.secondaryInsured : null;
                                        updates[policy.key + '/faceAmount'] = vm.policyCopy.faceAmount ? vm.policyCopy.faceAmount : null;
                                        updates[policy.key + '/dateOfIssue'] = vm.policyCopy.dateOfIssue ? vm.policyCopy.dateOfIssue : null;
                                        updates[policy.key + '/conversionDeadline'] = vm.policyCopy.conversionDeadline ? vm.policyCopy.conversionDeadline : null;
                                        updates[policy.key + '/annualPremiumAmount'] = vm.policyCopy.annualPremiumAmount ? vm.policyCopy.annualPremiumAmount : null;
                                        updates[policy.key + '/carrier'] = vm.policyCopy.carrier ? vm.policyCopy.carrier : null;
                                        updates[policy.key + '/isPolicyConvertible'] = vm.policyCopy.isPolicyConvertible ? vm.policyCopy.isPolicyConvertible : null;
                                        updates[policy.key + '/policyType'] = vm.policyCopy.policyType ? vm.policyCopy.policyType : null;
                                        updates[policy.key + '/secondaryInsured'] = vm.policyCopy.secondaryInsured ? vm.policyCopy.secondaryInsured : null;
                                        updates[policy.key + '/faceAmount'] = vm.policyCopy.faceAmount ? vm.policyCopy.faceAmount : null;
                                    });
                                    root.child('privatePolicies').update(updates).then(function() {
                                        vm.policyCopy = null;
                                        continueSave(nextPage);
                                    }).catch(function(err) {
                                        console.log(err);
                                    });
                                } else {
                                    continueSave(nextPage);
                                }
                            }).catch(function(err) {
                                console.log(err);
                            });
                        }

                        if (vm.policy.forms.application.status === 'Pending 3rd Party') {
                            delete vm.policyCopy.forms;
                            delete vm.policyCopy.policyIsActive;
                            delete vm.policyCopy.publicGuid;
                            delete vm.policyCopy.status;

                            root.child('publicForms').child(vm.policy.forms.application.publicGuid).update(vm.policyCopy).then(function() {
                                vm.policyCopy = null;
                                continueSave(nextPage);
                            });
                        }
                    } else {
                        continueSave(nextPage);
                    }
                });
            }
        }

        // function getObject(obj) {
        //     var newObj = {};
        //     for (var key in obj) {
        //         if (key.indexOf('$') < 0 && obj.hasOwnProperty(key)) {
        //             newObj[key] = obj[key];
        //         };
        //     }
        //     return newObj;
        // }

        function continueSave(nextPage) {
            if (nextPage === 'goToPolicy') {
                $state.go('policy', { 'pid': vm.policyKey });
            } else {
                vm.toast('success', 'Application Saved');
                vm.pageIsDirty = false;
                if (nextPage === 'close') {
                    $state.go('inventory');
                } else if (nextPage === 'next') {
                    vm.showPage++;
                    updateLabels();
                } else if (nextPage === 'previous') {
                    vm.showPage--;
                    updateLabels();
                } else if (nextPage === 'requestThirdParty') {
                    continueSendToThirdParty();
                } else {
                    vm.showPage = nextPage;
                    updateLabels();
                }
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        function resolvePath(path, obj) {
            return path.split('.').reduce(function(prev, curr) {
                return prev ? prev[curr] : ''
            }, obj || self)
        }

        function setPath(obj, path, value) {
            let level = 0;
            let setrecursively = true;

            path.reduce(function(a, b) {
                level++;

                if (setrecursively && typeof a[b] === "undefined" && level !== path.length) {
                    a[b] = {};
                    return a[b];
                }

                if (level === path.length) {
                    a[b] = value;
                    return value;
                } else {
                    return a[b];
                }
            }, obj);
        }
    }

    angular.module('yapp')
        .controller('ApplicationCtrl', ApplicationCtrl);
})();