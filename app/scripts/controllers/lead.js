(function() {
    'use strict';

    /* @ngInject */
    function LeadCtrl(ENV, $location, $scope, stdData, $state, notifyService, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, pandadocService, sweetAlert, policyService, brandingDomain) {
        var vm = this;
        vm.showPage = 1;
        var root = firebase.database().ref();
        vm.formFound = false;
        vm.policy = {
            policyIsActive: true,
            contacts: []
        };

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];

        vm.healthStatusOptions = ['Good', 'Slightly Bad', 'Bad', 'Very Bad', 'Terminal'];        

        vm.testFinished = function() {
            if (vm.isTest) {
                vm.showPage = 3;
                updateLabels();
            }
        }

        vm.calculateAge = function(birthday) {
            return moment().diff(moment(birthday, 'MM-DD-YYYY'), 'years');
        }

        vm.goToPage = function(page, doCreate) {
            $scope.$evalAsync(function(scope) {
                if (doCreate) {
                    // Verify required fields - only contact info is needed?
                    if (!vm.contact.emailAddress) {
                        sweetAlert.swal({
                            title: "Missing Field",
                            text: "Contact Email Address is required! Please enter it and try again.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }).then(function(result) {
                            updateLabels();
                        });
                    } else if (!vm.contact.phoneNumber) {
                        sweetAlert.swal({
                            title: "Missing Field",
                            text: "Contact Phone Number is required! Please enter it and try again.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#00B200",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }).then(function(result) {
                            updateLabels();
                        });
                    } else {
                        if (vm.contact) {
                            vm.policy.contacts.push({
                                link: 'Submitted Lead Form',
                                firstName: vm.contact.firstName,
                                lastName: vm.contact.lastName,
                                emailAddress: vm.contact.emailAddress,
                                phoneNumber: vm.contact.phoneNumber
                            });
                        }

                        // Start a new Policy!
                        console.log('Starting a new policy');

                        if (vm.policy.insured && (vm.policy.insured.firstName || vm.policy.insured.lastName)) {
                            vm.policy.insured.fullName = vm.policy.insured.firstName + ' ' + vm.policy.insured.lastName;
                        }
                        if (vm.policy.insured && vm.policy.insured.dateOfBirth) {
                            vm.policy.insured.dateOfBirth = '01/01/' + vm.policy.insured.dateOfBirth.toString();
                            vm.policy.insured.age = vm.calculateAge(vm.policy.insured.dateOfBirth);
                        }

                        // Verify Policy Eligibility
                        if (vm.policy.insured.age < 65 || vm.policy.faceAmount < 100000) {
                            vm.showPage = 97;
                            updateLabels();
                        } else {
                            vm.policy.createdAt = firebase.database.ServerValue.TIMESTAMP;

                            vm.policy.status = {
                                caseReceived: true,
                                caseReceivedAt: firebase.database.ServerValue.TIMESTAMP,
                                applicationInProgress: true,
                                display:  {
                                    step: 'Application',
                                    status: 'In Progress',
                                    text: 'Lead'
                                }
                            };
                            vm.policy.createdOnPlatform = 'isubmit';

                            vm.policy.latestStatus = "No updates have been posted here yet";
                            vm.policy.statusSelection = [{
                                class: 'success',
                                status: 'On Track'
                            }];

                            vm.policy.LSHID = 'L' + moment().format('X').toString().substr(2, 8);

                            vm.linkRoot = '//';
                            vm.linkRoot += (ENV === 'dev') ? 'isubmit-local.lshub.net:3000' : vm.subdomain
                            vm.linkRoot += '.' + vm.domain + '.' + vm.tld;

                            var data = {
                                pef: vm.pef,
                                policyData: vm.policy,
                                linkRoot: vm.linkRoot,
                                pageURL: document.referrer
                            };

                            policyService.createNewPolicy(data).then(function(response) {
                                console.log(response);
                                if (response.status === 'success') {
                                    vm.formLink = response.data.formLink;
                                    vm.policyKey = response.data.key;
                                    vm.policyStarted = true;
                                    vm.showPage = page;
                                    updateLabels();
                                } else {
                                    vm.showPage = 98;
                                    vm.formErrorMessage = response.message;
                                    updateLabels();
                                }
                            });
                        }
                    }
                } else {
                    if (page === 2) {
                        if (!vm.policy.faceAmount) {
                            sweetAlert.swal({
                                title: "Missing Field",
                                text: "Policy Face Value is required! Please enter it and try again.",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }).then(function(result) {
                                updateLabels();
                            });
                        } else if (!vm.policy.insured || !vm.policy.insured.dateOfBirth) {
                            sweetAlert.swal({
                                title: "Missing Field",
                                text: "Insured Date of Birth is required! Please enter it and try again.",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#00B200",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }).then(function(result) {
                                updateLabels();
                            });
                        } else {
                            if (vm.policy.insured && !vm.contact) {
                                vm.contact = {
                                    firstName: vm.policy.insured.firstName ? vm.policy.insured.firstName : null,
                                    lastName: vm.policy.insured.lastName ? vm.policy.insured.lastName : null
                                };
                            }
                            if (!vm.agent) {
                                vm.agent = {};
                            }
                            if (vm.policy.insured.age < 65 || vm.policy.faceAmount < 100000) {
                                vm.showPage = 97;
                                updateLabels();
                            } else {
                                vm.calculateEstimate();
                                vm.showPage = page;
                                updateLabels();
                            }
                        }
                    } else if (page === 99) {
                        resetData();
                        vm.showPage = page;
                        updateLabels();
                    } else {
                        vm.showPage = page;
                        updateLabels();
                    }
                }
            });
        }

        var EstimatePayout = {

            resultArr: [
                //Better/Same - Little Worse - Worse - Much Worse - Terminal
                [0, 0, 10, 25, 50], // 70 or Less
                [0, 5, 15, 25, 50], // 71 - 75
                [0, 5, 15, 30, 50], // 76 - 79
                [0, 5, 20, 30, 50], // 80
                [0, 5, 20, 35, 50], // 81 - 84
                [5, 15, 25, 40, 50], // 85 - 89
                [10, 20, 25, 35, 35], // 90
                [10, 20, 30, 35, 35], // 91 - 94
                [10, 20, 25, 30, 30], // 95 oe more                    
            ],

            yearBirth: '',
            healthChange: '',
            ageStage: 0,
            offerStage: 0,

            getAge: function(yearBirth) {
                var currentTime = new Date();
                var yearCurrent = currentTime.getFullYear();
                return yearCurrent - yearBirth;
            },

            changeAgeStage: function(yearBirth) {

                var age = this.getAge(yearBirth);

                if (age <= 70) { // 70 or Less
                    this.ageStage = 0;
                } else if (age >= 71 && age <= 75) { // 71 - 75
                    this.ageStage = 1;
                } else if (age >= 76 && age <= 79) { // 76 - 79
                    this.ageStage = 2;
                } else if (age == 80) { // 80
                    this.ageStage = 3;
                } else if (age >= 81 && age <= 84) { // 81 - 84
                    this.ageStage = 4;
                } else if (age >= 85 && age <= 89) { // 85 - 89
                    this.ageStage = 5;
                } else if (age == 90) { // 90
                    this.ageStage = 6;
                } else if (age >= 91 && age <= 94) { // 91 - 94
                    this.ageStage = 7;
                } else if (age >= 95) { // 95 or more
                    this.ageStage = 8;
                } else {
                    this.ageStage = 0;
                }
            },

            changeOfferStage: function(healthChange) {
                switch (healthChange) {
                    case 'Good':
                        this.offerStage = 0;
                        break;
                    case 'Slightly Bad':
                        this.offerStage = 1;
                        break;
                    case 'Bad':
                        this.offerStage = 2;
                        break;
                    case 'Very Bad':
                        this.offerStage = 3;
                        break;
                    case 'Terminal':
                        this.offerStage = 4;
                        break;
                    default:
                        this.offerStage = 0;
                }
            },

            updateEstimate: function() {
                this.yearBirth = vm.yearBorn;
                this.healthChange = vm.policy.insured.healthStatus;
                this.changeAgeStage(this.yearBirth);
                this.changeOfferStage(this.healthChange);
                this.calcOffer();
            },

            calcOffer: function() {
                var res = this.resultArr[this.ageStage][this.offerStage];
                var benefit = vm.policy.faceAmount;
                var offerResult = 0;

                var estimate = parseInt(benefit * res) / 100;
                vm.estimate = 'success';
                vm.estimateLow = estimate * .5;
                vm.estimateHigh = estimate * .85;
            }
        }

        vm.calculateEstimate = function() {
            if (!vm.policy.insured || !vm.policy.insured.dateOfBirth || !vm.policy.faceAmount || !vm.policy.insured.healthStatus) {
                vm.estimate = 'error';
            } else {
                vm.yearBorn = moment(vm.policy.insured.dateOfBirth).year();
                EstimatePayout.updateEstimate();
            }
        }

        function resetData() {
            vm.policy = {
                policyIsActive: true,
                contacts: []
            };
        }

        vm.startForm = function() {
            resetData();
            vm.pef = ($stateParams.pef) ? $stateParams.pef : 'err';
            vm.isTest = ($stateParams.test) ? $stateParams.test : false;
            vm.states = stdData.states;

            // Get form settings from URL
            if (vm.isTest) {
                vm.backgroundColor = ($stateParams.bc) ? '#' + $stateParams.bc : '#FFFFFF';
                vm.fontColor = ($stateParams.fc) ? '#' + $stateParams.fc : '#333333';
                vm.width = ($stateParams.w) ? $stateParams.w : '100%';
                vm.padding = ($stateParams.p) ? $stateParams.p + 'px' : '20px';
                vm.backgroundIsDark = lightOrDark(vm.backgroundColor);
                vm.formFound = true;
                vm.goToPage(1);
            }

            // Get form settings from the server
            if (!vm.isTest) {
                var data = {
                    pef: vm.pef
                };
                policyService.getPublicEmbedForm(data).then(function(response) {
                    vm.backgroundColor = (response.data.backgroundColor) ? response.data.backgroundColor : '#FFFFFF';
                    vm.fontColor = (response.data.fontColor) ? response.data.fontColor : '#333333';
                    vm.width = (response.data.width) ? response.data.width : '100%';
                    vm.padding = (response.data.padding) ? response.data.padding : '20px';
                    vm.backgroundIsDark = lightOrDark(vm.backgroundColor);
                    vm.formFound = true;
                    vm.formName = response.data.formName;
                    vm.goToPage(1);
                });
            }
        }

        vm.startForm();

        vm.goTo = function(route) {
            if (vm.pageIsDirty) {
                // Possibly prompt here to create a policy based on provided information?
                sweetAlert.swal({
                    title: "Save Changes?",
                    text: "You have unsaved changes. Do you want to save first?",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "No",
                    confirmButtonColor: "#00B200",
                    confirmButtonText: "Yes, Save!",
                    closeOnConfirm: false
                }).then(function(result) {
                    if (result.dismiss) {
                        $state.go(route);
                    } else {
                        $state.go(route);
                    }
                });
            } else {
                $state.go(route);
            }
        }

        vm.continueApplication = function() {
            if (vm.isTest) {
                sweetAlert.swal("", "When deployed to your site, this button will route the user to our \"3rd Party Policy Request\" form. They will be able to fill out all of the necessary details for the full application without needing to register an account.");
            }
        }

        vm.registerUser = function() {
            if (vm.isTest) {
                sweetAlert.swal("", "When deployed to your site, this button will route the user to the login page for iSubmit. They will be prompted to create either a 'Seller' or 'Seller Representative' account, which will be added to your organization hierarchy under the User this Form is set to assign leads to.")
            } else {
                setTimeout(function() {
                    window.open = (vm.linkRoot + '/register?spk=' + vm.policyKey, '_blank');
                }, 250);
            }
        }

        function updateLabels() {
            setTimeout(function() {
                if (jQuery('#leadForm .jvFloat').length === 0) {
                    jQuery('#leadForm .float-me').jvFloat();
                }
                jQuery('#leadForm .placeHolder:not(.active)').filter(function() {
                    return this.value && this.value.length !== 0;
                }).addClass('active');
                jQuery('#leadForm .pseudo-placeHolder').filter(function() {
                    if (jQuery('#' + this.attributes["linkid"].value).find('.ui-select-placeholder').css('display') === 'none') {
                        return true;
                    }
                }).addClass('active');
                jQuery('#leadForm .flatpickr:not(".flatpickr-input")').flatpickr({
                    dateFormat: "m-d-Y",
                    disableMobile: true,
                    onValueUpdate: function(selectedDates, dateStr, instance) {
                        var parts = instance.element.id.split('.');
                        instance.close();
                    }
                });
                $scope.$apply();
            }, 100);
        }

        function lightOrDark(color) {
            var r, g, b, hsp;
            if (color.match(/^rgb/)) {
                color = color.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/);
                r = color[1];
                g = color[2];
                b = color[3];
            } else {
                color = +("0x" + color.slice(1).replace(
                    color.length < 5 && /./g, '$&$&'));
                r = color >> 16;
                g = color >> 8 & 255;
                b = color & 255;
            }
            hsp = Math.sqrt(
                0.299 * (r * r) +
                0.587 * (g * g) +
                0.114 * (b * b)
            );
            if (hsp > 127.5) {
                // color is light       
                return false;
            } else {
                // color is dark
                return true;
            }
        }
    }

    angular.module('yapp')
        .controller('LeadCtrl', LeadCtrl);
})();