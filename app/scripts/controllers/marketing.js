(function() {
    'use strict';

    /* @ngInject */
    function MarketingCtrl($window, notifyService, stdData, policyService, $fancyModal, ENV, toastr, $location, $scope, $state, $stateParams, $firebaseStorage, $firebaseObject, $firebaseArray, authService, currentAuth, sweetAlert, brandingDomain, $sce, sharedService) {
        var vm = this;
        vm.pageIsDirty = false;
        vm.form = {
            trackingData: [],
            formShadow: true,
            formBorder: false,
            formTitle: 'Sell Your Life Insurance Policy!',
            trackingType: 'system'
        };
        vm.checkingOrg = true;
        vm.myOrgUplineName = 'n/a';
        vm.isBdn = false;
        vm.seatOrgLevels = [];
        vm.iteration = 0;

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        //vm.subdomain = vm.subdomain.replace(/demo/g,'local');
        vm.invitationLinkRoot = window.location.protocol + '//' + vm.subdomain + '.' + vm.domain + '.' + vm.tld;
        vm.leadformLinkRoot = window.location.protocol + '//leadforms.web.app';
        if (ENV === 'dev') {
            vm.invitationLinkRoot += ':3000';
        }

        vm.yesNo = stdData.yesNo;
        if (vm.platform === 'isubmit') {
            vm.roles = stdData.isubmitRoles;
        } else {
            vm.roles = stdData.imanagerRoles;
        }

        var root = firebase.database().ref();

        vm.storage = firebase.storage();
        vm.storageRef = vm.storage.ref('marketing');

        $scope.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        authService.getCurrentUser(currentAuth).then(function(user) {
            vm.currentUser = user;
            if (ENV === 'prod' && !vm.currentUser.auth.emailVerified) {
                $state.go('pending');
            } else {
                if ((!vm.currentUser.org || (vm.currentUser.org && !vm.currentUser.org.marketingAccess)) && (vm.currentUser.profile.myUpline)) {
                    vm.myUplineOptions = [];
                    vm.myUplineDisplay = [];
                    $window.async.forEachOfSeries(vm.currentUser.profile.myUpline, function(upline, key, callback) {
                        // Get details for each upline Org
                        root.child('orgs/' + upline.orgType).child(upline.orgKey).once('value', function(snap) {
                            if (snap.exists()) {
                                var org = snap.val();
                                upline.email = org.email;
                                upline.primaryContact = org.primaryContact;
                                upline.phone = org.phone;
                                upline.fax = org.fax;
                                vm.myUplineDisplay.push({
                                    isDefault: upline.isDefault,
                                    name: org.name,
                                    orgKey: upline.orgKey,
                                    email: org.email,
                                    primaryContact: org.primaryContact,
                                    phone: org.phone,
                                    fax: org.fax
                                });
                                if (org.name !== 'Atlas Life Settlements') {
                                    vm.myUplineOptions.push({
                                        isDefault: upline.isDefault,
                                        name: org.name,
                                        orgKey: upline.orgKey,
                                        email: org.email,
                                        primaryContact: org.primaryContact,
                                        phone: org.phone,
                                        fax: org.fax
                                    });
                                }
                            }
                            callback();
                        }).catch(function(err) {
                            callback(err);
                        });
                    }, function(err) {
                        if (err) {
                            console.log(err);
                        } else {
                            loadPage();
                        }
                    });
                } else {
                    loadPage();
                }
            }
        });

        function loadPage() {
            // Deal with querystring variables here to set initial state
        }

        vm.requestAccess = function() {
            vm.requestingAccess = true;
            var dataToSend = {
                orgType: vm.currentUser.org.orgType,
                orgKey: vm.currentUser.org.orgKey,
                orgName: vm.currentUser.org.name,
                orgPrimaryContact: vm.currentUser.org.primaryContact,
                orgPhone: vm.currentUser.org.phone,
                orgEmail: vm.currentUser.org.email,
                userName: vm.currentUser.profile.fullName,
                userPhone: vm.currentUser.profile.phone,
                userEmail: vm.currentUser.profile.email
            };
            authService.requestMarketingAccess(dataToSend).then(function() {
                vm.toast('success', 'Request for Marketing Access has been sent successfully!');
                vm.currentUser.org.marketingAccessRequested = true;
                vm.currentUser.org.marketingAccessRequestedAt = moment();
                vm.requestingAccess = false;
            });
        }

//    __  __            _        _   _               _____ _                    
//   |  \/  |          | |      | | (_)             |  __ (_)                   
//   | \  / | __ _ _ __| | _____| |_ _ _ __   __ _  | |__) |  ___  ___ ___  ___ 
//   | |\/| |/ _` | '__| |/ / _ \ __| | '_ \ / _` | |  ___/ |/ _ \/ __/ _ \/ __|
//   | |  | | (_| | |  |   <  __/ |_| | | | | (_| | | |   | |  __/ (_|  __/\__ \
//   |_|  |_|\__,_|_|  |_|\_\___|\__|_|_| |_|\__, | |_|   |_|\___|\___\___||___/
//                                            __/ |                             
//                                           |___/                              

        vm.getMarketingPieces = function() {
            vm.reviewMarketingPieces = true;
            //vm.marketingPieceTemplates = $firebaseArray(root.child('marketing/' + vm.currentUser.org.$id + '/marketingPieceTemplates'));
            vm.marketingPieceTemplates = [
                {
                    id: 123,
                    imgFront: '../../images/marketing/template-1.png',
                    imgBack: '../../images/marketing/template-1-back.png',
                    description: '4.25" x 5.5" postcard',
                    html: '<div id="template">This is the template</div>'
                },
                {
                    id: 456,
                    imgFront: '../../images/marketing/template-2.png',
                    imgBack: '../../images/marketing/template-2.png',
                    description: '4" x 6" postcard',
                    html: '<div id="template">This is the template</div>'
                },
                {
                    id: 789,
                    imgFront: '../../images/marketing/template-3.png',
                    imgBack: '../../images/marketing/template-3-back.png',
                    description: '8.5" x 11" trifold',
                    html: '<div id="template">This is the template</div>'
                }
            ];
        }

        vm.changeMarketingTemplate = function(id) {
            vm.editMarketingTemplate = true;
        }

        vm.newMarketingPiece = function() {
            vm.pickMarketingTemplate = true;
        }

        vm.selectMarketingTemplate = function(templateId) {
            vm.marketingTemplateId = templateId;
            //vm.createMarketingPiece = true;
        }

//    ______           _              _     _          _   ______                       
//   |  ____|         | |            | |   | |        | | |  ____|                      
//   | |__   _ __ ___ | |__   ___  __| | __| | ___  __| | | |__ ___  _ __ _ __ ___  ___ 
//   |  __| | '_ ` _ \| '_ \ / _ \/ _` |/ _` |/ _ \/ _` | |  __/ _ \| '__| '_ ` _ \/ __|
//   | |____| | | | | | |_) |  __/ (_| | (_| |  __/ (_| | | | | (_) | |  | | | | | \__ \
//   |______|_| |_| |_|_.__/ \___|\__,_|\__,_|\___|\__,_| |_|  \___/|_|  |_| |_| |_|___/

        vm.getEmbedForms = function() {
            vm.reviewForms = true;
            vm.forms = $firebaseArray(root.child('marketing/' + vm.currentUser.org.$id + '/forms'));
            resetFormFields().then(function() {
                vm.updateForm();
            });
        }

        function resetFormFields() {
            return new Promise(function(resolve, reject) {
                vm.form = {
                    leadsAssignedTo: {
                        name: vm.currentUser.profile.displayName,
                        company: vm.currentUser.org ? vm.currentUser.org.name : null,
                        role: vm.currentUser.profile.role,
                        userRef: vm.currentUser.auth.uid,
                        alertId: vm.currentUser.profile.alertId
                    },
                    signingAgent: {
                        firstName: vm.currentUser.profile.firstName,
                        lastName: vm.currentUser.profile.lastName,
                        email: vm.currentUser.profile.email
                    },
                    managedBy: 'broker',
                    linkRoot: vm.invitationLinkRoot,
                    formName: 'Embedded Form',
                    formTitle: 'Sell Your Life Insurance Policy!',
                    trackingData: [],
                    formOrg: vm.currentUser.org.$id,
                    formOrgType: vm.currentUser.org.orgType,
                    accentColor: '#009ADA',
                    formShadow: true,
                    formBorder: false,
                    frameWidth: '100%',
                    trackingType: 'system'
                };
                vm.formSaved = false;
                resolve();
            });
        }

        vm.updateForm = function() {
            $scope.$evalAsync(function(scope) {
                vm.iteration++;
                vm.frameSrc = vm.leadformLinkRoot + '?v=' + vm.iteration;
                vm.frameSrcPreview = vm.frameSrc + '&test=true';

                if (vm.form.accentColor) {
                    vm.frameSrcPreview += '&c=' + encodeURIComponent(vm.form.accentColor);
                }
                if (vm.form.frameWidth) {
                    vm.frameSrcPreview += '&w=' + encodeURIComponent(vm.form.frameWidth);
                }
                if (!vm.form.formShadow) {
                    vm.frameSrcPreview += '&s=false';
                }
                if (vm.form.formBorder) {
                    vm.frameSrcPreview += '&b=true';
                }
                vm.frameSrcPreview += '&t=' + encodeURIComponent(vm.form.formTitle);
                if (vm.form && vm.formSaved) {
                    vm.frameSrc += '&pef=' + vm.form.publicGuid;
                }
            });
        }
        
        vm.saveForm = function() {
            // deal with custom tracking data in input fields
            if (vm.form.newTrackingKey && vm.form.newTrackingValue) {
                if (!vm.form.trackingData) {
                    vm.form.trackingData = [];
                }
                vm.form.trackingData.push({
                    key: vm.form.newTrackingKey,
                    value: vm.form.newTrackingValue
                });
                delete vm.form.newTrackingKey;
                delete vm.form.newTrackingValue;
            }
            if (!vm.form.managedBy) {
                vm.form.managedBy = 'broker';
            }
            if (vm.form.sendEmailRadio === "false") {
                vm.form.sendEmail = false;
            } else {
                vm.form.sendEmail = true;
            }
            if (vm.formSaved) {
                vm.forms.$save(vm.form).then(function() {
                    vm.toast('success', 'Form updated');
                    $scope.$apply();
                });
            } else {
                var publicGuid = uuidv4();
                vm.form.publicGuid = publicGuid;
                vm.forms.$add(vm.form).then(function(ref) {
                    vm.formId = ref.key;
                    vm.formSaved = true;
                    root.child('publicEmbedForms/' + publicGuid).update({
                        fid: vm.formId,
                        ok: vm.currentUser.org.$id
                    }).then(function() {
                        vm.frameSrc = vm.leadformLinkRoot + '?pef=' + publicGuid;
                        vm.frameSrcPreview = vm.frameSrc + '&test=true';
                        vm.toast('success', 'Form saved');
                    });
                });
            }
        }

        vm.deleteForm = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this form? If this form has been embedded anywhere, it will stop working.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.forms.$remove(vm.form).then(function() {
                        vm.toast('success', 'Form has been deleted');
                        vm.createForm = false;
                        $scope.$apply();
                    });
                }
            });
        }

        vm.newForm = function() {
            resetFormFields();
            vm.createForm = true;
            vm.updateForm();
        }

        vm.editForm = function(form) {
            vm.form = form;
            if (vm.form.sendEmail === false) {
                vm.form.sendEmailRadio = "false"
            } else {
                vm.form.sendEmailRadio = "true"
            }
            vm.formId = form.$id;
            vm.createForm = true;
            vm.formSaved = true;
            vm.updateForm();
        }

//    _                     _ _               _____                      
//   | |                   | (_)             |  __ \                     
//   | |     __ _ _ __   __| |_ _ __   __ _  | |__) |_ _  __ _  ___  ___ 
//   | |    / _` | '_ \ / _` | | '_ \ / _` | |  ___/ _` |/ _` |/ _ \/ __|
//   | |___| (_| | | | | (_| | | | | | (_| | | |  | (_| | (_| |  __/\__ \
//   |______\__,_|_| |_|\__,_|_|_| |_|\__, | |_|   \__,_|\__, |\___||___/
//                                     __/ |              __/ |          
//                                    |___/              |___/           

        vm.getLandingPages = function() {
            vm.reviewLandingPages = true;
            vm.landingPageTemplates = $firebaseArray(root.child('landingPageTemplates'));
            vm.landingPages = $firebaseArray(root.child('marketing/' + vm.currentUser.org.$id + '/landingPages'));
        }

        vm.refreshLandingPageTokens = function() {
            vm.landingPageTemplate.tokens = [];
            vm.tokenTracking = [];
            vm.updateLandingPagePreview();
        }

        vm.refreshLandingPagePreview = function() {
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    if (vm.newLandingPage.tokens !== {}) {
                        if (vm.landingPageTemplate.tokens) {
                            vm.landingPageTemplate.tokens.forEach(function(t) {
                                if (t.tokenValue === 'ReferralLink') {
                                    vm.newLandingPage.tokens[t.tokenValue] = vm.invitationLinkRoot + '/register?ref=' + vm.currentUser.profile.myOrg.orgKey;
                                }
                            });
                        }
                        var tokenCounter = 1;
                        Object.keys(vm.newLandingPage.tokens).forEach(function(k) {
                            var replaceString = new RegExp('{{{' + k + '}}}', 'g');
                            if (tokenCounter === 1) {
                                vm.newLandingPage.html = vm.landingPageTemplate.html.replace(replaceString, vm.newLandingPage.tokens[k]);
                            } else {
                                vm.newLandingPage.html = vm.newLandingPage.html.replace(replaceString, vm.newLandingPage.tokens[k]);
                            }
                            tokenCounter++;
                        });
                    }
                    var iframe = document.getElementById('landingPagePreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.newLandingPage.html;
                    $scope.$apply();
                }, 0);
            });
        }

        vm.updateLandingPagePreview = function() {
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    var iframe = document.getElementById('landingPagePreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.landingPageTemplate.html.replace(/{{{logoImg}}}/g, 'https://d15k2d11r6t6rl.cloudfront.net/public/users/Integrators/BeeProAgency/184986_161521/logo-placeholder-1.png');

                    var regex = /{{{(.*?)}}}/g;
                    var matches = vm.landingPageTemplate.html.match(regex);
                    if (!vm.landingPageTemplate.tokens) {
                        vm.landingPageTemplate.tokens = [];
                    }
                    root.child('marketing/' + vm.currentUser.org.$id + '/standardTokens/landingPageTemplates').once('value', function(snap) {
                        var standardTokens = snap.val();
                        for (const match in matches) {
                            if (matches.hasOwnProperty(match)) {
                                const token = matches[match].replace('{{{','').replace('}}}','').trim();
                                if (vm.tokenTracking.length > 0 && vm.tokenTracking.indexOf(token.toLowerCase()) < 0 && token !== 'logoImg') {
                                    if (standardTokens) {
                                        Object.keys(standardTokens).forEach(function(t) {
                                            if (standardTokens[t].tokenValue.toLowerCase() === token.toLowerCase()) {
                                                vm.tokenTracking.push(token.toLowerCase());
                                                vm.landingPageTemplate.tokens.push({
                                                    tokenValue: token,
                                                    displayValue: standardTokens[t].displayValue,
                                                    isStandard: true
                                                });
                                            }
                                        });
                                    }
                                    if (vm.tokenTracking.indexOf(token.toLowerCase()) < 0) {
                                        vm.tokenTracking.push(token.toLowerCase());
                                        vm.landingPageTemplate.tokens.push({
                                            tokenValue: token,
                                            displayValue: token,
                                            isStandard: false
                                        });
                                    }
                                }
                            }
                        }
                        $scope.$apply();
                    });                    
                }, 0);
            });
        }

        vm.editLandingPageCampaign = function(campaign) {
            vm.newLandingPage = campaign;
            vm.campaignSaved = true;
            vm.editLandingPageTemplate = true;
            vm.previewingLandingPageTemplate = false;
            vm.editLandingPage = true;
            vm.landingPageTemplate = {
                description: vm.newLandingPage.description,
                html: sharedService.cloneObject(vm.newLandingPage.templateHtml),
                tokens: sharedService.cloneObject(vm.newLandingPage.templateTokens)
            };
            vm.refreshLandingPagePreview();
        }

        vm.saveLandingPage = function(landingPage) {
            vm.previewingLandingPageTemplate = false;
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    if (vm.newLandingPage.tokens !== {}) {
                        if (vm.landingPageTemplate.tokens && vm.landingPageTemplate.tokens.length > 0) {
                            vm.landingPageTemplate.tokens.forEach(function(t) {
                                if (t.tokenValue === 'ReferralLink') {
                                    vm.newLandingPage.tokens[t.tokenValue] = vm.invitationLinkRoot + '/register?ref=' + vm.currentUser.profile.myOrg.orgKey;
                                }
                            });
                        }
                        var tokenCounter = 1;
                        Object.keys(vm.newLandingPage.tokens).forEach(function(k) {
                            var replaceString = new RegExp('{{{' + k + '}}}', 'g');
                            if (tokenCounter === 1) {
                                vm.newLandingPage.html = vm.landingPageTemplate.html.replace(replaceString, vm.newLandingPage.tokens[k]);
                            } else {
                                vm.newLandingPage.html = vm.newLandingPage.html.replace(replaceString, vm.newLandingPage.tokens[k]);
                            }
                            tokenCounter++;
                        });
                    }
                    var iframe = document.getElementById('landingPagePreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.newLandingPage.html;
                    
                    if (vm.campaignSaved) {
                        vm.newLandingPage.description = vm.landingPageTemplate.description;                
                        vm.landingPages.$save(vm.newLandingPage).then(function(ref) {
                            vm.landingPageId = ref.key;
                            vm.toast('success','Landing Page Template saved');
                            $scope.$apply();
                        });
                    } else {
                        var publicGuid = uuidv4();
                        var landingPage = {
                            publicGuid: publicGuid,
                            tokens: vm.newLandingPage.tokens,
                            description: vm.landingPageTemplate.description,
                            templateHtml: vm.landingPageTemplate.html,
                            templateTokens: vm.landingPageTemplate.tokens,
                            imgPreview: vm.landingPageTemplate.imgPreview,
                            html: vm.newLandingPage.html
                        };
                        vm.landingPages.$add(landingPage).then(function(ref) {
                            vm.landingPageId = ref.key;
                            vm.toast('success','Landing Page Template saved');
                            $scope.$apply();
                        });
                    }
                }, 0);
            });
        }

        vm.saveLandingPageTemplate = function() {
            if (vm.landingPageTemplateSaved) {
                vm.landingPageTemplates.$save(vm.landingPageTemplate).then(function() {
                    vm.toast('success', 'Landing Page template updated');
                    $scope.$apply();
                });
            } else {
                var publicGuid = uuidv4();
                vm.landingPageTemplate.publicGuid = publicGuid;
                vm.landingPageTemplate.imgPreview = '';
                vm.landingPageTemplates.$add(vm.landingPageTemplate).then(function(ref) {
                    vm.landingPageTemplateId = ref.key;
                    vm.landingPageTemplateSaved = true;
                    vm.toast('success', 'Landing Page template saved');
                });
            }
        }

        vm.deleteLandingPage = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this Landing Page?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.landingPages.$remove(vm.newLandingPage).then(function() {
                        vm.toast('success','Landing Page has been deleted');
                        vm.landingPageTemplate = {}; 
                        vm.landingPageTemplateSaved = false; 
                        vm.editLandingPageTemplate = false; 
                        vm.editLandingPage = false;
                        $scope.$apply();
                    });
                }
            });
        }

        vm.deleteLandingPageTemplate = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this Template?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.landingPageTemplates.$remove(vm.landingPageTemplate).then(function() {
                        vm.toast('success', 'Landing Page Template has been deleted');
                        vm.landingPageTemplate = {}; 
                        vm.landingPageTemplateSaved = false; 
                        vm.editLandingPageTemplate = false; 
                        vm.editLandingPage = false;
                        $scope.$apply();
                    });
                }
            });
        }

        vm.selectLandingPageTemplate = function(template,editTemplate) {
            vm.campaignSaved = false;
            vm.landingPageTemplate = template;
            if (vm.landingPageTemplate.tokens) {
                vm.tokenTracking = [];
                vm.landingPageTemplate.tokens.forEach(function(t) {
                    vm.tokenTracking.push(t.tokenValue.toLowerCase());
                });
            }
            vm.editLandingPageTemplate = true;
            vm.landingPageTemplateSaved = true;
            vm.previewingLandingPageTemplate = true;
            if (vm.currentUser.profile.role.toLowerCase() === 'ibroker' && editTemplate) {
                vm.doEditLandingPageTemplate = true;
                vm.updateLandingPagePreview();
            } else {
                vm.doEditLandingPageTemplate = false;
                vm.newLandingPage = {
                    html: sharedService.cloneObject(vm.landingPageTemplate.html),
                    tokens: {
                        'logoImg': 'https://d15k2d11r6t6rl.cloudfront.net/public/users/Integrators/BeeProAgency/184986_161521/logo-placeholder-1.png'
                    }
                };
                vm.refreshLandingPagePreview();
            }
        }

        vm.newLandingPageTemplate = function() {
            vm.editLandingPageTemplate = true;
            vm.landingPageTemplateSaved = false;
            vm.doEditLandingPageTemplate = true;
            vm.landingPageTemplate = {};
        }

        vm.createNewLandingPage = function() {
            vm.pickLandingPageTemplate = true;
        }
        
//    ______                 _ _   _______                   _       _            
//   |  ____|               (_) | |__   __|                 | |     | |           
//   | |__   _ __ ___   __ _ _| |    | | ___ _ __ ___  _ __ | | __ _| |_ ___  ___ 
//   |  __| | '_ ` _ \ / _` | | |    | |/ _ \ '_ ` _ \| '_ \| |/ _` | __/ _ \/ __|
//   | |____| | | | | | (_| | | |    | |  __/ | | | | | |_) | | (_| | ||  __/\__ \
//   |______|_| |_| |_|\__,_|_|_|    |_|\___|_| |_| |_| .__/|_|\__,_|\__\___||___/
//                                                    | |                         
//                                                    |_|                         
 
        vm.getEmailCampaigns = function() {
            vm.reviewEmailCampaigns = true;
            vm.emailTemplates = $firebaseArray(root.child('emailTemplates'));
            vm.emailCampaigns = $firebaseArray(root.child('marketing/' + vm.currentUser.org.$id + '/emailCampaigns'));
        }

        vm.refreshEmailTokens = function() {
            vm.emailTemplate.tokens = [];
            vm.tokenTracking = [];
            vm.updateEmailPreview();
        }

        vm.refreshEmailPreview = function() {
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    if (vm.newEmailCampaign.tokens !== {}) {
                        vm.emailTemplate.tokens.forEach(function(t) {
                            if (t.tokenValue === 'ReferralLink') {
                                vm.newEmailCampaign.tokens[t.tokenValue] = vm.invitationLinkRoot + '/register?ref=' + vm.currentUser.profile.myOrg.orgKey;
                            }
                        });
                        var tokenCounter = 1;
                        Object.keys(vm.newEmailCampaign.tokens).forEach(function(k) {
                            var replaceString = new RegExp('{{{' + k + '}}}', 'g');
                            if (tokenCounter === 1) {
                                vm.newEmailCampaign.html = vm.emailTemplate.html.replace(replaceString, vm.newEmailCampaign.tokens[k]);
                            } else {
                                vm.newEmailCampaign.html = vm.newEmailCampaign.html.replace(replaceString, vm.newEmailCampaign.tokens[k]);
                            }
                            tokenCounter++;
                        });
                    }
                    var iframe = document.getElementById('emailPreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.newEmailCampaign.html;
                    $scope.$apply();
                }, 0);
            });
        }

        vm.updateEmailPreview = function() {
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    var iframe = document.getElementById('emailPreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.emailTemplate.html.replace(/{{{logoImg}}}/g, 'https://d15k2d11r6t6rl.cloudfront.net/public/users/Integrators/BeeProAgency/184986_161521/logo-placeholder-1.png');

                    var regex = /{{{(.*?)}}}/g;
                    var matches = vm.emailTemplate.html.match(regex);
                    if (!vm.emailTemplate.tokens) {
                        vm.emailTemplate.tokens = [];
                    }
                    root.child('marketing/' + vm.currentUser.org.$id + '/standardTokens/emailTemplates').once('value', function(snap) {
                        var standardTokens = snap.val();
                        for (const match in matches) {
                            if (matches.hasOwnProperty(match)) {
                                const token = matches[match].replace('{{{','').replace('}}}','').trim();
                                if (vm.tokenTracking.length > 0 && vm.tokenTracking.indexOf(token.toLowerCase()) < 0 && token !== 'logoImg') {
                                    if (standardTokens) {
                                        Object.keys(standardTokens).forEach(function(t) {
                                            if (standardTokens[t].tokenValue.toLowerCase() === token.toLowerCase()) {
                                                vm.tokenTracking.push(token.toLowerCase());
                                                vm.emailTemplate.tokens.push({
                                                    tokenValue: token,
                                                    displayValue: standardTokens[t].displayValue,
                                                    isStandard: true
                                                });
                                            }
                                        });
                                    }
                                    if (vm.tokenTracking.indexOf(token.toLowerCase()) < 0) {
                                        vm.tokenTracking.push(token.toLowerCase());
                                        vm.emailTemplate.tokens.push({
                                            tokenValue: token,
                                            displayValue: token,
                                            isStandard: false
                                        });
                                    }
                                }
                            }
                        }
                        $scope.$apply();
                    });                    
                }, 0);
            });
        }

        vm.editEmailCampaign = function(campaign) {
            vm.newEmailCampaign = campaign;
            vm.campaignSaved = true;
            vm.editEmailTemplate = true;
            vm.previewingEmailTemplate = false;
            vm.editEmail = true;
            vm.emailTemplate = {
                description: vm.newEmailCampaign.description,
                html: sharedService.cloneObject(vm.newEmailCampaign.templateHtml),
                tokens: sharedService.cloneObject(vm.newEmailCampaign.templateTokens)
            };
            vm.refreshEmailPreview();
        }

        vm.saveEmailCampaign = function(emailCampaign) {
            vm.previewingEmailTemplate = false;
            $scope.$evalAsync(function(scope) {
                setTimeout(function() {
                    if (vm.newEmailCampaign.tokens !== {}) {
                        vm.emailTemplate.tokens.forEach(function(t) {
                            if (t.tokenValue === 'ReferralLink') {
                                vm.newEmailCampaign.tokens[t.tokenValue] = vm.invitationLinkRoot + '/register?ref=' + vm.currentUser.profile.myOrg.orgKey;
                            }
                        });
                        var tokenCounter = 1;
                        Object.keys(vm.newEmailCampaign.tokens).forEach(function(k) {
                            var replaceString = new RegExp('{{{' + k + '}}}', 'g');
                            if (tokenCounter === 1) {
                                vm.newEmailCampaign.html = vm.emailTemplate.html.replace(replaceString, vm.newEmailCampaign.tokens[k]);
                            } else {
                                vm.newEmailCampaign.html = vm.newEmailCampaign.html.replace(replaceString, vm.newEmailCampaign.tokens[k]);
                            }
                            tokenCounter++;
                        });
                    }
                    var iframe = document.getElementById('emailPreview'),
                        iframedoc = iframe.contentDocument || iframe.contentWindow.document;

                    iframedoc.body.innerHTML = vm.newEmailCampaign.html;
                    
                    if (vm.campaignSaved) {
                        vm.newEmailCampaign.description = vm.emailTemplate.description;                
                        vm.emailCampaigns.$save(vm.newEmailCampaign).then(function(ref) {
                            vm.emailCampaignId = ref.key;
                            vm.toast('success','Email Template saved');
                            $scope.$apply();
                        });
                    } else {
                        var publicGuid = uuidv4();
                        var emailCampaign = {
                            publicGuid: publicGuid,
                            tokens: vm.newEmailCampaign.tokens,
                            description: vm.emailTemplate.description,
                            templateHtml: vm.emailTemplate.html,
                            templateTokens: vm.emailTemplate.tokens,
                            imgPreview: vm.emailTemplate.imgPreview,
                            html: vm.newEmailCampaign.html
                        };
                        vm.emailCampaigns.$add(emailCampaign).then(function(ref) {
                            vm.emailCampaignId = ref.key;
                            vm.toast('success','Email Template saved');
                            $scope.$apply();
                        });
                    }
                }, 0);
            });
        }

        vm.saveEmailTemplate = function() {
            if (vm.emailTemplateSaved) {
                vm.emailTemplates.$save(vm.emailTemplate).then(function() {
                    vm.toast('success', 'Email template updated');
                    $scope.$apply();
                });
            } else {
                var publicGuid = uuidv4();
                vm.emailTemplate.publicGuid = publicGuid;
                vm.emailTemplate.imgPreview = '';
                vm.emailTemplates.$add(vm.emailTemplate).then(function(ref) {
                    vm.emailTemplateId = ref.key;
                    vm.emailTemplateSaved = true;
                    vm.toast('success', 'Email template saved');
                });
            }
        }

        vm.deleteEmailCampaign = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this Email?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.emailCampaigns.$remove(vm.newEmailCampaign).then(function() {
                        vm.toast('success','Email has been deleted');
                        vm.emailTemplate = {}; 
                        vm.emailTemplateSaved = false; 
                        vm.editEmailTemplate = false; 
                        vm.editEmail = false;
                        $scope.$apply();
                    });
                }
            });
        }

        vm.deleteEmailTemplate = function() {
            sweetAlert.swal({
                title: "Are you sure?",
                text: "Do you really want to remove this Template?",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "No",
                confirmButtonColor: "#B20000",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then(function(result) {
                if (!result.dismiss) {
                    vm.emailTemplates.$remove(vm.emailTemplate).then(function() {
                        vm.toast('success', 'Email Template has been deleted');
                        vm.emailTemplate = {}; 
                        vm.emailTemplateSaved = false; 
                        vm.editEmailTemplate = false; 
                        vm.editEmail = false;
                        $scope.$apply();
                    });
                }
            });
        }

        vm.selectEmailTemplate = function(template,editTemplate) {
            vm.campaignSaved = false;
            vm.emailTemplate = template;
            if (vm.emailTemplate.tokens) {
                vm.tokenTracking = [];
                vm.emailTemplate.tokens.forEach(function(t) {
                    vm.tokenTracking.push(t.tokenValue.toLowerCase());
                });
            }
            vm.editEmailTemplate = true;
            vm.emailTemplateSaved = true;
            vm.previewingEmailTemplate = true;
            if (vm.currentUser.profile.role.toLowerCase() === 'ibroker' && editTemplate) {
                vm.doEditEmailTemplate = true;
                vm.updateEmailPreview();
            } else {
                vm.doEditEmailTemplate = false;
                vm.newEmailCampaign = {
                    html: sharedService.cloneObject(vm.emailTemplate.html),
                    tokens: {
                        'logoImg': 'https://d15k2d11r6t6rl.cloudfront.net/public/users/Integrators/BeeProAgency/184986_161521/logo-placeholder-1.png'
                    }
                };
                vm.refreshEmailPreview();
            }
        }

        vm.newEmailTemplate = function() {
            vm.editEmailTemplate = true;
            vm.emailTemplateSaved = false;
            vm.doEditEmailTemplate = true;
            vm.emailTemplate = {};
        }

        vm.createNewEmailCampaign = function() {
            vm.pickEmailTemplate = true;
        }
        
//     _____                           _ 
//    / ____|                         | |
//   | |  __  ___ _ __   ___ _ __ __ _| |
//   | | |_ |/ _ \ '_ \ / _ \ '__/ _` | |
//   | |__| |  __/ | | |  __/ | | (_| | |
//    \_____|\___|_| |_|\___|_|  \__,_|_|
 

        vm.addToStandardFields = function(token, type) {
            root.child('marketing/' + vm.currentUser.org.$id + '/standardTokens/' + type).push({
                tokenValue: token.tokenValue,
                displayValue: token.displayValue
            }).then(function() {
                token.isStandard = true;
                vm.toast('success', 'Standard Fields Updated');
                $scope.$apply();
            });
        }

        vm.addNewTracking = function() {
            if (!vm.form.trackingData) {
                vm.form.trackingData = [];
            }
            vm.form.trackingData.push({
                key: vm.form.newTrackingKey,
                value: vm.form.newTrackingValue
            });
            delete vm.form.newTrackingKey;
            delete vm.form.newTrackingValue;
        }

        vm.removeCustomData = function(index) {
            vm.form.trackingData.splice(index,1);
        }

        vm.toast = function(type, message) {
            toastr[type](message);
        }

        vm.showMessage = function(type, helpTitle, helpText) {
            if (type === 'Referral Link') {
                sweetAlert.swal({
                    title: "Referral Link",
                    text: "This is pulled from your Profile and cannot be edited. Anyone registering for the iSubmit platform through your link will be set up downline to you.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Who Should Sign') {
                sweetAlert.swal({
                    title: "Who Should Sign e-Applications",
                    html: "Leads who complete this form will have the option to go on to fill out the entire settlement application online. After completing that, they will have the option to e-sign.<br><br>In the event that someone goes all the way to signature without agent intervention, the person defined here will be listed as the agent and sent the application to sign.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Contact Number') {
                sweetAlert.swal({
                    title: "Support Phone Number",
                    html: "If you add a phone number here, it will be displayed on the final step of the lead form.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Followup Link') {
                sweetAlert.swal({
                    title: "What is a Follow-Up Link?",
                    html: "Use this field to provide a URL to an auto-scheduling system like Calendly. If a value is entered, it will be displayed on the final step of the lead form as a 'Schedule a Followup' button.",
                    type: "info"
                }).then(function() {});
            } else if (type === 'Webhooks') {
                sweetAlert.swal({
                    title: "What is a Webhooks URL?",
                    html: "You can either use an off-the-shelf system like Zapier, or have a developer set up a custom Webhooks URL. This is the web address where we will post the data from all form submissions. You can decide what you want to do with the data once received.",
                    type: "info"
                }).then(function() {});
            }
        }

        vm.setOnBehalfOf = function() {
            vm.settingCreatedOnBehalfOf = true;
            vm.gettingDownlineList = true;
            vm.seatMembers = [{
                name: vm.currentUser.profile.displayName,
                firstName: vm.currentUser.profile.firstName,
                lastName: vm.currentUser.profile.lastName,
                email: vm.currentUser.profile.email,
                company: vm.currentUser.org.name,
                role: vm.currentUser.profile.role,
                userRef: vm.currentUser.auth.uid,
                orgType: vm.currentUser.org.orgType,
                orgKey: vm.currentUser.org.$id,
                isMe: true
            }];
            if (vm.currentUser.org) {
                // Step 1: Prompt to pick a user from /orgs/seats/[orgKey], showing "displayName" and "email" fields from that list
                policyService.getFullDownline().then(function(response) {
                    if (response.status === 'success') {
                        setMemberArr(response);
                    } else {
                        authService.setAuthHeader().then(function() {
                            policyService.getFullDownline().then(function(response) {
                                if (response.status === 'success') {
                                    setMemberArr(response);
                                } else {
                                    console.log(err);
                                }
                            }).catch(function(err) {
                                console.log(err);
                            });
                        });
                    }
                }).catch(function(err) {
                    console.log(err);
                });
            }
        }

        function setMemberArr(response) {
            response.data.forEach(function(member) {
                if (member.userRef !== vm.currentUser.auth.uid) {
                    vm.seatMembers.push({
                        name: member.displayName,                        
                        email: member.email,
                        company: member.company,
                        role: member.role,
                        userRef: member.userRef,
                        orgType: member.orgType,
                        orgKey: member.orgKey,
                        isMe: false
                    });
                }
            });
            vm.gettingDownlineList = false;
            $scope.$apply();
        }

        vm.continueSetOnBehalfOf = function() {
            vm.form.leadsAssignedTo = vm.onBehalfOfPick;
            var assignedNameArr = vm.onBehalfOfPick.name.split(' ');
            vm.form.signingAgent.firstName = assignedNameArr[0].trim();
            vm.form.signingAgent.lastName = vm.onBehalfOfPick.name.replace(assignedNameArr[0], '').trim();
            vm.form.signingAgent.email = vm.onBehalfOfPick.email;
            vm.settingCreatedOnBehalfOf = false;
            vm.gettingDownlineList = false;
            vm.saveForm();
        }

        vm.formUpload = function(type, path, className, $event) {
            for (var i = 0; i < $event.target.files.length; i++) {
                vm.newFiles = [];
                vm.newFiles.push({
                    name: $event.target.files[i].name,
                    type: $event.target.files[i].type,
                    date: $event.target.files[i].lastModified,
                    file: $event.target.files[i]
                });
            }
            $scope.$apply();
            vm.doUpload(type, path, vm.newFiles, className);
        }

        vm.doUpload = function(type, path, files, className) {
            async.forEachOfSeries(files, function(fileObj, key, callback) {
                var metadata = {
                    contentType: fileObj.file.type,
                    customMetadata: {
                        'uploadedAt': moment(),
                        'uploadedBy': vm.currentUser.profile.displayName,
                        'uploadedById': vm.currentUser.auth.uid,
                        'uploadedByOrgKey': vm.currentUser.org ? vm.currentUser.org.$id : 'n/a',
                        'type': type,
                        'className': className
                    }
                };

                var storagePath = '';
                if (path === 'root') {
                    storagePath = type;
                } else {
                    storagePath = vm.currentUser.org ? vm.currentUser.org.$id : vm.currentUser.auth.uid;
                    storagePath += '/' + path;
                }

                var storage = $firebaseStorage(vm.storageRef.child(storagePath).child(fileObj.file.name));
                var uploadTask = storage.$put(fileObj.file, metadata);

                uploadTask.$progress(function(snapshot) {
                    if (snapshot.totalBytes > 0) {
                        fileObj.percentUploaded = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    } else {
                        fileObj.percentUploaded = 100;
                    }
                });

                uploadTask.$error(function(error) {
                    vm.toast('error', 'The file failed to upload. Please refresh the page and try again.');
                    console.log(error);
                });

                uploadTask.$complete(function(snapshot) {
                    fileObj.link = snapshot.downloadURL;
                    fileObj.uploadedAt = firebase.database.ServerValue.TIMESTAMP;
                    fileObj.uploadedBy = vm.currentUser.profile.displayName;
                    fileObj.uploadedById = vm.currentUser.auth.uid;
                    fileObj.uploadedByOrgKey = vm.currentUser.org ? vm.currentUser.org.$id : 'n/a'

                    if (path === 'root') {
                        delete fileObj.$$hashKey;
                        if (type === 'emailCampaignTemplatePreview') {
                            vm.emailTemplate.imgPreview = fileObj.link;
                            vm.emailTemplates.$save(vm.emailTemplate).then(function() {
                                callback();
                            });
                        } else if (type === 'landingPageTemplatePreview') {
                            vm.landingPageTemplate.imgPreview = fileObj.link;
                            vm.landingPageTemplates.$save(vm.landingPage).then(function() {
                                callback();
                            });
                        }
                    } else if (path === 'emailCompanyLogo') {
                        delete fileObj.$$hashKey;
                        vm.newEmailCampaign.tokens.logoImg = fileObj.link;
                        vm.refreshEmailPreview();                        
                    } else if (path === 'landingPageLogo') {
                        delete fileObj.$$hashKey;
                        vm.newLandingPage.tokens.logoImg = fileObj.link;
                        vm.refreshLandingPagePreview();
                    }
                }, fileObj);
            }, function(err) {
                if (err) {
                    console.log(err);
                } else {
                    vm.toast('success', 'All files uploaded to the server');
                    $scope.$apply();
                }
            });
        }

    }

    angular.module('yapp')
        .controller('MarketingCtrl', MarketingCtrl);
})();