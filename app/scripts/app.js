(function() {
    'use strict';

    var lshApp = angular
        .module('yapp', [
            'ngAnimate',
            'ui.router',
            'ui.router.state.events',
            'ui.select',
            'ui.bootstrap',
            'ngSanitize',
            'firebase',
            '19degrees.ngSweetAlert2',
            'ya.nouislider',
            'angular-google-analytics',
            'checklist-model',
            'ngclipboard',
            'toastr',
            'angular-ladda',
            'vesparny.fancyModal',
            'ngMask',
            'colorpicker.module',
            'dndLists'
        ]);
    lshApp
        .constant('ENV', 'prod')
        .constant('VER', 'v2.7.1')
        .constant('verifyAccounts', true)
        .config(['$stateProvider', '$sceDelegateProvider', '$urlRouterProvider', '$locationProvider', 'AnalyticsProvider', '$provide', 'toastrConfig', 'ENV', 'laddaProvider', function($stateProvider, $sceDelegateProvider, $urlRouterProvider, $locationProvider, AnalyticsProvider, $provide, toastrConfig, ENV, laddaProvider) {

            $sceDelegateProvider.resourceUrlWhitelist([
                'self',
                'http*://isubmit*.*/**',
                'http*://imanager*.*/**',
                'http*://leadforms*.*/**'
            ]);

            var subdomain = window.location.host.split('.')[0];
            var domain = window.location.host.split('.')[1];
            var tld = window.location.host.split('.')[2];

            if (subdomain.indexOf('-local') > -1) {
                if (ENV === 'demo') {
                    subdomain = subdomain.replace('-local', '-demo');
                } else {
                    subdomain = subdomain.replace('-local', '');
                }
                tld = tld.replace(':3000', '');
            }
            var accessDomain = subdomain + '_' + domain + '_' + tld;

            var imgrData = {
                leCompanies: [{
                    name: 'Predictive Resources',
                    active: true,
                    orderOptions: [{
                        hasDependency: false,
                        name: 'deliveryOptions',
                        title: 'Delivery options:',
                        isRequired: true,
                        showHelpBtn: false,
                        type: 'radio',
                        options: ['Standard', 'Rush']
                    }, {
                        hasDependency: false,
                        name: 'doYouHaveAnAccount',
                        title: 'Do you have an account?',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'Setting up a Predictive Resources Account',
                        helpBtnText: 'If you don\'t already have an account with Predictive Resources, please <a href="https://www.predictiveresources.com/Login/Register" target="_blank">click here</a> to set one up. Orders are placed under your own account and billed to you directly.',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }, {
                        hasDependency: true,
                        dependencyField: 'doYouHaveAnAccount',
                        dependencyValue: 'Yes',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'LS Hub Authorization',
                        helpBtnText: 'Please contact <a href="mailto:support@lshub.net">support@lshub.net</a> to begin the process of setting up authorization for automated LE ordering on your behalf',
                        name: 'isLSHubAuthorized',
                        title: 'Is LS Hub authorized to submit LE Orders on your behalf?',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }],
                    fileRequirements: ['Medical Records', 'HIPAA'],
                    logo: './images/partner-logos/predictive.png'
                }, {
                    name: 'AVS',
                    active: true,
                    orderOptions: [{
                        hasDependency: false,
                        name: 'deliveryOptions',
                        title: 'Delivery options:',
                        isRequired: true,
                        showHelpBtn: false,
                        type: 'radio',
                        options: ['Standard', 'Rush']
                    }, {
                        hasDependency: false,
                        name: 'doYouHaveAnAccount',
                        title: 'Do you have an account?',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'Setting up an AVS Account',
                        helpBtnText: 'If you don\'t already have an account with AVS, please <a href="https://www.avsllc.com/client-registration.aspx" target="_blank">click here</a> to set one up. Orders are placed under your own account and billed to you directly.',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }, {
                        hasDependency: true,
                        dependencyField: 'doYouHaveAnAccount',
                        dependencyValue: 'Yes',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'LS Hub Authorization',
                        helpBtnText: 'Please contact <a href="mailto:support@lshub.net">support@lshub.net</a> to begin the process of setting up authorization for automated LE ordering on your behalf',
                        name: 'isLSHubAuthorized',
                        title: 'Is LS Hub authorized to submit LE Orders on your behalf?',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }],
                    fileRequirements: ['Medical Records', 'HIPAA'],
                    logo: './images/partner-logos/avs.jpg'
                }, {
                    name: 'TwentyFirst',
                    active: false,
                    orderOptions: ['Standard'],
                    fileRequirements: ['Medical Records', 'Application', 'HIPAA'],
                    logo: './images/partner-logos/twentyfirst.png'
                }],
                pricingCompanies: [{
                    name: 'MAPS',
                    active: false,
                    orderOptions: [''],
                    fileRequirements: [],
                    logo: './images/partner-logos/maps.png'
                }],
                medicalSummaryCompanies: [{
                    name: 'iSupport',
                    active: true,
                    orderOptions: [{
                        hasDependency: false,
                        name: 'doYouHaveAnAccount',
                        title: 'Do you have an account?',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'Setting up an Account',
                        helpBtnText: 'Please contact <a href="mailto:support@lshub.net">LS Hub Support</a> to get started.',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }, {
                        hasDependency: false,
                        name: 'priceAgreement',
                        title: 'Do you agree to iSupport\'s pricing table?',
                        isRequired: true,
                        requiredValue: 'Yes',
                        showHelpBtn: true,
                        helpBtnTitle: 'Medical Summary Pricing',
                        helpBtnText: '<table class="table table-bordered"><thead><tr style="background-color:aliceblue"><th style="text-align:center"># of Pages in<br>Medical Records</th><th style="text-align:center; vertical-align: middle">Price</th></tr></thead><tbody><tr><td>Up to 100</td><td>$25.00</td></tr><tr><td>Every additional 50</td><td>$6.00</td></tr></tbody></table>',
                        type: 'radio',
                        options: ['Yes', 'No']
                    }],
                    fileRequirements: ['Medical Records'],
                    logo: './images/partner-logos/isupport.jpg'
                }]
            };

            var stdData = {
                fileCategories: [
                    'Application',
                    'Cost of Insurance',
                    'Case Documents',
                    'HIPAA',
                    'Illustration',
                    'Life Expectancy',
                    'Medical Records',
                    'Medical Summary',
                    'Policy Copy',
                    'VOC'
                ],
                allRoles: ['IMO', 'GA', 'Agent', 'Seller', 'Seller Representative', 'Marketer', 'Broker', 'Back Office', 'Buyer', 'Provider', 'Supplier', 'Admin', 'SAdmin'],
                isubmitRoles: ['IMO', 'GA', 'Agent', 'Marketer', 'Seller', 'Seller Representative'],
                imanagerRoles: ['Broker', 'Provider', 'Supplier', 'Buyer'],
                genders: [
                    { id: 'M', name: 'Male' },
                    { id: 'F', name: 'Female' }
                ],
                yesNo: ['Yes', 'No'],
                yesNoUnsure: ['Yes', 'No', 'Unsure'],
                leTypes: ['LE', 'eLE'],
                beneTypes: ['Revocable', 'Irrevocable'],
                maritalStatus: [{
                    name: 'Single/Never Married'
                }, {
                    name: 'Married'
                }, {
                    name: 'Divorced'
                }, {
                    name: 'Separated'
                }, {
                    name: 'Widow/Widower'
                }],
                policyTypes: ['Term', 'Universal Life', 'Survivorship Universal Life', 'Whole Life', 'Variable Universal Life', 'Group Life'],
                states: [
                    { id: 'AL', name: 'Alabama' },
                    { id: 'AK', name: 'Alaska' },
                    { id: 'AZ', name: 'Arizona' },
                    { id: 'AR', name: 'Arkansas' },
                    { id: 'CA', name: 'California' },
                    { id: 'CO', name: 'Colorado' },
                    { id: 'CT', name: 'Connecticut' },
                    { id: 'DE', name: 'Delaware' },
                    { id: 'DC', name: 'District Of Columbia' },
                    { id: 'FL', name: 'Florida' },
                    { id: 'GA', name: 'Georgia' },
                    { id: 'HI', name: 'Hawaii' },
                    { id: 'ID', name: 'Idaho' },
                    { id: 'IL', name: 'Illinois' },
                    { id: 'IN', name: 'Indiana' },
                    { id: 'IA', name: 'Iowa' },
                    { id: 'KS', name: 'Kansas' },
                    { id: 'KY', name: 'Kentucky' },
                    { id: 'LA', name: 'Louisiana' },
                    { id: 'ME', name: 'Maine' },
                    { id: 'MD', name: 'Maryland' },
                    { id: 'MA', name: 'Massachusetts' },
                    { id: 'MI', name: 'Michigan' },
                    { id: 'MN', name: 'Minnesota' },
                    { id: 'MS', name: 'Mississippi' },
                    { id: 'MO', name: 'Missouri' },
                    { id: 'MT', name: 'Montana' },
                    { id: 'NE', name: 'Nebraska' },
                    { id: 'NV', name: 'Nevada' },
                    { id: 'NH', name: 'New Hampshire' },
                    { id: 'NJ', name: 'New Jersey' },
                    { id: 'NM', name: 'New Mexico' },
                    { id: 'NY', name: 'New York' },
                    { id: 'NC', name: 'North Carolina' },
                    { id: 'ND', name: 'North Dakota' },
                    { id: 'OH', name: 'Ohio' },
                    { id: 'OK', name: 'Oklahoma' },
                    { id: 'OR', name: 'Oregon' },
                    { id: 'PA', name: 'Pennsylvania' },
                    { id: 'RI', name: 'Rhode Island' },
                    { id: 'SC', name: 'South Carolina' },
                    { id: 'SD', name: 'South Dakota' },
                    { id: 'TN', name: 'Tennessee' },
                    { id: 'TX', name: 'Texas' },
                    { id: 'UT', name: 'Utah' },
                    { id: 'VT', name: 'Vermont' },
                    { id: 'VI', name: 'Virgin Islands' },
                    { id: 'VA', name: 'Virginia' },
                    { id: 'WA', name: 'Washington' },
                    { id: 'WV', name: 'West Virginia' },
                    { id: 'WI', name: 'Wisconsin' },
                    { id: 'WY', name: 'Wyoming' }
                ]
            };

            $provide.value('stdData', stdData);
            $provide.value('imgrData', imgrData);
            $provide.value('brandingDomain', subdomain + '|' + domain + '|' + tld + '|' + accessDomain);

            laddaProvider.setOption({
                style: 'expand-left'
            });

            var gaTag = 'UA-116432175-3'; // prod
            if (ENV === 'demo') {
                gaTag = 'UA-116432175-4'; // demo
            }
            if (ENV !== 'dev') {
                AnalyticsProvider
                    .setAccount(gaTag)
                    .setPageEvent('$stateChangeSuccess')
                    .trackUrlParams(true);
            }

            $urlRouterProvider.when('/', '/login');
            $urlRouterProvider.otherwise('/login');

            $stateProvider
                .state('lead', {
                    url: '/lead?v&pef&fid&bc&fc&w&p&test',
                    templateUrl: 'views/forms/lead.html',
                    controller: 'LeadCtrl',
                    controllerAs: 'vm'
                })
                .state('base', {
                    abstract: true,
                    url: '',
                    templateUrl: 'views/base.html'
                })
                .state('pending', {
                    url: '/pending',
                    parent: 'base',
                    templateUrl: 'views/auth/pending.html',
                    controller: 'PendingCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$waitForSignIn();
                        }]
                    }
                })
                .state('login', {
                    url: '/login?ref&redirect&pid&recipient&bid&pk&step&ageMin&ageMax&premiumMin&premiumMax&faceMin&faceMax&search&pubId&bok&ic&spk',
                    params: {
                        ref: {squash: false, value: null},
                        redirect: {squash: false, value: null},
                        pid: {squash: false, value: null},
                        recipient: {squash: false, value: null},
                        bid: {squash: false, value: null},
                        pk: {squash: false, value: null},
                        step: {squash: false, value: null},
                        ageMin: {squash: false, value: null},
                        ageMax: {squash: false, value: null},
                        premiumMin: {squash: false, value: null},
                        premiumMax: {squash: false, value: null},
                        faceMin: {squash: false, value: null},
                        faceMax: {squash: false, value: null},
                        search: {squash: false, value: null},
                        pubId: {squash: false, value: null},
                        bok: {squash: false, value: null},
                        ic: {squash: false, value: null},
                        spk: {squash: false, value: null}
                    },
                    parent: 'base',
                    templateUrl: 'views/auth/login.html',
                    controller: 'LoginCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$waitForSignIn();
                        }]
                    }
                })
                .state('logout', {
                    url: '/logout',
                    parent: 'base',
                    templateUrl: 'views/auth/logout.html',
                    controller: 'LogoutCtrl',
                    controllerAs: 'vm'
                })
                .state('no-access', {
                    url: '/no-access',
                    parent: 'base',
                    templateUrl: 'views/auth/no-access.html',
                    controller: 'LogoutCtrl',
                    controllerAs: 'vm'
                })
                .state('not-authorized', {
                    url: '/not-authorized',
                    parent: 'base',
                    templateUrl: 'views/auth/not-authorized.html',
                    controller: 'LogoutCtrl',
                    controllerAs: 'vm'
                })
                .state('reset', {
                    url: '/reset?ref',
                    parent: 'base',
                    templateUrl: 'views/auth/reset.html',
                    controller: 'LoginCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$waitForSignIn();
                        }]
                    }
                })
                .state('register', {
                    url: '/register?ref&bok&ic&spk',
                    params: {
                        ref: {squash: false, value: null},
                        bok: {squash: false, value: null},
                        ic: {squash: false, value: null},
                        spk: {squash: false, value: null}
                    },
                    parent: 'base',
                    templateUrl: 'views/auth/register.html',
                    controller: 'LoginCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$waitForSignIn();
                        }]
                    }
                })
                .state('public', {
                    abstract: true,
                    parent: 'base',
                    url: '',
                    templateUrl: 'views/public/public.html',
                    controller: 'PublicCtrl',
                    controllerAs: 'vm'
                })
                .state('publicPolicy', {
                    url: '/view/policy?pubId',
                    parent: 'public',
                    templateUrl: 'views/policy/policy.html',
                    controller: 'PublicPolicyCtrl',
                    controllerAs: 'vm'
                })
                .state('publicApplication', {
                    url: '/view/application?pubId',
                    parent: 'public',
                    templateUrl: 'views/forms/application.html',
                    controller: 'PublicApplicationCtrl',
                    controllerAs: 'vm'
                })
                .state('publicMedical', {
                    url: '/view/medical?pubId&recipient',
                    parent: 'public',
                    templateUrl: 'views/forms/medical-q.html',
                    controller: 'PublicMedicalQCtrl',
                    controllerAs: 'vm'
                })
                .state('thank-you', {
                    url: '/public/thank-you',
                    parent: 'base',
                    templateUrl: 'views/public/thank-you.html',
                    controller: 'PublicCtrl',
                    controllerAs: 'vm'
                })
                .state('switch-domain', {
                    url: '/switch?tk',
                    parent: 'base',
                    templateUrl: 'views/auth/switch.html',
                    controller: 'SwitchCtrl',
                    controllerAs: 'vm'
                })
                .state('maintenance', {
                    url: '/maintenance',
                    parent: 'base',
                    templateUrl: 'views/public/maintenance.html',
                    controller: 'PublicCtrl',
                    controllerAs: 'vm'
                })
                .state('isubmit-broker-agreement', {
                    url: '/isubmit-broker-agreement',
                    parent: 'base',
                    templateUrl: 'views/public/isubmit-broker-agreement.html',
                    controller: 'ValidAuthCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$requireSignIn();
                        }]
                    }
                })
                .state('validAuth', {
                    abstract: true,
                    parent: 'base',
                    url: '',
                    templateUrl: 'views/validAuth.html',
                    controller: 'ValidAuthCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        "currentAuth": ["Auth", function(Auth) {
                            return Auth.$requireSignIn();
                        }]
                    }
                })
                .state('profile', {
                    url: '/profile?hostedpage_id&init',
                    parent: 'validAuth',
                    templateUrl: 'views/profile/profile.html',
                    controller: 'ProfileCtrl',
                    controllerAs: 'vm'
                })
                .state('marketing', {
                    url: '/marketing',
                    parent: 'validAuth',
                    templateUrl: 'views/marketing/marketing.html',
                    controller: 'MarketingCtrl',
                    controllerAs: 'vm'
                })
                .state('marketingMaterial', {
                    parent: 'marketing',
                    url: '/material',
                    templateUrl: 'views/marketing/marketing-material.html',
                    controller: 'MarketingCtrl',
                    controllerAs: 'vm'
                })
                .state('landingPages', {
                    parent: 'marketing',
                    url: '/landing-pages?id',
                    templateUrl: 'views/marketing/landing-pages.html',
                    conroller: 'MarketingCtrl',
                    controllerAs: 'vm'
                })
                .state('education', {
                    url: '/education',
                    parent: 'validAuth',
                    templateUrl: 'views/education/education.html',
                    controller: 'EducationCtrl',
                    controllerAs: 'vm'
                })
                .state('notifications', {
                    url: '/notifications',
                    parent: 'validAuth',
                    templateUrl: 'views/profile/notifications.html',
                    controller: 'NotificationsCtrl',
                    controllerAs: 'vm'
                })
                .state('inventory', {
                    url: '/inventory?ageMin&ageMax&premiumMin&premiumMax&faceMin&faceMax&search',
                    parent: 'validAuth',
                    templateUrl: 'views/inventory/inventory.html',
                    controller: 'InventoryCtrl',
                    controllerAs: 'vm'
                })
                .state('application', {
                    url: '/application?pid&step',
                    parent: 'validAuth',
                    templateUrl: 'views/forms/application.html',
                    controller: 'ApplicationCtrl',
                    controllerAs: 'vm'
                })
                .state('caseIntake', {
                    url: '/intake?pid&step',
                    parent: 'validAuth',
                    templateUrl: 'views/forms/case-intake.html',
                    controller: 'ApplicationCtrl',
                    controllerAs: 'vm'
                })
                .state('policy', {
                    url: '/policy?pid',
                    parent: 'validAuth',
                    templateUrl: 'views/policy/policy.html',
                    controller: 'PolicyCtrl',
                    controllerAs: 'vm'
                })
                .state('packView', {
                    url: '/pack?pkid',
                    parent: 'validAuth',
                    templateUrl: 'views/policy/pack-view.html',
                    controller: 'PackViewCtrl',
                    controllerAs: 'vm'
                })
                .state('auditLog', {
                    url: '/auditLog?pid',
                    parent: 'validAuth',
                    templateUrl: 'views/policy/auditLog.html',
                    controller: 'AuditLogCtrl',
                    controllerAs: 'vm'
                })
                .state('buyers', {
                    url: '/buyers?tab',
                    parent: 'validAuth',
                    templateUrl: 'views/buyers/buyers.html',
                    controller: 'BuyersCtrl',
                    controllerAs: 'vm'
                })
                .state('orgProfile', {
                    url: '/org/profile?ok',
                    parent: 'validAuth',
                    templateUrl: 'views/buyers/buyerProfile.html',
                    controller: 'BuyerProfileCtrl',
                    controllerAs: 'vm'
                })
                .state('buyerPolicy', {
                    url: '/bdn/policy?pid&bid&pk&lvl',
                    parent: 'validAuth',
                    templateUrl: 'views/policy/buyerPolicy.html',
                    controller: 'BuyerPolicyCtrl',
                    controllerAs: 'vm'
                })
                .state('calculator', {
                    url: '/calculator',
                    parent: 'validAuth',
                    templateUrl: 'views/policy/calculator.html',
                    controller: 'CalculatorCtrl',
                    controllerAs: 'vm'
                })
                .state('medical', {
                    url: '/medical?pid&recipient',
                    parent: 'validAuth',
                    templateUrl: 'views/forms/medical-q.html',
                    controller: 'MedicalQCtrl',
                    controllerAs: 'vm'
                })
                .state('sadmin', {
                    url: '/sadmin',
                    parent: 'validAuth',
                    templateUrl: 'views/admin/sadmin.html',
                    controller: 'SAdminCtrl',
                    controllerAs: 'vm'
                })
                .state('admin', {
                    url: '/admin',
                    parent: 'validAuth',
                    templateUrl: 'views/admin/admin.html',
                    controller: 'AdminCtrl',
                    controllerAs: 'vm'
                });

            $locationProvider.html5Mode(true);

            $provide.decorator('numberFilter', function($delegate) {
                $delegate.$stateful = true;
                return $delegate;
            });
            $provide.decorator('$locale', function($delegate) {
                $delegate.NUMBER_FORMATS.GROUP_SEP = ',';
                $delegate.NUMBER_FORMATS.DECIMAL_SEP = '.';
                return $delegate;
            });

            var config = {
                apiKey: "AIzaSyAT35FpWbnrTqtJHC5TVfHqMJ-E0My0gYc",
                authDomain: "lis-pipeline.firebaseapp.com",
                databaseURL: "https://lis-pipeline.firebaseio.com",
                projectId: "lis-pipeline",
                storageBucket: "gs://lis-pipeline.appspot.com",
                messagingSenderId: "84774104412"
            };
            if (ENV === 'demo') {
                config = {
                    apiKey: "AIzaSyBUOxpJjgE27b49CfkhzK6byYPWy0IWztI",
                    authDomain: "lis-pipeline-demo.firebaseapp.com",
                    databaseURL: "https://lis-pipeline-demo.firebaseio.com",
                    projectId: "lis-pipeline-demo",
                    storageBucket: "gs://lis-pipeline-demo.appspot.com",
                    messagingSenderId: "981280764752"
                };
            }
            if (ENV === 'dev') {
                config = {
                    apiKey: "AIzaSyDKZ2fDI3UXzBQ906tRguelaATS1dqOOi0",
                    authDomain: "lis-pipeline-dev.firebaseapp.com",
                    databaseURL: "https://lis-pipeline-dev.firebaseio.com",
                    projectId: "lis-pipeline-dev",
                    storageBucket: "gs://lis-pipeline-dev.appspot.com",
                    messagingSenderId: "78485162403"
                };
            }
            firebase.initializeApp(config);

            angular.extend(toastrConfig, {
                positionClass: 'toast-bottom-center'
            });
        }])

    .run(['Analytics', function(Analytics) {}])

    .run(["$rootScope", "$state", "$http", function($rootScope, $state, $http) {
        // window.Intercom("boot", {
        //     app_id: "nypa1t24"
        // });

        $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            if (error.detail === "AUTH_REQUIRED") {
                var redirect = angular.copy(toParams);
                redirect['redirect'] = toState.name;
                $state.go("login", redirect);
            }
        });

        //Uncomment the following lines to put the app in maintenance mode
        //$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
        // if (toParams['#'] !== 'test') {
        //     if (toState.name !== 'maintenance') {
        //         event.preventDefault();
        //         $state.go('maintenance');
        //     }
        // }
        //});

        var subdomain = window.location.host.split('.')[0];
        var title = '';
        if (subdomain.indexOf('isubmit') > -1) {
            title = 'iSubmit from LS Hub';
        } else if (subdomain.indexOf('imanager') > -1) {
            title = 'iManager from LS Hub';
        }
        $rootScope.title = title;
    }])

    .factory("Auth", ["$firebaseAuth",
        function($firebaseAuth) {
            return $firebaseAuth();
        }
    ]);
})();