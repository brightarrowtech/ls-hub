(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('authService', authService);

    authService.$inject = ['$window', 'firebase', '$firebaseAuth', '$firebaseObject', '$state', '$q', 'brandingDomain', 'ENV', '$http', 'verifyAccounts'];

    function authService($window, firebase, $firebaseAuth, $firebaseObject, $state, $q, brandingDomain, ENV, $http, verifyAccounts) {
        var vm = this;
        vm.firebaseAuthObject = $firebaseAuth();

        var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        //var server = 'http://localhost:5000/lis-pipeline/us-central1';
        if (ENV === 'demo') {
            server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
            //server = 'http://localhost:5000/lis-pipeline-demo/us-central1';
        } else if (ENV === 'dev') {
            server = 'http://localhost:5000/lis-pipeline-dev/us-central1';
        }

        vm.buyerRoles = ['Investor', 'Funder', 'Provider'];

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        vm.accessDomain = brandingDomainArr[3];
        if (vm.subdomain.toLowerCase().indexOf('isubmit') > -1) {
            vm.platform = 'isubmit';
        } else if (vm.subdomain.toLowerCase().indexOf('imanager') > -1) {
            vm.platform = 'imanager';
        }

        var service = {
            resetPassword: resetPassword,
            changeEmail: changeEmail,
            register: register,
            login: login,
            loginWithToken: loginWithToken,
            logout: logout,
            isLoggedIn: isLoggedIn,
            getCurrentUser: getCurrentUser,
            createUserProfile: createUserProfile,
            joinOrg: joinOrg,
            addUpline: addUpline,
            checkForAdminRights: checkForAdminRights,
            verifyEmail: verifyEmail,
            setCustomClaims: setCustomClaims,
            checkCustomClaims: checkCustomClaims,
            determineUserOrgType: determineUserOrgType,
            joinInvitedOrg: joinInvitedOrg,
            checkForPendingRegistration: checkForPendingRegistration,
            refreshDownlineUsers: refreshDownlineUsers,
            setAuthHeader: setAuthHeader,
            impersonateUser: impersonateUser,
            mintToken: mintToken,
            linkSellerWithPolicy: linkSellerWithPolicy,
            requestMarketingAccess: requestMarketingAccess,
            validateUpline: validateUpline
        };

        return service;

        ////////////

        function impersonateUser(data) {
            var deferred = $q.defer();
            firebase.database().ref('users/' + data.admin).update({
                impersonating: data.active ? data.uid : null
            }).then(function() {
                deferred.resolve({
                    status: 'success',
                    message: data.active ? 'Successfully impersonated user' : 'Successfully removed impersonation'
                });
            }).catch(function(err) {
                deferred.resolve({
                    status: 'error',
                    data: err
                });
            });
            return deferred.promise;
        }

        function linkSellerWithPolicy(authUser,sellerPolicyKey) {
            var data = {
                authUser: authUser,
                sellerPolicyKey: sellerPolicyKey
            };
            return $http.post(server + '/linkSellerWithPolicy', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function requestMarketingAccess(data) {
            return $http.post(server + '/requestMarketingAccess', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function refreshDownlineUsers(data) {
            return $http.post(server + '/refreshDownlineUsers', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function setCustomClaims(data) {
            return $http.post(server + '/setCustomClaims', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function validateUpline(uid, upline, index) {
            var deferred = $q.defer();
            firebase.database().ref('orgs/seats/' + upline.orgKey + '/' + uid).once('value', function(snap) {
                if (!snap.exists()) {
                    // User has been removed from this org. Remove the reference in their profile!
                    firebase.database().ref('users/' + uid + '/refKey').once('value', function(refSnap) {
                        if (refSnap.exists() && refSnap.val() === upline.orgKey) {
                            // The user still has a refKey for the org that removed them. Delete it.
                            firebase.database().ref('users/' + uid + '/refKey').remove().then(function() {});
                        }
                        firebase.database().ref('users/' + uid + '/myUpline/' + index).remove().then(function() {
                            deferred.resolve({
                                status: 'success',
                                data: 'org-removed'
                            });
                        });
                    });
                } else {
                    // All is well
                    deferred.resolve({
                        status: 'success',
                        data: 'no-change'
                    });
                }
            })
            return deferred.promise;
        }

        function checkCustomClaims(uid, claim, value) {
            var deferred = $q.defer();
            firebase.database().ref('users/' + uid).once('value', function(snap) {
                if (snap.exists()) {
                    if (claim === 'accessDomain' && snap.val()[claim].toLowerCase() === 'all') {
                        deferred.resolve({
                            status: 'success',
                            data: 'Claim is valid'
                        });
                    } else if (value && snap.val()[claim] === value) {
                        deferred.resolve({
                            status: 'success',
                            data: 'Claim is valid'
                        });
                    } else if (!value && snap.val()[claim]) {
                        deferred.resolve({
                            status: 'success',
                            data: 'Claim is valid'
                        });
                    } else {
                        deferred.resolve({
                            status: 'error',
                            data: 'Claim is not valid'
                        });
                    }

                } else {
                    deferred.resolve({
                        status: 'error',
                        data: 'User does not exist'
                    });
                }
            });
            return deferred.promise;
        }

        function resetPassword(email) {
            return vm.firebaseAuthObject.$sendPasswordResetEmail(email);
        }

        function changeEmail(email) {
            return vm.firebaseAuthObject.$updateEmail(email);
        }

        function register(user) {
            return vm.firebaseAuthObject.$createUserWithEmailAndPassword(user.email, user.password);
        }

        function login(user) {
            var deferred = $q.defer();
            vm.firebaseAuthObject.$signInWithEmailAndPassword(user.email, user.password).then(function(authUser) {
                firebase.auth().currentUser.getToken(true).then(function(token) {
                    $http.defaults.headers.common.Authorization = 'Bearer ' + token;
                    deferred.resolve(authUser);
                });
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function loginWithToken(token) {
            var deferred = $q.defer();
            firebase.auth().signInWithCustomToken(token).then(function(authUser) {
                deferred.resolve(authUser);
            }).catch(function(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        }

        function setAuthHeader() {
            var deferred = $q.defer();
            firebase.auth().currentUser.getToken(true).then(function(token) {
                $http.defaults.headers.common.Authorization = 'Bearer ' + token;
                deferred.resolve();
            });
            return deferred.promise;
        }

        function mintToken() {
            var deferred = $q.defer();

            firebase.auth().currentUser.getToken(true).then(function(token) {
                var data = {
                    token: token
                };
                $http.post(server + '/mintCustomToken', data).then(function(response) {
                    deferred.resolve(response);
                }, function(err) {
                    console.log(err);
                    deferred.resolve(err);
                });
            }).catch(function(err) {
                console.log(err);
                deferred.resolve(err);
            });
            return deferred.promise;
        }

        function verifyEmail() {
            var deferred = $q.defer();
            var user = firebase.auth().currentUser;
            user.sendEmailVerification().then(function() {
                deferred.resolve({
                    status: 'success',
                    message: 'Verification email sent'
                });
            }).catch(function(error) {
                deferred.resolve({
                    status: 'error',
                    message: error.message
                });
            });
            return deferred.promise;
        }

        function logout() {
            vm.firebaseAuthObject.$signOut();
            $state.go('login');
        }

        function isLoggedIn() {
            return vm.firebaseAuthObject.$getAuth();
        }

        function getCurrentUser(userAuth, allowWithoutSubscription) {
            var deferred = $q.defer();
            if (userAuth) {
                // User is currently logged in...pull relevant fields and add profile data
                vm.currentUser = {};
                vm.currentUser.auth = {
                    email: userAuth.email,
                    emailVerified: userAuth.emailVerified,
                    isAnonymous: userAuth.isAnonymous,
                    photoURL: userAuth.photoURL,
                    uid: userAuth.uid,
                    authProvider: userAuth.providerData[0].providerId
                }
                var profile = $firebaseObject(firebase.database().ref('users/' + userAuth.uid));
                profile.$loaded().then(function(profile) {
                    vm.currentUser.profile = profile;
                    if (vm.currentUser.profile.impersonating) {
                        firebase.database().ref('users/' + vm.currentUser.profile.impersonating).once('value', function(snap) {
                            if (snap.exists()) {
                                vm.currentUser.auth = {
                                    authProvider: "password",
                                    email: snap.val().email,
                                    emailVerified: true,
                                    isAnonymous: false,
                                    photoURL: "isubmit-demo_lshub_net",
                                    uid: vm.currentUser.profile.impersonating
                                };
                                var profile = $firebaseObject(firebase.database().ref('users/' + vm.currentUser.profile.impersonating));
                                profile.$loaded().then(function(profile) {
                                    vm.currentUser.profile = profile;
                                    vm.currentUser.impersonated = true;
                                    checkCustomClaims(vm.currentUser.auth.uid, 'accessDomain', vm.accessDomain).then(function(result) {
                                        if (result.status === 'success' || vm.currentUser.profile.accessDomain.toLowerCase() === 'all') {
                                            if (!profile.isSubscribed && !allowWithoutSubscription) {
                                                vm.firebaseAuthObject.$signOut();
                                                $state.go('profile');
                                            }
                                            if (vm.currentUser.profile.myOrg && vm.currentUser.profile.myOrg.orgType) {
                                                var orgType = determineUserOrgType(vm.currentUser.profile.role);
                                                var org = $firebaseObject(firebase.database().ref('orgs/' + orgType + '/' + vm.currentUser.profile.myOrg.orgKey));
                                                org.$loaded().then(function(org) {
                                                    vm.currentUser.org = org;
                                                    deferred.resolve(vm.currentUser);
                                                });
                                            } else {
                                                deferred.resolve(vm.currentUser);
                                            }
                                        } else {
                                            $state.go('no-access');
                                        }
                                    });
                                });
                            }
                        });
                    } else {
                        checkCustomClaims(vm.currentUser.auth.uid, 'accessDomain', vm.accessDomain).then(function(result) {
                            if (result.status === 'success' || vm.currentUser.profile.accessDomain.toLowerCase() === 'all') {
                                if (!profile.isSubscribed && !allowWithoutSubscription) {
                                    vm.firebaseAuthObject.$signOut();
                                    $state.go('profile');
                                }
                                if (vm.currentUser.profile.myOrg && vm.currentUser.profile.myOrg.orgType) {
                                    var orgType = determineUserOrgType(vm.currentUser.profile.role);
                                    var org = $firebaseObject(firebase.database().ref('orgs/' + orgType + '/' + vm.currentUser.profile.myOrg.orgKey));
                                    org.$loaded().then(function(org) {
                                        vm.currentUser.org = org;
                                        deferred.resolve(vm.currentUser);
                                    });
                                } else {
                                    deferred.resolve(vm.currentUser);
                                }
                            } else {
                                $state.go('no-access');
                            }
                        });
                    }
                });
            } else if (allowWithoutSubscription) {
                deferred.resolve(null);
            } else {
                $state.go('login');
            }
            return deferred.promise;
        }

        function uuidv4() {
            return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        function joinInvitedOrg(authUser, buyerInvite) {
            var deferred = $q.defer();

            console.log('AuthUser ============');
            console.log(authUser);

            if (buyerInvite && buyerInvite.orgKey) {
                var userProfileData = {
                    myOrg: {
                        orgKey: buyerInvite.orgKey,
                        orgType: buyerInvite.orgType,
                        orgStatus: 'pending'
                    }
                };
            }

            return deferred.promise;
        }

        function checkForPendingRegistration(subdomain, email) {
            if (subdomain.indexOf('imanager') > -1) {
                var data = {
                    email: email
                };
                return $http.post(server + '/checkForPendingRegistration', data).then(function(response) {
                    return success(response);
                }, function(err) {
                    return fail(err);
                });
            } else {
                return new Promise(function(resolve,reject) {
                    resolve({
                        status: 'success'
                    });
                });
            }
        }

        function createUserProfile(user, authUser, isReferral, refKey, subdomain, buyerInvite, sellerPolicyKey) {
            var deferred = $q.defer();
            var upline = [];

            if (sellerPolicyKey) {
                // If a registration is coming from a seller who filled a lead form, this will be a referral by the "assigned" lead
                isReferral = true;
            }

            // Add Atlas as default upline for non-referrals on iSubmit only
            if (!isReferral && subdomain.indexOf('imanager') < 0) {
                upline = [{
                    name: 'LS Hub',
                    orgKey: '-L8crXXb_c8NRSJUgRpn',
                    orgType: 'backoffice',
                    isDefault: true
                }];
            }

            var userProfileData = {
                accessDomain: user.accessDomain,
                displayName: user.firstName + ' ' + user.lastName,
                lastName: user.lastName,
                firstName: user.firstName,
                fullName: user.firstName + ' ' + user.lastName,
                email: user.email,
                phone: user.phone,
                company: user.company,
                isAuthorized: true,
                isSubscribed: true,
                role: user.role,
                tosAgreement: user.tosAgreement,
                uid: authUser.uid,
                myUpline: upline,
                alertId: uuidv4()
            };

            if (refKey) {
                userProfileData.refKey = refKey;
            }

            var updates = {};

            if (buyerInvite && buyerInvite.orgKey) {
                userProfileData.myOrg = {
                    orgKey: buyerInvite.orgKey,
                    orgType: buyerInvite.orgType,
                    orgStatus: 'pending'
                };
                updates['orgs/public/' + buyerInvite.orgKey + '/status'] = 'active';
                updates['orgs/' + buyerInvite.orgType + '/' + buyerInvite.orgKey + '/' + authUser.uid] = 'admin';
                // if (user.company !== '') {
                //     updates['orgs/' + buyerInvite.orgType + '/' + buyerInvite.orgKey + '/name'] = user.company;
                // }
                updates['orgs/' + buyerInvite.orgType + '/' + buyerInvite.orgKey + '/primaryContact'] = user.firstName + ' ' + user.lastName;
                updates['orgs/' + buyerInvite.orgType + '/' + buyerInvite.orgKey + '/phone'] = user.phone;
            }

            updates['users/' + authUser.uid] = userProfileData;

            firebase.database().ref().update(updates).then(function(ref) {
                authUser.updateProfile({
                    displayName: userProfileData.displayName,
                    photoURL: userProfileData.accessDomain
                }).then(function() {
                    deferred.resolve(authUser);
                }, function(error) {
                    deferred.resolve('Error: ' + error.message);
                });
            }).catch(function(err) {
                console.log('Error Creating User Profile');
                console.log(err);
            });
            return deferred.promise;
        }

        function joinOrg(authUser, refKey, refType) {
            var deferred = $q.defer();
            if (authUser) {
                var updates = {};
                updates['orgs/' + refType + '/' + refKey + '/pendingReferrals/' + authUser.uid] = {
                    name: authUser.displayName,
                    email: authUser.email,
                    emailVerified: authUser.emailVerified,
                    key: authUser.uid,
                    createdAt: firebase.database.ServerValue.TIMESTAMP
                };
                updates['users/' + authUser.uid + '/myOrg'] = {
                    orgKey: 'pending',
                    orgType: 'pending',
                    status: 'pending',
                    requestedAt: firebase.database.ServerValue.TIMESTAMP,
                    checkKey: refKey
                };
                firebase.database().ref().update(updates).then(function() {
                    firebase.database().ref('users/' + authUser.uid).update({
                        refKey: null
                    }).then(function() {
                        deferred.resolve('success');
                    });
                }).catch(function(err) {
                    console.log(err);
                    deferred.resolve(null);
                });
            } else {
                deferred.resolve(null);
            }
            return deferred.promise;
        }

        function addUpline(authUser, refKey, refName, refType, addAnyway) {
            var deferred = $q.defer();
            if (authUser) {
                firebase.database().ref('users/' + authUser.uid + '/myUpline').once('value', function(snap) {
                    if (snap.exists()) {
                        // User already has some upline Orgs....make sure new referring Org isn't already there, add if missing
                        var upline = snap.val();
                        var alreadyAdded = false;
                        upline.forEach(function(org) {
                            org.isDefault = false;
                            if (org.orgKey === refKey) {
                                alreadyAdded = true;
                                org.isDefault = true;
                            }
                        });
                        if (!alreadyAdded) {
                            upline.push({
                                name: refName,
                                orgKey: refKey,
                                orgType: refType,
                                isDefault: true
                            });
                            addUserToOrgSeats(authUser, refKey, refName, upline).then(function(result) {
                                deferred.resolve(result);
                            });
                        } else if (addAnyway) {
                            addUserToOrgSeats(authUser, refKey, refName, upline).then(function(result) {
                                deferred.resolve(result);
                            });
                        }
                    } else {
                        var upline = [{
                            name: refName,
                            orgKey: refKey,
                            orgType: refType,
                            isDefault: true
                        }];
                        addUserToOrgSeats(authUser, refKey, refName, upline).then(function(result) {
                            deferred.resolve(result);
                        });
                    }
                });
            } else {
                deferred.resolve(null);
            }
            return deferred.promise;
        }

        function addUserToOrgSeats(authUser, refKey, refName, upline) {
            var deferred = $q.defer();
            firebase.database().ref('orgs/public/' + refKey).once('value', function(snap) {
                if (snap.exists()) {
                    var seatsRemaining = snap.val().seatsRemaining;
                    if (seatsRemaining > 0) {
                        firebase.database().ref('users/' + authUser.uid).once('value', function(snap) {
                            if (snap.exists()) {
                                var userData = snap.val();
                                snap.ref.update({
                                    myUpline: upline
                                }).then(function(ref) {
                                    // Add User to orgs/seats node for refKey
                                    firebase.database().ref('orgs/seats/' + refKey + '/' + authUser.uid).update({
                                        userRef: authUser.uid,
                                        displayName: authUser.displayName,
                                        joinedOn: firebase.database.ServerValue.TIMESTAMP,
                                        email: (userData.email) ? userData.email : '',
                                        company: (userData.company) ? userData.company : '',
                                        role: (userData.role) ? userData.role : '',
                                        phone: (userData.phone) ? userData.phone : '',
                                        fax: (userData.fax) ? userData.fax : '',
                                        streetAddress: (userData.address) ? userData.address.address : '',
                                        cityStateZip: (userData.address) ? userData.address.city + ', ' + userData.address.state + ' ' + userData.address.zip : ''
                                    }).then(function(ref) {
                                        firebase.database().ref('orgs/public/' + refKey).update({
                                            seatsRemaining: seatsRemaining - 1
                                        }).then(function(ref) {
                                            deferred.resolve('success');
                                        });
                                    });
                                });
                            }
                        });
                    } else {
                        // There are no seats available
                        deferred.resolve('no-seats');
                    }
                } else {
                    // The Org was not found in the orgs/public node
                    deferred.resolve('no-org');
                }
            });
            return deferred.promise;
        }

        function verifyAccessDomain(authUser) {
            var deferred = $q.defer();
            firebase.database().ref('users/' + authUser.uid);
        }

        function determineUserOrgType(userRole) {
            // "Supplier" in this context is anyone on the iManager platform who happens to also originate cases that may go to other Buyers. 
            var buyerRoles = ['Broker', 'Supplier', 'Provider', 'Buyer'];
            var supplierRoles = ['IMO', 'GA', 'Agent', 'Marketer', 'Seller', 'Seller Representative'];
            var iBrokerRoles = ['iBroker'];
            var backOfficeRoles = ['Back Office'];
            var masterRoles = ['Admin'];
            //var brokerRoles = ['Broker'];

            if (buyerRoles.indexOf(userRole) > -1) {
                return 'buyer';
            } else if (supplierRoles.indexOf(userRole) > -1) {
                return 'supplier';
            } else if (iBrokerRoles.indexOf(userRole) > -1) {
                return 'ibroker';
            } else if (backOfficeRoles.indexOf(userRole) > -1) {
                return 'backoffice';
            } else if (masterRoles.indexOf(userRole) > -1) {
                return 'master';
            } else {
                return 'unknown';
            }
        }

        function checkForAdminRights(authUser) {
            var deferred = $q.defer();

            // First, get the user's profile data to determine their Role
            firebase.database().ref('users/' + authUser.uid).once('value', function(snap) {
                if (snap.exists()) {
                    var userProfile = snap.val();
                    // Now we know the user's Role, so any Orgs they are associated with can only be of a single Org Type
                    var userRole = userProfile.role;
                    var orgType = determineUserOrgType(userRole);

                    // Check in the appropriate Org Type node for any Orgs with UID = 'admin'
                    var data = {
                        uid: authUser.uid,
                        orgType: orgType
                    };
                    return $http.post(server + '/checkForAdminRights', data).then(function(response) {
                        if (response.data.status === 'success') {
                            var data = response.data.data;

                            // Determine how many Orgs matched the query
                            var orgCount = Object.keys(data).length;

                            if (orgCount === 1 && (!userProfile.myOrg || userProfile.myOrg.orgKey === 'pending')) {
                                // User is the newly appointed Admin of an Org! Link it up!
                                Object.keys(data).forEach(function(orgKey) {
                                    var org = data[orgKey];
                                    firebase.database().ref('users/' + authUser.uid).child('myOrg').update({
                                        orgKey: org.key,
                                        orgType: org.orgType,
                                        status: 'approved',
                                        uplineOrgKey: org.uplineOrgKey ? org.uplineOrgKey : null
                                    }).then(function() {
                                        deferred.resolve({
                                            status: 'promoted',
                                            message: org.name
                                        });
                                    });
                                });
                            } else if (orgCount === 1 && userProfile.myOrg && !userProfile.myPersonalOrg) {
                                // The one returned Org should be created by the current user. Verify and respond with no changes
                                Object.keys(data).forEach(function(orgKey) {
                                    var org = data[orgKey];
                                    if (org.createdById === authUser.uid) {
                                        deferred.resolve({
                                            status: 'no-change',
                                            message: ''
                                        });
                                    } else {
                                        // Something weird going on here...this should never happen
                                        deferred.resolve({
                                            status: 'error',
                                            message: 'Please contact LS Hub Support'
                                        });
                                    }
                                });
                            } else if (orgCount === 1 && userProfile.myOrg && userProfile.myPersonalOrg) {
                                // User had a backed up Org when they became the Admin of a new Org, but have since been removed from their Admin role
                                // Verify that the 1 returned Org is created by the user
                                Object.keys(data).forEach(function(orgKey) {
                                    var org = data[orgKey];
                                    if (org.createdById === authUser.uid) {
                                        var oldOrgKey = userProfile.myOrg.orgKey;
                                        // Restore the myOrg node with the myPersonalOrg data
                                        firebase.database().ref('users/' + authUser.uid + '/myOrg').update({
                                            orgKey: userProfile.myPersonalOrg.orgKey,
                                            orgType: userProfile.myPersonalOrg.orgType,
                                            uplineOrgKey: userProfile.myPersonalOrg.uplineOrgKey ? userProfile.myPersonalOrg.uplineOrgKey : null
                                        }).then(function() {
                                            // Remove the myPersonalOrg node
                                            firebase.database().ref('users/' + authUser.uid + '/myPersonalOrg').remove().then(function() {
                                                // User has been demoted. Done.
                                                deferred.resolve({
                                                    status: 'demoted',
                                                    message: ''
                                                });
                                            });
                                        });
                                    } else {
                                        // Something weird going on here...this should never happen
                                        deferred.resolve({
                                            status: 'error',
                                            message: 'Please contact LS Hub Support'
                                        });
                                    }
                                });
                            } else if (orgCount === 2 && userProfile.myOrg && userProfile.myPersonalOrg) {
                                // This user should have a backed up Org that they initially created, plus another one they are currently an Admin of
                                var validated = 0;
                                Object.keys(data).forEach(function(orgKey) {
                                    var org = data[orgKey];
                                    if (org.createdById === authUser.uid) {
                                        validated += 1;
                                    } else {
                                        validated += 2;
                                    }
                                });
                                if (validated === 2) {
                                    // User somehow has 2 personally created Orgs? Not right...
                                    deferred.resolve({
                                        status: 'error',
                                        message: 'Please contact LS Hub Support'
                                    });
                                } else if (validated === 3) {
                                    // Nothing has changed
                                    deferred.resolve({
                                        status: 'no-change',
                                        message: ''
                                    });
                                } else if (validated === 4) {
                                    // User has been set as the Admin for 2 different Orgs...not supported
                                    deferred.resolve({
                                        status: 'error',
                                        message: 'Please contact LS Hub Support'
                                    });
                                }
                            } else if (orgCount === 2 && userProfile.myOrg && !userProfile.myPersonalOrg) {
                                // This user should have one Org they are currently the owner of, plus another one they've just been added to
                                var validated = 0;
                                var personalOrg = {};
                                var newOrg = {};
                                Object.keys(data).forEach(function(orgKey) {
                                    var org = data[orgKey];
                                    if (org.createdById === authUser.uid) {
                                        personalOrg = org;
                                        personalOrg.key = org.key;
                                        validated += 1;
                                    } else {
                                        newOrg = org;
                                        newOrg.key = org.key;
                                        validated += 2;
                                    }
                                });
                                if (validated === 2) {
                                    // User somehow has 2 personally created Orgs? Not right...
                                    deferred.resolve({
                                        status: 'error',
                                        message: 'Please contact LS Hub Support'
                                    });
                                } else if (validated === 3) {
                                    // Archive the user's current Org as "myPersonalOrg" so it can be restored later if needed
                                    firebase.database().ref('users/' + authUser.uid).child('myPersonalOrg').update({
                                        orgKey: personalOrg.key,
                                        orgType: personalOrg.orgType,
                                        uplineOrgKey: personalOrg.uplineOrgKey ? personalOrg.uplineOrgKey : null
                                    }).then(function() {
                                        firebase.database().ref('users/' + authUser.uid).child('myOrg').update({
                                            orgKey: newOrg.key,
                                            orgType: newOrg.orgType,
                                            uplineOrgKey: newOrg.uplineOrgKey ? newOrg.uplineOrgKey : null
                                        }).then(function() {
                                            deferred.resolve({
                                                status: 'promoted',
                                                message: newOrg.name
                                            });
                                        });
                                    });
                                } else if (validated === 4) {
                                    // User has been set as the Admin for 2 different Orgs...not supported
                                    deferred.resolve({
                                        status: 'error',
                                        message: 'Please contact LS Hub Support'
                                    });
                                }
                            } else if ((orgCount === 2 && !userProfile.myOrg) || orgCount > 2) {
                                // Users cannot be the admin of more than 2 Orgs at a time (their own created Org plus a promoted Org)
                                deferred.resolve({
                                    status: 'error',
                                    message: 'Please contact LS Hub Support'
                                });
                            }
                        } else if (userProfile.myOrg && userProfile.myOrg.status !== 'pending') {
                            // This user was set up as an Admin on an Org, but has been removed
                            // ================================================================
                            // Remove the "myOrg" node from the user's profile
                            firebase.database().ref('users/' + authUser.uid + '/myOrg').remove().then(function() {
                                // User has been demoted. Done.
                                deferred.resolve({
                                    status: 'demoted',
                                    message: ''
                                });
                            });
                        } else {
                            // User is still not an Admin anywhere
                            deferred.resolve({
                                status: 'no-change',
                                message: ''
                            });
                        }
                    }, function(err) {
                        deferred.resolve({
                            status: 'error',
                            message: err.message
                        });
                    });
                } else {
                    deferred.resolve({
                        status: 'error',
                        message: 'No User Profile Found'
                    });
                }
            });
            return deferred.promise;
        }

        // Have the user delete their own seat from orgs/seats/orgKey/uid
        //firebase.database().ref('orgs/seats/' + userProfile.myOrg.orgKey + '/' + authUser.uid).remove().then(function() {
        //});

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return "ERROR: " + e.data.description;
            } else {
                return 'ERROR: Unknown error occurred';
            }
        }
    }

})();