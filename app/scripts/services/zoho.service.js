(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('zohoService', zohoService);

    zohoService.$inject = ['ENV', '$http', '$state', '$q'];

    function zohoService(ENV, $http, $state, $q) {
        var vm = this;

        var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        if (ENV === 'demo') {
            server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
        } else if (ENV === 'dev') {
            server = 'http://localhost:5000/lis-pipeline-dev/us-central1';
        }

        var service = {
            createSubscription: createSubscription,
            validateSubscription: validateSubscription,
            getHostedPageDetails: getHostedPageDetails
        };

        return service;

        function createSubscription(data) {
            return $http.post(server + '/createSubscription', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function validateSubscription(subscriptionId) {
            return $http.get(server + '/validateSubscription?subscriptionId=' + subscriptionId).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getHostedPageDetails(hostedPageId) {
            return $http.get(server + '/getHostedPageDetails?hostedPageId=' + hostedPageId).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return "ERROR: " + e.data.description;
            } else {
                return 'ERROR: Unknown error occurred';
            }
        }
    }
})();