(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('sharedService', sharedService);

    sharedService.$inject = ['ENV', '$http', '$state', '$q'];

    function sharedService(ENV, $http, $state, $q) {
        
        var service = {
            cloneObject: cloneObject
        };

        return service;

        function cloneObject(o) {
            const gdcc = "__getDeepCircularCopy__";
            if (o !== Object(o)) {
                return o; // primitive value
            }

            var set = gdcc in o,
                cache = o[gdcc],
                result;
            if (set && typeof cache == "function") {
                return null;
            }
            o[gdcc] = function() { return result; }; // overwrite
            if (o instanceof Array) {
                result = [];
                for (var i=0; i<o.length; i++) {
                    result[i] = cloneObject(o[i]);
                }
            } else {
                result = {};
                for (var prop in o)
                    if (prop.indexOf('$') < 0)
                        if (prop != gdcc)
                            result[prop] = cloneObject(o[prop]);
                        else if (set)
                            result[prop] = cloneObject(cache);
            }
            if (set) {
                o[gdcc] = cache; // reset
            } else {
                delete o[gdcc]; // unset again
            }
            return result;   
        }
    }
})();