(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('policyService', policyService);

    policyService.$inject = ['firebase', '$firebaseAuth', '$firebaseObject', '$state', '$q', 'brandingDomain', 'ENV', '$http'];

    function policyService(firebase, $firebaseAuth, $firebaseObject, $state, $q, brandingDomain, ENV, $http) {
        var vm = this;
        vm.firebaseAuthObject = $firebaseAuth();

        var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        //var server = 'http://localhost:5000/lis-pipeline/us-central1';
        if (ENV === 'demo') {
            //server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
            server = 'http://localhost:5000/lis-pipeline-demo/us-central1';
        } else if (ENV === 'dev') {
            server = 'http://localhost:5000/lis-pipeline-dev/us-central1';
        }

        var brandingDomainArr = brandingDomain.split('|');
        vm.subdomain = brandingDomainArr[0];
        vm.domain = brandingDomainArr[1];
        vm.tld = brandingDomainArr[2];
        vm.accessDomain = brandingDomainArr[3];

        var service = {
            createNewPolicy: createNewPolicy,
            getPublicEmbedForm: getPublicEmbedForm,
            getPolicyEstimate: getPolicyEstimate,
            buyerFlagPolicy: buyerFlagPolicy,
            getAgent: getAgent,
            toggleBuyerInterested: toggleBuyerInterested,
            buyerMarkViewed: buyerMarkViewed,
            setPolicyPermissions: setPolicyPermissions,
            getValidUplineOrgChains: getValidUplineOrgChains,
            getFullDownline: getFullDownline,
            stampFile: stampFile,
            getZillowData: getZillowData,
            closeOrArchiveCopies: closeOrArchiveCopies,
            esSearch: esSearch
        };

        return service;

        ////////////

        function esSearch(data) {
            let body = {
                searchObj: data.searchObj,
                uid: data.uid,
                orgKey: data.orgKey,
                orgGuid: data.orgGuid,
                searchTab: data.searchTab
            };
            return $http.post(server + '/esSearch', body).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getPolicyEstimate(data) {
            return $http.post(server + '/getPolicyEstimate', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getAgent(data) {
            return $http.post(server + '/getAgent', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function closeOrArchiveCopies(data) {
            return $http.post(server + '/closeOrArchiveCopies', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function createNewPolicy(data) {
            return $http.post(server + '/createNewPolicy', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getPublicEmbedForm(data) {
            return $http.post(server + '/getPublicEmbedForm', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getFullDownline() {
            return $http.post(server + '/getFullDownline', {}).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function stampFile(data) {
            return $http.post(server + '/stampFile', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getZillowData(data) {
            return $http.post(server + '/getZillowData', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function setPolicyPermissions(data) {
            return $http.post(server + '/setPolicyPermissions', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function buyerFlagPolicy(data) {
            return $http.post(server + '/buyerFlagPolicy', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function toggleBuyerInterested(data) {
            return $http.post(server + '/toggleBuyerInterested', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function buyerMarkViewed(data) {
            return $http.post(server + '/buyerMarkViewed', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function getValidUplineOrgChains(data) {
            return $http.post(server + '/getValidUplineOrgChains', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return "ERROR: " + e.data.description;
            } else {
                return 'ERROR: Unknown error occurred';
            }
        }
    }

})();