(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('pandadocService', pandadocService);

    pandadocService.$inject = ['$http', '$state', '$q', 'ENV'];

    function pandadocService($http, $state, $q, ENV) {
        var vm = this;

        var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        if (ENV === 'demo') {
            server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
        } else if (ENV === 'dev') {
            server = 'http://localhost:5000/lis-pipeline-dev/us-central1';
        }

        var service = {
            createDocumentFromTemplate: createDocumentFromTemplate,
            downloadDocument: downloadDocument,
            testDownload: testDownload
        };
        return service;

        function createDocumentFromTemplate(data) {
            return $http.post(server + '/createDocumentFromTemplate', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function downloadDocument(documentId) {
            return $http.get(server + '/downloadDocument?documentId=' + documentId).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function testDownload(documentId, policyKey, policyNumber, formName) {
            return $http.get(server + '/testDownload?docId=' + documentId + '&pk=' + policyKey + '&pn=' + policyNumber + '&fn=' + formName).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function deferredExample(data) {
            var deferred = $q.defer();
            if (a === 'b') {
                deferred.resolve();
            } else {
                deferred.resolve(null);
            }
            return deferred.promise;
        }

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return e.data;
            } else {
                return {
                    message: 'ERROR: Unknown error occurred'
                };
            }
        }
    }
})();