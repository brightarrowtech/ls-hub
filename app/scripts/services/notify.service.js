(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('notifyService', notifyService);

    notifyService.$inject = ['ENV', '$http', '$state', '$q'];

    function notifyService(ENV, $http, $state, $q) {
        var vm = this;

        var server = 'https://api.lshub.net';
        if (ENV === 'dev') {
            server = 'http://localhost:8000';
        }

        var cloudFunctions = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        if (ENV === 'demo') {
            cloudFunctions = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
            //cloudFunctions = 'http://localhost:5000/lis-pipeline-demo/us-central1';
        } else if (ENV === 'dev') {
            cloudFunctions = 'http://localhost:5000/lis-pipeline-dev/us-central1';
        }

        var service = {
            sendEmail: sendEmail,
            addNotification: addNotification,
            testAuth: testAuth
        };

        return service;

        function testAuth(data) {
            return $http.post(cloudFunctions + '/consolidateSeats', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function sendEmail(data) {
            return $http.post(server + '/email/send', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function addNotification(data) {
            return $http.post(cloudFunctions + '/sendInAppNotifications', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return "ERROR: " + e.data.description;
            } else {
                return 'ERROR: Unknown error occurred';
            }
        }
    }
})();