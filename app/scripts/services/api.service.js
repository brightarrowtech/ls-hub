(function() {
    'use strict';

    angular
        .module('yapp')
        .factory('apiService', apiService);

    apiService.$inject = ['ENV', '$http', '$state', '$q'];

    function apiService(ENV, $http, $state, $q) {
        var vm = this;

        var server = 'https://api.lshub.net';
        //var server = 'http://localhost:8000';
        //var server = 'https://us-central1-lis-pipeline.cloudfunctions.net';
        if (ENV === 'dev') {
            server = 'http://localhost:8000';
            //server = 'https://us-central1-lis-pipeline-demo.cloudfunctions.net';
        }
        //server = 'http://localhost:5000/lis-pipeline-demo/us-central1';

        var service = {
            fillPDF: fillPDF,
            downloadZIP: downloadZIP,
            sendEmail: sendEmail
        };

        return service;

        function fillPDF(data) {
            return $http.post(server + '/pdf/fillAndDownload', data).then(function(response) {
            //return $http.post(server + '/api/pdf/fillAndDownload', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function sendEmail(data) {
            return $http.post(server + '/email/send', data).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function downloadZIP(files) {
            return $http.post(server + '/files/zip', files).then(function(response) {
                return success(response);
            }, function(err) {
                return fail(err);
            });
        }

        function success(response) {
            return response.data;
        }

        function fail(e) {
            if (e.data) {
                return "ERROR: " + e.data.description;
            } else {
                return 'ERROR: Unknown error occurred';
            }
        }
    }
})();