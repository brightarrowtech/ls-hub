(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name yapp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of yapp
     */
    angular.module('yapp')
        .directive("ngUploadChange", function() {
            return {
                scope: {
                    ngUploadChange: "&"
                },
                link: function($scope, $element, $attrs) {
                    $element.on("change", function(event) {
                        $scope.ngUploadChange({ $event: event })
                    })
                    $scope.$on("$destroy", function() {
                        $element.off();
                    });
                }
            }
        })

    .directive('googleplace', ['policyService', function(policyService) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, model) {
                var options = {
                    types: [],
                    componentRestrictions: {}
                };
                scope[element[0].id] = new google.maps.places.Autocomplete(element[0], options);

                google.maps.event.addListener(scope[element[0].id], 'place_changed', function() {
                    var place = scope[element[0].id].getPlace();
                    var components = {};
                    var addressFields = {};
                    if (place && place.address_components) {
                        place.address_components.forEach(function(component) {
                            component.types.forEach(function(type) {
                                components[type] = component.short_name;
                            });
                        });
                        addressFields.oneLiner = element[0].value;
                        addressFields.address = (components.street_number) ? components.street_number + ' ' : '';
                        addressFields.address += (components.route) ? components.route : '';
                        addressFields.city = (components.locality) ? components.locality : '';
                        addressFields.state = (components.administrative_area_level_1) ? components.administrative_area_level_1 : '';
                        addressFields.zip = (components.postal_code) ? components.postal_code : '';
                        addressFields.country = (components.country) ? components.country : '';

                        if (element[0].id === 'primaryInsuredAddress') {
                            // GET ZILLOW DATA HERE AND ADD LINK AND VALUE TO addressFields
                            // var data = {
                            //     address: addressFields.address,
                            //     cityStateZip: addressFields.city + ' ' + addressFields.state + ', ' + addressFields.zip
                            // };
                            // policyService.getZillowData(data).then(function(response) {
                            //     addressFields.zillowValue = response.data.zillowValue;
                            //     addressFields.zillowLink = response.data.zillowLink;
                            //     scope.vm.policy.insured.address = addressFields;
                            // });
                            scope.vm.policy.insured.address = addressFields;
                        } else if (element[0].id === 'ownerAddress') {
                            scope.vm.policy.owner.address = addressFields;
                        } else if (element[0].id === 'secondaryInsuredAddress') {
                            scope.vm.policy.secondaryInsured.address = addressFields;
                        } else if (element[0].id === 'pcpAddress') {
                            scope.vm.form.data.pcp.address = addressFields;
                        } else if (element[0].id === 'specOneAddress') {
                            scope.vm.form.data.specOne.address = addressFields;
                        } else if (element[0].id === 'specTwoAddress') {
                            scope.vm.form.data.specTwo.address = addressFields;
                        } else if (element[0].id === 'specThreeAddress') {
                            scope.vm.form.data.specThree.address = addressFields;
                        } else if (element[0].id === 'userAddress') {
                            scope.vm.updatedInfo.address = addressFields;
                        } else if (element[0].id === 'buyerAddress') {
                            scope.vm.updatedInfo.address = addressFields;
                        } else if (element[0].id === 'currentBeneAddress') {
                            scope.vm.policy.currentBene.address = addressFields;
                        } else if (element[0].id === 'currentSecondaryBeneAddress') {
                            scope.vm.policy.currentSecondaryBene.address = addressFields;
                        } else if (element[0].classList.contains('buyer-address') && element[0].attributes['buyer-idx'].value > -1) {
                            var buyerIdx = element[0].attributes['buyer-idx'].value;
                            scope.vm.buyers[buyerIdx].address = addressFields;
                        }
                        scope.$apply(function() {
                            //model.$setViewValue(addressFields.oneLiner);
                        });
                    }
                });
            }
        };
    }])

    .directive('format', function($filter) {

        return {
            require: '?ngModel',
            link: function(scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                var symbol = "$"; // dummy usage

                ctrl.$formatters.unshift(function(a) {
                    if (ctrl.$modelValue && ctrl.$modelValue !== '') {
                        return symbol + $filter(attrs.format)(ctrl.$modelValue);
                    }
                });

                ctrl.$parsers.unshift(function(viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val(symbol + $filter('number')(plainNumber));
                    return plainNumber;
                });
            }
        };
    })

    .directive('icheck', function($timeout, $parse) {
        return {
            require: 'ngModel',
            link: function($scope, element, $attrs, ngModel) {
                return $timeout(function() {
                    var value = $attrs['value'];
                    $scope.$watch($attrs['ngModel'], function(newValue) {
                        $(element).iCheck('update');
                    })

                    $scope.$watch($attrs['ngDisabled'], function(newValue) {
                        $(element).iCheck(newValue ? 'disable' : 'enable');
                        $(element).iCheck('update');
                    })

                    return $(element).iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    }).on('ifToggled', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                    });
                });
            }
        };
    });
})();